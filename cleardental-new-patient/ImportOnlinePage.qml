// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {

    CDTranslucentPane {
        id: centerPane
        anchors.centerIn: parent
        ColumnLayout {
            CDHeaderLabel {
                text: "Listing URL"
            }
            TextField {
                id: urlField
                Layout.minimumWidth: 750
                text: rootWin.currentURL;
            }
        }
    }

    Button {
        text: "Get Patient New List"
        highlighted: true
        Material.accent: Material.LightGreen
        anchors.top: centerPane.bottom
        anchors.right: centerPane.right
        anchors.margins: 10

        onClicked: {
            rootWin.currentURL = urlField.text;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", urlField.text,true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    //console.debug(xhr.responseText);
                    rootWin.patList  = JSON.parse(xhr.responseText);
                    mainView.push("ListPatientsPage.qml")

                }
            }
            xhr.send();
        }
    }
}
