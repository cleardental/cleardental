// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0

CDTranslucentDialog {
    id: schPatientDia

    property string patientID: "ignoreMe"

    ColumnLayout {

        CDTranslucentPane {
            Layout.fillWidth: true
            backMaterialColor: Material.Yellow
            RowLayout {
                CDHeaderLabel {
                    text: schPatientDia.patientID
                }

                CDButton {
                    CDToolLauncher {
                        id: toolLauncher
                    }
                    CDFileLocations {
                        id: fileLocs
                    }

                    text: "View Folder"
                    onClicked: {
                        toolLauncher.launchDolphin(fileLocs.getPatientDirectory(schPatientDia.patientID));
                    }
                }
            }
        }

        RowLayout {
            CDNewPatInterviewPane {
                id: intPane
                patientID: schPatientDia.patientID
                Layout.fillHeight: true
                Layout.minimumWidth: 400
                showDentalPlan: true
            }

            ColumnLayout {

                CDScheduleDateTimePane {
                    id: schDateTime
                    Layout.fillWidth: true
                }

                CDTranslucentPane {
                    id: txPlanPane
                    Layout.fillWidth: true
                    GridLayout {
                        columns: 2
                        width: parent.width
                        CDHeaderLabel {
                            Layout.columnSpan: 2
                            text: "More Info"
                        }
                        CDDescLabel {
                            text: "What to schedule"
                        }
                        ComboBox {
                            id: examTypeBox
                            model: ["Comprehensive Exam and Prophy", "Limited Exam", "Only Comprehensive Exam"];
                            Layout.minimumWidth: 350
                        }

                        CDDescLabel {
                            text: "Comment"
                        }
                        TextField {
                            id: commentField
                            Layout.fillWidth: true
                        }
                        CDDescLabel {
                            text: "Provider"
                        }
                        CDProviderBox {
                            id: providerBox
                            Layout.fillWidth: true
                        }
                    }
                }
            }

        }

        RowLayout {
            Layout.fillWidth: true
            Button {
                text: "Cancel"
                icon.name: "dialog-cancel"
                Material.accent: Material.Red
                highlighted: true
                icon.width: 32
                icon.height: 32
                font.pointSize: 18
                onClicked: {
                    schPatientDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {

                CDScheduleManager {
                    id: schMan
                }

                CDGitManager {
                    id: gitMan
                }

                CDCommonFunctions {
                    id: comFun
                }

                text: "Add Appointment"
                onClicked: {
                    var procedures = [];

                    var compExamObj = ({})
                    compExamObj["ProcedureName"] = "Comprehensive Exam";
                    compExamObj["DCode"] = "D0150";

                    var prophyObj = ({})
                    prophyObj["ProcedureName"] = "Prophy";
                    prophyObj["DCode"] = "D1110";


                    var limitedExamObj = ({})
                    limitedExamObj["ProcedureName"] = "Limited Exam";
                    limitedExamObj["DCode"] = "D0140";

                    if(examTypeBox.currentIndex === 0) {
                        comFun.addTxItem(patientID,"Primary","Unphased",compExamObj);
                        comFun.addTxItem(patientID,"Primary","Unphased",prophyObj);
                        procedures.push(compExamObj);
                        procedures.push(prophyObj);
                    }
                    else if(examTypeBox.currentIndex === 1) {
                        comFun.addTxItem(patientID,"Primary","Unphased",limitedExamObj);
                        procedures.push(limitedExamObj);
                    }
                    else {
                        comFun.addTxItem(patientID,"Primary","Unphased",compExamObj);
                        procedures.push(compExamObj);
                    }

                    schMan.addAppointment(schDateTime.currentDate,
                                          schDateTime.currentStartTime,
                                          schDateTime.currentChair,
                                          patientID,JSON.stringify(procedures),
                                          providerBox.providerIDs[providerBox.currentIndex],
                                          schDateTime.currentApptDuration,commentField.text);
                    gitMan.commitData("Added New Patient Appointment for " + patientID);
                    schPatientDia.accept();
                }
                enabled: schDateTime.currentStartTime.length > 0
            }
        }
    }
}
