// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
     CDTranslucentPane {
         id: centerPane
         anchors.centerIn: parent

         GridLayout {
             columns: 2
             CDHeaderLabel {
                 text: "Contact Information"
                 Layout.columnSpan: 2
             }

             CDDescLabel {text: "Street Address (Line 1)";}
             TextField {
                 id:addr1;
                 placeholderText: "Street Address (Line 1)"
                 Layout.fillWidth: true
                 Layout.minimumWidth: 360
             }

             CDDescLabel {text: "Street Address (Line 2)";}
             TextField {
                 id:addr2;
                 placeholderText: "Street Address (Line 2) if needed"
                 Layout.fillWidth: true
             }

             CDDescLabel {text: "City";}
             TextField{
                 id:city;
                 placeholderText: "City of the patient"
                 Layout.fillWidth: true
             }

             CDDescLabel {text: "State";}
             CDStatePicker {
                 id: state
                 Layout.fillWidth: true
             }

             CDDescLabel {text: "Zip code";}
             TextField{
                 id: zip
                 placeholderText: "Zip/Postal code"
                 Layout.fillWidth: true
             }


             MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

             CDDescLabel {text: "Preferred Contact Method"}

             ComboBox {
                 id: contactMethod
                 textRole: "key"
                 valueRole: "value"
                 Layout.minimumWidth: 240

                 model: ListModel {
                     ListElement {
                         key: "Call Home"
                         value: "callHome"
                     }
                     ListElement {
                         key: "Call Cellphone"
                         value: "callCell"
                     }
                     ListElement {
                         key: "Call Work"
                         value: "callWork"
                     }
                     ListElement {
                         key: "Text Cellphone"
                         value: "textCell"
                     }
                     ListElement {
                         key: "Email Personal Address"
                         value: "emailPersonalAddr"
                     }
                     ListElement {
                         key: "Email Work Address"
                         value: "mailWorkAddr"
                     }
                     ListElement {
                         key: "Send Facebook Message"
                         value: "sendFacebook"
                     }

                     ListElement {
                         key: "DM via Instagram"
                         value: "sendInstagram"
                     }
                     ListElement {
                         key: "Send WhatsApp message"
                         value: "sendWhatsApp"
                     }
                 }
             }

             MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

             CDDescLabel {text: "Home Phone Number"}
             TextField{id: homePhone;Layout.fillWidth: true;}
             CDDescLabel {text: "Cell Phone Number"}
             TextField{id: cellPhone;Layout.fillWidth: true;}
             CDDescLabel {text: "Work Phone Number"}
             TextField{id: workPhone;Layout.fillWidth: true;}

             MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

             CDDescLabel {text: "Home Email Address"}
             TextField{id: homeEmail;Layout.fillWidth: true;}
//             CDDescLabel {text: "Work Email Address"} //maybe someday we will support it
//             TextField{id: workEmail;Layout.fillWidth: true;}

             MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

             CDDescLabel {text: "Facebook Name/ID"}
             TextField{id: facebookName;Layout.fillWidth: true;}
             CDDescLabel {text: "Instagram Name"}
             TextField{id: instagramName;Layout.fillWidth: true;}
             CDDescLabel {text: "WhatsApp Number"}
             TextField{id: whatsAppName;Layout.fillWidth: true;}

         }
     }

     CDAddButton {
         icon.name: "go-next"
         text: "Next"
         anchors.top: centerPane.bottom
         anchors.right: centerPane.right
         anchors.margins: 10

         CDPatientFileManager {
             id: patFileMan
         }

         onClicked: {
             rootWin.impNewPatObj["howContact"] = contactMethod.currentValue
             rootWin.impNewPatObj["homePhone"] = homePhone.text;
             rootWin.impNewPatObj["cellPhone"] = cellPhone.text
             rootWin.impNewPatObj["workPhone"] = workPhone.text;
             rootWin.impNewPatObj["homeEmail"] = homeEmail.text
             rootWin.impNewPatObj["facebookName"] = facebookName.text
             rootWin.impNewPatObj["instagramName"] = instagramName.text
             rootWin.impNewPatObj["whatsAppName"] = whatsAppName.text;

             rootWin.impNewPatObj["addr1"] = addr1.text;
             rootWin.impNewPatObj["addr2"] = addr2.text;
             rootWin.impNewPatObj["HomeCity"] = city.text;
             rootWin.impNewPatObj["HomeZip"] = zip.text;
             rootWin.impNewPatObj["HomeState"] = state.currentValue

             rootWin.impNewPatId= patFileMan.importPatient(rootWin.impNewPatObj)

             mainView.push("PatientDentalPlanPage.qml")
         }
     }

}
