// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    visible: true
    title: qsTr("Import New Patient")

    property var patList: ({})
    property string selectedPatID: ""
    property string currentURL: locFile.value("NewPatURL","")
    property var patTxPlanObj: ({})
    property var schDate;
    property var schTime;

    property var impNewPatObj: ({})
    property string impNewPatId: ""


    header:CDBlankToolBar {
        headerText: "Import New Patient"
        ToolButton {
            icon.name: "go-previous"
            onClicked: mainView.pop()
            icon.width: 64
            icon.height: 64
            anchors.left: parent.left
            visible: mainView.depth > 1
        }
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: locFile
        fileName: fileLocs.getLocalPracticePreferenceFile()
        category: "GeneralInfo"
        Component.onCompleted: {
            rootWin.currentURL = locFile.value("NewPatURL");
        }
    }

    StackView {
        id: mainView
        anchors.fill: parent
        initialItem: NewPatStartPage{}
    }

}
