// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    backMaterialColor: Material.LightGreen

    function updateTxNameDropDown() {
        txPlanBox.model = Object.keys(rootWin.treatmentPlansObj);
    }

    RowLayout {
        id: txPlanSelectRow
        function addNewPhase() {
            var phases = Object.keys(treatmentPlansObj[selectedTxPlan]);
            var newPhaseNumber = phases.length;
            rootWin.treatmentPlansObj[rootWin.selectedTxPlan][newPhaseNumber] = [];
            rootWin.generatePhasePanels();
        }


        CDDescLabel {
            text: "Treatment Plan"
        }
        ComboBox {
            id: txPlanBox
            Layout.minimumWidth: 250
            model: Object.keys(rootWin.treatmentPlansObj)
            onCurrentIndexChanged: {
                rootWin.selectedTxPlan = model[currentIndex]
            }
        }
        Button {
            text: "Rename Treatment Plan"
            enabled: txPlanBox.currentIndex != 0
            onClicked: {
                renameTxPlanDia.openWithNameAndIndex(rootWin.selectedTxPlan,
                                                     txPlanBox.currentIndex);
            }
        }

        Button {
            text: "Add Tx Plan"
            highlighted: true
            Material.accent: Material.Green
            icon.name: "list-add"
            onClicked: {
                addTxPlanDia.open();
            }

            CDTranslucentDialog {
                id: addTxPlanDia
                ColumnLayout {
                    CDTranslucentPane {
                        Layout.fillWidth: true
                        ColumnLayout {
                            CDHeaderLabel {
                                text: "Add New Treatment Plan"
                            }
                            RowLayout {
                                CDDescLabel {
                                    text: "Treatment Plan Name"
                                }
                                TextField {
                                    id: newTxPlanName
                                }
                            }
                        }
                    }
                    RowLayout {
                        Layout.fillWidth: true
                        CDCancelButton {
                            onClicked: {
                                addTxPlanDia.reject();
                            }
                        }
                        Label{Layout.fillWidth: true;}
                        CDAddButton {
                            text: "Add Treatment Plan"
                            onClicked: {
                                addTxPlanDia.accept();
                            }
                        }
                    }
                }

                onAccepted: {
                    rootWin.treatmentPlansObj[newTxPlanName.text] = ({});
                    rootWin.treatmentPlansObj[newTxPlanName.text]["Unphased"] = [];
                    updateTxNameDropDown();
                }
            }
        }

        Button {
            text: "Add Phase"
            highlighted: true
            Material.accent: Material.Green
            icon.name: "list-add"
            onClicked: {
                txPlanSelectRow.addNewPhase();
            }
        }

        Button {
            text: "Auto Phase Procedures"
            onClicked: {
                rootWin.phaseSortProcedures();
            }
        }

        Button {
            text: "Manual Add Tx"
            highlighted: true
            Material.accent: Material.Green
            icon.name: "list-add"
            onClicked: {
                manualAddTxDia.open();
            }
        }

        RenameTxPlanDia {
            id: renameTxPlanDia
            anchors.centerIn:  Overlay.overlay
            onAccepted: {

                updateTxNameDropDown();

            }
        }
    }

}


