// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: manualAddTxDia

    function isPosterior(getTooth) {
        getTooth = parseInt(getTooth);
        var returnMe = true;

        if((getTooth >= 6) && (getTooth <= 11)) { //maxillary anterior
            returnMe = false;
        } else if((getTooth >= 22)
                  && (getTooth <= 27)) { //mandibular anterior
            returnMe = false;
        }
        return returnMe;
    }

    Settings {
        id: priceSettings
        fileName: fLocs.getLocalPracticePreferenceFile();
        category: "Prices"
    }

    ColumnLayout {
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Manual Add New Treatment Item"
                    Layout.columnSpan: 2
                }
                CDDescLabel {
                    text: "Treatment Name"
                }
                TxNameBox {
                    id: txNameField
                    Layout.fillWidth: true

                    onCurrentValueChanged: {
                        dCodeField.text = currentValue
                        var locationType = model[currentIndex]["LocationType"];
                        if(locationType === "N/A") {
                            genSite.checked = true;
                        }
                        else if(locationType === "tooth") {
                            toothSite.checked = true;
                        }
                        else if(locationType === "tooth+surface") {
                            toothSurfaceSite.checked = true;
                            usesSurfaces.checked = true;
                        }
                        else if(locationType === "quad") {
                            quadSite.checked = true;
                        }
                        else if(locationType === "arch") {
                            archSite.checked = true;
                        }

                        basePriceField.text = priceSettings.value(currentValue,0);
                    }
                }

                CDDescLabel {
                    text: "Phase"
                }

                ComboBox {
                    id: phaseBox
                    Layout.minimumWidth: 150
                    property var myPhaseCount: rootWin.currentPhaseCount
                    onMyPhaseCountChanged: {
                        if(rootWin.selectedTxPlan.length < 3) {
                            return;
                        }

                        model= Object.keys(rootWin.treatmentPlansObj[
                                               rootWin.selectedTxPlan])
                        currentIndex = find("Unphased");
                    }
                }

                CDDescLabel {
                    text: "Site Type"
                }

                RowLayout {
                    RadioButton {
                        id: genSite
                        text: "Generalized or N/A"
                        checked: true
                    }
                    RadioButton {
                        id: toothSite
                        text: "Tooth"
                    }
                    RadioButton {
                        id: toothSurfaceSite
                        text: "Tooth+Surface"
                    }
                    RadioButton {
                        id: quadSite
                        text: "Quadrant"
                    }
                    RadioButton {
                        id: archSite
                        text: "Arch"
                    }
                }

                CDDescLabel {
                    text: "Location"
                }

                Label {
                    visible: genSite.checked
                    text: "N/A"
                }

                ComboBox {
                    id: toothBox
                    visible: toothSite.checked || toothSurfaceSite.checked
                    Component.onCompleted: {
                        var makeModel = [];
                        for(var i=1;i<=32;i++) {
                            makeModel.push(i);
                        }
                        for(i=65;i<85;i++) { //65 = "A"; 85 = "U"; 84 = "T"
                            var toothChar = String.fromCharCode(i);
                            makeModel.push(toothChar);
                        }

                        model = makeModel;
                    }
                }

                RowLayout {
                    id: quadRow
                    visible: quadSite.checked
                    RadioButton {
                        id: ul
                        checked: true
                        text: "Upper Left"
                        property string jsonText: "UL"
                    }
                    RadioButton {
                        id: ur
                        text: "Upper Right"
                        property string jsonText: "UR"
                    }
                    RadioButton {
                        id: ll
                        text: "Lower Left"
                        property string jsonText: "LL"
                    }
                    RadioButton {
                        id: lr
                        text: "Lower Right"
                        property string jsonText: "LR"
                    }

                    ButtonGroup {
                        id: quadButtons
                        buttons: [ul,ur,ll,lr]

                    }
                }

                RowLayout {
                    id: archRow
                    visible: archSite.checked
                    RadioButton {
                        id: ua
                        checked: true
                        text: "Upper Arch"
                    }
                    RadioButton {
                        id: la
                        text: "Lower Arch"
                    }
                }

                RowLayout {
                    visible: toothSite.checked || toothSurfaceSite.checked
                    Layout.columnSpan: 2
                    CheckBox {
                        id: usesSurfaces
                        text: "Uses surfaces"
                    }
                    RowLayout {
                        opacity: usesSurfaces.checked ? 1:0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }

                        CDSurfaceRow {
                            id: surfRow
                            workingToothNumb: toothBox.currentText
                        }
                    }
                }

                CDDescLabel {
                    text: "Material"
                }

                TextField {
                    id: materialField
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "DCode"
                }

                TextField {
                    id: dCodeField
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Base Price ($)"
                }

                TextField {
                    id: basePriceField
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Comments"
                }

                TextField {
                    id: commentsField
                    Layout.fillWidth: true
                }
            }

        }

        RowLayout {
            Layout.columnSpan: 2
            CDCancelButton {
                onClicked: {
                    manualAddTxDia.close();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Add New Treatment"
                onClicked: {
                    var addMe = ({});
                    addMe["ProcedureName"] = txNameField.currentText;

                    if(toothSite.checked) {
                        addMe["Tooth"] = toothBox.displayText;

                    }
                    else if(toothSurfaceSite.checked) {
                        addMe["Tooth"] = toothBox.displayText;
                        addMe["Surfaces"] =surfRow.getSurfaces();
                    }

                    else if(quadSite.checked) {
                        addMe["Location"] = quadButtons.checkedButton.jsonText;
                    }
                    else if(archSite.checked) {
                        if(ua.checked) {
                            addMe["Location"] = "UA"
                        }
                        else {
                            addMe["Location"] = "LA"
                        }
                    }

                    if(materialField.text.length > 0) {
                        addMe["Material"] = materialField.text;
                    }

                    if(dCodeField.text.length > 0) {
                        addMe["DCode"] = dCodeField.text;
                    }

                    if(basePriceField.text.length > 0) {
                        addMe["BasePrice"] = basePriceField.text
                    }

                    if(commentsField.text.length > 0) {
                        addMe["Comments"] = commentsField.text
                    }


                    rootWin.treatmentPlansObj[selectedTxPlan]
                            [phaseBox.displayText].push(addMe);

                    rootWin.generatePhasePanels();

                    manualAddTxDia.close();
                }
            }
        }

    }
}
