// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: creditCardPaymentPane

    backMaterialColor: Material.Yellow

    property alias getAmountGotten: amountGotten.text
    property alias getConfirmNumber: confirmNumber.text

    ColumnLayout {
        GridLayout {
            columns: 2
            CDDescLabel {
                text: "Amount billed out ($)"
            }
            TextField {
                id: amountGotten
                Layout.minimumWidth: 150
                validator: DoubleValidator {}
            }

            CDDescLabel {
                text: "Receipt / Confirmation Number"
            }
            TextField {
                id: confirmNumber
                Layout.minimumWidth: 150
            }
        }
    }
}
