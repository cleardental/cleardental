// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: addMembershipPaymentsDialog

    property int rowCounter: 0
    property real totalPaymentCounter:0
    property var paymentArray: []

    Settings {
        id: membershipSettings
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Membership"
    }

    function addRow() {
        if(rowCounter > 0) { //use the previous row and add a month
            var dateCompIndex = rowCounter * 3;
            var dateFieldItem = addPaymentsGrid.children[dateCompIndex];
            var prevDateObj = dateFieldItem.getDateObj();
            var newDateObj = new Date(prevDateObj.getFullYear(),prevDateObj.getMonth()+1,prevDateObj.getDate());
            dateComp.createObject(addPaymentsGrid,{"rowIndex":rowCounter});
            addPaymentsGrid.children[dateCompIndex+3].setDateObj(newDateObj);
        }
        else {
            dateComp.createObject(addPaymentsGrid,{"rowIndex":rowCounter}) //add in today
        }

        amountFieldComp.createObject(addPaymentsGrid,{"rowIndex":rowCounter,
                                         "text": membershipSettings.value("PricePerMonth",35)})
        remButtonComp.createObject(addPaymentsGrid,{"rowIndex":rowCounter})
        rowCounter++;
    }

    function deleteRow(getIndex) {
        for(var i=0;i<addPaymentsGrid.children.length;i++) {
            if("rowIndex" in addPaymentsGrid.children[i]) {
                if(addPaymentsGrid.children[i]["rowIndex"] === getIndex) {
                    var killMe = addPaymentsGrid.children[i];
                    killMe.destroy();
                }
            }
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                RowLayout {
                    CDHeaderLabel {
                        text: "Add Membership Payments"
                    }
                    CDAddButton {
                        text: "Add Row"
                        onClicked: addRow();
                    }
                }


                GridLayout {
                    id: addPaymentsGrid
                    columns: 3
                    CDDescLabel {text: "Date"}
                    CDDescLabel {text: "Amount ($)"}
                    CDDescLabel {text: "Remove"}
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: addMembershipPaymentsDialog.reject();
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                text: "Add Payments"
                onClicked: {
                    totalPaymentCounter =0;
                    paymentArray = []
                    for(var i=3;i<addPaymentsGrid.children.length;i+=3) {
                        var dateFieldItem = addPaymentsGrid.children[i];
                        var amountFieldItem = addPaymentsGrid.children[i+1];
                        //no need for the delete button

                        totalPaymentCounter+= parseFloat(amountFieldItem.text);
                        paymentArray.push([dateFieldItem.getDateObj(),amountFieldItem.text]);
                    }
                    addMembershipPaymentsDialog.accept();
                }
            }
        }
    }

    Component {
        id: dateComp
        CDDatePicker {
            property int rowIndex: -1
            property bool deleteMe:true
        }
    }
    Component {
        id: amountFieldComp
        TextField {
            property int rowIndex: -1
            property bool deleteMe:true
        }
    }
    Component {
        id: remButtonComp
        CDDeleteButton {
            property int rowIndex: -1
            property bool deleteMe:true
            enabled: rowIndex != 0
            onClicked: {
                addMembershipPaymentsDialog.deleteRow(rowIndex);
            }
        }
    }



    Component.onCompleted: {
        addRow();
    }



}
