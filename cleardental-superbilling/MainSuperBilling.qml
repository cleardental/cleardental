// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Billing " + " [ID: " + PATIENT_FILE_NAME + "]")

    property var ledgerList: []
    property bool newIns: false;
    property var lastUsedDate:  new Date()

    function saveAndRefresh(commitInfo) {
        var ledgerListString = JSON.stringify(ledgerList,null,"\t");
        ledMan.saveFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME),ledgerListString);
        gitMan.commitData("Updated ledger for " + PATIENT_FILE_NAME + " with " + commitInfo);
        billingListView.oldScrollAmount = billingListView.contentY
        rootWin.ledgerList = []; //force a refresh
        rootWin.ledgerList = JSON.parse(ledgerListString);
        billingListView.contentY = billingListView.oldScrollAmount
    }

    function refreshView() {
        var ledgerListString = ledMan.readFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME));
        if(ledgerListString.length < 2) {
            rootWin.ledgerList = [];
        }
        else {
            rootWin.ledgerList = JSON.parse(ledgerListString);
        }
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    CDConstLoader{
        id: commonConsts
    }

    Settings {
        id: dentalPlanSettings
        fileName: fLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    header: CDPatientToolBar {
        headerText: "Billing: "
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    BillingDrawer {
        id: drawer
        width: prefWidth + 20
        height: rootWin.height
    }

    BillingListView {
        id: billingListView
        anchors.fill: parent
        anchors.margins: 10
    }

    ManualAddHistoryDialog {
        id: manualAddHistoryDialog
        anchors.centerIn: parent
        onAccepted: refreshView()
    }

    CDCancelButton {
        id: printButton
        icon.name: "document-print"
        text: "Print Ledger"
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 10

        CDPrinter {
            id: printer
        }

        onClicked: {
            printer.printPatientLedger(PATIENT_FILE_NAME,ledgerList)
        }
    }

    CDButton {
        icon.name: "document-export"
        text: "Submit Claims"
        anchors.left: parent.left
        anchors.bottom: printButton.top
        anchors.margins: 10
        onClicked: {
            submitClaimsDia.open();
        }

        SubmitClaimsDialog {
            id: submitClaimsDia

            onAccepted: {
                saveAndRefresh("Submitted claims for " + PATIENT_FILE_NAME);
            }
        }
    }

    ColumnLayout {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        spacing: 10

        CDAddButton {
            id: addLoosePaymentButton
            text: "Add Spread Payment"


            onClicked:  {
                addPaymentDialog.open();
            }
            AddSpreadPaymentDialog {
                id: addSpreadPaymentDia
            }

            AddLoosePaymentDialog {
                id: addPaymentDialog
                anchors.centerIn: parent
                onAccepted: refreshView()
            }
        }

        CDAddButton {
            id: manualAddHistoryButton
            text: "Manual Add History"
            visible: commonConsts.isDebugMode()
            onClicked: manualAddHistoryDialog.open();
        }

        CDAddButton {
            text: "Add Membership Payments"

            Component.onCompleted: {
                dentalPlanSettings.category = "Membership";
                visible =  JSON.parse(dentalPlanSettings.value("HasMembership",false))
            }

            onClicked: {
                addMembershipPaymentsDialog.open();
            }

            AddMembershipPaymentsDialog {
                id: addMembershipPaymentsDialog

                Settings {
                    id: membershipPlanSettings
                    fileName: fLocs.getLocalPracticePreferenceFile()
                    category: "Membership"
                }

                function writeOfTx(getTxIndex) {
                    var basePrice = ledgerList[getTxIndex]["Procedure"]["BasePrice"];

                    var totalExistingPayments=0;
                    for(var i=0;i<ledgerList[getTxIndex]["Payments"].length;i++) {
                        var paymentAmount = ledgerList[getTxIndex]["Payments"][i]["Amount"];
                        totalExistingPayments += parseFloat(paymentAmount);
                    }
                    if(totalExistingPayments < basePrice) {
                        var addAmount = basePrice - totalExistingPayments;
                        var addObj = {
                            "Type": "Credit",
                            "Amount": addAmount,
                            "Reason": "Membership Plan",
                            "Date": new Date()
                        }
                        ledgerList[getTxIndex]["Payments"].push(addObj)

                    }

                }

                onAccepted: {
                    //first do all the writeoffs

                    //Exams (D0120,D0140,D0150,D0180)
                    var writeOffExams = JSON.parse(membershipPlanSettings.value("CoversExams",true));
                    var examDCodes = ["D0120","D0140","D0150","D0180"]

                    //Radiographs (D02**,D03**)
                    var writeOffRadiographs = JSON.parse(membershipPlanSettings.value("CoversRadiographs",true));

                    //Cleanings (D1110,D1120,D4355,D4910)
                    var writeOffCleanings = JSON.parse(membershipPlanSettings.value("CoversCleanings",true));
                    var cleaningDCodes = ["D1110","D1120","D4355","D4910"];

                    //fluoride (D1206, D1208)
                    var writeOffFluoride = writeOffCleanings; //Until we have another varible, this makes sense
                    var fluorideCodes = ["D1206","D1208"]

                    //Sealants (D1351)
                    var writeOffSealants =  JSON.parse(membershipPlanSettings.value("CoversSelants",false));
                    var sealantCodes = ["D1351"] //maybe there will be other codes in the future

                    for(var i=0;i<ledgerList.length;i++) {
                        var txLine = ledgerList[i];
                        var procedureObj = txLine["Procedure"];
                        if("DCode" in procedureObj) {
                            var iDCode = procedureObj["DCode"];
                            if(examDCodes.includes(iDCode)) { //exams
                                if(writeOffExams) {
                                    writeOfTx(i);
                                }
                            }
                            else if( iDCode.startsWith("D02") || iDCode.startsWith("D03") ) { //radiographs
                                if(writeOffRadiographs) {
                                    writeOfTx(i);
                                }
                            }
                            else if(cleaningDCodes.includes(iDCode)) { //cleanings
                                if(writeOffCleanings) {
                                    writeOfTx(i);
                                }
                            }
                            else if(fluorideCodes.includes(iDCode)) { //fluoride
                                if(writeOffFluoride) {
                                    writeOfTx(i);
                                }
                            }
                            else if(sealantCodes.includes(iDCode)) { //sealants
                                if(writeOffSealants) {
                                    writeOfTx(i);
                                }
                            }
                        }
                    }

                    //Now add in all the payments for the membership plan as a fake procedure
                    var membershipProcedureObj = {
                        "ClaimStatus": "N/A",
                        "Procedure": {
                            "BasePrice": totalPaymentCounter,
                            "DCode": "MEMPAY",
                            "ProcedureName": "Membership Payment"
                        },
                        "ProcedureDate": new Date()
                        //no provider because it's complicated....
                    }
                    var makePaymentsArray = [];
                    for(i=0;i<paymentArray.length;i++) {
                        var addPaymentObj = {
                            "Type": "Membership Payment",
                            "Amount": paymentArray[i][1],
                            "Date": paymentArray[i][0]
                        }
                        makePaymentsArray.push(addPaymentObj);
                    }
                    membershipProcedureObj["Payments"] = makePaymentsArray
                    ledgerList.push(membershipProcedureObj);

                    saveAndRefresh();
                }
            }
        }
    }

    CDTranslucentPane {
        id: totalPane
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10
        setOpacity: .75
        property bool patOwes: false
        backMaterialColor: patOwes ? Material.Red : Material.Green
        RowLayout {
            id: summaryRow

            CDDescLabel {
                text: "Current Balance"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        returnMe += parseFloat( ledgerList[w]["Procedure"]["BasePrice"]);
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] === "Dental Plan Payment") {
                                returnMe -= parseFloat(paymentItem["WriteOff"]);
                            }
                            returnMe -= parseFloat(paymentItem["Amount"]);
                        }
                    }
                    totalPane.patOwes = returnMe > 0.01;
                    return "$"+returnMe.toFixed(2);
                }
            }
            Label {
                Layout.minimumWidth: 50
            }
            CDDescLabel {
                text: "Total Collections"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] !== "Credit") {
                                returnMe += parseFloat(paymentItem["Amount"]);
                            }
                        }
                    }
                    return "$"+returnMe.toFixed(2);
                }
            }
            Label {
                Layout.minimumWidth: 50
            }
            CDDescLabel {
                text: "Total Writeoffs"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] === "Credit") {
                                returnMe += parseFloat(paymentItem["Amount"]);
                            }
                            else if(paymentItem["Type"] === "Dental Plan Payment") {
                                returnMe += parseFloat(paymentItem["WriteOff"]);
                            }
                        }
                    }
                    return "$"+returnMe.toFixed(2);
                }
            }

        }

    }

    Component.onCompleted: {
        refreshView();
    }

}
