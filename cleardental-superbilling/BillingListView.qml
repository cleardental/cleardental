// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

ListView {
    id: billingListView
    model: ledgerList.length
    spacing: 10
    displayMarginBeginning: 1024
    displayMarginEnd: 1024*1024

    property int oldScrollAmount: 0
    footer: Label {
        height: 128
    }


    function calculateCollections(getIndex) {
        var paymentArray = ledgerList[getIndex]["Payments"];
        var returnMe =0;
        for(var i=0;i<paymentArray.length;i++) {
            var paymentItem = paymentArray[i];
            if(paymentItem["Type"] !== "Credit") {
                returnMe += parseFloat(paymentItem["Amount"]);
            }
        }
        return returnMe;
    }

    function calculateWriteOffs(getIndex) {
        var paymentArray = ledgerList[getIndex]["Payments"];
        var returnMe =0;
        for(var i=0;i<paymentArray.length;i++) {
            var paymentItem = paymentArray[i];
            if(paymentItem["Type"] === "Credit") {
                returnMe += parseFloat(paymentItem["Amount"]);
            }
            else if(paymentItem["Type"] === "Dental Plan Payment") {
                returnMe += parseFloat(paymentItem["WriteOff"]);
            }
        }
        return returnMe;
    }


    delegate: TxItemViewDelegate {
        width: billingListView.width
        procedureLedgerObject: ledgerList[index]
        ledgerIndex: index
        totalCollectionsFromTx: calculateCollections(index)
        totalWriteoffsFromTx: calculateWriteOffs(index)
    }

    //displayMarginEnd: 1024
    ScrollBar.vertical: ScrollBar {id: scrollBar }
}
