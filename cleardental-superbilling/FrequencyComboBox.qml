// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.12

ComboBox {
    id: freqComboBox
    model: [
        {
            ShowText: "1 per 6 months",
            Value: "1p6"
        },
        {
            ShowText: "1 per 12 months",
            Value: "1p12"
        },
        {
            ShowText: "2 per 12 months",
            Value: "2p12"
        },
        {
            ShowText: "1 per year",
            Value: "1py"
        },
        {
            ShowText: "2 per year",
            Value: "2py"
        },
        {
            ShowText: "3 per year",
            Value: "3py"
        },
        {
            ShowText: "4 per year",
            Value: "4py"
        }
    ]

    textRole: "ShowText"
    valueRole: "Value"
}
