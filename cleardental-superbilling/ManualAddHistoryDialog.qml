// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: manualAddHistoryDialog

    ColumnLayout {
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Manual Add history"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                     text: "Procedure JSON object"
                }

                TextArea {
                    id: procedureJson
                    Layout.fillWidth: true
                    Layout.minimumHeight: 240
                    Layout.minimumWidth: 240
                }
                CDDescLabel {
                    text: "Procedure Completed"
                }

                CheckBox {
                    id: procedureCompletedBox
                }

                CDDescLabel {
                    visible: procedureCompletedBox.checked
                    text: "Procedure Date"
                }

                CDCalendarDatePicker {
                    id: procedureDateCal
                    visible: procedureCompletedBox.checked
                }

                CDDescLabel {
                    text: "Claim Status"
                }

                ComboBox {
                    id: claimStatusBox
                    editable: false
                    model: ["Unsent", "Sent", "Do Not Submit", "Rejected", "Submitted Again", "Given Up",
                        "Paid Partially", "Fully Paid"]
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Provider"
                }
                CDProviderBox {
                    id: providerBox
                    Layout.fillWidth: true
                }
            }

        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    manualAddHistoryDialog.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                text: "Add Procedure"
                onClicked: {
                    var addMe = ({});
                    addMe["Procedure"] = JSON.parse(procedureJson.text);
                    if(procedureCompletedBox.checked) {
                        addMe["ProcedureDate"] = procedureDateCal.myDate;
                    }
                    addMe["Payments"] = [];
                    addMe["ClaimStatus"] = claimStatusBox.currentValue
                    addMe["ProviderUsername"] = providerBox.providerIDs[providerBox.currentIndex];
                    rootWin.ledgerList.push(addMe);
                    var ledgerListString = JSON.stringify(rootWin.ledgerList, null, '\t');
                    ledMan.saveFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME),ledgerListString);
                    gitMan.commitData("Added procedure for "  + PATIENT_FILE_NAME + " of " +
                                      JSON.stringify(procedureJson.text));
                    manualAddHistoryDialog.accept();
                }
            }
        }
    }

}
