// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.15
import QtQuick.Particles 2.15

ParticleSystem{
    id: billingParticleSystem
    property string emitString: "$"
    property int emitLevel: 1
    property bool goingUp: true

    ItemParticle {
        anchors.fill: parent
        delegate: Label {
            text: emitString
            font.pointSize: Math.max( Math.random() * 32,1)
            font.family: "Noto Color Emoji"
        }
    }

    Wander {
        xVariance: 250;
        pace: 25;
    }

    Emitter {
        anchors.fill: parent
        startTime: 15000
        emitRate: emitLevel
        lifeSpan: 15000
        acceleration: PointDirection{ y: goingUp ? -25 : 25; xVariation: 10; yVariation: 10 }
        size: 24
        sizeVariation: 160
    }
}
