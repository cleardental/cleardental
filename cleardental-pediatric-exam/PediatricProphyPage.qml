// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {

    CDTranslucentPane {
        id: prophyPane
        anchors.centerIn: parent
        backMaterialColor: Material.Green

        ColumnLayout {
            RowLayout {
                spacing: 20
                CDDescLabel {
                    text: "Patient's OHI"
                }
                ComboBox {
                    id: patOHI
                    model: ["Excellent", "Good", "Poor", "Very Poor"]
                    Layout.minimumWidth: 150
                    Component.onCompleted: {
                        currentIndex = 1
                    }
                }
            }

            RowLayout {
                spacing: 20
                CDDescLabel {
                    text: "Patient's Behavior"
                }
                ComboBox {
                    id: patBeh
                    model: ["Excellent", "Good", "Poor", "Very Poor"]
                    Layout.minimumWidth: 150
                    Component.onCompleted: {
                        currentIndex = 1
                    }
                }
            }


            CheckBox {
                id: ohiReviewed
                text: "OHI Reviewed"
            }

            CheckBox {
                id: scaledWithUltra
                text: "Scaled with ultrasonic scaler"
            }

            CheckBox {
                id: scaledWithHand
                text: "Scaled with hand scaler"
            }

            CheckBox {
                id: polishedProphy
                text: "Polished with prophy paste"
            }

            RowLayout {
                CheckBox {
                    id: didFlu
                    text: "Fluride Treatment applied"
                }
                ComboBox {
                    id: fluorideType
                    opacity: didFlu.checked ? 1:0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                    enabled: didFlu.checked
                    Layout.minimumWidth: 200
                    model: ["Varnish", "Gel + Tray", "Foam + Tray"]
                }
            }

            RowLayout {
                spacing: 20
                CDDescLabel {
                    text: "Next Prophy"
                }
                ComboBox {
                    id: nextProphyBox
                    model: ["12 Months", "6 Months", "3 Months"]
                    Layout.minimumWidth: 150
                    Component.onCompleted: {
                        currentIndex = 1
                    }
                }
            }

        }
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDFinishProcedureButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        text: "Finish procedure"
        onClicked: {
            var procedures = [];
            if(rootWin.isCompExam) {
                var compExamObj = ({})
                compExamObj["ProcedureName"] = "Comprehensive Exam";
                compExamObj["DCode"] = "D0150";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",compExamObj);
                procedures.push(compExamObj);
            }
            else {
                var recareExam = ({});
                recareExam["ProcedureName"] = "Periodic Exam";
                recareExam["DCode"] = "D0120";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",recareExam);
                procedures.push(recareExam);
            }

            if(scaledWithHand.checked || polishedProphy.checked) {
                var prophyObj = ({})
                prophyObj["ProcedureName"] = "Pediatric Prophy";
                prophyObj["DCode"] = "D1120";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",prophyObj);
                procedures.push(prophyObj);
            }



            if(didFlu.checked) {
                var flu = ({});
                flu["ProcedureName"] = "Fluoride varnish";
                flu["DCode"] = "D1206";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",flu);
                procedures.push(flu);
            }

            rootWin.prophyCaseNoteString = "Patient presented for prophy.\n";
            rootWin.prophyCaseNoteString += "OHI: " + patOHI.currentText + "\n";
            if(ohiReviewed.checked) {
                rootWin.prophyCaseNoteString += "OHI reviewed.\n";
            }
            if(scaledWithUltra.checked) {
                rootWin.prophyCaseNoteString += "Scaled using ultrasonic scaler.\n";
            }
            if(scaledWithHand.checked) {
                rootWin.prophyCaseNoteString += "Scaled using hand scaler.\n";
            }
            if(polishedProphy.checked) {
                rootWin.prophyCaseNoteString += "Polished with prophy paste.\n";
            }
            if(didFlu.checked) {
                rootWin.prophyCaseNoteString += "Fluoride varnish applied.\n";
            }

            rootWin.prophyCaseNoteString += "Patient's behavior: "+ patBeh.currentText +".\n";


            finDia.txItemsToComplete = procedures;
            finDia.caseNoteString = rootWin.initCaseNoteString + rootWin.examCaseNoteString +
                    rootWin.hardTissueCaseNoteString + rootWin.prophyCaseNoteString;
            finDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finDia

    }
}


