// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTransparentPage {

    ColumnLayout {
        anchors.centerIn: parent
        spacing: 25


        CDReviewRadiographPane {
            Layout.minimumWidth: 640
        }

        CDTranslucentPane {
            id: examItemsPane
            backMaterialColor: Material.Yellow

            CDToolLauncher {
                id: launcher
            }

            ColumnLayout {
                CDReviewLauncher {
                    text: "Take Photographs"
                    launchEnum: CDToolLauncher.TakePhotograph
                    iniProp: "Photograph"
                }

                CDReviewLauncher {
                    id: extraOralButton
                    text: "Extra Oral exam"
                    launchEnum: CDToolLauncher.ExtraoralExam
                    iniProp: "EoE"
                }

                CDReviewLauncher {
                    id: intraOralButton
                    text: "Intraoral / Soft tissue exam"
                    launchEnum: CDToolLauncher.IntraoralExam
                    iniProp: "EoE"
                }

                RowLayout {
                    Label {
                        text: "Oral Cancer screening:";
                        font.bold: true
                    }
                    RadioButton {
                        id: negOCS
                        text: "Negative"
                        checked: true
                    }
                    RadioButton {
                        text: "Positive"
                    }
                }
            }
        }
    }

    Button {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        highlighted: true
        text: "Next"
        Material.accent: Material.Green
        icon.name: "go-next"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            rootWin.examCaseNoteString =  "Oral Cancer screening: " + (negOCS.checked ? "Negative" : "Positive") + ".\n";
            mainStack.push("ChartExistingPedoTeeth.qml");
        }
    }
}


