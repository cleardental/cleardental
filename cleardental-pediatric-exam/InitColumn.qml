// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDTransparentPage {
    ColumnLayout {
        id: initColumn
        anchors.centerIn: parent
        spacing: 25

        CDTranslucentPane {
            id: basicInfoPane

            Layout.alignment: Qt.AlignHCenter

            backMaterialColor: Material.Orange

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Presentation"
                }

                CDDescLabel {
                    text: "Type of Exam"
                }

                ComboBox {
                    id: examType
                    model: ["Comprehensive Exam", "Periodic Exam"]
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Who is with patient"
                }
                RowLayout {
                    ComboBox {
                        id: whoWithPatientBox
                        model: ["Mother", "Father", "Guardian", "Other"]
                    }
                    TextField {
                        id: otherPersonTextField

                        opacity: (whoWithPatientBox.currentIndex ===3) ? 1:0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                        Layout.minimumWidth: 300
                    }
                }

                Label {
                    text: "Chief Complaint"
                    font.bold: true
                }
                TextField {
                    id: ccTextField
                    text: "Nothing (Only Exam and Cleaning)"
                    Layout.minimumWidth: 300
                }
                CDReviewLauncher {
                    id: perReviewButton
                    text: "Personal Information Review"
                    launchEnum: CDToolLauncher.ReviewPatPersonal
                    iniProp: "Personal"
                }
            }
        }

        CDMedReviewPane {
            id: medReview
            Layout.alignment: Qt.AlignHCenter
            Layout.minimumWidth: 920

        }
    }

    Button {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        highlighted: true
        text: "Next"
        Material.accent: Material.Green
        icon.name: "go-next"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            if(whoWithPatientBox.currentText == "Other") {
                rootWin.initCaseNoteString += "Patient presented with " + otherPersonTextField.text + " for a " +
                        examType.currentText + ".\n";
            }
            else {
                rootWin.initCaseNoteString += "Patient presented with " + whoWithPatientBox.currentText + " for a " +
                        examType.currentText + ".\n";
            }
            rootWin.initCaseNoteString +=  "CC: " + ccTextField.text + "\n";

            rootWin.isCompExam = (examType.currentIndex==0);
            mainStack.push("ExamItemsPane.qml");
        }
    }


    Component.onCompleted: {
        var procedureObj = JSON.parse(PROCEDURE_JSON);
        if("ProcedureName" in procedureObj) {
            if(procedureObj["ProcedureName"].startsWith("Comprehensive")) {
                rootWin.isCompExam = true;
                examType.currentIndex = 0;
            }
            else {
                rootWin.isCompExam = false;
                examType.currentIndex = 1;
            }
        }
    }

}

