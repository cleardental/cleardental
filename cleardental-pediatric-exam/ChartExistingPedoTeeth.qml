// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: chartPage
    property int patAge: getAge() //in months

    font.family: "Monda"
    font.pointSize: 24

    CDFileLocations {
        id: ch_Locs
    }

    Settings {
        id: ch_patPersonalSettings
        fileName: ch_Locs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Personal"
    }

    Settings {
        id: ch_hardTissue
        fileName: ch_Locs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }


    CDGitManager {
        id: ch_gitMan
    }

    CDFileLocations {
        id: fLocs
    }

    CDCommonFunctions {
        id: comFuns
    }

    function getAge() {
        var returnMe =0;
        var textDOBArray = ch_patPersonalSettings.value("DateOfBirth","1/1/0001").split("/");
        var dobMonth = parseInt(textDOBArray[0]);
        var dobDay = parseInt(textDOBArray[1]);
        var dobYear =  parseInt(textDOBArray[2]);
        var today = new Date();

        var monthDiff = (today.getFullYear() - dobYear) * 12;
        monthDiff -= dobMonth;
        monthDiff += today.getMonth() + 1;

        return monthDiff;
    }

    function makeTxPlanArray(getTooth) {
        var txPlanItems = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME);
        var returnMe = [];
        for(var i=0;i<txPlanItems.length;i++) {
            var txItem = txPlanItems[i];
            if("Tooth" in txItem) {
                // @disable-check M126
                if(txItem["Tooth"] == getTooth) {
                    returnMe.push(txItem);
                }
            }
        }
        return returnMe;
    }

    function makeExistingArray(getTooth) {
        var fullString = ch_hardTissue.value(getTooth,"");
        return comFuns.getHardTissueAttrs(fullString);
    }

    function setCheckedPerm(getTooth) {
        //first check to see if we are doing a comp exam
        if(rootWin.isCompExam) {
            //So then go by age
            getTooth = parseInt(getTooth);

            var ageYear = patAge / 12;
            if(ageYear < 6) {
                return false;
            }

            var fakeNumber =0; //compress the left and right side

            if(getTooth <= 16) { //maxillary
                if(getTooth > 8) { //left
                    fakeNumber = 17 -getTooth;
                }
                else {
                    fakeNumber = getTooth;
                }
            }
            else { //mandibular
                if(getTooth < 25) { //left
                    fakeNumber = 32 - (getTooth - 17)
                }
                else {
                    fakeNumber = getTooth;
                }
            }

            switch(fakeNumber) {
            case 1:
            case 32:
                return ageYear >= 17;
            case 2:
                return ageYear >= 12;
            case 3:
                return ageYear >= 6;
            case 4:
                return ageYear >= 10;
            case 5:
                return ageYear >= 10;
            case 6:
                return ageYear >= 11;
            case 7:
                return ageYear >= 8;
            case 8:
                return ageYear >= 7;
            case 25:
                return ageYear >= 6;
            case 26:
                return ageYear >= 7;
            case 27:
                return ageYear >= 9;
            case 28:
                return ageYear >= 10;
            case 29:
                return ageYear >= 11;
            case 30:
                return ageYear >= 6;
            case 31:
                return ageYear >= 11;
            default:
                return false;
            }
        }
        else {
            //If we have seen the patient before
            var fullString = ch_hardTissue.value(getTooth,"");
            var attrArray = comFuns.getHardTissueAttrs(fullString);
            return !attrArray.includes("Missing");
        }

    }

    function setVisibleDec(getIndex) {
        if( (getIndex < 4) ||
                ((getIndex > 13) && (getIndex <20)) ||
                (getIndex > 29)) {
            return false;
        }
        return true;

    }

    function setTextDec(getIndex) {
        if(!setVisibleDec(getIndex)) {
            return "X"
        }

        if(getIndex < 16) {
            return String.fromCharCode(61 + getIndex);
        }
        else {
            return String.fromCharCode(55 + getIndex);
        }
    }

    function setCheckedDec(getIndex) {
        //first check if it is already charted
        var setAlready = ch_hardTissue.value(setTextDec(getIndex),"");
        //console.debug("toothNumb: " +setTextDec(getIndex))
        //console.debug("setAlready: " +setAlready)
        if(setAlready.length > 1) {
            if(setAlready.includes("Missing")) {
                return false;
            }
            else {
                //console.debug("Going to return " + true)
                return true;
            }
        }

        //Otherwise go based on age

        var ageYear = patAge / 12;

        if(patAge < 6) {
            return false;
        }
        else if(ageYear > 12) {
            return false;
        }

        switch(getIndex) {
        case 4:
        case 13: //max 2nd molar
            return ((patAge >= 25) && (ageYear < 12));
        case 5:
        case 12://max 1st molar
            return ((patAge >= 13) && (ageYear < 11));
        case 6:
        case 11: //max canine
            return ((patAge >= 16) && (ageYear < 12));
        case 7:
        case 10: //max lateral
            return ((patAge >= 9) && (ageYear < 8));
        case 8:
        case 9: //max central
            return ((patAge >= 8) && (ageYear < 7));
        case 20:
        case 29: //man 2nd molar
            return ((patAge >= 23) && (ageYear < 12));
        case 21:
        case 28: //man 1st molar
            return ((patAge >= 14) && (ageYear < 11));
        case 22:
        case 27: //man canine
            return ((patAge >= 17) && (ageYear < 12));
        case 23:
        case 26: //man lateral
            return ((patAge >= 10) && (ageYear < 8));
        case 24:
        case 25: //man central
            return ((patAge >= 6) && (ageYear < 7));
        default:
            return false;
        }

    }

    function updateMissingTooth(getToothNumb,getExisting) {
        var fullString = ch_hardTissue.value(getToothNumb,"");
        fullString = comFuns.remHardTissueAttr(fullString,"Missing"); //if it there; not anymore
        if(!getExisting) {
            fullString = comFuns.addHardTissueAttr(fullString,"Missing");
        }

        ch_hardTissue.setValue(getToothNumb,fullString);
        ch_hardTissue.sync();
    }

    CDAddButton {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        text: "Tx Plan Sealants"
        onClicked: {
            sealDia.open();
        }

        SealantDialog {
            id: sealDia

        }
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Orange

        GridLayout {
            id: fullToothGrid
            columns: 16
            CDHeaderLabel {
                text: "Chart Teeth"
                Layout.columnSpan: 16
            }

            Repeater { ////////////MAXILLARY PERM
                model: 16
                Button {
                    id: permButton
                    text: index +1
                    checkable: true
                    checked: setCheckedPerm(text)
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175


                    CDToothScene3D {
                        id: toothScene3D
                        anchors.fill: parent
                        anchors.topMargin: 10
                        toothNumb: parent.text
                        existingList: makeExistingArray(parent.text)
                        txPlanList: makeTxPlanArray(parent.text)
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            permButton.toggle();
                            updateMissingTooth(permButton.text,permButton.checked);
                            toothScene3D.existingList = makeExistingArray(parent.text);
                            toothScene3D.reloadInformation();
                        }
                    }

                    Button {
                        icon.name: "document-edit"
                        icon.width: 32
                        icon.height: 32
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.margins: 5
                        width: 42
                        height: 42
                        highlighted: true
                        Material.accent: Material.Lime
                        onClicked: {
                            chartDialog.toothNumber = parent.text;
                            chartDialog.open();
                        }
                    }
                }
            }

            Repeater {
                model: 3
                Item{
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175
                }
            }
            Repeater { ////////////MAXILLARY PRIMARY
                model: 10
                Button {
                    id: deciduousButton
                    text: setTextDec(index+4)
                    checkable: true
                    //visible: setVisibleDec(index+1)
                    checked: setCheckedDec(index+4)
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175

                    CDToothScene3D {
                        id: toothScene3DmaxDe
                        anchors.fill: parent
                        anchors.topMargin: 10
                        toothNumb: parent.text
                        existingList: makeExistingArray(parent.text)
                        txPlanList: makeTxPlanArray(parent.text)
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.toggle();
                            updateMissingTooth(parent.text,parent.checked);
                            toothScene3DmaxDe.existingList = makeExistingArray(parent.text);
                            toothScene3DmaxDe.reloadInformation();
                        }
                    }

                    Button {
                        icon.name: "document-edit"
                        icon.width: 32
                        icon.height: 42
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.margins: 5
                        width: 42
                        height: 42
                        highlighted: true
                        Material.accent: Material.Lime
                        onClicked: {
                            chartDialog.toothNumber = parent.text;
                            chartDialog.open();
                        }
                    }

                }
            }
            Repeater {
                model: 3
                Item{
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175
                }
            }

            MenuSeparator {
                Layout.columnSpan: 16
                Layout.fillWidth: true
            }
            Repeater {  ////////////MANDIBULAR PERM
                model: 16
                Button {
                    id: permManButton
                    text: 32-index
                    checkable: true
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175
                    checked: setCheckedPerm(text)

                    CDToothScene3D {
                        id: toothScene3DManPerm
                        anchors.fill: parent
                        anchors.topMargin: 10
                        toothNumb: parent.text
                        existingList: makeExistingArray(parent.text)
                        txPlanList: makeTxPlanArray(parent.text)
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.toggle();
                            updateMissingTooth(parent.text,parent.checked);
                            toothScene3DManPerm.existingList = makeExistingArray(parent.text);
                            toothScene3DManPerm.reloadInformation();
                        }
                    }

                    Button {
                        icon.name: "document-edit"
                        icon.width: 32
                        icon.height: 32
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.margins: 5
                        width: 42
                        height: 42
                        highlighted: true
                        Material.accent: Material.Lime
                        onClicked: {
                            chartDialog.toothNumber = parent.text;
                            chartDialog.open();
                        }
                    }

                }
            }

            Repeater {
                model: 3
                Item{
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175
                }
            }

            Repeater { ////////////MANIBULAR PRIMARY
                model: 10
                Button {
                    id: deciduousManButton
                    text: setTextDec(29-index)
                    visible: setVisibleDec(29-index)
                    checkable: true
                    checked: setCheckedDec(29-index)
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 175

                    CDToothScene3D {
                        id: toothScene3DManDec
                        anchors.fill: parent
                        anchors.topMargin: 10
                        toothNumb: parent.text
                        existingList: makeExistingArray(parent.text)
                        txPlanList: makeTxPlanArray(parent.text)
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            parent.toggle();
                            updateMissingTooth(parent.text,parent.checked);
                            toothScene3DManDec.existingList = makeExistingArray(parent.text);
                            toothScene3DManDec.reloadInformation();
                        }
                    }

                    Button {
                        icon.name: "document-edit"
                        icon.width: 32
                        icon.height: 32
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.margins: 5
                        width: 42
                        height: 42
                        highlighted: true
                        Material.accent: Material.Lime
                        onClicked: {
                            chartDialog.toothNumber = parent.text;
                            chartDialog.open();
                        }
                    }

                }
            }
        }
    }

    ChartDialog {
        id: chartDialog
        anchors.centerIn: parent
        toothNumber: "A"

        onAccepted: {
            for(var i=1;i<fullToothGrid.children.length;i++) {
                var child = fullToothGrid.children[i];
                if("text" in child) {
                    if(child.text === toothNumber) {
                        child.children[0].existingList = makeExistingArray(toothNumber)
                        child.children[0].txPlanList = makeTxPlanArray(toothNumber)
                        child.children[0].reloadInformation();
                    }

                }
            }
        }
    }

    CDButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        highlighted: true
        text: "Next"
        Material.accent: Material.Green
        icon.name: "go-next"
        onClicked: {
            ch_gitMan.commitData("Updated hard tissue charting for " + PATIENT_FILE_NAME);
            //caseNoteString += "Updated hard tissue charting.\n";
            mainStack.push("PediatricProphyPage.qml")
        }
    }
}


