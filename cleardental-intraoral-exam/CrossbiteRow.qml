// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

Row {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "None")) {
            return;
        }

        antCross.checked = prevString.includes(antCross.text);
        antEdge.checked = prevString.includes(antEdge.text);
        antOpenBite.checked = prevString.includes(antOpenBite.text);
        leftCross.checked = prevString.includes(leftCross.text);
        rightCross.checked = prevString.includes(rightCross.text);
    }

    function generateSaveString() {
        var returnMe = "";
        if(noCross.checked) {
            returnMe = noCross.text
        }
        else {
            var pos = [];
            if(antCross.checked) {
                pos.push(antCross.text);
            }
            else if(antEdge.checked) {
                pos.push(antEdge.text);
            }
            else if(antOpenBite.checked) {
                pos.push(antOpenBite.text);
            }
            if(leftCross.checked) {
                pos.push(leftCross.text);
            }
            if(rightCross.checked) {
                pos.push(rightCross.text);
            }
            returnMe = pos.join(" and ")
        }

        return returnMe;
    }

    RadioButton {
        id: noCross
        text: "None"
        checked: true
        onCheckedChanged:  {
            if(checked) {
                antCross.checked = false;
                leftCross.checked = false;
                rightCross.checked = false;
            }
        }
    }

    CheckBox {
        id: antCross
        text: "Ant. Cros."
        onCheckedChanged:  {
            if(checked) {
                noCross.checked = false;
            }
        }
    }

    CheckBox {
        id: antEdge
        text: "Ant. Edge-to-Edge"
        onCheckedChanged:  {
            if(checked) {
                noCross.checked = false;
            }
        }
    }

    CheckBox {
        id: antOpenBite
        text: "Ant. Open Bite"
        onCheckedChanged:  {
            if(checked) {
                noCross.checked = false;
            }
        }
    }

    CheckBox {
        id: leftCross
        text: "Left Post."
        onCheckedChanged:  {
            if(checked) {
                noCross.checked = false;
            }
        }
    }

    CheckBox {
        id: rightCross
        text: "Right Post."
        onCheckedChanged:  {
            if(checked) {
                noCross.checked = false;
            }
        }
    }

    move: Transition {
        NumberAnimation {
            properties: "x,y"
            duration: 200
        }
    }
}
