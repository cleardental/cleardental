// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "None")) {
            return;
        }

        if(prevString.includes(attrPath.text)) {
            attrPath.checked = true;
        }
        else if(prevString.includes(attrEnd.text)) {
            attrEnd.checked = true;
        }
        else if(prevString.includes(attrCross.text)) {
            attrCross.checked = true;
        }
        else {
            attriOther.checked = true;
            attriOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(attrWNL.checked) {
            returnMe = attrWNL.text
        }
        else if(attrPath.checked) {
            returnMe = attrPath.text
        }
        else if(attrEnd.checked) {
            returnMe = attrEnd.text;
        }
        else if(attrCross.checked) {
            returnMe = attrCross.text;
        }
        else if(attriOther.checked) {
            returnMe = attriOtherInput.text;
        }
        return returnMe;
    }

    RadioButton {
        id: attrWNL
        text: "None"
        checked: true
    }
    RadioButton {
        id: attrPath
        text: "Pathway"
    }
    RadioButton {
        id: attrEnd
        text: "End-to-end"
    }
    RadioButton {
        id: attrCross
        text: "Crossover"
    }
    RadioButton {
        id: attriOther
        text: "Other"
    }
    TextField {
        id: attriOtherInput
        opacity: attriOther.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
