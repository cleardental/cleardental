// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "Normal")) {
            return;
        }

        if(prevString.includes(lineaAlba.text)) {
            lineaAlba.checked = true;
        }
        else {
            bLesionNoted.checked = true;
            bLesionNotedInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(bMucoWNL.checked) {
            returnMe = bMucoWNL.text;
        }
        else if(lineaAlba.checked) {
            returnMe = lineaAlba.text;
        }
        else if(bLesionNoted.checked) {
            returnMe = bLesionNotedInput.text;
        }

        return returnMe;
    }

    RadioButton {
        id: bMucoWNL
        text: "Normal"
        checked: true
    }

    RadioButton {
        id: lineaAlba
        text: "Linea alba noted"
    }

    RadioButton {
        id: bLesionNoted
        text: "Other lesion noted"
    }

    TextField {
        id: bLesionNotedInput
        opacity: bLesionNoted.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
