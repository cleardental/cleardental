// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "No significant findings")) {
            return;
        }

        if(prevString.includes(fomTori.text)) {
            fomTori.checked = true;
        }
        else {
            fomOther.checked = true;
            fomOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(fomWNL.checked) {
            returnMe = fomWNL.text
        }
        else if(fomTori.checked) {
            returnMe = fomTori.text
        }
        else if(fomOther.checked) {
            returnMe = fomOtherInput.text;
        }
        return returnMe;
    }

    RadioButton {
        id: fomWNL
        text: "No significant findings"
        checked: true
    }
    RadioButton {
        id: fomTori
        text: "Mandibular Tori present"
    }
    RadioButton {
        id: fomOther
        text: "Other"
    }
    TextField {
        id: fomOtherInput
        opacity: fomOther.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
