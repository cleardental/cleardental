// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtMultimedia 5.12

CDTransparentPage {
    id: root

    CDFileLocations {
        id: fLocs
    }

    CDPatientFileManager {
        id: fileManager
    }

    property alias touchView: vidOutput
    property alias wallView: wallMonitorView
    property alias ceilingView: topMonitorView

    property string selectedButton;

    CDTranslucentPane {
        anchors.centerIn: parent
        GridLayout {
            columns: 2
            CDDescLabel {
                text: "Camera"
            }

            ComboBox {
                id: selectCamera
                model:  QtMultimedia.availableCameras
                valueRole: "deviceId"
                textRole: "displayName"
                Layout.fillWidth: true
                onActivated: {
                    camera.deviceId = QtMultimedia.availableCameras[currentIndex].deviceId
                }
            }

            CDDescLabel {
                text: "Which tooth"
            }


            ColumnLayout {
                RowLayout {
                    id: maxRow
                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 16
                        RoundButton {
                            id: maxButton
                            text: index+1
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(maxButton);
                            }
                            onClicked: selectedButton = text;
                        }
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 16
                        RoundButton {
                            id: manButton
                            text: 32 - index
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(manButton);
                            }
                            onClicked: selectedButton = text;
                        }
                    }
                }

                ButtonGroup {
                    id: butGroup
                }
            }

            // CDDescLabel {
            //     text: "Which Screen"
            // }

            // RowLayout {
            //     RadioButton {
            //         id: touchRadio
            //         text: "Touchscreen"
            //         checked: true
            //     }
            //     RadioButton {
            //         id: wallRadio
            //         text: "Wall Screen"
            //     }
            //     RadioButton {
            //         id: ceilingRadio
            //         text: "Ceiling"
            //     }
            //     ButtonGroup {
            //         buttons: [touchRadio, wallRadio,ceilingRadio]
            //         onClicked: {
            //             camera.stop();
            //             var oldID = camera.deviceId;
            //             camera.deviceId = 10;
            //             if(checkedButton.text == "Touchscreen") {
            //                 root.touchView.source = camera;
            //                 root.wallView.source = null
            //                 root.ceilingView.source = null
            //             }
            //             else if(checkedButton.text == "Wall Screen") {
            //                 root.touchView.source = null;
            //                 root.wallView.source = camera
            //                 root.ceilingView.source = null
            //             }
            //             else { //Ceiling
            //                 root.touchView.source = null;
            //                 root.wallView.source = null
            //                 root.ceilingView.source = camera
            //             }
            //             //camera.deviceId = oldID;
            //             //camera.start();
            //         }
            //     }
            // }

            VideoOutput {
                Layout.columnSpan: 2
                id: vidOutput
                source: camera
//                Layout.minimumWidth: 640
//                Layout.minimumHeight: 480
                Layout.maximumWidth: root.width * .7
                Layout.maximumHeight: root.height * .7
                Layout.alignment: Qt.AlignHCenter
            }

            Image {
                Layout.columnSpan: 2
                id: candidateImg
                fillMode: Image.PreserveAspectFit
                visible: source.length > 0
                Layout.maximumWidth: root.width * .7
                Layout.maximumHeight: root.height * .7
            }

            Button {
                Layout.columnSpan: 2
                id: takePhotoButton
                text: "Take photo"
                icon.name: "camera-photo-symbolic"
                Layout.alignment: Qt.AlignHCenter
                onClicked: {
                    camera.imageCapture.captureToLocation(fLocs.getTempImageLocation());
                    vidOutput.visible = false;
                    visible = false;
                    buttonRow.visible = true;
                }
            }
            RowLayout {
                id: buttonRow
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignHCenter
                visible: false
                Button {
                    id: looksGood
                    text: "Save and continue"
                    Material.accent: Material.Green
                    highlighted: true
                    onClicked: {
                        fileManager.importNewIntraOralImage(PATIENT_FILE_NAME, selectedButton,
                                                            fLocs.getTempImageLocation());
                        candidateImg.visible = false;
                        takePhotoButton.visible =true;
                        vidOutput.visible = true;
                        buttonRow.visible = false;
                    }
                }

                Button {
                    id:takeAgainButton
                    text: "Retake"
                    visible: candidateImg.visible
                    Material.accent: Material.Red
                    highlighted: true
                    onClicked: {
                        candidateImg.visible = false;
                        takePhotoButton.visible =true;
                        vidOutput.visible = true;
                        buttonRow.visible = false;
                    }
                }
            }
        }

    }

    Camera {
        id: camera
        deviceId: QtMultimedia.defaultCamera.deviceId
        captureMode: Camera.CaptureStillImage
        imageCapture {
            onImageCaptured: {
                console.debug(preview)
                candidateImg.source = preview
                candidateImg.visible = true
            }
        }
        Component.onCompleted: {
            camera.start();
        }
    }

    // Window {
    //     x: 0
    //     y:0

    //     VideoOutput {
    //         id: topMonitorView
    //         //source: camera
    //         anchors.fill: parent
    //     }
    // }

    // Window {
    //     x: 0
    //     y:1080
    //     VideoOutput {
    //         id: wallMonitorView
    //         //source: camera
    //         visible: false
    //         anchors.fill: parent
    //     }
    // }

}
