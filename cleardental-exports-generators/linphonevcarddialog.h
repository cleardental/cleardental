#ifndef LINPHONEVCARDDIALOG_H
#define LINPHONEVCARDDIALOG_H

#include <QDialog>

namespace Ui {
class LinphoneVCardDialog;
}

class LinphoneVCardDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LinphoneVCardDialog(QWidget *parent = nullptr);
    ~LinphoneVCardDialog();

public slots:
    void handleStart();

private:
    Ui::LinphoneVCardDialog *ui;
};

#endif // LINPHONEVCARDDIALOG_H
