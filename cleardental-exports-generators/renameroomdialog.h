// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef RENAMEROOMDIALOG_H
#define RENAMEROOMDIALOG_H

#include <QDialog>
#include <QPair>

namespace Ui {
class RenameRoomDialog;
}

class RenameRoomDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RenameRoomDialog(QWidget *parent = nullptr);
    ~RenameRoomDialog();

    QList<QPair<QString,QString>> getNewOldList();


private:
    Ui::RenameRoomDialog *ui;
};

#endif // RENAMEROOMDIALOG_H
