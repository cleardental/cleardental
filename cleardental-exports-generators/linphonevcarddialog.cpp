#include "linphonevcarddialog.h"
#include "ui_linphonevcarddialog.h"

#include <QDir>
#include <QSettings>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QMessageBox>

#include "cdfilelocations.h"
#include "cdpatientmanager.h"

LinphoneVCardDialog::LinphoneVCardDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::LinphoneVCardDialog)
{
    ui->setupUi(this);

    QString defaultFriendloc = QDir::homePath() + "/.local/share/linphone/friends.db";
    ui->friendsDBLineEdit->setText(defaultFriendloc);

    QSettings readPracInfo(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    readPracInfo.beginGroup("GeneralInfo");
    QString defaultPhoneNumber = readPracInfo.value("PhoneNumber","").toString();
    ui->pracPhoneNumberLineEdit->setText(defaultPhoneNumber);

    connect(ui->confirmButtonBox,&QDialogButtonBox::accepted,this,&LinphoneVCardDialog::handleStart);
}

LinphoneVCardDialog::~LinphoneVCardDialog()
{
    delete ui;
}

void LinphoneVCardDialog::handleStart()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(ui->friendsDBLineEdit->text());
    if (!db.open())
    {
        QMessageBox::critical(this,"Could not connect to database","Could not connect to database");
    }
    else
    {
        qDebug() << "Connected to DB";

        QStringList allPatIds = CDPatientManager::getAllPatientIds();
        foreach(QString patID, allPatIds) {
            QSqlQuery insQuery;
            insQuery.prepare("INSERT INTO friends (sip_uri, vCard, friend_list_id, subscribe_policy,send_subscribe,presence_received) "
                              "VALUES (:sip_uri, :vCard, :friend_list_id, :subscribe_policy, :send_subscribe, :presence_received)");

            //First get the displayName

            QSettings patInfoSettings(CDFileLocations::getPersonalFile(patID),QSettings::IniFormat);
            patInfoSettings.beginGroup("Name");

            QString displayName = patInfoSettings.value("FirstName","").toString();
            if(patInfoSettings.value("PreferredName","").toString().trimmed().length() >1) {
                displayName += " \\\"" + patInfoSettings.value("PreferredName","").toString().trimmed() +
                               "\\\"";
            }
            displayName += " " + patInfoSettings.value("LastName","").toString();

            if(displayName.trimmed().length()>2) {
                patInfoSettings.endGroup();
                patInfoSettings.beginGroup("Phones");
                QString patPhoneNumber;
                if(patInfoSettings.value("CellPhone","").toString().trimmed().length() >1) {
                    patPhoneNumber = patInfoSettings.value("CellPhone","").toString().trimmed();
                }
                else {
                    patPhoneNumber = patInfoSettings.value("HomePhone","").toString().trimmed();
                }

                QString realPhoneNumber;
                for (const QChar c : qAsConst(patPhoneNumber)) {
                    if (c.isDigit() || c == '.') {
                        realPhoneNumber.append(c);
                    }
                }

                if(realPhoneNumber.length() > 3) {
                    QString sip_uri = "\"" + displayName + "\" <sip:" + realPhoneNumber + "@" + ui->voipURLLineEdit->text() + ">";
                    QString vCard = "BEGIN:VCARD\nVERSION:4.0\nFN:" + displayName + "\n";
                    vCard+= "IMPP:sip:" + realPhoneNumber + "@" + ui->voipURLLineEdit->text() + "\nEND:VCARD";

                    insQuery.bindValue(":sip_uri",sip_uri);
                    insQuery.bindValue(":vCard",vCard);
                    insQuery.bindValue(":vCard",vCard);
                    insQuery.bindValue(":friend_list_id",1);
                    insQuery.bindValue(":subscribe_policy",1);
                    insQuery.bindValue(":send_subscribe",0);
                    insQuery.bindValue(":presence_received",0);
                    if(insQuery.exec()) {
                        qDebug()<<"Inserted in "<<patID;
                    }
                    else {
                        qDebug()<<"Error with "<<patID;
                        qDebug()<<insQuery.lastError();
                    }
                }
            }
        }
        QMessageBox::information(this,"Done","Done with " + QString::number(allPatIds.length()) + " patients");
        this->close();
    }



}
