// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef GENFAKEPATSDIALOG_H
#define GENFAKEPATSDIALOG_H

#include <QDialog>
#include <QImage>

namespace Ui {
class GenFakePatsDialog;
}

class GenFakePatsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GenFakePatsDialog(QWidget *parent = nullptr);
    ~GenFakePatsDialog();

private slots:
    void handleGenPatients();
    QStringList getPhotosFromDir(QString dir);
    QImage generateFakeRadiograph(int width, int height);

private:
    Ui::GenFakePatsDialog *ui;
};

#endif // GENFAKEPATSDIALOG_H
