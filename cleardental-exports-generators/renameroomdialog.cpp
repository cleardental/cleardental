// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "renameroomdialog.h"
#include "ui_renameroomdialog.h"

RenameRoomDialog::RenameRoomDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RenameRoomDialog)
{
    ui->setupUi(this);
}

RenameRoomDialog::~RenameRoomDialog()
{
    delete ui;
}

QList<QPair<QString, QString> > RenameRoomDialog::getNewOldList()
{
    QList<QPair<QString, QString> > returnMe;

    if(ui->oldName1->text().length() > 1) {
        QPair<QString, QString> addMe;
        addMe.first = ui->oldName1->text();
        addMe.second = ui->newName1->text();
        returnMe.append(addMe);
    }

    if(ui->oldName2->text().length() > 1) {
        QPair<QString, QString> addMe;
        addMe.first = ui->oldName2->text();
        addMe.second = ui->newName2->text();
        returnMe.append(addMe);
    }

    if(ui->oldName3->text().length() > 1) {
        QPair<QString, QString> addMe;
        addMe.first = ui->oldName3->text();
        addMe.second = ui->newName3->text();
        returnMe.append(addMe);
    }

    if(ui->oldName4->text().length() > 1) {
        QPair<QString, QString> addMe;
        addMe.first = ui->oldName4->text();
        addMe.second = ui->newName4->text();
        returnMe.append(addMe);
    }

    if(ui->oldName5->text().length() > 1) {
        QPair<QString, QString> addMe;
        addMe.first = ui->oldName5->text();
        addMe.second = ui->newName5->text();
        returnMe.append(addMe);
    }

    return returnMe;

}

