// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef IMPORTRADIOGRAPHDIALOG_H
#define IMPORTRADIOGRAPHDIALOG_H

#include <QDialog>

namespace Ui {
class ImportRadiographDialog;
}

class ImportRadiographDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImportRadiographDialog(QWidget *parent = nullptr);
    ~ImportRadiographDialog();

    QString getPatID();
    QString getSource();
    QString getRadiographType();
    QDate getDate();

public slots:
    void handlePatSelected();
    void doImport();

private:
    Ui::ImportRadiographDialog *ui;
};

#endif // IMPORTRADIOGRAPHDIALOG_H
