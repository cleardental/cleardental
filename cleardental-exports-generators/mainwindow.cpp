// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QPushButton>
#include <QDebug>
#include <QDir>
#include <QTextStream>
#include <QVariant>
#include <QSettings>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMap>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "importradiographdialog.h"
#include "renameroomdialog.h"
#include "cdpatientmanager.h"

#include "cdfilelocations.h"
#include "genfakepatsdialog.h"
#include "linphonevcarddialog.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->generatePhoneCSVButton,SIGNAL(clicked(bool)),this,SLOT(handleGeneratePhoneCSV()));
    connect(ui->renamesSeatsButton,SIGNAL(clicked(bool)),this,SLOT(handleRenameSeats()));
    connect(ui->importRadiographButton,SIGNAL(clicked(bool)),this,SLOT(handleImportRadiograph()));
    connect(ui->genFakePatsButton,SIGNAL(clicked(bool)),this,SLOT(handleGenFakePats()));
    connect(ui->outputDentalPlansCSVButton,SIGNAL(clicked(bool)),this,SLOT(handleGenerateDentalPlanCSV()));
    connect(ui->convertBillingButton,SIGNAL(clicked(bool)),this,SLOT(handleConvertBilling()));
    connect(ui->linphoneVCardButton,SIGNAL(clicked(bool)),this,SLOT(handleLinphoneDia()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleGeneratePhoneCSV()
{
    QFile writeToMe(QDir::homePath() + "/exportPatPhone.csv");

    writeToMe.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream output(&writeToMe);

    QVariantList fullList = CDPatientManager::getAllPatients();

    //foreach(QVariant pat, CDPatientManager::getAllPatients()) {
    QVariantMap phoneNumbs;
    for(int i=0;i< fullList.length();i++ ) {
        QVariant pat = fullList.at(i);
        QVariantMap patMap = pat.toMap();
        if(patMap.value("CellPhone").toString().length() > 3) {
            QString rawCell = patMap.value("CellPhone").toString();
            QString filteredCell = rawCell.remove(QRegExp("[^a-zA-Z\\d\\s]"));
            if(!phoneNumbs.contains(filteredCell)) {
                output<<",\""<<patMap.value("FirstName").toString()<<" "<<patMap.value("LastName").toString()<<
                    " (Cell)\","<<patMap.value("CellPhone").toString()<<",\"General\",,\"\"\n";
                phoneNumbs.insert(filteredCell,1);
            }
        }
        else if(patMap.value("HomePhone").toString().length() > 3) {
            QString rawHome = patMap.value("HomePhone").toString();
            QString filteredHome = rawHome.remove(QRegExp("[^a-zA-Z\\d\\s]"));
            if(!phoneNumbs.contains(filteredHome)) {
                output<<",\""<<patMap.value("FirstName").toString()<<" "<<patMap.value("LastName").toString()<<
                    " (Home)\","<<patMap.value("HomePhone").toString()<<",\"General\",,\"\"\n";
                phoneNumbs.insert(filteredHome,1);
            }

        }
        if(phoneNumbs.keys().length() > 999) {
            qDebug()<<i;
            i = fullList.length();
        }
    }
    ui->statusbar->showMessage("Exported to " + QDir::homePath() + "/exportPatPhone.csv");
}

void MainWindow::handleRenameSeats()
{
    RenameRoomDialog dia;
    if(dia.exec() == QDialog::Accepted) {
        QList<QPair<QString, QString> > pairs = dia.getNewOldList();
        for(int i=0;i<pairs.length();i++) {
            QPair<QString, QString> pair = pairs.at(i);
            replaceChairNames(pair.first,pair.second);
        }
    }
}

void MainWindow::handleSwitchToNewHardTissue()
{
    QSettings test(QDir::homePath() +"/test.ini",QSettings::IniFormat);
    foreach(QString patID, CDPatientManager::getAllPatientIds()) {
        QSettings hardTissueSet(CDFileLocations::getHardTissueChartFile(patID),QSettings::IniFormat);

        foreach(QString key, hardTissueSet.allKeys()) {
            QString oldValue = hardTissueSet.value(key,"").toString();
            QStringList oldAttrs = oldValue.split("|");
            QJsonArray newAttrs;
            foreach(QString oldAttr, oldAttrs) {
                if(oldAttr.length() > 0) {
                    newAttrs.append(oldAttr.trimmed());
//                    if(oldAttr.contains("Missing")) {
//                        newAttrs.append("Missing");
//                        newAttrs.insert("Missing",true);
//                    }
//                    else if(oldAttr.contains("Amalgam")) {
//                        newAttrs.insert("Amalgam",true);
//                        newAttrs.insert("AmalgamSurfaces",getSurfaces(oldAttr));
//                    }
//                    else if(oldAttr.contains("Decay")) {
//                        newAttrs.insert("Decay",true);
//                        newAttrs.insert("DecaySurfaces",getSurfaces(oldAttr));
//                    }
                    QJsonDocument newDoc;
                    newDoc.setArray(newAttrs);
                    QString newString = newDoc.toJson(QJsonDocument::Compact);
                    //qDebug()<<"Old: "<<oldAttrs<<"\tNew: "<<newString;

                    test.setValue(patID + key,newString);
                }
            }

        }

    }
    test.sync();
    ui->statusbar->showMessage("Done!");
}

void MainWindow::handleImportRadiograph()
{
    ImportRadiographDialog dia;
    dia.exec();
}

void MainWindow::handleGenFakePats() {
    GenFakePatsDialog dia;
    dia.exec();
}

void MainWindow::handleGenerateDentalPlanCSV()
{
    QMap<QString,int> totals;

    foreach(QString patID, CDPatientManager::getAllPatientIds()) {
        QFile checkIns(CDFileLocations::getDentalPlansFile(patID));
        if(checkIns.exists()) {
            QSettings readIns(CDFileLocations::getDentalPlansFile(patID),QSettings::IniFormat);
            readIns.beginGroup("Primary");
            QString insName = readIns.value("Name","").toString();
            if(!totals.contains(insName)) {
                totals[insName] = 1;
            }
            else {
                totals[insName]++;
            }
        }
    }

    QFile writeToMe(QDir::homePath() + "/exportPatDentalPlans.csv");
    writeToMe.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream output(&writeToMe);

    foreach(QString insName, totals.keys()) {
        output << "\"" << insName << "\", " << QString::number(totals[insName]) << "\n";
    }

    ui->statusbar->showMessage("Exported to " + QDir::homePath() + "/exportPatDentalPlans.csv");

}

void MainWindow::handleConvertBilling()
{
    QTime perfCounter = QTime::currentTime();
    int procedureCounter=0;
    foreach(QString patID, CDPatientManager::getAllPatientIds()) {
        QFile checkIns(CDFileLocations::getLedgerFile(patID));
        if(checkIns.exists()) {
            checkIns.open(QIODevice::ReadOnly | QIODevice::Text);
            QJsonDocument patLedger = QJsonDocument::fromJson(checkIns.readAll());
            checkIns.close();
            QJsonArray ledgerList = patLedger.array();
            QJsonArray newLedgerArray;
            foreach(QJsonValue ledgerItem, ledgerList) {
                QJsonObject ledgerItemObject = ledgerItem.toObject();
                if(ledgerItemObject.value("Type").toString() == "Procedure") {
                    procedureCounter++;
                    QJsonObject procedureObject = ledgerItemObject.value("Procedure").toObject();

                    QJsonObject newProcedureObject;
                    newProcedureObject.insert("Procedure",procedureObject);
                    newProcedureObject.insert("ProcedureDate",ledgerItemObject.value("Date"));

                    QJsonArray emptyArray;
                    newProcedureObject.insert("Payments",emptyArray);
                    newProcedureObject.insert("ClaimStatus","N/A");
                    newProcedureObject.insert("ProviderUsername","tshah"); //huge bug for cash patients!
                    newLedgerArray.append(newProcedureObject);
                }
            }
            QJsonDocument newLedger;
            newLedger.setArray(newLedgerArray);
            QFile newLedgerFile(CDFileLocations::getFullBillingFile(patID));
            if(!newLedgerFile.exists()) {
                newLedgerFile.open(QIODevice::WriteOnly | QIODevice::Text);
                newLedgerFile.write(newLedger.toJson());
                newLedgerFile.close();
            }
        }
    }

    ui->statusbar->showMessage("Went though " + QString::number(procedureCounter) + " procedures in " +
                               QString::number(perfCounter.msecsTo(QTime::currentTime())) + " milliseconds");

}

void MainWindow::handleLinphoneDia()
{
    LinphoneVCardDialog dia;
    dia.exec();
}

void MainWindow::replaceChairNames(QString oldName, QString newName)
{
    QDir apptDir(CDFileLocations::getLocalAppointmentsDirectory());
    foreach(QString month, apptDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
        QDir monthDir(CDFileLocations::getLocalAppointmentsDirectory() + "/" + month + "/");
        foreach(QString day, monthDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
            QDir dayDir(CDFileLocations::getLocalAppointmentsDirectory() + "/" + month + "/" + day + "/");
            foreach(QString year, dayDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
                QDir yearDir(CDFileLocations::getLocalAppointmentsDirectory() + "/" + month + "/" + day + "/" + year);
                foreach(QString filename, yearDir.entryList(QDir::Files | QDir::NoDotAndDotDot)) {
                    QSettings apptFile(CDFileLocations::getLocalAppointmentsDirectory() + "/" + month + "/" + day +
                                       "/" + year + "/" + filename,QSettings::IniFormat);

                    QString originalName = apptFile.value("Chair").toString();
                    if(originalName.compare(oldName,Qt::CaseInsensitive) == 0) {
                        apptFile.setValue("Chair",newName);
                    }
                }
            }
        }
    }
}

QString MainWindow::getSurfaces(QString input)
{
    QString rightSide = input.split(":")[1].trimmed();
    return rightSide;
}

