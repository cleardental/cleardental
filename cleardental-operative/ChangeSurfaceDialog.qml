// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: changeSurfaceDialog
    property bool isPost: comFuns.isPost(rootWin.procedureObj["Tooth"])

    CDTextFileManager {
        id: textFileMan
    }

    CDFileLocations {
        id: fileLocs
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Change surfaces"
                }
                RowLayout {
                    CDDescLabel {
                        text: "New Surfaces"
                    }
                    CDSurfaceRow {
                        id: surfRow

                        Component.onCompleted: {
                            workingToothNumb = rootWin.procedureObj["Tooth"]
                            parseSurfaces(rootWin.procedureObj["Surfaces"]);
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    changeSurfaceDialog.reject();
                }
            }
            Label {Layout.fillWidth: true;}
            CDAddButton {
                text: "Make Changes"
                icon.name: "document-edit"
                onClicked: {
                    var oldTxObj = rootWin.procedureObj

                    var newSurfaces = surfRow.getSurfaces();
                    var newSurfaceCount = newSurfaces.length;
                    var newDCode = oldTxObj["DCode"];
                    if(changeSurfaceDialog.isPost) {
                        switch(newSurfaceCount) {
                        default:
                        case 1:
                            newDCode = "D2391";
                            break;
                        case 2:
                            newDCode = "D2392";
                            break;
                        case 3:
                            newDCode = "D2393";
                            break;
                        case 4:
                        case 5:
                            newDCode = "D2394";
                            break;
                        }
                    }
                    else {
                        switch(newSurfaceCount) {
                        default:
                        case 1:
                            newDCode = "D2330";
                            break;
                        case 2:
                            newDCode = "D2331";
                            break;
                        case 3:
                            newDCode = "D2332";
                            break;
                        case 4:
                        case 5:
                            newDCode = "D2335";
                            break;
                        }
                    }

                    var jsonTxPlans = textFileMan.readFile(fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
                    var txPlansObj = ({});
                    txPlansObj = JSON.parse(jsonTxPlans);
                    var txPlanNames = Object.keys(txPlansObj);
                    for(var txPlanNameI =0; txPlanNameI< txPlanNames.length; txPlanNameI++) {
                        var txPlan = txPlansObj[txPlanNames[txPlanNameI]];
                        var phaseNames = Object.keys(txPlan);
                        for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                            var phaseList = txPlan[phaseNames[phasNameI]];
                            for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                                if((phaseList[phaseItemI]["Tooth"] == oldTxObj["Tooth"]) && // @disable-check M126
                                        ( phaseList[phaseItemI]["DCode"] == oldTxObj["DCode"])) {//@disable-check M126
                                    phaseList[phaseItemI]["ProcedureName"] = comFuns.getNameForCode(newDCode);
                                    phaseList[phaseItemI]["DCode"] = newDCode;
                                    phaseList[phaseItemI]["Surfaces"] = newSurfaces;
                                    phaseList[phaseItemI]["BasePrice"] = comFuns.getPriceForCode(newDCode);
                                    rootWin.procedureObj = phaseList[phaseItemI];
                                    rootWin.title = comFuns.makeTxItemString(rootWin.procedureObj) +
                                            " [ID: " + PATIENT_FILE_NAME + "]";
                                }
                            }
                        }
                    }

                    jsonTxPlans = JSON.stringify(txPlansObj, null, '\t')
                    textFileMan.saveFile(fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),jsonTxPlans);

                    changeSurfaceDialog.accept();
                }
            }
        }
    }

}
