// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

//Children: https://www.aapd.org/globalassets/media/policies_guidelines/r_usefulmeds.pdf

ComboBox {
    id: drugBox
    editable: true
    model: drugModel
    
    ListModel {
        id: drugModel
        ListElement {
            name: "Antibiotics"
            clickable: false
        }
        ListElement {
            name: "Amoxicillin"
            reason: "For infection"
            strengthModel: "250 mg;500 mg"
            strengthIndex: 1
            scheduleIndex: 3 //QID
            amount: 1
            form: 0
            days: 7
            clickable: true
            pediatric_QID_Low: 20
            pediatric_QID_High: 40
            pediatric_QID_Max: 500
            pediatric_BID_Low: 25
            pediatric_BID_High: 45
            pediatric_BID_Max: 875

        }
        ListElement {
            name: "Erythromycin"
            reason: "For infection"
            strengthModel: "250 mg;333 mg;500 mg"
            strengthIndex: 0
            scheduleIndex: 3 //QID
            amount: 1
            form: 0
            days: 7
            clickable: true
        }
        ListElement {
            name: "Clindamycin"
            reason: "For infection"
            strengthModel: "75 mg;150 mg;300 mg;600 mg"
            strengthIndex: 1
            scheduleIndex: 3 //QID
            amount: 1
            days: 7
            form: 0
            clickable: true
            pediatric_QID_Low: 10
            pediatric_QID_High: 25
            pediatric_QID_Max: 450
        }
        ListElement {
            name: "Azithromycin"
            reason: "For infection"
            strengthModel: "500 mg;1000 mg;2000 mg"
            strengthIndex: 1
            scheduleIndex: 0 //QD
            amount: 1
            days: 7
            form: 0
            clickable: true
            pediatric_QD_Low: 10
            pediatric_QD_High: 30
            pediatric_QD_Max: 500
        }
        ListElement {
            name: "Doxycycline"
            reason: "For infection"
            strengthModel: "25 mg;50 mg;100 mg"
            strengthIndex: 2
            scheduleIndex: 1 //BID
            amount: 1
            days: 7
            form: 0
            clickable: true
            pediatric_BID_Low: 2
            pediatric_BID_High: 2.2
            pediatric_BID_Max: 100
        }
        ListElement {
            name: "Amoxicillin/Clavulanate"
            reason: "For infection"
            strengthModel: " 200 mg/28.5 mg;400 mg/57 mg;125 mg/31.25 mg;250 mg/62.5 mg;250 mg/125 mg;500 mg/125 mg;875 mg/125 mg;200 mg/28.5 mg"
            strengthIndex: 5
            scheduleIndex: 1 //BID
            amount: 1
            days: 7
            form: 0
            clickable: true
            pediatric_BID_Low: 25
            pediatric_BID_High: 45
            pediatric_BID_Max: 875
        }
        ListElement {
            name: "Rinse"
            clickable: false
        }
        ListElement {
            name: "Chlorhexidine Gluconate 0.12%"
            strengthModel: "15 ml;30 ml"
            strengthIndex: 0
            scheduleIndex: 0 //QD
            amount: 1
            days: 5
            form: 1
            clickable: true
        }
        ListElement {
            name: "Nystatin"
            strengthModel: "100,000 units/g"
            strengthIndex: 0
            scheduleIndex: 1 //BID
            days: 14
            form: 2
            amount: 1
            clickable: true
        }
        ListElement {
            name: "Fluoride"
            clickable: false
        }
        ListElement {
            name: "Fluoride Tablet"
            reason: "Caries Prevention"
            strengthModel: "0.25mg;0.50mg;1.00mg"
            strengthIndex: 1
            scheduleIndex: 0 //QD
            days: 183
            amount: 1
            form: 0
            clickable: true
        }
        ListElement {
            name: "Fluoride Drop"
            reason: "Caries Prevention"
            strengthModel: "0.25mg/mL;0.50mg/mL;1.00mg/mL"
            strengthIndex: 1
            scheduleIndex: 0 //QD
            amount: 1
            days: 183
            form: 1
            clickable: true
        }
        ListElement {
            name: "Anxiolytic"
            clickable: false
        }
        ListElement {
            name: "Diazepam"
            reason: "For stress"
            strengthModel: "5 mg;2 mg;10 mg;2.5 mg;20 mg"
            strengthIndex: 1
            amount: 5 //good for the next 5 appointments
            scheduleIndex: 9 //PRN
            days:0
            form: 0
            clickable: true
        }
        ListElement {
            name: "Toothpaste"
            clickable: false
        }
        ListElement {
            name: "Prevident"
            reason: "Caries Prevention"
            strengthModel: "5000 ppm Booster Plus"
            strengthIndex: 1
            scheduleIndex: 0 //QD
            amount: 1
            form: 2
            days: 0
            clickable: true
        }
        ListElement {
            name: "Saliva production"
            clickable: false
        }
        ListElement {
            name: "Pilocarpine"
            reason: "To Combat Xerostomia"
            strengthModel: "5 mg;7.5 mg"
            strengthIndex: 0
            amount: 1
            form: 0
            scheduleIndex: 3 //QID
            days: 25 //normally bottles of 100
            clickable: true
        }
        ListElement {
            name: "Pain management"
            clickable: false
        }
        ListElement {
            name: "Methylprednisolone"
            reason: "For pain / soreness"
            clickable: true
            strengthModel: "2 mg;4 mg;8 mg;16 mg;32 mg"
            strengthIndex: 0
            amount: 1
            form: 0
            scheduleIndex: 0 //QID
            days: 5
        }
        ListElement {
            name: "Methylprednisolone (medrol dose pack)"
            reason: "For pain / soreness"
            clickable: true
            strengthModel: "As directed"
            strengthIndex: 0
            amount: 1
            form: 0
            scheduleIndex: 0 //QID
            days: 5
        }
        ListElement {
            name: "Vicodin"
            reason: "For pain"
            clickable: true
            strengthModel: "5 mg/300 mg;7.5 mg/300 mg;10 mg/300 mg"
            strengthIndex: 0
            amount: 1
            form: 0
            scheduleIndex: 3 //QID
            days: 5
        }

    }
    
    delegate: ItemDelegate {
        width: drugBox.width
        enabled: clickable
        contentItem: Label {
            text: name
            font.bold: !clickable
            verticalAlignment: Text.AlignVCenter

            MenuSeparator {
                width: parent.width
                anchors.bottom: parent.bottom
                visible: !clickable
            }
        }
        highlighted: drugBox.highlightedIndex === index
    }
    textRole: "name"
    
    onCurrentIndexChanged: {
        var getItem = drugModel.get(currentIndex);
        if(getItem.clickable) {
            strBox.model = getItem.strengthModel.split(";");
            strBox.currentIndex = getItem.strengthIndex;
            scheduleBox.currentIndex = getItem.scheduleIndex;
            rxDayBox.value = getItem.days;
            amountBox.value = getItem.amount;
            formBox.currentIndex = getItem.form;

            if(getItem.reason) {
                reasonfield.text = getItem.reason;
            } else {
                reasonfield.text = "";
            }

            if(getItem.instruction) {
                instructField.text = getItem.instruction;
            }
            else {
                instructField.text = "";
            }
        }

        kidWarningLabel.updateText();

    }

    Component.onCompleted: {
        currentIndex=1;
    }
}
