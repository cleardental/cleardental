// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {

    ColumnLayout {
        id: dayReport
        anchors.fill: parent

//        property date startDay: new Date(1970,1,1);
//        property date endDay: new Date(3000,1,1);
        property date startDay: new Date(2000,1,1);
        property date endDay: new Date();

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Overall Report"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Patients Seen"
                }

                Label {
                    text: reportMaker.patientsSeen(dayReport.startDay,dayReport.endDay)
                }

                CDDescLabel {
                    text: "Patients No-showed"
                }

                Label {
                    text: reportMaker.patientsNoShowed(dayReport.startDay,dayReport.endDay)

                }

                CDDescLabel {
                    text: "Total Production"
                }

                Label {
                    text: "$" + (reportMaker.totalProduction(dayReport.startDay,dayReport.endDay)/100)

                }

                CDDescLabel {
                    text: "Total Collections"
                }

                Label {
                    text: "$" + (reportMaker.totalCollections(dayReport.startDay,dayReport.endDay)/100)

                }

                CDDescLabel {
                    text: "Total Writeoffs"
                }

                Label {
                    text: "$" + (reportMaker.totalWriteoffs(dayReport.startDay,dayReport.endDay)/100)

                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Orange

            ColumnLayout {
                CDHeaderLabel {
                    text: "Provider Report"
                }

                CDDoctorListManager {
                    id: docMan
                }

                Repeater {
                    model: docMan.getListOfProviderFiles().length
                    RowLayout {
                        Settings {
                            id: docInfo
                            fileName: docMan.getListOfProviderFiles()[index]
                        }
                        CDDescLabel {
                            id: desLabel
                            Component.onCompleted: {
                                text =  docInfo.value("UnixID")
                            }
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalProductionLabel
                            text: "Production: $" + (reportMaker.totalProduction(dayReport.startDay,dayReport.endDay,
                                                                          desLabel.text) /100.0)
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalCollectionLabel
                            text: "Collection: $" + (reportMaker.totalCollections(dayReport.startDay,dayReport.endDay,
                                                                          desLabel.text) /100.0)
                        }
                    }
                }
            }
        }
    }


}

