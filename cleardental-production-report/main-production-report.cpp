// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cdreporter.h"
#include "cddoctorlistmanager.h"
#include "cdlocationmanager.h"
#include "cdschedulemanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Production-Report");

    QApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDReporter>("dental.clear", 1, 0, "CDReporter");
    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");
    qmlRegisterType<CDLocationManager>("dental.clear", 1, 0,"CDClinicLocationManager");



    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/MainProductionReport.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
