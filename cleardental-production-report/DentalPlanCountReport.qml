// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

import QtCharts 2.15

CDTransparentPage {
    RowLayout {
        id: dentalPlanReport
        anchors.fill: parent


        ChartView {
            id: chart
            title: "Dental Plans"
            antialiasing: true
            legend.visible:  false
            Layout.fillWidth: true
            Layout.fillHeight: true
            backgroundColor: "transparent"
            titleFont.pointSize: 24
            animationOptions: ChartView.AllAnimations
            //theme: ChartView.ChartThemeBlueIcy



            PieSeries {
                id: pieSeries
            }

            Component.onCompleted: {
                var dentalPlanCounts = reportMaker.getDentalPlanCount();
                var dentalPlanNames = Object.keys(dentalPlanCounts);
                var otherSliceCount =0;
                for(var i=0;i<dentalPlanNames.length;i++) {
                    var dentalPlanName = dentalPlanNames[i];
                    var dentalPlanCount = dentalPlanCounts[dentalPlanName];
                    if(dentalPlanCount > 10) {
                        var slice = pieSeries.append(dentalPlanName + " ("  + dentalPlanCount + ")" , dentalPlanCount);
                        slice.labelVisible = true;

                    } else {
                        otherSliceCount += dentalPlanCount;
                    }
                }
                slice = pieSeries.append("All others (" + otherSliceCount + ")" , otherSliceCount);
                slice.labelVisible = true;
            }
        }

    }

}
