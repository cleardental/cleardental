// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: procedureCountReport
        anchors.fill: parent

        property int millisecondsPerDay: 24 * 60 * 60 * 1000;

        property var reportLabels: []

        function updateReport() {
            var reportCountObj = reportMaker.getProcedureCounts(fromDayPicker.myDate,toDayPicker.myDate);
            //format: [DCode] -> [count,name,collection]

            reportLabels = [];

            var dCodes = Object.keys(reportCountObj);

            for(var i=0;i<dCodes.length;i++) {
                var dCode = dCodes[i];
                var procedureCount = reportCountObj[dCode][0];
                var procedureName = reportCountObj[dCode][1];
                var procedureCollections = reportCountObj[dCode][2] / 100.0;

                reportLabels.push(procedureName + " (" + dCode + ")");
                reportLabels.push(procedureCount + " ($" + procedureCollections+")");
            }
            console.debug(reportLabels);
            reportRepeater.model = reportLabels.length
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                columns: 2
                columnSpacing: 10
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Select Range"
                }
                CDDescLabel {
                    text: "From"
                }
                CDDescLabel {
                    text: "To"
                }

                CDCalendarDatePicker {
                    id: fromDayPicker
                    onMyDateChanged: procedureCountReport.updateReport();
                    Component.onCompleted: {
                        var today = new Date();
                        var setDate = new Date(today.getFullYear(),today.getMonth()-1,today.getDate());
                        myDate = setDate;
                    }
                }

                CDCalendarDatePicker {
                    id: toDayPicker
                    onMyDateChanged: procedureCountReport.updateReport();
                }
                Label {
                    id: infoLabel
                    Layout.columnSpan: 2
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue
            Layout.minimumWidth: countGrid.width + 25
            Layout.minimumHeight: 500

            Flickable {
                id: flickResults
                anchors.fill: parent
                width: countGrid.width
                height: countGrid.preferredHeight
                contentWidth: countGrid.width
                contentHeight: countGrid.height
                clip: true

                ScrollBar.vertical: ScrollBar{}

                GridLayout {
                    id: countGrid
                    columns: 2
                    CDHeaderLabel {
                        text: "Procedure Count"
                        Layout.columnSpan: 2
                    }

                    Repeater {
                        id: reportRepeater
                        model: procedureCountReport.reportLabels.length
                        Label {
                            text: procedureCountReport.reportLabels[index];
                            font.bold: (index%2) == 0
                        }
                    }
                }

            }
        }


    }

}
