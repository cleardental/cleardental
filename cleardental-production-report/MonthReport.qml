// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {

    ColumnLayout {
        id: dayReport
        anchors.fill: parent
        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                id: monthPicker
                columns: 2

                property date startDay
                property date endDay

                function updateDays() {
                    startDay = new Date(selectedYearBox.currentIndex + 2000, selectedMonthBox.currentIndex,1);
                    endDay = new Date(selectedYearBox.currentIndex + 2000, selectedMonthBox.currentIndex+1,0);
                }


                CDHeaderLabel {
                    text: "Select Month and Year"
                    Layout.columnSpan: 2
                }

                ComboBox {
                    id: selectedMonthBox
                    model: ["January", "February", "March", "April", "May",
                        "June", "July", "August", "September", "October", "November",
                        "December"]

                    Component.onCompleted: {
                        var today = new Date()
                        currentIndex = today.getMonth();
                    }

                    onCurrentIndexChanged: monthPicker.updateDays();
                }

                ComboBox {
                    id: selectedYearBox

                    Component.onCompleted: {
                        var makeModel = [];
                        for(var i=2000;i<2050;i++) {
                            makeModel.push(i)
                        }
                        model = makeModel;

                        var today = new Date()
                        currentIndex = today.getFullYear() - 2000;
                    }

                    onCurrentIndexChanged: monthPicker.updateDays();
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Overall Report"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Patients Seen"
                }

                Label {
                    text: reportMaker.patientsSeen(monthPicker.startDay,monthPicker.endDay)

                }

                CDDescLabel {
                    text: "Patients No-showed"
                }

                Label {
                    text: reportMaker.patientsNoShowed(monthPicker.startDay,monthPicker.endDay)

                }

                CDDescLabel {
                    text: "Total Production"
                }

                Label {
                    text: "$" + Number(reportMaker.totalProduction(monthPicker.startDay,monthPicker.endDay)/100).toLocaleCurrencyString()

                }

                CDDescLabel {
                    text: "Total Collections"
                }

                Label {
                    text: "$" + Number(reportMaker.totalCollections(monthPicker.startDay,monthPicker.endDay)/100).toLocaleCurrencyString()

                }

                CDDescLabel {
                    text: "Total Writeoffs"
                }

                Label {
                    text: "$" + Number(reportMaker.totalWriteoffs(monthPicker.startDay,monthPicker.endDay)/100).toLocaleCurrencyString()

                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Orange

            ColumnLayout {
                CDHeaderLabel {
                    text: "Provider Report"
                }

                CDDoctorListManager {
                    id: docMan
                }

                Repeater {

                    model: docMan.getListOfProviderFiles().length

                    RowLayout {
                        Settings {
                            id: docInfo
                            fileName: docMan.getListOfProviderFiles()[index]
                        }
                        CDDescLabel {
                            id: desLabel
                            Component.onCompleted: {
                                text =  docInfo.value("UnixID")
                            }
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalProductionLabel
                            text: "Production: $"+ Number(reportMaker.totalProduction(monthPicker.startDay,monthPicker.endDay,
                                                                          desLabel.text) /100.0).toLocaleCurrencyString()
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalCollectionLabel
                            text: "Collection: $"+ Number(reportMaker.totalCollections(monthPicker.startDay,monthPicker.endDay,
                                                                          desLabel.text) /100.0).toLocaleCurrencyString()
                        }
                    }
                }
            }
        }
    }


}

