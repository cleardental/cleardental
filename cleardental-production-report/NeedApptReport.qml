// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ListView {
    id: balanceListView

    CDToolLauncher { id: toolLauncher }

    Settings {
        id: iniReader
    }

    Settings {
        id :apptReader
    }

    CDFileLocations {
        id: fLocs
    }

    function getPhoneNumber(patID) {
        iniReader.fileName = fLocs.getPersonalFile(patID);
        iniReader.category = "Phones";
        var phoneNumber = iniReader.value("CellPhone");
        if (phoneNumber.length > 0) {
            return phoneNumber;
        } else {
            phoneNumber = iniReader.value("HomePhone","No Phone Number on File");
            return phoneNumber;
        }
    }

    property int rowWidth: (rootWin.width-100) / 6
    ScrollBar.vertical: ScrollBar { }
    headerPositioning: ListView.OverlayHeader
    clip: true

    property var needsApptList: reportMaker.needsAppt()
    model: needsApptList.length
    header: Pane {
        z: 1024
        background: Rectangle {
            color: Material.color(Material.LightBlue)
            anchors.fill: parent
            radius: 10
            opacity: .75
        }

        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth*2
                text: "Patient Name"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Previous Appointment"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Phone Number"
            }

            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth*2
                text: "Last Appointment Status"
            }
            Label {
                Layout.minimumWidth: balanceListView.rowWidth
                Layout.maximumWidth: balanceListView.rowWidth
            }
        }
    }

    delegate: Component {
        Pane {
            background: Rectangle {
                color: "white"
                opacity: .25

                MouseArea {
                    anchors.fill: parent
                    onContainsMouseChanged: {
                        if(containsMouse) {
                            parent.opacity = .75
                        }
                        else {
                            parent.opacity = .25
                        }
                    }
                    hoverEnabled: true
                }

                Behavior on opacity {
                    PropertyAnimation {
                        duration: 150
                    }
                }

            }
            RowLayout {
                Label {
                    text: needsApptList[index]
                    Layout.minimumWidth: balanceListView.rowWidth*2
                    Layout.maximumWidth: balanceListView.rowWidth*2
                }
                Label {
                    text: reportMaker.getLastAppt(needsApptList[index])
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                }
//                Label {
//                    text: "$" + reportMaker.getPayments(needsApptList[index])
//                    Layout.minimumWidth: balanceListView.rowWidth
//                    Layout.maximumWidth: balanceListView.rowWidth
//                }
//                Label {
//                    text: "$" + -1*reportMaker.getCharges(needsApptList[index])
//                    Layout.minimumWidth: balanceListView.rowWidth
//                    Layout.maximumWidth: balanceListView.rowWidth
//                }
                CDCopyLabel {
                    text: getPhoneNumber(needsApptList[index])
                    Layout.minimumWidth: balanceListView.rowWidth*0.75
                    Layout.maximumWidth: balanceListView.rowWidth*0.75
                }
                Label {
                    Layout.minimumWidth: balanceListView.rowWidth*0.25
                    Layout.maximumWidth: balanceListView.rowWidth*0.25
                }

                Label {
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                    text: {
                        apptReader.fileName = reportMaker.getLastApptFile(needsApptList[index]);
                        apptReader.sync();
                        return apptReader.value("Status","");
                    }
                }
                Button {
                    Layout.minimumWidth: balanceListView.rowWidth/2
                    Layout.maximumWidth: balanceListView.rowWidth/2
                    text: "Schedule"
                    onClicked: toolLauncher.launchTool(CDToolLauncher.SchedulePatient,needsApptList[index]);
                }
                Button {
                    Layout.minimumWidth: balanceListView.rowWidth /2
                    Layout.maximumWidth: balanceListView.rowWidth /2
                    text: "Case Notes"
                    onClicked: toolLauncher.launchTool(CDToolLauncher.ReviewCaseNotes,needsApptList[index]);
                }
            }
        }
    }
}
