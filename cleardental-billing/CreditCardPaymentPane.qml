// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

Pane {
    id: creditCardPaymentPane

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    ColumnLayout {
        GridLayout {
            columns: 2
            Label {
                text: "Amount billed out ($)"
                font.bold: true
            }
            TextField {
                id: amountGotten
                Layout.minimumWidth: 150
            }

            Label {
                text: "Receipt / Confirmation Number"
                font.bold: true
            }
            TextField {
                id: confirmNumber
                Layout.minimumWidth: 150
            }
        }
        CDAddButton {
            text: "Add credit card payment"
            Layout.alignment: Qt.AlignRight
            onClicked: {
                var addMe = {
                    Date : new Date(),
                    CCAmount : amountGotten.text,
                    Type : "Credit Card Payment",
                    Notes : "Confirmation Number: " + confirmNumber.text,
                }

                rootWin.ledgerObj.push(addMe);
                rootWin.saveLedger();

                addPaymentDialog.close();
            }
        }
    }
}
