// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: deleteLedgerItemDia

    ColumnLayout {
        CDTranslucentPane {
            backMaterialColor: Material.Pink
            ColumnLayout {
                CDHeaderLabel {
                    text: "Are you sure you want to delete this entry? "
                }
            }
        }
        RowLayout {
            CDCancelButton {
                text: "No, keep the entry"
                onClicked: {
                    deleteLedgerItemDia.reject()
                }
            }
            Label {
                Layout.fillWidth: true
            }

            CDDeleteButton {
                text: "Yes, delete the entry"
                onClicked: {
                    deleteLedgerItemDia.accept()
                }
            }
        }
    }
}
