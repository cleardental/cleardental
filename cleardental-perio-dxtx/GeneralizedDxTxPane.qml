// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: genDxPane
    backMaterialColor: Material.BlueGrey

    property bool genSCRP: fullMouthSCRP.checked

    function generateDxString() {
        var returnMe = "";
        if(genNormal.checked) {
            returnMe = genNormal.text;
        }
        else if(genNormalRedPerio.checked) {
            returnMe = genNormalRedPerio.text;
        }
        else if(genGinv.checked) {
            returnMe = gingDxPane.gingDx + " " + genGinv.text;
        }
        else if(genPerio.checked) {
            returnMe = genPerio.text + ": " +
                    perioStagePane.perioStage + ", " +
                    perioGradePane.perioGrade;
        }
        else if(otherGenDx.checked) {
            returnMe = otherDx.text;
        }

        return returnMe;
    }

    function generateTxPlanObj() {
        var returnMe = [];
        var addMe = ({});


        if(genProphy.checked) {
            addMe["ProcedureName"] = comFuns.getNameForCode("D1110"); //Prophy
            addMe["DCode"] = "D1110";
            addMe["BasePrice"] = pricesSettings.value("D1110",0);
            returnMe = [addMe];
        }
        else if(fullDebride.checked) {
            addMe["ProcedureName"] =  comFuns.getNameForCode("D4355"); //"Full Mouth Debridement"
            addMe["DCode"] = "D4355";
            addMe["BasePrice"] = pricesSettings.value("D4355",0);
            returnMe = [addMe];
        }
        else if(fullMouthSCRP) {
            var D4341Str =  comFuns.getNameForCode("D4341"); //SCRP (4+ Teeth)

            var addMeUL =  ({});
            addMeUL["Location"] = "UL";
            addMeUL["ProcedureName"] = D4341Str;
            addMeUL["DCode"] = "D4341";
            addMeUL["BasePrice"] = pricesSettings.value("D4341",0);

            var addMeUR =  ({});
            addMeUR["Location"] = "UR";
            addMeUR["ProcedureName"] = D4341Str;
            addMeUR["DCode"] = "D4341";
            addMeUR["BasePrice"] = pricesSettings.value("D4341",0);

            var addMeLL =  ({});
            addMeLL["Location"] = "LL";
            addMeLL["ProcedureName"] = D4341Str;
            addMeLL["DCode"] = "D4341";
            addMeLL["BasePrice"] = pricesSettings.value("D4341",0);

            var addMeLR =  ({});
            addMeLR["Location"] = "LR";
            addMeLR["ProcedureName"] = D4341Str;
            addMeLR["DCode"] = "D4341";
            addMeLR["BasePrice"] = pricesSettings.value("D4341",0);

            returnMe = [addMeUL,addMeUR,addMeLL,addMeLR];
        }

        return returnMe;

    }

    ColumnLayout {
        id: radioDxs
        Label {
            text: "General Diagnosis"
            font.pointSize: 24
            font.underline: true
        }
        Row {
            //Layout.minimumHeight: 100
            RadioButton {
                id: genNormal
                text: "Normal"
                height: perioStagePane.height
            }
            RadioButton {
                id: genNormalRedPerio
                text: "Normal on a Reduced Periodontium"
                height: perioStagePane.height
            }
            RadioButton {
                id: genGinv
                text: "Gingivitis"
                checked: true
                height: perioStagePane.height
            }

            GingDxPane {
                id: gingDxPane

                visible: opacity > 0
                opacity: genGinv.checked ? 1:0

                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }

            RadioButton {
                id: genPerio
                text: "Periodontitis"
                height: perioStagePane.height
            }

            PerioStagePane {
                id: perioStagePane
                visible: opacity > 0
                opacity: genPerio.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }

            PerioGradePane {
                id: perioGradePane
                visible: opacity > 0
                opacity: genPerio.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }

            RadioButton {
                id: otherGenDx
                text: "Other"
                height: perioStagePane.height
            }

            TextField {
                id: otherDx
                visible: opacity > 0
                opacity: otherGenDx.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
                width: 400
                height: perioStagePane.height
            }

            move: Transition {
                NumberAnimation {
                    properties: "x"
                    duration: 200
                }
            }
        }

        Label {
            text: "General Treatment"
            font.pointSize: 24
            font.underline: true
        }

        RowLayout {
            RadioButton {
                id: genProphy
                text: "Prophy"
                checked: true
            }
            RadioButton {
                id: fullDebride
                text: "Full Debridement"
            }
            RadioButton {
                id: fullMouthSCRP
                text: "Full Mouth SCRP"
            }
            CheckBox {
                id: referPerio
                text: "Referral to Periodontist"
            }
        }
    }


}
