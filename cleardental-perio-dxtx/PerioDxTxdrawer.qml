// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

Drawer {
    id: perioDxTxdrawer
    height: rootWin.height
    width: rootWin.width /3

    background: Rectangle {
        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 0.5; color: "white" }
            GradientStop { position: 1.0; color: "transparent" }
        }
        anchors.fill: parent
    }

    CDImageWinDia {
        id: perioDia
        imgSource: "qrc:/guides/periodontitis-NIH.jpg"
    }

    ColumnLayout {
        anchors.left:  parent.left
        anchors.right: parent.right
        anchors.margins: 10
        CDCancelButton {
            Layout.fillWidth: true
            icon.name: ""
            text: "Show Periodontal pocket diagram"
            onClicked: perioDia.open();
        }
    }
}
