// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Periodontal DxTX [ID: " + PATIENT_FILE_NAME + "]")
    property var allPerioDepths: ({}); //[Date][Tooth][Buccal/Lingual][Pocket/Recession][values]
    property var sortedDates: []
    property bool loadedPastValues: false
    property int selectedDateIndex: dateBox.currentIndex


    function loadPastValues() {
//        var perioHistory = gitReader.getFileHistory(fLocs.getPerioChartFile(PATIENT_FILE_NAME));
//        for(var i=0;i<perioHistory.length;i++) {
//            var chartDate = perioHistory[i]["Date"];
//            sortedDates.push({obj: new Date(perioHistory[i]["Date"]),str: perioHistory[i]["Date"]});
//            allPerioDepths[chartDate] = ({});
//            chartHistorySettings.fileName = perioHistory[i]["File"];
//            for(var toothI =1;toothI<=32;toothI++) {
//                allPerioDepths[chartDate][toothI] = ({});
//                chartHistorySettings.category = toothI;
//                allPerioDepths[chartDate][toothI]["Buccal"] = ({});
//                allPerioDepths[chartDate][toothI]["Buccal"]["Pockets"] =
//                        chartHistorySettings.value("BuccalPockets","_,_,_").split(",");
//                allPerioDepths[chartDate][toothI]["Buccal"]["Recession"] =
//                        chartHistorySettings.value("BuccalRecession","_,_,_").split(",");
//                allPerioDepths[chartDate][toothI]["Lingual"] = ({});
//                allPerioDepths[chartDate][toothI]["Lingual"]["Pockets"] =
//                        chartHistorySettings.value("LingualPockets","_,_,_").split(",");
//                allPerioDepths[chartDate][toothI]["Lingual"]["Recession"] =
//                        chartHistorySettings.value("LingualRecession","_,_,_").split(",");

//            }
//        }

//        sortedDates = sortedDates.sort((a, b) => b.obj - a.obj); //latest is first
//        loadedPastValues = true;

//        dateBox.model = Object.keys(allPerioDepths);
    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: perioChartSettings
        fileName: fLocs.getPerioChartFile(PATIENT_FILE_NAME)
        category: "Diagnosis"
    }

    Settings {
        id: reviewSettings
        fileName: fLocs.getReviewsFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: pricesSettings
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Prices"
    }

    Settings {
        id: chartHistorySettings
    }

    CDTextFileManager {
        id: textMan
    }

    CDGitManager {
        id: gitMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    header: CDPatientToolBar {
        headerText: "Periodontal Dx and Tx Planning:"

        ToolButton {
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: perioDxTxdrawer.open();
        }
    }

    PerioDxTxdrawer {
        id: perioDxTxdrawer
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: saveButton.top
        anchors.margins: 10


        RowLayout {
            QuadDxTxPane {
                id: ulDxTxPane
                quadName: "ur"
                Layout.fillHeight: true
                Layout.fillWidth: true
                enableTreatment: !genDxPane.genSCRP
            }
            QuadDxTxPane {
                id: urDxTxPane
                quadName: "ul"
                Layout.fillHeight: true
                Layout.fillWidth: true
                enableTreatment: !genDxPane.genSCRP
            }
            QuadDxTxPane {
                id: llDxTxPane
                quadName: "ll"
                Layout.fillHeight: true
                Layout.fillWidth: true
                enableTreatment: !genDxPane.genSCRP
            }
            QuadDxTxPane {
                id: lrDxTxPane
                quadName: "lr"
                Layout.fillHeight: true
                Layout.fillWidth: true
                enableTreatment: !genDxPane.genSCRP
            }
        }

        RowLayout {
            GeneralizedDxTxPane {
                Layout.fillWidth: true
                id: genDxPane
            }

            ProphyRecallPane {
                id: prophyRecallPane
                Layout.preferredHeight: genDxPane.height
            }
        }


    }

    CDSaveAndCloseButton {
        id: saveButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            perioChartSettings.category = "Diagnosis";
            perioChartSettings.setValue("GeneralDx",genDxPane.generateDxString());
            perioChartSettings.setValue("ProphyMonths",prophyRecallPane.recallValue);
            perioChartSettings.sync();

            var txPlanObj = ({});
            var txPlanJOSN = textMan.readFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
            if(txPlanJOSN.length > 2) {
                txPlanObj = JSON.parse(txPlanJOSN);
            }
            comFuns.ensurePrimaryAndUnphased(txPlanObj);

            var txPlanItems = genDxPane.generateTxPlanObj();
            txPlanItems.push(ulDxTxPane.generateTxPlanObj());
            txPlanItems.push(urDxTxPane.generateTxPlanObj());
            txPlanItems.push(llDxTxPane.generateTxPlanObj());
            txPlanItems.push(lrDxTxPane.generateTxPlanObj());

            var addPerioFollow= false;
            for(var i=0;i<txPlanItems.length;i++) {
                var txItem = txPlanItems[i];

                if('BasePrice' in txItem) { //in case we got an empty
                    txPlanObj["Primary"]["Unphased"].push(txItem);
                    if(txItem["ProcedureName"].includes("Root Planing")) {
                        addPerioFollow = true;
                    }
                }
            }
            if(addPerioFollow) {
                var addMe = ({});
                addMe["ProcedureName"] = "Periodontal Followup";
                addMe["BasePrice"] = 0;
                txPlanObj["Primary"]["Unphased"].push(addMe);
            }



            txPlanJOSN = JSON.stringify(txPlanObj, null, '\t');
            textMan.saveFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),txPlanJOSN);

            comFuns.updateReviewFile("PerioDxTx",PATIENT_FILE_NAME);
            gitMan.commitData("Added periodontal Dx/Tx for " + PATIENT_FILE_NAME);

            Qt.quit();
        }

    }

    CDCancelButton {
        CDPrinter {
            id: printer
        }
        icon.name: "document-print"
        text: "Print Chart"
        onClicked: {
            printer.printPeriodontalChart(PATIENT_FILE_NAME);
        }

        anchors.right: saveButton.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
    }

    RowLayout {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        visible: false
        CDDescLabel {
            text: "Compare to Date"
        }

        ComboBox {
            id: dateBox
            Layout.minimumWidth: 360
        }
    }

}
