// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1


CDTranslucentDialog {
    id: newMessageDia

    Settings {
        id: readAccountInfo
        fileName: fileLocs.getLocalPracticePreferenceFile()
        category: "Accounts"
    }

    CDPatientReminder {
        id: patReminder
    }

    function forceLoadPat(getPatName) {
        selectPatRadio.checked = true;
        patBox.text = getPatName;
        patBox.refreshNumbers();
    }

    function sendTheText() {
        var username = readAccountInfo.value("VoipMSUsername","")
        var password = readAccountInfo.value("VoipMSPassword","")
        var apiRequestStringStart = "https://voip.ms/api/v1/rest.php?api_username="+username+
                "&api_password="+password+"&method=sendSMS&did=5083873733&dst=";


        if(selectPatRadio.checked) {
            var fullRequest = apiRequestStringStart + numberSelectionBox.currentValue.replace(/\D/g,'') +
                    "&message=" +messageArea.text;
            patReminder.get(fullRequest);
            var addMe = {
                "Time" : Date(),
                "Sender" : "Us (5083873733)",
                "Reciever" : patBox.text,
                "Contents" : messageArea.text
            };
            var defMessageJSON = textMan.readFile(fileLocs.getPatientMessagesFile(patBox.text));
            var defMessageList= [];
            if(defMessageJSON.length > 0) {
                defMessageList = JSON.parse(defMessageJSON);
            }

            defMessageList.push(addMe);
            defMessageJSON = JSON.stringify(defMessageList, null, '\t');
            textMan.saveFile(fileLocs.getPatientMessagesFile(patBox.text),defMessageJSON);
            gitMan.commitData("Sent out message \"" + messageArea.text +"\" to " + patBox.text);

        }
        else {
            fullRequest = apiRequestStringStart + manualNumber.text + "&message=" +messageArea.text;
            console.debug(fullRequest);
            patReminder.get(fullRequest);

            addMe = {
                "Time" : Date(),
                "Sender" : "Us (5083873733)",
                "Reciever" : manualNumber.text,
                "Contents" : messageArea.text
            };

            defMessageJSON = textMan.readFile(fileLocs.getPatientMessagesFile(""));
            defMessageList = JSON.parse(defMessageJSON);
            defMessageList.push(addMe);
            defMessageJSON = JSON.stringify(defMessageList, null, '\t');
            textMan.saveFile(fileLocs.getPatientMessagesFile(""),defMessageJSON);
            gitMan.commitData("Sent out message \"" + messageArea.text +"\" to " + manualNumber.text);
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "New Message"
                }
                RowLayout {
                    CDDescLabel {
                        text: "Who to send to"
                    }
                    ColumnLayout {
                        RadioButton {
                            id: selectPatRadio
                            checked: true
                            text: "Select Patient"
                        }
                        RadioButton {
                            id: manualRadio
                            text: "Manual type in number"
                        }
                    }
                    RowLayout {
                        visible: selectPatRadio.checked
                        CDPatientComboBox {
                            id: patBox
                            Layout.minimumWidth: 360

                            Settings {
                                id: loadPhoneInfo
                            }

                            function refreshNumbers() {
                                loadPhoneInfo.fileName = fileLocs.getPersonalFile(patBox.text);
                                loadPhoneInfo.category = "Phones";
                                var makeModel = [];
                                var cellPhoneNumber = loadPhoneInfo.value("CellPhone","");
                                var homePhoneNumber = loadPhoneInfo.value("HomePhone","");

                                if(cellPhoneNumber.length > 1) {
                                    var addMe = {
                                        DisplayNumber: "Cell Phone (" + cellPhoneNumber + ")",
                                        PhoneNumber: cellPhoneNumber
                                    }
                                    makeModel.push(addMe);
                                }
                                if(homePhoneNumber.length > 1) {
                                    addMe = {
                                        DisplayNumber: "Home Phone (" + homePhoneNumber + ")",
                                        PhoneNumber: homePhoneNumber
                                    }
                                    makeModel.push(addMe);
                                }
                                numberSelectionBox.model = makeModel;
                            }

                            onPatientSelected: {
                                refreshNumbers();
                            }

                        }
                        ComboBox {
                            id: numberSelectionBox
                            Layout.minimumWidth: 360
                            valueRole: "PhoneNumber"
                            textRole: "DisplayNumber"
                        }
                    }
                    RowLayout {
                        visible: !selectPatRadio.checked
                        TextField {
                            id: manualNumber
                            placeholderText: "Enter number here"
                            Layout.minimumWidth: 360
                        }
                    }
                }
                TextField {
                    id: messageArea
                    Layout.fillWidth: true
                    placeholderText: "Put message in here"
                    maximumLength: 160
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    newMessageDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                text: "Send"
                icon.name: "document-send"
                onClicked:  {
                    sendTheText();
                    newMessageDia.accept();
                }
            }
        }

    }
}
