// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

ApplicationWindow {
    id: rootWin
    visible: true
    title: qsTr("Review Interview [ID: " + PATIENT_FILE_NAME + "]")
    width: 1920
    height: 1000
    visibility: ((Screen.height < 1200) || (Screen.width < 2000)) ?
                    ApplicationWindow.Maximized : ApplicationWindow.Windowed
    x: (Screen.width - 1920) / 2
    y: (Screen.height - 1000) / 2
    flags:  visibility === ApplicationWindow.Maximized ?  Qt.WA_TranslucentBackground | Qt.FramelessWindowHint :
                                           Qt.WA_TranslucentBackground

    font.family: "Barlow Semi Condensed"
    font.pointSize: 12

    color: "transparent"

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    header: CDPatientToolBar {
        headerText: "Review New Patient Interview: "
    }


    CDFileLocations {
        id: fileLocs
    }


    Settings {
        id: reviewedInterviewSettings
        fileName: fileLocs.getReviewsFile(patientFileName);
    }

    Settings {
        id: interviewData
        fileName: fileLocs.getNewPatInterviewFile(patientFileName);
    }

    CDGitManager {
        id: noChangeGitman
    }

    function loadData() {
        interviewData.category = "Pain"
        isInPain.text = interviewData.value("IsInPain","") === "1" ?
                    "Yes": "No";
        painLength.text = interviewData.value("PainLength","").toString();
        painLocation.text = interviewData.value("PainLocation","").toString();
        saveTooth.text = interviewData.value("SaveTooth","").toString();
        whenCome.text = interviewData.value("WhenCome","").toString();

        interviewData.category = "DentalExp"
        pastIssues.text = interviewData.value("PastIssues","").toString();
        lastCleaning.text = interviewData.value("LastCleaning","").toString();
        lastSCRP.text = interviewData.value("LastSCRP","").toString();
        oftenBrush.text = interviewData.value("OftenBrush","").toString();
        oftenFloss.text = interviewData.value("OftenFloss","").toString();
        oftenBleed.text = interviewData.value("OftenBleed","").toString();
        oftenHeadaches.text = interviewData.value(
                    "OftenHeadaches","").toString();
        oftenGrinding.text = interviewData.value("OftenGrinding","").toString();
        lookingToGetDone.text = interviewData.value(
                    "LookingToGetDone","").toString();
        bracesBefore.text = interviewData.value("BracesBefore","").toString();
    }

    ColumnLayout {
        anchors.centerIn: parent

        CDTranslucentPane {
            backMaterialColor: Material.Red

            ColumnLayout {
                CDHeaderLabel {
                    text: "Pain"
                }
                RowLayout {
                    Label {
                        text: "Is in Pain?"
                        font.bold: true
                    }
                    Label {
                        id: isInPain
                    }
                }

                GridLayout {
                    columns: 2
                    visible: isInPain.text == "Yes"
                    Label {
                        text: "Pain Length"
                        font.bold: true
                    }
                    Label {
                        id: painLength
                    }
                    Label {
                        text: "Pain Location"
                        font.bold: true
                    }
                    Label {
                        id: painLocation
                    }
                    Label {
                        text: "Wishes to save tooth?"
                        font.bold: true
                    }
                    Label {
                        id: saveTooth
                    }
                    Label {
                        text: "When can the patient come in"
                        font.bold: true
                    }
                    Label {
                        id: whenCome
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.LightBlue
            GridLayout {
                columns: 2
                visible: isInPain.text != "Yes"

                Label {
                    text: "Dental Experience"
                    font.pointSize: 24
                    font.underline: true
                    Layout.columnSpan: 2
                }

                Label {
                    text: "Past Dental Issues"
                    font.bold: true
                }
                Label {
                    id: pastIssues
                }
                Label {
                    text: "Last Cleaning"
                    font.bold: true
                }
                Label {
                    id: lastCleaning
                }
                Label {
                    text: "Last SCRP"
                    font.bold: true
                }
                Label {
                    id: lastSCRP
                }
                Label {
                    text: "How often brushing"
                    font.bold: true
                }
                Label {
                    id: oftenBrush
                }
                Label {
                    text: "How often flossing"
                    font.bold: true
                }
                Label {
                    id: oftenFloss
                }
                Label {
                    text: "How often bleeding"
                    font.bold: true
                }
                Label {
                    id: oftenBleed
                }
                Label {
                    text: "How often headaches"
                    font.bold: true
                }
                Label {
                    id: oftenHeadaches
                }
                Label {
                    text: "How often grinding"
                    font.bold: true
                }
                Label {
                    id: oftenGrinding
                }
                Label {
                    text: "Looking to get done"
                    font.bold: true
                }
                Label {
                    id: lookingToGetDone
                }

                Label {
                    text: "Ever had braces before"
                    font.bold: true
                }
                Label {
                    id: bracesBefore
                }
            }
        }
        Component.onCompleted: rootWin.loadData();
    }

    CDSaveAndCloseButton {
        text: "Finish and Close"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
    }
}
