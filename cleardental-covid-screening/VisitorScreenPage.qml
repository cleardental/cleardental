// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import Qt.labs.settings 1.1

CDTransparentPage {
    id: vistorScreenPage

    CDFileLocations {
        id: fLocs
    }

    CDCommonFunctions {
        id: comFun
    }

    CDGitManager {
        id: gitMan
    }

    Flickable {
        anchors.fill: parent

        contentWidth: colLayout.width
        contentHeight: colLayout.height + 10
        flickableDirection: Flickable.VerticalFlick

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: colLayout
            x: (vistorScreenPage.width - colLayout.width)/2
            y: 10
            CDTranslucentPane {
                backMaterialColor: Material.Yellow
                Layout.fillWidth: true
                GridLayout {
                    columns: 2
                    width: parent.width
                    CDHeaderLabel {
                        text: "Vistor Information"
                        Layout.columnSpan: 2
                    }
                    CDDescLabel {
                        text: "Name"
                    }
                    TextField {
                        id: nameField
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "Phone number"
                    }
                    TextField {
                        id: phoneField
                        Layout.fillWidth: true
                        inputMethodHints: Qt.ImhDialableCharactersOnly
                    }
                }
            }

            ScreenPane {
                id: screenPane
            }

            CDSaveAndCloseButton {
                Layout.alignment: Qt.AlignRight

                Settings {
                    id: covidFile
                    fileName: fLocs.getCOVIDStatusFile(rootWin.rootPatName)
                }

                onClicked: {
                    screenPane.saveResultsToJSON(nameField.text,phoneField.text);
                    gitMan.commitData("Added covid screening results");
                    Qt.quit();
                }
            }
        }

    }
}
