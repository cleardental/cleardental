// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    //contentWidth: 350
    backMaterialColor: Material.Lime

    property var allTxObj: ({})

    CDCommonFunctions {
        id: commonFuns
    }

    function loadTooth() {
        //console.debug("Load: " + txPlanNameBox.currentIndex)
        clearValues();
        var currTxPlanName = txPlanNameBox.modelList[txPlanNameBox.currentIndex];

        if(typeof allTxObj[currTxPlanName] !== "undefined") {
            var currentTxPlan = allTxObj[currTxPlanName];
            //Go through all the phases
            var phaseNames = Object.keys(allTxObj[currTxPlanName]);
            for(var phaseI=0;phaseI<phaseNames.length;phaseI++) {
                var phaseName = phaseNames[phaseI];
                var phase = currentTxPlan[phaseName]; //returns an array
                for(var txI=0;txI<phase.length;txI++) {
                    var treatment = phase[txI];
                    if((typeof treatment["Tooth"] !== "undefined") &&
                            (parseInt(treatment["Tooth"]) ===
                             rootWin.currentTooth)){
                        if(treatment["ProcedureName"] === "Watch") {
                            watchBox.checked = true;
                            txWatchSurfaces.parseSurfaces(treatment["Surfaces"]);
                        }
                        else if(treatment["ProcedureName"].includes("Composite")) {
                            txCompBox.checked = true;
                            txCompSurfaces.parseSurfaces(treatment["Surfaces"]);
                        }
                        else if(treatment["ProcedureName"].includes("Amalgam")) {
                            txAmalgamBox.checked = true;
                            txAmalSurfaces.parseSurfaces(treatment["Surfaces"]);
                        }
                        else if(treatment["ProcedureName"].includes("Crown")) {
                            txCrownBox.checked = true;
                            txCrownRow.setCrownTypeByName(treatment["Material"]);
                            if("PrevPlacement" in treatment) {
                                crownPlacementDate.setDateObj( new Date(treatment["PrevPlacement"]));
                            }
                        }
                        else if(treatment["ProcedureName"].includes("Extraction")) {
                            extractBox.checked = true;
                            extractTypeBox.currentIndex = extractTypeBox.find(treatment["ExtractionType"])
                        }
                        else if(treatment["ProcedureName"].includes("Placement")) {
                            txImplant.checked = true;
                        }
                        else if(treatment["ProcedureName"].includes("RCT")) {
                            txRCT.checked = true;
                            rctVisitsBox.currentIndex = rctVisitsBox.find(treatment["Visits"]);
                        }
                        else if(treatment["ProcedureName"].includes("Post and Core")) {
                            postCoreCheck.checked = true;
                            postCoreTypeBox.currentIndex = postCoreTypeBox.find(treatment["Material"]);
                        }
                        else if(treatment["ProcedureName"] === "Sealant") {
                            sealant.checked = true;
                        }
                    }
                }
            }
        }
    }

    function addTxToPhase(phaseString, treatmentPlanObject, treatmentObject) {
        if(typeof treatmentPlanObject[phaseString] == "undefined") {
            treatmentPlanObject[phaseString] = [];
        }
        treatmentPlanObject[phaseString].push(treatmentObject);
    }

    function removeTreatmentFromTooth(txPlanName, phaseName, toothnumb) {
        var txPlanList = allTxObj[txPlanName][phaseName];
        if(typeof txPlanList !== "undefined") {
            for(var i=0;i<txPlanList.length;i++) {
                var txObj = txPlanList[i];
                if(txObj["Tooth"] === toothnumb) {
                    txPlanList.splice(i,1);
                    i--;
                }
            }
        }
    }

    function save() {
        //console.debug("Save: " + txPlanNameBox.currentIndex)
        var currTxPlanName = txPlanNameBox.modelList[txPlanNameBox.currentIndex];

        if(typeof allTxObj[currTxPlanName] == "undefined") {
            allTxObj[currTxPlanName] = ({});
        }

        var txPlanObj = allTxObj[currTxPlanName];

        removeTreatmentFromTooth(currTxPlanName,"Unphased", rootWin.currentTooth);

        if(watchBox.checked) { //watch
            var watchObj = ({});
            watchObj["Tooth"] = rootWin.currentTooth;
            watchObj["ProcedureName"] = "Watch";
            watchObj["Surfaces"] = txWatchSurfaces.getSurfaces();
            addTxToPhase("Unphased",txPlanObj,watchObj);
        }

        if(txCompBox.checked) { //composite
            var compObj = ({});
            compObj["Tooth"] = rootWin.currentTooth;
            compObj["Surfaces"] = txCompSurfaces.getSurfaces();
            if(txCompSurfaces.isPost) {
                switch(compObj["Surfaces"].length) {
                default:
                case 1:
                    compObj["DCode"] = "D2391";
                    break;
                case 2:
                    compObj["DCode"] = "D2392";
                    break;
                case 3:
                    compObj["DCode"] = "D2393";
                    break;
                case 4:
                case 5:
                    compObj["DCode"] = "D2394";
                    break;
                }
            }
            else {
                switch(compObj["Surfaces"].length) {
                default:
                case 1:
                    compObj["DCode"] = "D2330";
                    break;
                case 2:
                    compObj["DCode"] = "D2331";
                    break;
                case 3:
                    compObj["DCode"] = "D2332";
                    break;
                case 4:
                case 5:
                    compObj["DCode"] = "D2335";
                    break;
                }
            }
            compObj["ProcedureName"] = commonFuns.getNameForCode(compObj["DCode"]);
            compObj["BasePrice"] = priceFile.value(compObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,compObj);
        }

        if(txAmalgamBox.checked) { //amalgam
            var amalObj = ({});
            amalObj["Tooth"] = rootWin.currentTooth;
            amalObj["Surfaces"] = txAmalSurfaces.getSurfaces();
            if(txAmalSurfaces.isPost) {
                switch(amalObj["Surfaces"].length) {
                default:
                case 1:
                    amalObj["DCode"] = "D2140";
                    break;
                case 2:
                    amalObj["DCode"] = "D2150";
                    break;
                case 3:
                    amalObj["DCode"] = "D2160";
                    break;
                case 4:
                case 5:
                    amalObj["DCode"] = "D2161";
                    break;
                }
            }
            else {
                switch(amalObj["Surfaces"].length) {
                default:
                case 1:
                    amalObj["DCode"] = "D2140";
                    break;
                case 2:
                    amalObj["DCode"] = "D2150";
                    break;
                case 3:
                    amalObj["DCode"] = "D2160";
                    break;
                case 4:
                case 5:
                    amalObj["DCode"] = "D2161";
                    break;
                }
            }
            amalObj["ProcedureName"] = commonFuns.getNameForCode(amalObj["DCode"]);
            amalObj["BasePrice"] = priceFile.value(amalObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,amalObj);
        }

        if(txCrownBox.checked) {
            var crownObj = ({});
            crownObj["Tooth"] = rootWin.currentTooth;

            crownObj["Material"] = txCrownRow.getCrownTypeName();
            if(crownObj["Material"] ===  "Stainless Steel") {
                crownObj["DCode"] = "D2930";
            }
            else if(crownObj["Material"]==="Porcelain Fused to Metal") {
                crownObj["DCode"] = "D2750";
            }
            else if(crownObj["Material"]==="Full Cast Gold") {
                crownObj["DCode"] = "D2790";
            }
            else if(crownObj["Material"]==="Zirconia") {
                crownObj["DCode"] = "D2740";
            }
            else { //includes Lithium Disilicate
                crownObj["DCode"] = "D2740";
            }
            crownObj["ProcedureName"] = commonFuns.getNameForCode(crownObj["DCode"]);
            crownObj["BasePrice"] = priceFile.value(crownObj["DCode"],0);
            if(crownPlacementDate.visible) {
                crownObj["PrevPlacement"] = crownPlacementDate.getDateObj();
            }

            addTxToPhase("Unphased",txPlanObj,crownObj);
        }

        if(extractBox.checked) {
            var extObj = ({});
            extObj["Tooth"] = rootWin.currentTooth;

            if(extractTypeBox.displayText === "Surgical") {
                extObj["DCode"] = "D7210";
                extObj["ExtractionType"] = "Surgical";
            }
            else { //regular
                extObj["DCode"] = "D7140";
                extObj["ExtractionType"] = "Regular";
            }
            extObj["ProcedureName"] = commonFuns.getNameForCode(extObj["DCode"]);
            extObj["BasePrice"] = priceFile.value(extObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,extObj);
        }

        if(txImplant.checked) {
            var implantObj = ({});
            implantObj["Tooth"] = rootWin.currentTooth;
            implantObj["DCode"] = "D6010";
            implantObj["ProcedureName"] = commonFuns.getNameForCode("D6010");
            implantObj["BasePrice"] = priceFile.value(implantObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,implantObj);

            var implantAbutObj = ({});
            implantAbutObj["Tooth"] = rootWin.currentTooth;
            implantAbutObj["DCode"] = "D6057";
            implantAbutObj["ProcedureName"] = commonFuns.getNameForCode("D6057");
            implantAbutObj["BasePrice"] = priceFile.value(implantAbutObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,implantAbutObj);

            var implantCrownObj = ({});
            implantCrownObj["Tooth"] = rootWin.currentTooth;
            implantCrownObj["DCode"] = "D6058";
            implantCrownObj["ProcedureName"] = commonFuns.getNameForCode("D6058");
            implantCrownObj["BasePrice"] = priceFile.value(implantCrownObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,implantCrownObj);
        }

        if(txRCT.checked) {
            var rctObj = ({});
            rctObj["Tooth"] = rootWin.currentTooth;

            if(commonFuns.isPost(rootWin.currentTooth)) {
                if((rootWin.currentTooth === 4) ||
                        (rootWin.currentTooth === 5) ||
                        (rootWin.currentTooth === 12) ||
                        (rootWin.currentTooth === 13) ||
                        (rootWin.currentTooth === 20) ||
                        (rootWin.currentTooth === 21) ||
                        (rootWin.currentTooth === 28) ||
                        (rootWin.currentTooth === 29) ){ //premolar
                    rctObj["DCode"] = "D3320";

                } else { //molar
                    rctObj["DCode"] = "D3330";
                }
            }
            else { //anterior
                rctObj["DCode"] = "D3310";
            }
            rctObj["ProcedureName"] = commonFuns.getNameForCode(rctObj["DCode"]);
            rctObj["BasePrice"] = priceFile.value(rctObj["DCode"],0);
            rctObj["Visits"] = rctVisitsBox.displayText;
            addTxToPhase("Unphased",txPlanObj,rctObj);
        }

        if(postCoreCheck.checked) {
            var postCoreObj = ({});
            postCoreObj["Tooth"] = rootWin.currentTooth;
            if(postCoreTypeBox.displayText === "Pre-fab") {
                postCoreObj["Material"] = "Prefab";
                postCoreObj["DCode"] = "D2954";
            }
            else { //cast
                postCoreObj["Material"] = "Cast";
                postCoreObj["DCode"] = "D2952";
            }
            postCoreObj["ProcedureName"] = commonFuns.getNameForCode(postCoreObj["DCode"]);
            postCoreObj["BasePrice"] = priceFile.value(postCoreObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,postCoreObj);
        }

        if(sealant.checked) {
            var sealantObj = ({});
            sealantObj["Tooth"] = rootWin.currentTooth;
            sealantObj["DCode"] = "D1351";
            sealantObj["ProcedureName"] = commonFuns.getNameForCode("D1351");
            sealantObj["BasePrice"] = priceFile.value(sealantObj["DCode"],0);
            addTxToPhase("Unphased",txPlanObj,sealantObj);
        }

        allTxObj[txPlanNameBox.modelList[txPlanNameBox.currentIndex]] = txPlanObj;

        var writeMe = JSON.stringify(allTxObj, null, '\t');
        txPlanFileManager.saveFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),writeMe);

    }

    function clearValues() {
        watchBox.checked = false;
        txWatchSurfaces.clearSurfaces();
        txCompBox.checked = false;
        txCompSurfaces.clearSurfaces();
        txAmalgamBox.checked = false;
        txAmalSurfaces.clearSurfaces();
        txCrownBox.checked = false;
        extractBox.checked = false;
        txImplant.checked = false;
        txRCT.checked = false;
        postCoreCheck.checked = false;
        sealant.checked = false;
    }

    Component.onCompleted: {
        var allTxJSON = txPlanFileManager.readFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        if(allTxJSON.length === 0) {
            return;
        }
        allTxObj = JSON.parse(allTxJSON);
        loadTooth();
    }

    CDTextFileManager {
        id: txPlanFileManager
    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: priceFile
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Prices"

    }

    CDTranslucentDialog {
        id: moreTxDia
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "More Treatments"
                }
                CheckBox {
                    id: sealant
                    text: "Sealant"
                }


                RowLayout {
                    Layout.fillWidth: true
                    Label {
                        Layout.fillWidth: true
                    }
                    CDButton {
                        text: "Done"
                        highlighted: true
                        Material.accent: Material.Cyan
                        onClicked: {
                            moreTxDia.close();
                        }
                    }
                }
            }
        }
    }

    Column {
        Label {
            text: "Treatment Plan"
            font.pointSize: 24
            font.underline: true
        }

        RowLayout {
            Label {
                text: "Tx Plan:"
                font.bold: true
            }
            ComboBox {
                id: txPlanNameBox
                property var modelList:  ["Primary"]
                model: modelList

                popup.onOpened: {
                    save();
                }

                onCurrentIndexChanged: {
                    loadTooth();
                }

            }
            Button {
                Material.accent: Material.Green
                icon.name: "list-add"
                highlighted: true
                onClicked: {
                    if(txPlanNameBox.modelList.length === 1) {
                        txPlanNameBox.modelList.push("Tx Plan 2");
                    }
                    else {
                        var nextTxPlanNumb = 3;
                        for(var i=1;i<txPlanNameBox.modelList.length;i++) {
                            var parts = txPlanNameBox.modelList[i].split(" ");
                            var intVal = parseInt(parts[2]);
                            if(intVal > nextTxPlanNumb) {
                                nextTxPlanNumb = intVal;
                            }
                            else if(intVal === nextTxPlanNumb) {
                                nextTxPlanNumb++;
                            }
                        }
                        txPlanNameBox.modelList.push("Tx Plan " +
                                                     nextTxPlanNumb);
                    }

                    txPlanNameBox.model = txPlanNameBox.modelList;
                    txPlanNameBox.currentIndex =
                            txPlanNameBox.modelList.length -1;
                }
            }

            Button {
                Material.accent: Material.Red
                icon.name: "list-remove"
                highlighted: true
                enabled: txPlanNameBox.displayText != "Primary"
                onClicked: {
                    txPlanNameBox.modelList.splice(txPlanNameBox.currentIndex,
                                                   1);
                    txPlanNameBox.model = txPlanNameBox.modelList;
                }
            }
        }

        ColumnLayout {
            CheckBox {
                id: watchBox
                text: "Watch"
            }

            CDSurfaceRow {
                id: txWatchSurfaces
                visible: watchBox.checked
                opacity: watchBox.checked  ? 1:0
                workingToothNumb: rootWin.currentTooth
            }
        }



        ColumnLayout {
            CheckBox {
                id: txCompBox
                text: "Composite"
            }

            CDSurfaceRow {
                id: txCompSurfaces
                visible: txCompBox.checked
                opacity: txCompBox.checked  ? 1:0
                workingToothNumb: rootWin.currentTooth
            }
        }


        ColumnLayout {
            CheckBox {
                id: txAmalgamBox
                text: "Amalgam"
            }

            CDSurfaceRow {
                id: txAmalSurfaces
                visible: txAmalgamBox.checked
                opacity: txAmalgamBox.checked  ? 1:0
                workingToothNumb: rootWin.currentTooth
            }
        }

        ColumnLayout {
            CheckBox {
                id: txCrownBox
                text: "Crown"
            }
            CrownTypeRow {
                id: txCrownRow
                visible: txCrownBox.checked
                opacity: txCrownBox.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }
            RowLayout {
                id: prevCrownPlacementRow
                visible: txCrownBox.checked && rootWin.existingCrown
                opacity:  txCrownBox.checked && rootWin.existingCrown ? 1:0
                CDDescLabel {
                    text: "Date of placement"
                }

                CDDatePicker {
                    id: crownPlacementDate
                }

                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }
        }


        RowLayout {
            CheckBox {
                id: extractBox
                text: "Extract"
            }
            ComboBox {
                id: extractTypeBox
                visible: extractBox.checked
                opacity: extractBox.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
                model: ["Regular","Surgical"]
            }
        }


        CheckBox {
            id: txImplant
            text: "Implant"
        }

        RowLayout {
            CheckBox {
                id: txRCT
                text: "RCT"
            }
            Label {
                opacity: txRCT.checked ? 1 : 0
                visible: opacity>0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
                text: "Visits:"
            }
            ComboBox {
                id: rctVisitsBox
                model: ["1","2","3"]
                opacity: txRCT.checked ? 1 : 0
                visible: opacity>0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }
        }


        RowLayout {
            CheckBox {
                id: postCoreCheck
                text: "Post/Core"
            }
            ComboBox {
                id: postCoreTypeBox
                visible: postCoreCheck.checked
                opacity: postCoreCheck.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
                model: ["Pre-fab","Cast"]
            }
        }

        Button {
            text: "More..."
            onClicked: {
                moreTxDia.open();
            }
        }

        move: Transition {
            NumberAnimation {
                properties: "x,y"
                duration: 300
            }
        }

    }


}
