// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: qsTr("Hard Tissue Exam " + " [ID: " + PATIENT_FILE_NAME + "]")

    property int currentTooth: 1
    property string fillingSurfaces: ""
    property string decaySurfaces: ""
    property string fractureSurfaces: ""
    property bool endoIssue: false
    property bool existingCrown: false

    header: CDPatientToolBar {
        headerText: "Hard Tissue Charting:"

        ToolButton {
            icon.name: "go-previous"
            visible: mainStack.depth > 1
            icon.width: 64
            icon.height: 64
            anchors.left: parent.left
            onClicked: mainStack.pop();
        }

    }

    StackView {
        id: mainStack
        anchors.fill: parent
        initialItem: SelectToothToChart{}
    }

    CDToothScene3D { //this is a stupid workaround to prevent the application from freezing
        //https://bugreports.qt.io/browse/QTBUG-86460
        toothNumb: "1"
        width: 1
        height: 1
    }

    CDCommonFunctions {
        id: commonFuns
    }
}
