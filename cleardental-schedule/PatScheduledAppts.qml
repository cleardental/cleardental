// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0


ColumnLayout {
    id: schedAppts
    Layout.minimumWidth: 360
    CDHeaderLabel {
        text: "Scheduled Appointments"
    }

    Flickable {
        // this still doesn't scroll :/
        Layout.minimumWidth: apptColLayout.width
        Layout.maximumWidth: 500
        contentHeight: apptColLayout.height
        contentWidth: apptColLayout.width
        Layout.maximumHeight: 360
        Layout.minimumHeight: 360
        Layout.fillWidth: true
        clip: true

        ScrollBar.vertical: ScrollBar{}

        ColumnLayout {
            id: apptColLayout

            Repeater {
                id: apptRep
                property var apptListModel: []
                property var todayTime;

                Label {

                    property bool wasNoShow: false
                    font.strikeout: wasNoShow

                    Settings {
                        id: loadPastApptSet
                    }


                    Component.onCompleted: {
                        var fName = apptRep.apptListModel[index][0];
                        loadPastApptSet.fileName = fName;
                        fName = fName.replace(".ini", " ");
                        var lastSlash = fName.lastIndexOf("/") +1;
                        var lastProcedureList = JSON.parse(loadPastApptSet.value("Procedures","[]"));
                        wasNoShow  = loadPastApptSet.value("Status") === "No Show"
                        var proceduresStringArray= [];
                        for(var i=0;i<lastProcedureList.length;i++) {
                            var procedureString = commonFuns.makeTxItemString(lastProcedureList[i]);
                            proceduresStringArray.push(procedureString);
                        }
                        text = fName.substring(lastSlash) + proceduresStringArray.join(",");

                        // @disable-check M126
                        if(apptRep.todayTime == apptRep.apptListModel[index][1]) {
                            color = Material.color(Material.Green,Material.Shade900)
                            font.bold = true;
                        }
                        else if(apptRep.todayTime > apptRep.apptListModel[index][1]) { //past
                            color = Material.color(Material.BlueGrey,Material.Shade900)
                        }
                        else { //future appt.
                            color = Material.color(Material.LightBlue,Material.Shade900);
                        }
                    }
                }

                Component.onCompleted: {
                    var todayDate = new Date();
                    todayTime = todayDate.setUTCHours(0,0,0,0);


                    var apptList = scheduleDB.getAppointments(schButton.patientID);
                    var apptListPairs = [];

                    for(var i=0;i<apptList.length;i++) {

                        var lastSlash = apptList[i].lastIndexOf("/") +1;
                        var fLongName = apptList[i].substring(lastSlash);
                        var atSymbol = fLongName.lastIndexOf("@");
                        var pureDate = fLongName.substring(0,atSymbol);
                        var monthDayYearArray = pureDate.split("-");
                        var mills = commonFuns.convertTextDateToMils(monthDayYearArray[0],
                                                                     monthDayYearArray[1],
                                                                     monthDayYearArray[2]);
                        apptListPairs.push([apptList[i],mills]);
                    }
                    apptListModel = apptListPairs.sort(function(a,b){return a[1] - b[1];});
                    model = apptListModel.length;
                }
            }
        }
    }

}
