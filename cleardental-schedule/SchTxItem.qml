// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.15
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

Item {
    property var txItemObj: ({})
    property var oldParent: parent
    height: 35
    z: 1024


    DragHandler {
        id: dragHandle
        property bool wasActive: false
        onActiveChanged: {
            if(!active) {
                manageProceduresDia.handleDrop(parent)
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: Material.color(Material.Cyan)
        opacity: .7
        radius: 5
    }

    Label {
        anchors.centerIn: parent
        text: comFuns.makeTxItemString(txItemObj)
    }

    Drag.active: dragHandle.active
    Drag.hotSpot.x: width/2
    Drag.hotSpot.y: height/2
    parent:  dragHandle.active ?  dragRow : oldParent


}
