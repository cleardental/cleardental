// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10

Label {
    Layout.minimumWidth: 100
    Layout.maximumWidth:  100
    elide: Text.ElideRight
}
