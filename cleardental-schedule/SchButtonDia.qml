// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/22/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: schButtonDia


    CDToolLauncher{
        id: toolLauncher
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: apptWriter
        fileName: schButton.fileName
    }

    CDCommonFunctions {
        id: commonFuns
    }

    onVisibleChanged: {
        rootWin.dialogShown = visible;
    }

    ColumnLayout {

        CDTranslucentPane {
            backMaterialColor: Material.Blue

            RowLayout {

                ColumnLayout {
                    id: col1
                    Layout.alignment: Qt.AlignTop
                    PatImage {

                        id: patImage
                    }
                    ContactInfoGrid {
                        id: contactInfo
                    }
                }


                ColumnLayout {
                    id: col2
                    Layout.alignment: Qt.AlignTop
                    ApptDetailsGrid {
                        id: apptDetailsGrid
                    }
                    PatScheduledAppts {
                        id: schedAppts
                    }
                }
                ColumnLayout {
                    id: col3
                    Layout.alignment: Qt.AlignTop
                    PatInfoCol {
                        id: patInfo
                    }
                    ProcedureGrid {
                        id: procedureGrid
                    }
                }

            } //end of the major row layout
        } //end of pane

        RowLayout {

            CDDeleteButton {
                Layout.alignment: Qt.AlignHCenter
                text: "Delete appointment"
                onClicked: deleteApptDia.open();
                DeleteApptDialog {
                    id: deleteApptDia
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDSaveAndCloseButton {

                CDGitManager {
                    id: gitMan
                }

                onClicked: {
                    apptWriter.setValue("Duration",apptDetailsGrid.durationValue);
                    apptWriter.setValue("Status",apptDetailsGrid.statusText);
                    apptWriter.setValue("StartTime",apptDetailsGrid.getStart);
                    apptWriter.setValue("Comments",apptDetailsGrid.getComments);
                    apptWriter.setValue("Chair",apptDetailsGrid.getChair);
                    apptWriter.setValue("ProviderID",apptDetailsGrid.getSelectedProvider())
                    apptWriter.sync();

                    gitMan.commitData("Updated appointment for " + patientID);

//                    currentDayFlickable.loadApptButtons();
                    loadUpDay(selectedDay);
                }
            }
        } // end of buttons on bottom

    }

    Component.onDestruction: {
        loadUpDay(selectedDay);
    }

}
