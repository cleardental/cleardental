// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details
// Updated 6/3/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ColumnLayout {
    CDHeaderLabel {
        text: patLabel.text
    }
    Image {
        id: proImge
        Layout.maximumHeight: 300
        Layout.maximumWidth: 300
        Layout.alignment: Qt.AlignTop
        mipmap: true
        fillMode: Image.PreserveAspectFit
        source: patientID.length > 0 ? "file://" + fLocs.getProfileImageFile(patientID) : ""
        visible: status == Image.Ready
    }
    Rectangle {
        Layout.maximumHeight: 300
        Layout.maximumWidth: 300
        Layout.minimumHeight: 300
        Layout.minimumWidth: 300
        visible: !proImge.visible
        gradient: Gradient {
            GradientStop { position: 0.0; color: "lightsteelblue" }
            GradientStop { position: 1.0; color: "lightblue" }
        }
        radius: 5

        CDDescLabel {
            anchors.centerIn: parent
            text: "No Photo Available"
        }

    }
}
