// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: manageProceduresDia

    property string patientID: ""
    property var currentTxPlanList: []

    CDCommonFunctions {
        id: comFuns
    }

    function isScheduledTx(getItem) {
        var returnMe = false;
        for(var i=0;i<currentTxPlanList.length;i++) {
            var compItem = currentTxPlanList[i]
            if(comFuns.isEqual(compItem,getItem)) {
                returnMe = true;
                i = currentTxPlanList.length;
            }
        }

        return returnMe;
    }

    function redrawColumns() {
        plannedPane.plannedArray = [];
        var fullList = comFuns.getAllTxPlanItems(patientID);
        for(var i=0;i<fullList.length;i++) {
            var txItem = fullList[i];
            if(!isScheduledTx(txItem)) {
                plannedPane.plannedArray.push(txItem);
            }
        }

        //Because we want to force a refresh for the models
        plannedView.model = 0
        scheduledView.model = 0;

        plannedView.model = plannedPane.plannedArray.length
        scheduledView.model = currentTxPlanList.length
    }


    function handleDrop(droppedItem) {
        var legitX = droppedItem.x + (droppedItem.width / 2);
        var isSch = isScheduledTx(droppedItem.txItemObj);
        //console.debug(legitX);
        if(isSch && (legitX < 370)) { //remove from appointment
            for(var i=0;i<currentTxPlanList.length;i++) {
                var compItem = currentTxPlanList[i]
                if(comFuns.isEqual(compItem,droppedItem.txItemObj)) {
                    currentTxPlanList.splice(i,1);
                    i = currentTxPlanList.length;
                }
            }
        }
        else if( (!isSch) && (legitX > 350)) { //add to the appointment
            currentTxPlanList.push(droppedItem.txItemObj);

        }
        redrawColumns();

    }

    ColumnLayout {
        Item {
            id: dragRow
            Layout.preferredWidth:  775
            Layout.preferredHeight:  550

            CDTranslucentPane {
                id: plannedPane
                backMaterialColor: Material.Orange
                anchors.left: parent.left
                anchors.margins: 5
                property var plannedArray: []
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Planned Procedures"
                    }
                    Rectangle {
                        id: plannedRect
                        radius: 10
                        color: Qt.rgba(1,0,0,.2)
                        height: 480
                        width:360

                        ListView {
                            id: plannedView
                            model: plannedPane.plannedArray.length
                            spacing: 5

                            delegate: SchTxItem {
                                width: plannedView.width
                                txItemObj: plannedPane.plannedArray[index]
                                oldParent: plannedView
                            }
                            anchors.fill: parent
                            anchors.margins: 10
                            clip: true
                        }
                    }
                }
            }
            CDTranslucentPane {
                id: scheduledPane
                anchors.left: plannedPane.right
                anchors.margins: 5
                backMaterialColor: Material.Lime
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Scheduled Procedures"
                    }
                    Rectangle {
                        radius: 10
                        color: Qt.rgba(0,1,1,.2)
                        height: 480
                        width:360

                        ListView {
                            id: scheduledView
                            model: manageProceduresDia.currentTxPlanList.length
                            delegate: SchTxItem {
                                width: plannedView.width
                                txItemObj: manageProceduresDia.currentTxPlanList[index]
                                oldParent: scheduledView
                            }
                            spacing: 5
                            anchors.fill: parent
                            anchors.margins: 10
                            clip: true
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: manageProceduresDia.reject();
            }
            Label {
                Layout.fillWidth: true
            }
            CDSaveButton {
                text: "Save Changes"

                CDGitManager {
                    id: gitMan
                }
                onClicked: {
                    apptWriter.setValue("Procedures",JSON.stringify(manageProceduresDia.currentTxPlanList))
                    apptWriter.sync();
                    gitMan.commitData("Updated procedures for " + manageProceduresDia.patientID);
                    manageProceduresDia.accept();
                }
            }
        }
    }


    onVisibleChanged: {
        if(visible) {
            redrawColumns();
        }
    }

}
