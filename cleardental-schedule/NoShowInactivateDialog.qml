// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: noShowInactivateDialog

    property int numbOfNoShows: scheduleDB.getNoShowAppointments(schButton.patientID)
    CDGitManager {
        id: gitMan
    }

    ColumnLayout {
        CDTranslucentPane {
            backMaterialColor: Material.DeepOrange
            ColumnLayout {
                CDHeaderLabel {
                    text: "Deactivate Patient"
                }
                CDDescLabel {
                    text: "Patient has had " + numbOfNoShows +
                          " no-shows. Would you like to de-activate the patient?"
                }
                RowLayout {
                    CDCancelButton {
                        text: "No, keep the patient"
                        onClicked: {
                            noShowInactivateDialog.close();
                        }
                    }
                    Label {
                        Layout.fillWidth: true
                    }
                    CDDeleteButton {
                        text: "Yes, de-activate the patient"
                        Settings {
                            id: setInactive
                            fileName: fLocs.getPersonalFile(schButton.patientID);
                            category: "Preferences"
                        }

                        onClicked: {
                            var oldValue = setInactive.value("ScheduleAlert","");
                            oldValue+= " Do not schedule: Patient had " + numbOfNoShows + " no shows!";
                            var newValue = oldValue.trim();
                            setInactive.setValue("ScheduleAlert",newValue);
                            setInactive.sync();
                            gitMan.commitData("Inactivated " + schButton.patientID)
                            noShowInactivateDialog.close();
                        }
                    }
                }
            }
        }
    }
}
