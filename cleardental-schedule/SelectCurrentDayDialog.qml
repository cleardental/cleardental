// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

Dialog {
    id: selectDayDia
    property alias setDate: datePicker.myDate

    background: Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .75
        radius: 10
    }

    onVisibleChanged: {
        rootWin.dialogShown = visible;
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Select Day"
                }

                CDCalendarDatePicker {
                    id: datePicker


                    onUserSelectedDay: {
                        rootWin.selectedDay = myDate;
                        selectDayDia.close();
                    }

                }
            }
        }

    }


}
