// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ColumnLayout {


    // "Contact Information" is not 100% reflective of what is shown
    CDHeaderLabel {
        text: "Contact Information"
    }
    GridLayout {
        Layout.columnSpan: 2
        columns: 2
        CDDescLabel {
            text: "Date of Birth"
        }
        Label {
            text: schButton.patDOB
        }
        CDDescLabel {
            text: "Age"
        }
        Label {
            CDCommonFunctions {
                id: ageCommonFuns
            }

            text: ageCommonFuns.getYearsOld(schButton.patientID)
        }
        CDDescLabel {
            text: "Preferred Contact Method"
        }
        Label {
            text: schButton.prefContactMethod
            Layout.fillWidth: true
        }

        RowLayout {
            Layout.columnSpan: 2
            visible: genderlabel.text.length > 1
            Settings {
                id: genderSettings
                fileName: fileLocs.getPersonalFile(schButton.patientID);
                category: "Personal"
            }
            CDDescLabel {
                text: "Gender"
            }
            Label {
                id: genderlabel
                Component.onCompleted: { //stupid async issue
                    text = genderSettings.value("Gender","");
                }
                font.bold: true
                color: "white"
                style: Text.Outline
                styleColor: "black"
                background: Rectangle {
                    anchors.fill: parent
                    gradient: Gradient {
                        GradientStop { position: 0.0/7; color: "#9400D3" }
                        GradientStop { position: 1.0/7; color: "#4B0082" }
                        GradientStop { position: 2.0/7; color: "#0000FF" }
                        GradientStop { position: 3.0/7; color: "#00FF00" }
                        GradientStop { position: 4.0/7; color: "#FFFF00" }
                        GradientStop { position: 5.0/7; color: "#FF7F00" }
                        GradientStop { position: 6.0/7; color: "#FF0000" }
                        GradientStop { position: 7.0/7; color: "red" }
                    }
                }
            }
        }

        RowLayout {
            CDDescLabel {
                text: "Cell Phone"
            }
            Button {
                text: "Send reminder"
                onClicked: {
                    rootWin.remindPatient(schButton.fileName)
                    gitMan.commitData("Sent out reminder texts for " + schButton.patientID);
                }
            }

        }


        CDCopyLabel {
            text: schButton.cellPhoneNumber
            Layout.fillWidth: true
            readOnly: true
        }
        CDDescLabel {
            text: "Home Phone"
        }
        CDCopyLabel {
            text: schButton.homePhoneNumber
            Layout.fillWidth: true
            readOnly: true
        }
        CDDescLabel {
            text: "Email"
        }
        CDCopyLabel {
            text: schButton.emailAddr
            Layout.fillWidth: true
            readOnly: true
        }
    }
}
