// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: changeDateDia
    property string currentDate: ""
    property string appointmentFileLocation: ""
    property bool isBlocker: false

    ColumnLayout {
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Change date"
                    Layout.columnSpan: 2
                }
                CDDescLabel {
                    text: "Current Date"
                }
                Label {
                    text: changeDateDia.currentDate
                }
                CDDescLabel {
                    text: "New Date"
                }
                CDCalendarDatePicker {
                    id: calPick
                }
            }

        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    changeDateDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                icon.name: ""
                text: "Move Date"
                onClicked: {
                    if(!isBlocker) {
                        apptStatusBox.currentIndex = 0;
                        apptWriter.setValue("Status", apptStatusBox.currentText);
                        apptWriter.sync();
                    }
                    gitMan.commitData("Updated appointment for " + patientID);
                    scheduleDB.moveAppointment(appointmentFileLocation,calPick.myDate)
                    changeDateDia.accept();
                }
            }
        }
    }

}
