// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: addAptDialog

    property int setStartTime: 8*60
    property int setDuration: 60
    property int setChairIndex: 0
    property date setDate: selectedDay
    property string setPatID: ""
    property string setProviderID: ""
    property string setProcedureName: ""
    property string setProcedureComment: ""

    Settings {id: apptLoader}

    onVisibleChanged: {
        rootWin.dialogShown = visible;
    }

    function parseTime(inputTime) {
        //11:30 AM
        var hourMin = inputTime.split(":");
        var hour = parseInt(hourMin[0]);
        var min = hourMin[1].substring(0,2);
        var AMPM = inputTime.split(" ")[1];

        if(AMPM === "PM") {
            if(hour !== 12) {
                hour += 12;
            }
        }
        return (60*hour) + parseInt(min);
    }

    function parseTimeBlock(getTimeBlock, apptStartTime,apptDuration) {
        var returnMe = false;
        var openTimesList=getTimeBlock.split("|");
        var endingMins = apptStartTime + apptDuration;

        for(var i=0;i<openTimesList.length;i++) {
            var schChunk = openTimesList[i].trim();
            var schTimeParts = schChunk.split(" ");
            if(schChunk.startsWith("Open")) { //check before open
                var openTime = parseTime(schTimeParts[1] + " " +schTimeParts[2]);
                if(apptStartTime < openTime) {
                    return true;
                }
            }
            else if(schChunk.startsWith("Closed")) {
                var closingTime =parseTime(schTimeParts[1] + " " + schTimeParts[2]);
                if(endingMins > closingTime) {
                    return true;
                }
            }
            else if(schChunk.startsWith("Lunch")) {
                var lunchStart = parseTime(schTimeParts[6] + " " + schTimeParts[7]);
                var lunchDuration = parseInt(schTimeParts[3]);
                var lunchEnd = lunchStart + lunchDuration;
                if((apptStartTime > lunchStart) && (apptStartTime < lunchEnd)) {
                    return true;
                } else if((endingMins > lunchStart) && (endingMins < lunchEnd)) {
                    return true;
                }
            }
        }
        return returnMe;
    }

    function checkTime() {
        //first check if the clinic is open or not
        var dateObj = apptDate.myDate;
        var timeString = (startHour.currentIndex + 1) + ":" +startMin.currentText + " " + startAMPM.currentText;
        var myStartTime = parseTime(timeString);

        var dayList = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
        var dayOfWeek = dateObj.getDay();
        var isOpen = readOpenCloseTimes.value("Open"+dayList[dayOfWeek],"false")
        if( isOpen === "true") {
            var openTimes = readOpenCloseTimes.value(dayList[dayOfWeek]+"Hours","");
            var apptDuration = (durationBox.currentIndex+1) * 15;
            closedTimeWarning.visible = parseTimeBlock(openTimes,myStartTime,apptDuration);
        }
        else {
            closedTimeWarning.visible = true;
        }

        //Assuming we are open, check the chair
        doubleBookWarning.visible = false;
        if(!closedTimeWarning.visible) {
            var apps = scheduleDB.getAppointments(dateObj);
            for(var i=0;i<apps.length;i++) {
                apptLoader.fileName = apps[i];
                var apptStartTimeString = apptLoader.value("StartTime","12:00 AM");
                var startTimeMins = parseTime(apptStartTimeString);
                var checkerApptDur = parseInt(apptLoader.value("Duration",15));
                var endTimeMins = startTimeMins+checkerApptDur;
                var checkChair = apptLoader.value("Chair","");
                var myChair = chairBox.displayText;
                if(checkChair === myChair) {
                    var myEndTime = myStartTime + apptDuration;
                    if((myStartTime > startTimeMins)
                            && (myStartTime < endTimeMins)) {
                        doubleBookWarning.visible = true;
                    }
                    else if((myEndTime > startTimeMins)
                            && (myEndTime < endTimeMins)) {
                        doubleBookWarning.visible = true;
                    }
                }
            }

        }

    }

    function getProcedures(patID) {
        var returnMe = [];
        for (var i in comFuns.getAllTxPlanItems(patID)) {
            var txItemObj = comFuns.getAllTxPlanItems(patID)[i];
            returnMe.push(comFuns.makeTxItemString(txItemObj));
        }
        return returnMe;
    }

    ColumnLayout {
        Flickable {
            Layout.minimumWidth: contentWidth
            clip: true
            Layout.minimumHeight: 360
            Layout.preferredHeight: contentHeight
            Layout.maximumHeight: rootWin.height - 125
            contentHeight: addPane.height
            contentWidth: addPane.width
            CDTranslucentPane {
                id: addPane



                GridLayout {
                    columns: 2

                    CDHeaderLabel {
                        text: "Add New Appointment"
                        Layout.columnSpan: 2
                    }

                    Label {
                        text: "Date"
                        font.bold: true
                    }

                    CDCalendarDatePicker {
                        id: apptDate
                        onMyDateChanged: checkTime();
                    }

                    Label {
                        text: "Start Time"
                        font.bold: true
                    }
                    RowLayout {
                        ComboBox {
                            id: startHour
                            model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                            Layout.preferredWidth: 75
                            currentIndex: 7
                            onCurrentIndexChanged: {checkTime();}
                        }

                        Label {
                            text: ":"
                        }

                        ComboBox {
                            id: startMin
                            property var minModel: []
                            Layout.preferredWidth: 75
                            Component.onCompleted:  {
                                for(var i=0;i<60;i+=5) {
                                    if(i < 10) {
                                        minModel.push("0"+i);
                                    }
                                    else {
                                        minModel.push(i);
                                    }
                                }
                                model = minModel
                            }
                            onCurrentTextChanged: {checkTime();}
                        }

                        ComboBox {
                            id: startAMPM
                            model: ["AM","PM"]
                            Layout.preferredWidth: 75
                            onCurrentTextChanged: {checkTime();}
                        }
                    }

                    Label {
                        id: closedTimeWarning
                        Layout.fillWidth: true
                        Layout.columnSpan: 2
                        text: "Warning: Clinic is closed during this appointment"
                        visible: false
                        color: Material.color(Material.Red)
                    }

                    Label {
                        text: "Chair"
                        font.bold: true
                    }
                    ComboBox {
                        id: chairBox
                        Layout.fillWidth: true
                        onCurrentIndexChanged: {checkTime();}
                    }

                    Label {
                        id: doubleBookWarning
                        Layout.fillWidth: true
                        Layout.columnSpan: 2
                        text: "Warning: There is already an appointment in this chair and time"
                        visible: false
                        color: Material.color(Material.Red)
                    }

                    Label {
                        text: "Patient"
                        font.bold: true
                    }
                    CDPatientComboBox {
                        id: patBox
                        Layout.fillWidth: true

                        onTextChanged: schAlert.updateAlert();
                        onPatientSelected: {
                            repTxs.model = 0;
                            repTxs.model = comFuns.getAllTxPlanItems(patBox.text).length;
                        }
                    }

                    Button {
                        id:butPeriodic
                        text: "Add Exam"
                        visible: !getProcedures(patBox.text).includes("Periodic Exam") && patMan.getAllPatientIds().includes(patBox.text)
                        Layout.columnSpan: butProphy.visible ? 1 : 2
                        onClicked: {
                            var txObj = ({});

                            if(comFuns.hadACompExam(patBox.text)) {
                                txObj["ProcedureName"] = "Periodic Exam";
                                txObj["DCode"] = "D0120";
                            }
                            else {
                                txObj["ProcedureName"] = "Comprehensive Exam";
                                txObj["DCode"] = "D0150";
                            }

                            comFuns.addTxItem(patBox.text,"Primary","Unphased",txObj);
                            repTxs.model = comFuns.getAllTxPlanItems(patBox.text).length;
                            butPeriodic.visible = !getProcedures(patBox.text).includes("Periodic Exam")
                        }
                    }
                    Button {
                        id:butProphy
                        text: "Add Prophy"
                        visible: !getProcedures(patBox.text).includes("Prophy") && patMan.getAllPatientIds().includes(patBox.text)
                        Layout.columnSpan: butPeriodic.visible ? 1 : 2
                        onClicked: {
                            var txObj = ({});
                            txObj["ProcedureName"] = "Prophy";
                            txObj["DCode"] = "D1110";
                            comFuns.addTxItem(patBox.text,"Primary","Unphased",txObj);
                            repTxs.model = comFuns.getAllTxPlanItems(patBox.text).length;
                            butProphy.visible = !getProcedures(patBox.text).includes("Periodic Exam")
                        }
                    }

                    Label {
                        id: schAlert

                        function updateAlert() {
                            personalSettings.fileName = fLocs.getPersonalFile(patBox.text);
                            personalSettings.category = "Preferences"
                            text = personalSettings.value("ScheduleAlert","");
                            var doNotSchedule = JSON.parse(personalSettings.value("DoNotSchedule",false));
                            var dismissed = JSON.parse(personalSettings.value("Dismissed",false));
                            if(doNotSchedule || dismissed) {
                                addPane.backMaterialColor = Material.Purple;
                                addApptBut.Material.accent= Material.Yellow
                                addApptBut.text = "Double book";
                                text+= "Do not schedule the patient"
                            }
                            else {
                                addPane.backMaterialColor = Material.Green
                                addApptBut.Material.accent= Material.Green
                                addApptBut.text = "Add Appointment";
                            }
                        }

                        Layout.fillWidth: true
                        Layout.columnSpan: 2
                        visible: schAlert.text.length > 1
                        color: Material.color(Material.Red)

                        Settings {
                            id: personalSettings
                        }
                    }

                    CDDescLabel {
                        text: "Procedures"
                    }

                    Flickable {
                        Layout.fillWidth: true
                        height: 200
                        contentHeight: procedureCol.height
                        contentWidth: procedureCol.width
                        clip: true
                        flickableDirection: Flickable.VerticalFlick
                        ScrollBar.vertical: ScrollBar {}

                        ColumnLayout {
                            id: procedureCol
                            Layout.fillWidth: true

                            CDTextFileManager {id: textMan}

                            CDCommonFunctions {id: comFuns;}

                            property var selectedTxs : ({})

                            Repeater {
                                id: repTxs
                                model: comFuns.getAllTxPlanItems(patBox.text).length
                                CheckBox {
                                    property var txItemObj: comFuns.getAllTxPlanItems(patBox.text)[index]
                                    text: comFuns.makeTxItemString(txItemObj)
                                    onCheckStateChanged: {
                                        procedureCol.selectedTxs[JSON.stringify(txItemObj)] = checked;
                                    }
                                }
                            }
                        }
                    }

                    Label {
                        text: "Provider"
                        font.bold: true
                    }
                    CDProviderBox {
                        id: providerBox
                        Layout.fillWidth: true
                    }

                    Label {
                        text: "Duration"
                        font.bold: true
                    }
                    ComboBox {
                        id: durationBox
                        property var durModel: []
                        Layout.fillWidth: true
                        onCurrentIndexChanged: {checkTime();}
                        Component.onCompleted:  {
                            for(var i=15;i<=180;i+=15) {
                                var hours = Math.floor(i / 60);
                                var mins = i%60;
                                if(hours === 0) {
                                    durModel.push(mins + " minutes")
                                }
                                else if(hours === 1) {
                                    if(mins === 0) {
                                        durModel.push("1 hour")
                                    }
                                    else {
                                        durModel.push("1 hour and " + mins + " minutes")
                                    }
                                }
                                else {
                                    if(mins === 0) {
                                        durModel.push(hours + " hours")
                                    }
                                    else {
                                        durModel.push(hours + " hours and " + mins + " minutes")
                                    }
                                }
                            }
                            model = durModel
                            currentIndex =1; //30 mins
                        }
                    }

                    Label {
                        text: "Comments"
                        font.bold: true

                    }
                    TextField {
                        id: commentField
                        Layout.fillWidth: true
                    }

                }

            }

        }



        RowLayout {
            Layout.fillWidth: true

            CDCancelButton {
                onClicked: {
                    addAptDialog.close();
                }
            }

            Label {
                Layout.fillWidth: true
            }


            CDAddButton {
                id: addApptBut
                text: "Add Appointment"
                enabled: patBox.text.length > 5
                onClicked: {
                    var addDate = apptDate.myDate;
                    var addTime = (startHour.currentIndex + 1) + ":" +startMin.displayText + " " +
                            startAMPM.currentText;
                    var addChair = chairBox.displayText;
                    var addPatID = patBox.text;

                    var selectedPros = [];
                    var proKeys = Object.keys(procedureCol.selectedTxs);
                    for(var i=0;i<proKeys.length;i++) {
                        var key = proKeys[i];
                        if(procedureCol.selectedTxs[key]) {
                            selectedPros.push(JSON.parse(key));
                        }
                    }
                    var addProcedure = JSON.stringify(selectedPros);
                    var addProviderID = providerBox.getSelectedProviderID();
                    var addDuration = (durationBox.currentIndex+1) * 15;
                    var addComment = commentField.text;
                    scheduleDB.addAppointment(addDate, addTime, addChair,addPatID, addProcedure,addProviderID,
                                              addDuration,addComment);
                    currentDayFlickable.loadApptButtons();
                    addAptDialog.close();
                }

            }
        }

        onVisibleChanged: {
            apptDate.myDate = setDate;

            setStartTime = setStartTime- (setStartTime%5);
            var setStartHour = Math.floor(setStartTime / 60);
            startAMPM.currentIndex=0;
            if(setStartHour === 12) {
                startHour.currentIndex = 11;
                startAMPM.currentIndex = 1;
            }
            else if(setStartHour > 12) {
                startAMPM.currentIndex=1;
                startHour.currentIndex = setStartHour-13;
            }
            else {
                startHour.currentIndex = setStartHour -1;
            }
            var setStartMin = setStartTime% 60;
            startMin.currentIndex = setStartMin / 5;

            //I have no clue why I have to do this, but it is the only way
            //to make the model show up in the ComboBox
            var setMyModel= [];
            for(var i=0;i<rootWin.selectedDayChairs.length;i++) {
                setMyModel.push(rootWin.selectedDayChairs[i]);
            }
            chairBox.model = setMyModel;
            chairBox.currentIndex = setChairIndex;

            checkTime();

            repTxs.model = 0;
            procedureCol.selectedTxs =  ({});

            //in case we are adding another appointment
            commentField.text = "";
            patBox.text = "";

            rootWin.keyboardSaveButton = addApptBut;
        }

    }
}
