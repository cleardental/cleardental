// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

Dialog {
    signal jumpDateSelected(var newDate)

    background: Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .75
        radius: 10
    }

    CDTranslucentPane {
        ColumnLayout {
            CDHeaderLabel {
                text: "Search Appointments/Blockers"
            }

            TextField {
                id: searchField
                Layout.fillWidth: true
                onTextChanged: {
                    if(text.length > 2) {
                        searchResultList.refreshList();
                    }
                }
            }

            ListView {
                id: searchResultList
                Layout.fillWidth: true
                Layout.minimumHeight: width
                clip: true

                ScrollBar.vertical: ScrollBar { policy:ScrollBar.AsNeeded }

                property var searchResultData;

                CDScheduleManager {
                    id: schMan

                }

                function refreshList() {
                    searchResultData = schMan.getSearchResult(searchField.text);
                    //console.debug(JSON.stringify(searchResultData,null,"\t"));
                    searchResultList.model = searchResultData.length;

                }

                delegate: RowLayout {
                    property int jumpWidth: jumpButton.width
                    SearchResultRowLabel {
                        text: searchResultList.searchResultData[index]["Type"] === "Appointment" ?
                                  searchResultList.searchResultData[index]["PatientID"] : "Blocker"
                    }
                    SearchResultRowLabel {
                        text: searchResultList.searchResultData[index]["Comments"]
                    }
                    SearchResultRowLabel {
                        text: searchResultList.searchResultData[index]["Date"]
                    }
                    Button {
                        id: jumpButton
                        text: "Jump"
                        onClicked: {
                            var inputDateMonth = searchResultList.searchResultData[index]["DateMonth"] -1;
                            var inputDateDay = searchResultList.searchResultData[index]["DateDay"];
                            var inputDateYear = searchResultList.searchResultData[index]["DateYear"];

                            var inputDate = new Date(inputDateYear,inputDateMonth,inputDateDay)
                            jumpDateSelected(inputDate);
                        }
                    }
                }
            }
        }

    }

    onVisibleChanged: {
        if(visible) {
            searchField.forceActiveFocus();
        }

        rootWin.dialogShown = visible;
    }
}
