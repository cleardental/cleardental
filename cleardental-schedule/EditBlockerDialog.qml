// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0


CDTranslucentDialog {
    id: editBlocker

    CDToolLauncher{
        id: toolLauncher
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: apptWriter
        fileName: schButton.fileName
    }

    CDGitManager {
        id: gitMan
    }

    onVisibleChanged: {
        rootWin.dialogShown = visible;
    }

    ColumnLayout {
        CDTranslucentPane {
            backMaterialColor: Material.Grey
            GridLayout {
                columns: 2
                CDHeaderLabel{
                    text: "Edit blocker"
                    Layout.columnSpan: 2
                }
                CDDescLabel {
                    text: "Date"
                }

                RowLayout {
                    Label {
                        text: getDateString(selectedDay)
                    }
                    Button {
                        text: "Change Date"
                        ChangeApptDateDialog {
                            id: changeDateDia
                            currentDate: getDateString(selectedDay)
                            appointmentFileLocation: schButton.fileName
                            isBlocker: true
                            onAccepted: {
                                currentDayFlickable.loadApptButtons();
                            }
                        }

                        onClicked: {
                            changeDateDia.open();
                        }
                    }
                }

                CDDescLabel {
                    text: "Chair"
                }

                RowLayout {
                    ComboBox {
                        id: chairBox
                        enabled: !allChairsBox.checked
                        Component.onCompleted: {
                            //I have no clue why I have to do this, but it is the only way
                            //to make the model show up in the ComboBox
                            var setMyModel= [];
                            for(var i=0;i<rootWin.selectedDayChairs.length;i++) {
                                setMyModel.push(rootWin.selectedDayChairs[i]);
                            }
                            chairBox.model = setMyModel;
                            currentIndex = find(apptWriter.value("Chair"));
                        }
                    }

                    CheckBox {
                        id: allChairsBox
                        text: "Block Out All Chairs"
                        Component.onCompleted: {
                            checked = apptWriter.value("Chair","") === "ALL_CHAIRS"
                        }
                    }

                }

                CDDescLabel {
                    text: "Start Time"
                }

                RowLayout {
                    ComboBox {
                        id: startHour
                        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                        Layout.preferredWidth: 75
                        Component.onCompleted: {
                            currentIndex = indexOfValue(schButton.startTime.split(":")[0])
                        }
                    }

                    Label {
                        text: ":"
                    }

                    ComboBox {
                        id: startMin
                        property var minModel: []
                        Layout.preferredWidth: 75

                        Component.onCompleted: {
                            for(var i=0;i<60;i+=5) {
                                if(i < 10) {
                                    minModel.push("0"+i);
                                }
                                else {
                                    minModel.push(i);
                                }
                            }
                            model = minModel

                            currentIndex = indexOfValue(schButton.startTime.split(":")[1].split(" ")[0])
                        }
                    }

                    ComboBox {
                        id: startAMPM
                        model: ["AM","PM"]
                        Layout.preferredWidth: 75

                        Component.onCompleted: {
                            currentIndex = indexOfValue(schButton.startTime.split(":")[1].split(" ")[1]);
                        }
                    }
                }

                CDDescLabel {
                    text: "Duration"
                }

                ComboBox {
                    id: durationBox
                    property var durModel: []
                    Layout.minimumWidth: 220
                    Component.onCompleted:  {
                        for(var i=15;i<=180;i+=15) {
                            var hours = Math.floor(i / 60);
                            var mins = i%60;
                            if(hours === 0) {
                                durModel.push(mins + " minutes")
                            }
                            else if(hours === 1) {
                                if(mins === 0) {
                                    durModel.push("1 hour")
                                }
                                else {
                                    durModel.push("1 hour and " + mins + " minutes")
                                }
                            }
                            else {
                                if(mins === 0) {
                                    durModel.push(hours + " hours")
                                }
                                else {
                                    durModel.push(hours + " hours and " + mins + " minutes")
                                }
                            }
                        }
                        model = durModel

                        currentIndex= (schButton.duration/15)-1;
                    }
                }

                CDDescLabel {
                    text: "Comments"
                }

                TextField {
                    Layout.fillWidth: true
                    id: commentField
                    text: schButton.comments
                    selectByMouse: true
                }

                MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

                ColumnLayout {
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    CDDeleteButton {
                        Layout.alignment: Qt.AlignHCenter
                        text: "Delete Block"
                        onClicked: deleteDia.open();
                    }

                    CDTranslucentDialog {
                        id: deleteDia

                        CDTranslucentPane {
                            backMaterialColor: Material.Pink
                            ColumnLayout {
                                CDHeaderLabel {
                                    text: "Are you sure you want to delete the appointment?"
                                }

                                Button {
                                    text: "Yes, delete the appointment"
                                    highlighted: true
                                    Material.accent: Material.Red
                                    font.pointSize: 18
                                    onClicked: {
                                        scheduleDB.removeAppointment(schButton.fileName);
                                        currentDayFlickable.loadApptButtons();
                                    }
                                }

                                Button {
                                    text: "No, keep the appointment"
                                    highlighted: true
                                    Material.accent: Material.Green
                                    font.pointSize: 18
                                    onClicked: {
                                        schButtonDia.close();
                                        deleteDia.close()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            CDCancelButton {
                onClicked: {
                    editBlocker.reject();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDSaveAndCloseButton {
                onClicked: {
                    apptWriter.setValue("Duration",(durationBox.currentIndex+1) * 15);
                    var setStart = (startHour.currentIndex + 1) + ":" +
                            startMin.displayText + " " + startAMPM.currentText;

                    apptWriter.setValue("StartTime",setStart);
                    apptWriter.setValue("Comments",commentField.text);

                    if(allChairsBox.checked) {
                        apptWriter.setValue("Chair","ALL_CHAIRS");
                    }
                    else {
                        apptWriter.setValue("Chair",chairBox.currentText);
                    }

                    apptWriter.sync();
                    gitMan.commitData("Updated blocker for " + getDateString(selectedDay));
                    currentDayFlickable.loadApptButtons();
                    //editBlocker.accept(); //the dialog actually gets killed before this could get called
                }
            }
        }
    }

}
