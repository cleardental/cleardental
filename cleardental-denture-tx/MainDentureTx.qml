// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: "Denture Tx [ID:" + PATIENT_FILE_NAME + "]"

    property var treatmentPlansObj: ({})
    property string selectedTxPlan: ""
    property string fakeTxPlanName: "🌷🌹🌷" //for force refresh

    header: CDPatientToolBar {
        headerText: "Denture Treatment Planning:"

        ToolButton {
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: dentureDraw.open();
        }
    }

    Drawer {
        id: dentureDraw

        height: rootWin.height
        width: rootWin.width /3

        background: Rectangle {
            gradient: Gradient {
                GradientStop { position: 0.0; color: "white" }
                GradientStop { position: 0.5; color: "white" }
                GradientStop { position: 1.0; color: "transparent" }
            }
            anchors.fill: parent
        }

        CDImageWinDia {
            id: pathwayDia
            imgSource: "qrc:/guides/denturePathways.png"
        }

        ColumnLayout {
            anchors.left:  parent.left
            anchors.right: parent.right
            anchors.margins: 10
            CDCancelButton {
                Layout.fillWidth: true
                icon.name: ""
                text: "Show Denture Pathways"
                onClicked: pathwayDia.open();
            }
        }
    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: perioChartSettings
        fileName: fLocs.getPerioChartFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: hardTissueChart
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: pricesSettings
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Prices"
    }

    CDTextFileManager {
        id: textMan
    }

    CDGitManager {
        id: gitMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    function saveTreatment() {
        //First go through all the tx plan items and remove "temp" from them
        var txPlanNames = Object.keys( rootWin.treatmentPlansObj);
        for(var planNameI=0; planNameI < txPlanNames.length; planNameI++) {
            var txPlanName = txPlanNames[planNameI];
            var phaseNames = Object.keys( rootWin.treatmentPlansObj[txPlanName] );
            for(var phaseNameI=0; phaseNameI < phaseNames.length; phaseNameI++) {
                var phaseName = phaseNames[phaseNameI];
                var phaseLength = rootWin.treatmentPlansObj[txPlanName][phaseName].length;
                for(var i=0;i<phaseLength;i++) {
                    var txObj = rootWin.treatmentPlansObj[txPlanName][phaseName][i];
                    if("Temp" in txObj) {
                        delete rootWin.treatmentPlansObj[txPlanName][phaseName][i]["Temp"];
                    }
                }
            }
        }

        //Now save it to the actual file
        var txPlansJSON = JSON.stringify(rootWin.treatmentPlansObj,null,"\t");
        textMan.saveFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),txPlansJSON);
    }

    TxPlanSelectRow {
        id: txPlanSelectRow
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        anchors.top: parent.top
    }

    ExistingDentureStatusPane {
        id: existingPane
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        anchors.top: txPlanSelectRow.bottom
    }

    RemDenTxPlanStylesCol {
        id: remDenTxPlanStylesCol
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        height: 100
        anchors.top: existingPane.bottom
    }

    ToothInfoPopup {
        id: tInfoPop
        anchors.centerIn: parent
    }

    TxPlanPopup {
        id: txPlanPop
        anchors.centerIn: parent
    }

    GridLayout {
        id: toothGrid
        anchors.top: remDenTxPlanStylesCol.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        columns: 18
        Button {
            id: maxCDButton
            text: "Maxillary Complete\nDenture"
            checkable: true
            onCheckedChanged: {
                if(checked) {
                    maxRPDButton.checked = false;
                }
            }
        }
        Button {
            id: maxRPDButton
            text: "Maxillary Removable\nPartial Denture"
            checkable: true
            onCheckedChanged: {
                if(checked) {
                    maxCDButton.checked = false;
                }
            }
        }

        Repeater {
            model: 16
            TxPlanButton {
                toothNumb: index+1
                Layout.minimumWidth: 85
                Layout.minimumHeight: 85
            }
        }

        Button {
            id: manCDButton
            text: "Complete Mandibular\nDenture"
            checkable: true
            onCheckedChanged: {
                if(checked) {
                    manRPDButton.checked = false;
                }
            }
        }
        Button {
            id: manRPDButton
            text: "Partial Mandibular\nDenture"
            checkable: true
            onCheckedChanged: {
                if(checked) {
                    manCDButton.checked = false;
                }
            }
        }

        Repeater {
            model: 16
            TxPlanButton {
                toothNumb: 32-index
                Layout.minimumWidth: 85
                Layout.minimumHeight: 85
            }
        }
    }

    CDSaveAndCloseButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        onClicked: {
            saveTreatment();
            existingPane.saveData();
            gitMan.commitData("Updated treatment plan with dentures for " + PATIENT_FILE_NAME);
            Qt.quit();
        }
    }


    Component.onCompleted: {
        var txPlansJSON = textMan.readFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        if(txPlansJSON.length > 0) {
            rootWin.treatmentPlansObj = JSON.parse(txPlansJSON);
        }
        selectedTxPlan = Object.keys(rootWin.treatmentPlansObj)[0];
        existingPane.loadData();
    }
}
