// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    RowLayout {
        id: txPlanSelectRow

        function addNewTxPlan() {
            var txPlanNames = Object.keys(rootWin.treatmentPlansObj);
            var unphasedArray = [];
            if(txPlanNames.length === 1) {
                rootWin.treatmentPlansObj["Tx Plan 2"] = {"Unphased": []};
            }
            else {
                var nextTxPlanNumb = 3;
                for(var i=1;i<txPlanNames.length;i++) {
                    var parts = txPlanNames[i].split(" ");
                    var intVal = parseInt(parts[2]);
                    if(intVal > nextTxPlanNumb) {
                        nextTxPlanNumb = intVal;
                    }
                    else if(intVal === nextTxPlanNumb) {
                        nextTxPlanNumb++;
                    }
                }
                rootWin.treatmentPlansObj["Tx Plan " + nextTxPlanNumb] = {"Unphased": []};
            }

            txPlanBox.model =  Object.keys(rootWin.treatmentPlansObj);
            txPlanBox.currentIndex = txPlanBox.model.length -1;
        }

        Label {
            text: "Treatment Plan"
            font.bold: true
        }
        ComboBox {
            id: txPlanBox
            Layout.minimumWidth: 250
            model: Object.keys(rootWin.treatmentPlansObj)
            onCurrentIndexChanged: {
                rootWin.selectedTxPlan = model[currentIndex]
            }
        }
        Button {
            text: "Save Treatment Plans"
            highlighted: true
            Material.accent: Material.Blue
            icon.name: "document-save"
            onClicked: {
                saveTreatment();
            }
        }
        Button {
            text: "New Treatment Plan"
            highlighted: true
            Material.accent: Material.Green
            icon.name: "list-add"
            onClicked: txPlanSelectRow.addNewTxPlan()
        }
    }
}


