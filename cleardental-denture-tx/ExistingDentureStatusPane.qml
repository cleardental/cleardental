// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    backMaterialColor: Material.Orange
    ColumnLayout {
        RowLayout {
            CDHeaderLabel {
                text: "Patient Presented With"
            }
        }
        RowLayout {
            CDDescLabel {
                text: "Maxillary"
            }
            RadioButton {
                id: exMaxNothingRadioButton
                text: "Nothing"
                checked: true
            }
            RadioButton {
                id: exMaxCDRadioButton
                text: "Complete Denture"
            }
            RadioButton {
                id: exMaxRPDRadioButton
                text: "RPD"
            }
            CDDatePicker {
                id: exMaxDate
                visible: exMaxCDRadioButton.checked || exMaxRPDRadioButton.checked
            }
        }
        RowLayout {
            CDDescLabel {
                text: "Mandibular"
            }
            RadioButton {
                id: exManNothingRadioButton
                text: "Nothing"
                checked: true
            }
            RadioButton {
                id: exManCDRadioButton
                text: "Complete Denture"
            }
            RadioButton {
                id: exManRPDRadioButton
                text: "RPD"
            }
            CDDatePicker {
                id: exManDate
                visible: exManCDRadioButton.checked || exManRPDRadioButton.checked
            }
        }
    }

    Settings {
        id: exSaveData
        fileName: fLocs.getRemovableStatusFile(PATIENT_FILE_NAME)
        category: "Existing"
    }

    function loadData() {
        if(JSON.parse(exSaveData.value("Maxillary"))) {
            if(exSaveData.value("MaxillaryType") === "CD") {
                exMaxCDRadioButton.checked = true;
            }
            else {
                exMaxRPDRadioButton.checked = true;
            }
            exMaxDate.setDate(exSaveData.value("MaxillaryDate"))
        }
        if(JSON.parse(exSaveData.value("Mandibular"))) {
            if(exSaveData.value("MandibularType") === "CD") {
                exManCDRadioButton.checked = true;
            }
            else {
                exManRPDRadioButton.checked = true;
            }
            exManDate.setDate(exSaveData.value("MandibularDate"))
        }
    }

    function saveData() {
        exSaveData.setValue("Maxillary",exMaxDate.visible);
        if(exMaxDate.visible) {
            if(exMaxCDRadioButton.checked) {
                exSaveData.setValue("MaxillaryType","CD");
            }
            else {
                exSaveData.setValue("MaxillaryType","RPD");
            }
            exSaveData.setValue("MaxillaryDate",exMaxDate.getDate());
        }

        exSaveData.setValue("Mandibular",exManDate.visible);
        if(exManDate.visible) {
            if(exManCDRadioButton.checked) {
                exSaveData.setValue("MandibularType","CD");
            }
            else {
                exSaveData.setValue("MandibularType","RPD");
            }
            exSaveData.setValue("MandibularDate",exManDate.getDate());
        }
    }
}
