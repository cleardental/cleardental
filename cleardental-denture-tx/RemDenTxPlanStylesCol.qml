// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ColumnLayout {
    id: remDenTxPlanStylesCol

    property var screenList: [];

    property var cdImmModel: [
        "Interim Denture \u2192 New Complete Denture",
        "Immediate Denture \u2192 Lab Hard Reline same denture"
    ]
    
    property var cdNoExtModel: ["New Complete Denture"]
    
    property var rpdImmModel: [
        "Interim RPD \u2192 Final RPD",
        "Wait if needed \u2192 Final RPD"
    ]
    
    property var finalRPDMaterialModel: [
        "Cast Metal", "Valplast","Acrylic/Resin"
    ]

    function isAlreadyScheduledForExt(getTooth) {
        var returnMe = false;
        var txPlanObj = rootWin.treatmentPlansObj[rootWin.selectedTxPlan];
        var phaseNames = Object.keys(txPlanObj);
        for(var phasei=0;phasei<phaseNames.length;phasei++) {
            var phaseName = phaseNames[phasei];
            var phaseList = txPlanObj[phaseName];
            for(var txi =0; txi < phaseList.length; txi++) {
                var txObj =  phaseList[txi];
                if(("Tooth" in txObj) &&
                        (txObj["Tooth"] === getTooth) &&
                        (txObj["ProcedureName"] === "Extraction")) {
                    return true;
                }
            }
        }
        return returnMe;
    }

    function isMissing(getTooth) {
        if((getTooth < 1) || (getTooth > 32)) {
            return true;
        }
        var returnMe = false;
        var info = hardTissueChart.value(getTooth,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                return true;
            }
        }

        return returnMe;
    }

    function cleartxPlan(tempString) {
        if(rootWin.selectedTxPlan.length < 1) {
            return;
        }

        var txPlanObj = rootWin.treatmentPlansObj[rootWin.selectedTxPlan];
        var phaseNames = Object.keys(txPlanObj);
        for(var phasei=0;phasei<phaseNames.length;phasei++) {
            var phaseName = phaseNames[phasei];
            var phaseList = txPlanObj[phaseName];
            for(var txi =0; txi < phaseList.length; txi++) {
                var txObj =  phaseList[txi];
                if(("Temp" in txObj) &&
                        (txObj["Temp"]  === tempString) ) {
                    phaseList.splice(txi,1);
                    txi--;
                }
            }
        }

        var realTxPlanName = rootWin.selectedTxPlan
        rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
        rootWin.selectedTxPlan=realTxPlanName;
    }

    function txPlanMaxCD() {
        if(rootWin.selectedTxPlan.length < 2) {
            return;
        }

        cleartxPlan("MaxCD");
        var unphList = rootWin.treatmentPlansObj[rootWin.selectedTxPlan]
                ["Unphased"];
        var i=0;

        //extract any maxillary teeth
        for(i=0;i<=16;i++) {
            if( (!isMissing(i)) && (!isAlreadyScheduledForExt(i)) ) {
                var addExt = {
                    Tooth: i,
                    ProcedureName: "Extraction" ,
                    ExtractionType:  maxCDExt.checked ? "Surgical": "Regular",
                    DCode:  maxCDExt.checked ? "D7210": "D7140",
                    BasePrice: maxCDExt.checked ?
                                   pricesSettings.value("D7210",0) :
                                   pricesSettings.value("D7140",0),
                    Temp: "MaxCD"
                };
                unphList.push(addExt);
            }
        }

        if(maxCDStyleBox.currentIndex === 0 ) {//Interim->New
            var iterm = {
                ProcedureName: comFuns.getNameForCode("D5810"),
                DentureType: "Interim",
                Location: "UA",
                DCode: "D5810",
                BasePrice: pricesSettings.value("D5810",0),
                Temp: "MaxCD"
            }
            unphList.push(iterm);

            var newDen = {
                ProcedureName: comFuns.getNameForCode("D5110"),
                DentureType: "Regular",
                Location: "UA",
                DCode: "D5110",
                BasePrice: pricesSettings.value("D5110",0),
                Temp: "MaxCD"
            }
            unphList.push(newDen);
        }
        else if(maxCDStyleBox.currentIndex === 1) {//Immediate->Hard
            var imm = {
                ProcedureName: comFuns.getNameForCode("D5130"),
                DentureType: "Immediate",
                Location: "UA",
                DCode: "D5130",
                BasePrice: pricesSettings.value("D5130",0),
                Temp: "MaxCD"
            }
            unphList.push(imm);

            var hardReline = {
                ProcedureName: comFuns.getNameForCode("D5750"),
                Material: "Hard",
                RelineLocation: "Lab",
                Location: "UA",
                DCode: "D5750",
                BasePrice: pricesSettings.value("D5750",0),
                Temp: "MaxCD"
            }
            unphList.push(hardReline);
        }

        if(maxCDReline.checked) {
            var softReline = {
                ProcedureName: comFuns.getNameForCode("D5730"),
                Material: "Soft",
                RelineLocation: "Chair",
                Location: "UA",
                DCode: "D5730",
                BasePrice: pricesSettings.value("D5730",0),
                Temp: "MaxCD"
            }
        }

        var realTxPlanName = rootWin.selectedTxPlan
        rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
        rootWin.selectedTxPlan=realTxPlanName;
    }


    function txPlanManCD() {
        if(rootWin.selectedTxPlan.length < 2) {
            return;
        }

        cleartxPlan("ManCD");
        var unphList = rootWin.treatmentPlansObj[rootWin.selectedTxPlan]
                ["Unphased"];
        var i=0;

        //extract any mandibular teeth
        for(i=17;i<=32;i++) {
            if( (!isMissing(i)) && (!isAlreadyScheduledForExt(i)) ) {
                var addExt = {
                    Tooth: i,
                    ProcedureName: "Extraction" ,
                    ExtractionType:  maxCDExt.checked ? "Surgical": "Regular",
                    DCode:  maxCDExt.checked ? "D7210": "D7140",
                    BasePrice: maxCDExt.checked ?
                                   pricesSettings.value("D7210",0) :
                                   pricesSettings.value("D7140",0),
                    Temp: "ManCD"
                };
                unphList.push(addExt);
            }
        }

        if(manCDStyleBox.currentIndex === 0 ) {//Interim->New
            var iterm = {
                ProcedureName: comFuns.getNameForCode("D5811"),
                DentureType: "Interim",
                Location: "LA",
                DCode: "D5811",
                BasePrice: pricesSettings.value("D5811",0),
                Temp: "ManCD"
            }
            unphList.push(iterm);

            var newDen = {
                ProcedureName: comFuns.getNameForCode("D5120"),
                DentureType: "Regular",
                Location: "LA",
                DCode: "D5120",
                BasePrice: pricesSettings.value("D5120",0),
                Temp: "ManCD"
            }
            unphList.push(newDen);
        }
        else if(manCDStyleBox.currentIndex === 1) {//Immediate->Hard
            var imm = {
                ProcedureName: comFuns.getNameForCode("D5140"),
                DentureType: "Immediate",
                Location: "LA",
                DCode: "D5140",
                BasePrice: pricesSettings.value("D5140",0),
                Temp: "ManCD"
            }
            unphList.push(imm);

            var hardReline = {
                ProcedureName: comFuns.getNameForCode("D5751"),
                Material: "Hard",
                RelineLocation: "Lab",
                Location: "LA",
                DCode: "D5751",
                BasePrice: pricesSettings.value("D5751",0),
                Temp: "ManCD"
            }
            unphList.push(hardReline);
        }

        if(manCDReline.checked) {
            var softReline = {
                ProcedureName: comFuns.getNameForCode("D5731"),
                Material: "Soft",
                RelineLocation: "Chair",
                Location: "LA",
                DCode: "D5731",
                BasePrice: pricesSettings.value("D5731",0),
                Temp: "ManCD"
            }
        }

        var realTxPlanName = rootWin.selectedTxPlan
        rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
        rootWin.selectedTxPlan=realTxPlanName;
    }

    function txPlanMaxRPD() {
        if(rootWin.selectedTxPlan.length < 2) {
            return;
        }

        cleartxPlan("MaxRPD");
        var unphList = rootWin.treatmentPlansObj[rootWin.selectedTxPlan]
                ["Unphased"];

        if(maxRPDStyleBox.currentIndex === 0) { //Interim -> Final
            var iterm = {
                ProcedureName: comFuns.getNameForCode("D5820"),
                DentureType: "Interim",
                Location: "UA",
                DCode: "D5820",
                BasePrice: pricesSettings.value("D5820",0),
                Temp: "MaxRPD"
            }
            unphList.push(iterm);
        }

        //For for the actual final RPD
        if(maxRPDFinalMat.currentIndex === 0) {
            var castMetal = {
                ProcedureName: comFuns.getNameForCode("D5213"),
                DentureType: "Final",
                Material: "Cast Metal",
                Location: "UA",
                DCode: "D5213",
                BasePrice: pricesSettings.value("D5213",0),
                Temp: "MaxRPD"
            }
            unphList.push(castMetal);
        }
        else if(maxRPDFinalMat.currentIndex === 1) {
            var valplast = {
                ProcedureName: comFuns.getNameForCode("D5225"),
                DentureType: "Final",
                Material: "Flexible",
                Location: "UA",
                DCode: "D5225",
                BasePrice: pricesSettings.value("D5225",0),
                Temp: "MaxRPD"
            }
            unphList.push(valplast);
        }
        else if(maxRPDFinalMat.currentIndex === 2) {
            var acryl = {
                ProcedureName: comFuns.getNameForCode("D5211"),
                DentureType: "Final",
                Material: "Resin",
                Location: "UA",
                DCode: "D5211",
                BasePrice: pricesSettings.value("D5211",0),
                Temp: "MaxRPD"
            }
            unphList.push(acryl);
        }

        var realTxPlanName = rootWin.selectedTxPlan
        rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
        rootWin.selectedTxPlan=realTxPlanName;
    }

    function txPlanManRPD() {
        if(rootWin.selectedTxPlan.length < 2) {
            return;
        }

        cleartxPlan("ManRPD");
        var unphList = rootWin.treatmentPlansObj[rootWin.selectedTxPlan]
                ["Unphased"];

        if(manRPDStyleBox.currentIndex === 0) { //Interim -> Final
            var iterm = {
                ProcedureName: comFuns.getNameForCode("D5821"),
                DentureType: "Interim",
                Location: "LA",
                DCode: "D5821",
                BasePrice: pricesSettings.value("D5821",0),
                Temp: "ManRPD"
            }
            unphList.push(iterm);
        }

        //For for the actual final RPD
        if(manRPDFinalMat.currentIndex === 0) {
            var castMetal = {
                ProcedureName: comFuns.getNameForCode("D5214"),
                DentureType: "Final",
                Material: "Cast Metal",
                Location: "LA",
                DCode: "D5214",
                BasePrice: pricesSettings.value("D5214",0),
                Temp: "ManRPD"
            }
            unphList.push(castMetal);
        }
        else if(manRPDFinalMat.currentIndex === 1) {
            var valplast = {
                ProcedureName: comFuns.getNameForCode("D5226"),
                DentureType: "Final",
                Material: "Flexible",
                Location: "LA",
                DCode: "D5226",
                BasePrice: pricesSettings.value("D5226",0),
                Temp: "ManRPD"
            }
            unphList.push(valplast);
        }
        else if(manRPDFinalMat.currentIndex === 2) {
            var acryl = {
                ProcedureName: comFuns.getNameForCode("D5212"),
                DentureType: "Final",
                Material: "Resin",
                Location: "LA",
                DCode: "D5212",
                BasePrice: pricesSettings.value("D5212",0),
                Temp: "ManRPD"
            }
            unphList.push(acryl);
        }

        var realTxPlanName = rootWin.selectedTxPlan
        rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
        rootWin.selectedTxPlan=realTxPlanName;

    }

    function showGuideWindow(setImgSrc) {
        for(var i=1;i<Qt.application.screens.length;i++) {
            var res = showImgComp.createObject();
            res.x=0;
            res.y=1080*(i-1);
            res.imgSrc = setImgSrc
            screenList.push(res);
        }

    }

    function hideGuideWindow() {
        while(screenList.length > 0) {
            screenList.pop().destroy();
        }
    }

    Component {
        id: showImgComp

        CDAppWindow {
            x: 0
            y: 1080
            width: 1920
            height: 1080
            visibility: ApplicationWindow.Windowed
            property alias imgSrc: fillImg.source

            Image {
                id: fillImg
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    
    RowLayout {
        id: maxCDTxRow
        visible: maxCDButton.checked
        onVisibleChanged: {
            if(visible) {
                txPlanMaxCD();
            }
            else {
                cleartxPlan("MaxCD");
            }
        }
        spacing: 20
        CDButton {
            text: "Show CD Image"
            checkable: true
            checked: false

            onCheckedChanged: {
                if(checked) {
                    showGuideWindow("qrc:/guides/zaleske-matrix-CD.webp")
                }
                else {
                    hideGuideWindow()
                }
            }
        }

        CDDescLabel {
            text: "Maxillary Complete Denture"
        }
        ComboBox {
            id: maxCDStyleBox
            model: remDenTxPlanStylesCol.cdImmModel
            Layout.minimumWidth: 400
            onCurrentIndexChanged: {
                txPlanMaxCD();
            }
        }
        CheckBox {
            id: maxCDReline
            text: "Soft Reline after 6 months"
            checked: true
            onCheckStateChanged: {
                txPlanMaxCD();
            }
        }

        CheckBox {
            id: maxCDExt
            text: "Assume extractions will be surgical"
            onCheckStateChanged: {
                txPlanMaxCD();
            }
        }
    }
    
    RowLayout {
        id: maxRPDTxRow
        visible: maxRPDButton.checked
        onVisibleChanged: {
            if(visible) {
                txPlanMaxRPD();
            }
            else {
                cleartxPlan("MaxRPD");
            }
        }
        spacing: 20

        CDButton {
            text: "Show RPD Image"
            checkable: true
            checked: false

            onCheckedChanged: {
                if(checked) {
                    showGuideWindow("qrc:/guides/zaleske-matrix-RPD-metal.jpg")
                }
                else {
                    hideGuideWindow()
                }
            }
        }

        Label {
            text: "Maxillary RPD"
            font.bold: true
        }
        ComboBox {
            id: maxRPDStyleBox
            model: remDenTxPlanStylesCol.rpdImmModel
            Layout.minimumWidth: 250
            onCurrentIndexChanged: {
                txPlanMaxRPD();
            }
        }
        Label {
            Layout.minimumWidth: 100
        }
        
        Label {
            text: "Final RPD Material"
            font.bold: true
        }
        ComboBox {
            id: maxRPDFinalMat
            model: remDenTxPlanStylesCol.finalRPDMaterialModel
            Layout.minimumWidth: 150
            onCurrentIndexChanged: {
                txPlanMaxRPD();
            }
        }
    }
    
    RowLayout {
        id: manCDTxRow
        visible: manCDButton.checked
        onVisibleChanged: {
            if(visible) {
                txPlanManCD();
            }
            else {
                cleartxPlan("ManCD");
            }
        }
        spacing: 20

        CDButton {
            text: "Show CD Image"
            checkable: true
            checked: false

            onCheckedChanged: {
                if(checked) {
                    showGuideWindow("qrc:/guides/zaleske-matrix-CD.webp")
                }
                else {
                    hideGuideWindow()
                }
            }
        }

        Label {
            text: "Mandibular Complete Denture"
            font.bold: true
        }
        ComboBox {
            id: manCDStyleBox
            model: remDenTxPlanStylesCol.cdImmModel
            Layout.minimumWidth: 400
            onCurrentIndexChanged: {
                txPlanManCD();
            }
        }
        CheckBox {
            id: manCDReline
            text: "Soft Reline after 6 months"
            checked: true
        }

        CheckBox {
            id: manCDExt
            text: "Assume extractions will be surgical"
            onCheckStateChanged: {
                txPlanManCD();
            }
        }
    }
    
    RowLayout {
        id: manRPDTxRow
        visible: manRPDButton.checked
        onVisibleChanged: {
            if(visible) {
                txPlanManRPD();
            }
            else {
                cleartxPlan("ManRPD");
            }
        }
        spacing: 20

        CDButton {
            text: "Show RPD Image"

            onCheckedChanged: {
                if(checked) {
                    showGuideWindow("qrc:/guides/zaleske-matrix-RPD-metal.jpg")
                }
                else {
                    hideGuideWindow()
                }
            }
        }

        Label {
            text: "Mandibular RPD"
            font.bold: true
        }
        ComboBox {
            id: manRPDStyleBox
            model: remDenTxPlanStylesCol.rpdImmModel
            Layout.minimumWidth: 250
            onCurrentIndexChanged: {
                txPlanManRPD();
            }
        }
        Label {
            Layout.minimumWidth: 100
        }
        
        Label {
            text: "Final RPD Material"
            font.bold: true
        }
        ComboBox {
            id: manRPDFinalMat
            model: remDenTxPlanStylesCol.finalRPDMaterialModel
            Layout.minimumWidth: 150
            onCurrentIndexChanged: {
                txPlanManRPD();
            }
        }
    }
}
