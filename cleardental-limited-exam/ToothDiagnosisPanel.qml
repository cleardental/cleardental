// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    ColumnLayout {
        anchors.centerIn: parent

        CDTranslucentPane {
            backMaterialColor: Material.Yellow
            CDHeaderLabel {
                text: "Tooth: " + rootWin.ccTooth
            }
            Layout.alignment: Qt.AlignHCenter
        }

        RowLayout {
            CDTranslucentPane {
                backMaterialColor: Material.Cyan
                Layout.fillWidth: true
                Layout.fillHeight: true
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Signs and Symptoms"
                    }

                    RowLayout {
                        CDDescLabel {
                            text: "Issue started"
                        }
                        ComboBox {
                            editable: true
                            model: ["Recently (less than 24 hours ago)",
                                "Some time ago (between a day and 2 weeks ago)",
                                "I've had it for a while now (2 weeks to 6 months)",
                                "For a long time (more than 6 months)"]
                            Layout.fillWidth: true
                        }

                    }

                    RowLayout {
                        CheckBox {
                            id: chipBox
                            text: "Chip/Fracture"
                        }
                        CheckBox {
                            id: senBox
                            text: "Sensitive"
                        }

                        CheckBox {
                            id: cariousBox
                            text: "Carious lesion"
                        }

                        RowLayout {
                            opacity: (chipBox.checked || senBox.checked || cariousBox.checked) ? 1 : 0
                            enabled: opacity > 0
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }

                            Label {
                                Layout.minimumWidth: 20
                            }

                            Label {
                                text: "Affected Surfaces"
                                font.bold: true
                            }
                            Label {
                                Layout.minimumWidth: 20
                            }

                            CDSurfaceRow {
                                id: surRow
                                workingToothNumb: rootWin.ccTooth
                            }
                        }
                    }



                    RowLayout {
                        CheckBox {
                            id: painBox
                            text: "Pain"
                        }
                        RowLayout {
                            opacity: painBox.checked ? 1: 0
                            enabled: opacity != 0
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            CheckBox {
                                id: painPerBox
                                text: "On percussion"
                            }

                            CheckBox {
                                id: painDullBox
                                text: "Dull/aching"
                            }

                            CheckBox {
                                id: noLongerPain
                                text: "It actually don't hurt <b>now</b>"
                            }

                            CDTranslucentPane {
                                backMaterialColor: Material.Orange

                                opacity: painPerBox.checked ? 1: 0
                                enabled: opacity != 0
                                Behavior on opacity {
                                    PropertyAnimation {
                                        duration: 300
                                    }
                                }

                                RowLayout {
                                    CDDescLabel {
                                        text: "Duration"
                                    }
                                    RadioButton {
                                        id: sharpPain
                                        text: "Sharp / short"
                                        checked: true
                                    }
                                    RadioButton {
                                        id: longPain
                                        text: "Long"
                                    }
                                }
                            }
                        }
                    }
                }
            }

            CDReviewRadiographPane {
                id: radioRev
                Layout.minimumWidth: 360
                toothToReview: rootWin.ccTooth
            }

        }



        CDTranslucentPane {
            backMaterialColor: Material.Pink
            Layout.fillWidth: true
            ColumnLayout {
                Label {
                    text: "Diagnosis"
                    font.pointSize: 24
                    font.underline: true
                }

                RowLayout {
                    CheckBox {
                        id: pulpitis
                        text: "Pulpitis"
                    }
                    RadioButton {
                        id: revPup
                        text: "Reversible"
                        checked: true
                        opacity: pulpitis.checked ? 1: 0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }

                    }
                    RadioButton {
                        id: irrPup
                        text: "Irreversible"
                        opacity: pulpitis.checked ? 1: 0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                }

                RowLayout {
                    CheckBox {
                        id: infectedBox
                        text: "Infected"
                    }
                    CheckBox {
                        id: paInfectionBox
                        text: "Periapical lesion (radiographic)"
                        opacity: infectedBox.checked ? 1: 0
                        enabled: opacity != 0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                    CheckBox {
                        id: nonVitalBox
                        text: "Non-vital"
                        opacity: infectedBox.checked ? 1: 0
                        enabled: opacity != 0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                    CheckBox {
                        id: swellingBox
                        text: "Swelling"
                        opacity: infectedBox.checked ? 1: 0
                        enabled: opacity != 0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                }

                RowLayout {
                    CheckBox {
                        id: otherDxBox
                        text: "Other Diagnosis"
                    }
                    TextField {
                        id: otherDxField
                        visible: opacity != 0
                        opacity: otherDxBox.checked ? 1: 0
                        placeholderText: "Please enter the diagnosis"
                        Layout.fillWidth: true
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                }

            }
        }

        CDTranslucentPane {
            Layout.fillWidth: true
            ColumnLayout {
                CDHeaderLabel {
                    text: "Treatment"
                }
                RowLayout {
                    RadioButton {
                        id: watchToothBut
                        text: "Watch Tooth"
                    }
                }

                Row {
                    RadioButton {
                        id: saveToothBut
                        text: "Save Tooth"
                    }
                    RowLayout {
                        CheckBox {
                            id: composite
                            text: "Composite"
                        }
                        CDSurfaceRow {
                            id: compSurfaces
                            workingToothNumb: rootWin.ccTooth
                            visible: opacity > 0
                            opacity: composite.checked ? 1 : 0

                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                        }

                        opacity: saveToothBut.checked ? 1: 0

                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }

                    }

                    RowLayout {
                        id: deep
                        CheckBox {
                            id: rctBox
                            text: "Root Canal Treatment"
                        }
                        CheckBox {
                            id: postCoreBox
                            text: "Post/Core"
                        }

                        CheckBox {
                            id: crownBox
                            text: "Crown"
                        }

                        opacity: saveToothBut.checked ? 1: 0

                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }

                    RowLayout {
                        id: otherTx
                        CheckBox {
                            id: otherCheck
                            text: "Other"
                        }
                        ComboBox {
                            id: otherTxBox
                            visible: otherCheck.checked

                            CDConstLoader {
                                id: constLoader
                            }

                            valueRole: "DCode"
                            textRole: "ProcedureName"
                            editable: true
                            Layout.minimumWidth: 360

                            Component.onCompleted: {
                                var jsonText = constLoader.getDCodesJSON();
                                var jsonObj = JSON.parse(jsonText);
                                model = jsonObj;
                            }
                        }
                    }

                    move: Transition {
                        NumberAnimation {
                            properties: "x,y"
                            duration: 200
                        }
                    }
                }

                RowLayout {
                    RadioButton {
                        id: extToothBut
                        text: "Extract Tooth"
                    }

                    RowLayout {
                        RadioButton {
                            id: simpleExtBut
                            text: "Simple Extraction"
                        }
                        RadioButton {
                            id: surgicalExtBut
                            text: "Surgical Extraction"
                        }

                        CheckBox {
                            id: isRestorableBox
                            text: "Tooth is actually restorable\nbut patient doesn't want to save it"
                        }

                        opacity: extToothBut.checked ? 1: 0

                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                }

                ButtonGroup {
                    id: butGroup
                    buttons: [watchToothBut,saveToothBut,extToothBut]
                }
            }
        }
    }

    CDFinishProcedureButton {
        id: fin
        opacity: butGroup.checkState === Qt.PartiallyChecked ? 1: 0
        enabled: opacity > 0
        Material.accent: Material.Cyan
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        CDCommonFunctions {
            id: comFuns
        }

        onClicked: {
            var limitedExamObj = ({});
            limitedExamObj["ProcedureName"] = "Limited Exam";
            limitedExamObj["DCode"] = "D0140";

            var add1stPA = ({});
            add1stPA["ProcedureName"] = "First periapical"
            add1stPA["DCode"] = "D0220"
            add1stPA["Tooth"] = rootWin.ccTooth

            var caseNote= "Patient presented for a limited exam.\n";
            caseNote += "CC: Tooth #" + rootWin.ccTooth + "\n";

            if(chipBox.checked || senBox.checked || cariousBox.checked) {
                var chipLine = "Tooth #" + rootWin.ccTooth + " presented with " + surRow.choosenSurfaces + " ";
                if(chipBox.checked) {
                    chipLine += "fracture "
                }
                if(senBox.checked) {
                    chipLine += "sensitivity "
                }
                if(cariousBox.checked) {
                    chipLine += "caries "
                }
                caseNote += chipLine.trim() + ".\n";
            }

            if(painBox.checked) {
                var painLine = "Tooth #" + rootWin.ccTooth + " presented with ";
                var percusLength = "";
                if(sharpPain.checked) {
                    percusLength = "sharp / short"
                }
                else {
                    percusLength = "long / lingering"
                }

                if(painPerBox.checked && painDullBox.checked) {
                    painLine += " pain on percussion (" + percusLength + ") and dull aching pain"
                }
                else if(painPerBox.checked) {
                    painLine += percusLength + " pain on percussion"
                }
                else {
                    painLine += " dull aching pain"
                }
                if(noLongerPain.checked) {
                    painLine += ", but the tooth today does not present with pain.\n"
                }
                else {
                    painLine += ".\n";
                }
                caseNote += painLine;
            }

            caseNote += "Dx: ";
            var dXs = [];

            if(pulpitis.checked) {
                if(revPup.checked) {
                    dXs.push("Reversible Pulpitis");
                }
                else {
                    dXs.push("Irreversible Pulpitis");
                }
            }

            if(paInfectionBox.checked) {
                dXs.push("Infected tooth with a periapical lesion (radiographic)");
            }

            if(nonVitalBox.checked) {
                dXs.push("Infected and nonvital tooth");
            }

            if(swellingBox.checked) {
                dXs.push("Infected tooth with swelling");
            }

            if(otherDxBox.checked) {
                dXs.push(otherDxField.text);
            }

            caseNote += dXs.join("; ") + "\n";

            if(watchToothBut.checked) {
                var watchObj = ({});
                watchObj["Tooth"] = rootWin.ccTooth;
                watchObj["ProcedureName"] = "Watch";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",watchObj);
            }


            else if(saveToothBut.checked) {
                var saveStrs = [];

                if(composite.checked) {
                    saveStrs.push("a " + compSurfaces.getSurfaces() + " Composite");
                    var compObj = ({});
                    compObj["Tooth"] = rootWin.ccTooth;
                    compObj["ProcedureName"] = "Composite";
                    compObj["Surfaces"] = compSurfaces.getSurfaces();
                    if(compSurfaces.isPost) {
                        switch(compObj["Surfaces"].length) {
                        default:
                        case 1:
                            compObj["DCode"] = "D2391";
                            break;
                        case 2:
                            compObj["DCode"] = "D2392";
                            break;
                        case 3:
                            compObj["DCode"] = "D2393";
                            break;
                        case 4:
                        case 5:
                            compObj["DCode"] = "D2394";
                            break;
                        }
                    }
                    else {
                        switch(compObj["Surfaces"].length) {
                        default:
                        case 1:
                            compObj["DCode"] = "D2330";
                            break;
                        case 2:
                            compObj["DCode"] = "D2331";
                            break;
                        case 3:
                            compObj["DCode"] = "D2332";
                            break;
                        case 4:
                        case 5:
                            compObj["DCode"] = "D2335";
                            break;
                        }
                    }
                    comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",compObj);
                }

                if(rctBox.checked) {
                    saveStrs.push(rctBox.text);
                    var rctObj = ({});
                    rctObj["Tooth"] = rootWin.ccTooth;
                    rctObj["ProcedureName"] = "Root Canal Treatment";
                    if(comFuns.isPost(rootWin.ccTooth)) {
                        if((rootWin.ccTooth === 4) ||
                                (rootWin.ccTooth === 5) ||
                                (rootWin.ccTooth === 12) ||
                                (rootWin.ccTooth === 13) ||
                                (rootWin.ccTooth === 20) ||
                                (rootWin.ccTooth === 21) ||
                                (rootWin.ccTooth === 28) ||
                                (rootWin.ccTooth === 29) ){ //premolar
                            rctObj["DCode"] = "D3320";

                        } else { //molar
                            rctObj["DCode"] = "D3330";
                        }
                    }
                    else { //anterior
                        rctObj["DCode"] = "D3310";
                    }
                    comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",rctObj);
                }

                if(postCoreBox.checked) {
                    saveStrs.push(postCoreBox.text);
                    var postCoreObj = ({});
                    postCoreObj["ProcedureName"] = "Post and Core";
                    postCoreObj["Tooth"] = rootWin.ccTooth;
                    postCoreObj["Material"] = "Cast"; //TODO: make it easy to change the pre-fab
                    postCoreObj["DCode"] = "D2952";
                    comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",postCoreObj);
                }

                if(crownBox.checked) {
                    saveStrs.push(crownBox.text);
                    var crownObj = ({});
                    crownObj["Tooth"] = rootWin.ccTooth;
                    crownObj["ProcedureName"] = "Crown";
                    crownObj["Material"] = "Zirconia" //TODO: go based on doctor's prefs
                    crownObj["DCode"] = "D2740";
                    comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",crownObj);
                }

                if(otherCheck.checked) {
                    var customTxObj = ({});
                    customTxObj["Tooth"] = rootWin.ccTooth;
                    customTxObj["ProcedureName"] = otherTxBox.currentText;
                    customTxObj["DCode"] =otherTxBox.currentValue;
                    comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",customTxObj);
                }

                caseNote += "Tx: Patient wishes to save the tooth with " + saveStrs.join(" and ");
            }
            else { //extract the tooth
                if(isRestorableBox.checked) {
                    caseNote += "Patient was made aware that the tooth was in fact restorable but elected to have " +
                            "the tooth extracted instead.\n"
                }
                caseNote += "Tx: Extraction of tooth #" + rootWin.ccTooth + "\n"

                var extObj = ({});
                extObj["Tooth"] = rootWin.ccTooth;
                extObj["ProcedureName"] = "Extraction";

                if(simpleExtBut.checked) {
                    extObj["DCode"] = "D7140";
                    extObj["ExtractionType"] = "Regular";
                }
                else {
                    extObj["DCode"] = "D7210";
                    extObj["ExtractionType"] = "Surgical";
                }
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",extObj);
            }

            comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,add1stPA);
            comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,limitedExamObj);
            finProcedDia.txItemsToComplete = comFuns.findTxDCode(PATIENT_FILE_NAME,"D0140").concat(
                        comFuns.findTxDCode(PATIENT_FILE_NAME,"D0220"));
            finProcedDia.caseNoteString = caseNote;
            finProcedDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finProcedDia
    }
}

