// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: takeRadioCol


    CDSensorPane {
        id: sensPane
        anchors.centerIn: parent
    }

    CDButton {
        text: "Next"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        icon.name: "go-next"
        Material.accent: Material.Lime
        highlighted: true

        //visible: opacity > 0
        //opacity: sensPane.doneTakingRadiographs ? 1 : 0
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
        onClicked: {
            mainStack.push("ToothDiagnosisPanel.qml")
        }
    }

    Component.onCompleted: {
        sensPane.prepareForTooth(rootWin.ccTooth);
    }

}
