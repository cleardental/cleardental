// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10
import QtMultimedia 5.12

CDTransparentPage {
    id: videoPage

    property alias showCeiling: showCeilingSwitch.checked
    property bool showWall: showWallSwitch.checked

    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            CDHeaderLabel {
                text: rootWin.selectedMediaCat + " Video"
            }
            Label {
                text: videoPlaylist.currentItemSource
            }
            RowLayout {
                CDDescLabel {
                    text: "Show Ceiling"
                }
                Switch {
                    id: showCeilingSwitch
                    checked: true
                }
            }

            RowLayout {
                CDDescLabel {
                    text: "Show Wall"
                }
                Switch {
                    id: showWallSwitch
                    checked: true
                }
            }

            RowLayout {
                Layout.fillWidth: true
                CDButton {
                    text: "Previous"
                    enabled:  videoPlaylist.currentIndex != 0
                    onClicked: {
                        videoPlaylist.previous();
                    }
                }
                Label {
                    Layout.fillWidth: true
                }
                CDButton {
                    text: "Next"
                    onClicked: {
                        videoPlaylist.next();
                    }
                }
            }
            RowLayout {
                Layout.fillWidth: true
                CDDescLabel {
                    text: "Volume"
                }
                Slider {
                    id: volSilder
                    Layout.fillWidth: true
                    from: 0
                    to: 1
                    value: 1
                }
            }
            ProgressBar {
                Layout.fillWidth: true
                to: audioComp.duration
                value: audioComp.position
            }
        }
    }

    Audio {
        id: audioComp
        playlist: videoPlaylist
        volume: volSilder.value
    }

    Playlist {
        id: videoPlaylist
        playbackMode: Playlist.Loop
    }

    Component {
        id: makeWindow

        CDAppWindow {
            function start() {
                theVid.play();
            }

            x: 0
            y: 0
            width: 1920
            height: 1080
            property bool onTop: false
            Video {
                id: theVid
                playlist: videoPlaylist
                anchors.fill: parent
                volume: 0
            }
            visible: onTop ? videoPage.showCeiling :videoPage.showWall

            Label {
                id: currTimeLabel

                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.margins: 25
                font.pixelSize: 72
                color: Material.backgroundColor
                horizontalAlignment: Qt.AlignRight
                styleColor: Material.foreground
                style: Text.Outline

                function getDateTimeString() {
                    var currentTime = new Date();
                    text= currentTime.toLocaleTimeString('en-US') + "\n" + currentTime.toLocaleDateString();
                }

                SequentialAnimation {
                    id: updateTimeAni
                    loops: Animation.Infinite
                    ScriptAction {
                        script: currTimeLabel.getDateTimeString()
                    }
                    PauseAnimation { duration: 15 * 1000 }
                }

                Component.onCompleted: updateTimeAni.running = true
            }
        }
    }

    Component.onCompleted: {
        var makeVideoList = mediaFiles.getVideoList(rootWin.selectedMediaCat);
        for(var i=0;i<makeVideoList.length;i++) {
            videoPlaylist.addItem("file://" + makeVideoList[i]);
        }
        audioComp.play();

        for(var screenI=0; screenI <= 1;screenI++) {
            var window    = makeWindow.createObject(videoPage);
            if(screenI === 0) {
                window.y = 0;
                window.onTop = true;
            }
            else if(screenI === 1) {
                window.y = 1080;
                window.onTop = false;
            }
            window.start();
        }
    }
}
