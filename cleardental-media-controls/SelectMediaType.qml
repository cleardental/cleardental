// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 20
            CDHeaderLabel {
                text: "Select Media Type"
            }
            CDCancelButton {
                icon.name: ""
                text: "Play a Video"
                onClicked: {
                    mainView.push("SelectVideoPage.qml");
                }
            }
            CDCancelButton {
                icon.name: ""
                text: "Play a Game"
                onClicked: {
                    mainView.push("PlayGamePage.qml");
                }
            }
        }


    }

}
