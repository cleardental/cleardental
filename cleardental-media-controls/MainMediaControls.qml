// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    visible: true
    title: qsTr("Media Controls")

    property string selectedMediaCat: ""
    property bool runningGame: false

    CDMediaFiles {
        id: mediaFiles
    }

    CDToolLauncher {
        id: toolLauncher
    }


    header:CDBlankToolBar {
        headerText: "Media Controls"
        ToolButton {
            icon.name: "go-previous"
            onClicked: {
                mainView.pop();
                toolLauncher.killGame(); //don't worry, it checks if it is playing a game before it kills now
                runningGame = false;
            }
            icon.width: 64
            icon.height: 64
            anchors.left: parent.left
            visible: mainView.depth > 1
        }
    }

    CDFileLocations {
        id: fileLocs
    }

    StackView {
        id: mainView
        anchors.fill: parent
        initialItem: SelectMediaType{}
    }

    Component.onDestruction: {
        toolLauncher.killGame(); //kill the game in case the "X" button was clicked instead
    }
}
