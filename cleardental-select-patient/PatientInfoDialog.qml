// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentDialog {
    id: patInfoDialog
    
    property string getPatID: ""

    Settings {
        id: personalReader
        fileName: fLocs.getPersonalFile(getPatID)
    }

    CDFileLocations {
        id: fLocs
    }

    CDToolLauncher {
        id: tLauncher
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.minimumWidth: 512
            GridLayout {
                columns: 3

                CDHeaderLabel {
                    Layout.columnSpan: 3
                    text: getPatID
                }

                Image {
                    id: proImage
                    Layout.maximumWidth: 512
                    Layout.maximumHeight: 256
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    Layout.alignment: Qt.AlignHCenter
                }

                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "First Name"
                    }
                    Label {
                        id: firstNameLabel
                    }
                    CDDescLabel {
                        text: "Last Name"
                    }
                    Label {
                        id: lastNameLabel
                    }
                    CDDescLabel {
                        text: "Date Of Birth"
                    }
                    Label {
                        id: dobNameLabel
                    }

                    CDDescLabel {
                        text: "Home Phone"
                    }
                    CDCopyLabel {
                        id: homePhoneLabel
                    }

                    CDDescLabel {
                        text: "Cell Phone"
                    }
                    CDCopyLabel {
                        id: cellPhoneLabel
                    }
                    CDDescLabel {
                        text: "Email"
                    }
                    CDCopyLabel {
                        id: emailLabel
                    }
                }

                ScheduledApptCol {
                    id: schCol
                    patID: getPatID
                }

                GridLayout {
                    Layout.columnSpan: 3
                    columns: 5
                    CDButton {
                        text: "Billing"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.Billing,getPatID);
                        }
                    }

                    CDButton {
                        text: "Old Billing"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.LegacyBilling,getPatID);
                        }
                    }

                    CDButton {
                        text: "Messaging"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.Messaging,getPatID);
                        }
                    }

                    CDButton {
                        text: "Periodontal Charting"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.PerioCharting,getPatID);
                        }
                    }

                    CDButton {
                        text: "Periodontal DxTx"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.PerioDxTx,getPatID);
                        }
                    }

                    CDButton {
                        text: "Take Photographs"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.TakePhotograph,getPatID);
                        }
                    }

                    CDButton {
                        text: "Prescription"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.Prescription,getPatID);
                        }
                    }

                    CDButton {
                        text: "Claims"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.DentalPlanClaims,getPatID);
                        }
                    }

                    CDButton {
                        text: "Take Radiographs"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.TakeRadiograph,getPatID);
                        }
                    }

                    CDButton {
                        text: "Case Notes"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.ReviewCaseNotes,getPatID);
                        }
                    }

                    CDButton {
                        text: "Medical History"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.ReviewPatMedical,getPatID);
                        }
                    }

                    CDButton {
                        text: "Personal Information"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.ReviewPatPersonal,getPatID);
                        }
                    }


                    CDButton {
                        text: "Review Radiographs"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.ReviewRadiographs,getPatID);
                        }
                    }

                    CDButton {
                        text: "Review Tx Plan"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.ReviewTxPlan,getPatID);
                        }
                    }

                    CDButton {
                        text: "Schedule Patient"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.SchedulePatient,getPatID);
                        }
                    }

                    CDButton {
                        text: "Vitals"
                        onClicked: {
                            tLauncher.launchTool(CDToolLauncher.Vitals,getPatID);
                        }
                    }

                    CDButton {
                        text: "Check Folder"
                        onClicked: {
                            tLauncher.launchDolphin(fLocs.getPatientDirectory(getPatID))
                        }
                    }

                }
            }
        }

        CDCancelButton {
            text: "Close"
            onClicked: {
                patInfoDialog.close();
            }
        }
    }


    onVisibleChanged: {
        personalReader.category = "Name"
        firstNameLabel.text = personalReader.value("FirstName","");
        lastNameLabel.text = personalReader.value("LastName","");

        personalReader.category = "Personal"
        dobNameLabel.text = personalReader.value("DateOfBirth","");

        proImage.source = "file://" + fLocs.getProfileImageFile(getPatID);
        personalReader.category = "Phones"
        homePhoneLabel.text = personalReader.value("HomePhone","");
        cellPhoneLabel.text = personalReader.value("CellPhone","");
        personalReader.category = "Emails"
        emailLabel.text = personalReader.value("Email","");
    }
}
