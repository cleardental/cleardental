// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.15
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14
import Qt.labs.qmlmodels 1.0
import dental.clear 1.0

CDAppWindow {
    id: rootWin
    title: qsTr("Select Patient")

    property var fullPatList: []

    CDPatientManager {
        id: patman
    }

    CDConstLoader {
        id: constLoader
    }

    Settings {
        id: patInfoLoader
    }

    Settings {
        id: patInsLoader
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: textFileMan
    }

    function rawNumber(getInput) {
        return getInput.replace(/[()\ \s-]+/g, '');
    }

    function readPatCaseNotes(getPatID) {
        var caseNoteJSON = textFileMan.readFile(fLocs.getCaseNoteFile(getPatID));
        if(caseNoteJSON.length > 3) {
            var caseNoteList = JSON.parse(caseNoteJSON);
            var superCaseNoteString = "";
            for(var i=0;i<caseNoteList.length;i++) {
                superCaseNoteString += caseNoteList[i]["CaseNote"] + "\n";
            }
            var searchUpper = searchField.text.toUpperCase();
            superCaseNoteString = superCaseNoteString.toUpperCase()
            return superCaseNoteString.indexOf(searchUpper) >= 0;
        }
        else {
            return false;
        }
    }

    function readPatIns(getPatID) {
        patInsLoader.fileName = fLocs.getDentalPlansFile(getPatID);
        patInsLoader.category = "Primary";
        var priInsName = patInsLoader.value("Name","");
        patInsLoader.category = "Secondary";
        var secInsName = patInsLoader.value("Name","");

        priInsName = priInsName.toUpperCase();
        secInsName = secInsName.toUpperCase();
        var searchUpper = searchField.text.toUpperCase();

        var returnMe = false;
        returnMe = returnMe | priInsName.indexOf(searchUpper) >= 0;
        returnMe = returnMe | secInsName.indexOf(searchUpper) >= 0;
        return returnMe;
    }

    function patFilterThough(patObj) {
        var searchUpper = searchField.text.toUpperCase();
        var returnMe = false;

        if(searchNameBox.checked) {
            returnMe = returnMe | patObj["FirstName"].toUpperCase().indexOf(searchUpper) >= 0;
            returnMe = returnMe | patObj["LastName"].toUpperCase().indexOf(searchUpper) >= 0;
            returnMe = returnMe | patObj["PreferredName"].toUpperCase().indexOf(searchUpper) >= 0;
        }
        if(searchPhoneBox.checked) {
            searchUpper = rawNumber(searchUpper);
            if( (!returnMe) && (!isNaN(searchUpper))) {
                var homePhone = rawNumber(patObj["HomePhone"]);
                var cellPhone = rawNumber(patObj["CellPhone"]);
                returnMe = returnMe | homePhone.indexOf(searchUpper) >= 0;
                returnMe = returnMe | cellPhone.indexOf(searchUpper) >= 0;
            }
        }
        if(searchCaseBox.checked) {
            returnMe = returnMe | readPatCaseNotes(patObj["patID"]);
        }
        if(searchInsBox.checked) {
            returnMe = returnMe | readPatIns(patObj["patID"]);
        }

        return returnMe;
    }

    function refreshPatList() {
        patientListModel.clear();

        if(filterBox.currentIndex === 0) { //"Only Active"
            if(searchField.text.length > 0) {
                for(var i=0;i<fullPatList.length;i++) {
                    if( (!fullPatList[i]["Dismissed"]) && (patFilterThough(fullPatList[i]))) {
                        patientListModel.append(fullPatList[i]);
                    }
                }
            }
            else {
                for(i=0;i<fullPatList.length;i++) {
                    if(!fullPatList[i]["Dismissed"]) {
                        patientListModel.append(fullPatList[i]);
                    }
                }
            }

        }

        else if(filterBox.currentIndex === 2) { //"Only Search"
            if(searchField.text.length < 3) {
                return;
            }

            for(i=0;i<fullPatList.length;i++) {
                if(patFilterThough(fullPatList[i])) {
                    patientListModel.append(fullPatList[i]);
                }
            }
        }
        else {
            if(searchField.text.length > 0) {
                for(i=0;i<fullPatList.length;i++) {
                    if(patFilterThough(fullPatList[i])) {
                        patientListModel.append(fullPatList[i]);
                    }
                }
            }
            else {
                for(i=0;i<fullPatList.length;i++) {
                    patientListModel.append(fullPatList[i]);
                }
            }
        }
    }

    header: CDBlankToolBar {
        id: searchBar
        height: 72

        Label {
            id: selPatLabel
            text:  "Select Patient"
            font.pointSize: 32
            font.bold: true
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 10

        }

        RowLayout {
            anchors.left: selPatLabel.right
            anchors.margins: 25
            height: parent.height
            TextField {
                id: searchField
                placeholderText: "Search Here"
                onTextChanged: refreshPatList();
                Layout.minimumWidth: 300
            }

            CDDescLabel {
                text: "Show"
                Layout.minimumWidth: 100
                horizontalAlignment: Text.AlignRight
            }

            ComboBox {
                id: filterBox
                Layout.minimumWidth: 200
                model: ["All active patients","All patients","Only Search"]
                onCurrentIndexChanged: {
                    refreshPatList();
                }
            }
            CheckBox {
                id: searchNameBox
                text: "Search by Name"
                checked: true
                onCheckedChanged: {
                    refreshPatList();
                }
            }
            CheckBox {
                id: searchPhoneBox
                text: "Search by Phone Number"
                checked: true
                onCheckedChanged: {
                    refreshPatList();
                }
            }
            CheckBox {
                id: searchCaseBox
                text: "Search by Case Note"
                onCheckedChanged: {
                    refreshPatList();
                }
            }
            CheckBox {
                id: searchInsBox
                text: "Search by Insurance Name"
                onCheckedChanged: {
                    refreshPatList();
                }
            }
        }
    }

    ListView {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 5
        ScrollBar.vertical: ScrollBar {}
        model: ListModel {
            id:patientListModel
        }

        displayMarginBeginning: 128
        displayMarginEnd: 128

        delegate: RowLayout {
            spacing: 5
            Image {
                id: proImage
                source: "file://" + fLocs.getProfileImageFile(patID)
                Layout.maximumWidth: 128
                Layout.maximumHeight: 128
                fillMode: Image.PreserveAspectFit
                smooth: true
                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource:Rectangle {
                        width: 128
                        height: 128
                        radius: 5
                    }
                }
                visible: status == Image.Ready
            }
            Rectangle {
                Layout.maximumHeight: 128
                Layout.maximumWidth: 128
                Layout.minimumHeight: 128
                Layout.minimumWidth: 128
                visible: !proImage.visible
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "lightsteelblue" }
                    GradientStop { position: 1.0; color: "lightblue" }
                }
                radius: 5

                CDDescLabel {
                    anchors.centerIn: parent
                    text: "No Photo Available"
                }
            }
            Label {
                text: patID
                Layout.minimumWidth: 360
            }

            CDButton {
                text: "More Info"
                PatientInfoDialog {
                    id: patInfoDia
                    getPatID: patID
                }

                onClicked: patInfoDia.open();
            }

            Label {
                color: Material.color(Material.Red)
                text: ScheduleAlert
            }

            Label {
                color: Material.color(Material.Red)
                text: "Do Not Schedule"
                visible: DoNotSchedule
            }

            Label {
                color: Material.color(Material.Red)
                text: "Dismissed"
                visible: Dismissed
            }

            Label {

                id: lastAppointmentLabel

                CDScheduleManager {
                    id: m_schMan
                }
                CDCommonFunctions {
                    id: m_comFuns
                }
                Settings {
                    id: m_apptReader
                }

                Component.onCompleted:  {
                    var apptList = m_schMan.getAppointments(patID);

                    var apptListPairs = [];

                    for(var i=0;i<apptList.length;i++) {
                        var lastSlash = apptList[i].lastIndexOf("/") +1;
                        var fLongName = apptList[i].substring(lastSlash);
                        var atSymbol = fLongName.lastIndexOf("@");
                        var pureDate = fLongName.substring(0,atSymbol);
                        var monthDayYearArray = pureDate.split("-");
                        var mills = m_comFuns.convertTextDateToMils(monthDayYearArray[0],
                                                                     monthDayYearArray[1],
                                                                     monthDayYearArray[2]);
                        apptListPairs.push([apptList[i],mills]);
                    }
                    var sortedList = apptListPairs.sort(function(a,b){return a[1] - b[1];});

                    if(sortedList.length === 0) {
                        text = "Never here"
                    }
                    else {
                        var apptFilename = sortedList[sortedList.length-1][0];

                        var todayDate = new Date();
                        var todayTime = todayDate.setUTCHours(0,0,0,0);

                        lastSlash = apptFilename.lastIndexOf("/") +1;
                        fLongName = apptFilename.substring(lastSlash);
                        atSymbol = fLongName.lastIndexOf("@");
                        pureDate = fLongName.substring(0,atSymbol);

                        if(todayTime > sortedList[sortedList.length-1][1]) { //past
                            m_apptReader.fileName = apptFilename;
                            var apptStatus = m_apptReader.value("Status","");
                            if(apptStatus.length > 1) {
                                text = "Last appt: " + pureDate + " (" + apptStatus + ")";
                            }
                            else {
                                text = "Last appt: " + pureDate;
                            }
                        }
                        else {
                            text = "Next appt: " + pureDate;
                        }

                    }

                }
            }

        }
    }

    Component.onCompleted: {
        var nodeType = constLoader.getNodeType();
        if(nodeType.startsWith("touch")) {
            filterBox.currentIndex = 3;
        }

        var allPatIDs = patman.getAllPatientIds();

        for(var i=0;i<allPatIDs.length;i++) {
            var addMe = ({});
            addMe["patID"] = allPatIDs[i];
            patInfoLoader.fileName = fLocs.getPersonalFile(allPatIDs[i]);
            patInfoLoader.category = "Name";
            addMe["FirstName"] = patInfoLoader.value("FirstName","")
            addMe["LastName"] = patInfoLoader.value("LastName","")
            addMe["PreferredName"] = patInfoLoader.value("PreferredName","")
            patInfoLoader.category = "Preferences";
            addMe["ScheduleAlert"] = patInfoLoader.value("ScheduleAlert","");
            addMe["Dismissed"] = JSON.parse(patInfoLoader.value("Dismissed",false));
            addMe["DoNotSchedule"] = JSON.parse(patInfoLoader.value("DoNotSchedule",false));
            patInfoLoader.category = "Phones";
            addMe["CellPhone"] = patInfoLoader.value("CellPhone","");
            addMe["HomePhone"] = patInfoLoader.value("HomePhone","");
            fullPatList.push(addMe);
        }

        refreshPatList();
    }


    PatientInfoDialog {
        id: patInfoDialog
        anchors.centerIn: parent
    }
}
