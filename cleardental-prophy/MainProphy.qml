// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Prophy [ID: " + PATIENT_FILE_NAME + "]")

    property var ledgerObj: []

    header: CDPatientToolBar {
        headerText: "Prophy:"
        ToolButton {
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: drawer.open();
            anchors.left: parent.left
        }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
        showOtherRooms: false
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    CDToolLauncher {
        id: launcher
    }

    CDRadiographManager {
        id: radMan
    }

    CDCommonFunctions {
        id: comFuns
    }



    ColumnLayout {
        anchors.centerIn: parent
        spacing: 10

        RowLayout {
            Layout.fillWidth: true
            CDMedReviewPane {
                id: medRev
                Layout.fillHeight: true
                Layout.minimumWidth: 860
            }
            CDReviewRadiographPane {
                Layout.minimumWidth: 480
                id: revRadio
                toothToReview: -1
            }
            CDTranslucentPane {
                backMaterialColor: Material.LightGreen
                Layout.fillHeight: true

                ColumnLayout {
                    Label {
                        font.underline: true
                        font.pointSize: 24
                        text: "Ready for Doctor Exam"
                    }

                    ComboBox {
                        id: chairStatusBox
                        model: ["Not ready yet", "Ready for Exam", "Waiting for doctor"]
                        Layout.fillWidth: true

                        Settings {
                            id: chairStatusFile
                            fileName: fLocs.getLocalChairStatus()
                        }

                        onCurrentTextChanged: {
                            chairStatusFile.setValue("chairName", chairStatusBox.currentText)
                            chairStatusFile.sync();
                            gitMan.commitData("Updated chair status for room " + CHAIR_NAME);
                        }

                    }

                    Button {
                        text: "Launch Periodic Exam"
                        Layout.alignment: Qt.AlignHCenter
                        onClicked: {
                            launcher.launchTool(CDToolLauncher.PeriodicExam,PATIENT_FILE_NAME);
                        }
                    }
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            ColumnLayout {
                RowLayout {
                    spacing: 10
                    Label {
                        text: "Radiographs Due"
                        font.bold: true
                    }
                    Label {
                        text: radMan.getBWRadiographDueDate(PATIENT_FILE_NAME)
                        color: text.startsWith("Now") ? Material.color(Material.Red) : Material.color(Material.Green)
                    }
                }

                RowLayout {
                    spacing: 20
                    Label {
                        text: "Periodontal Charting"
                        font.bold: true
                    }

                    Label {
                        function findPerioDue() {
                            var lastPerio = reviewPerio.value("PerioCharting","Never");
                            if(lastPerio !== "Never") {
                                var dateParts = lastPerio.split("/");
                                var addYears = parseInt(pracPref.value("Perio",1));
                                var dueDate = new Date(dateParts[2], parseInt(dateParts[0]) + 1 ,
                                                       parseInt(dateParts[1]) + addYears);
                                var today = new Date();
                                if(dueDate < today) { //Past due
                                    return "Due (Last Done " + lastPerio + ")";
                                }
                                else {
                                    return "Not Due (Last Done " + lastPerio + ")";
                                }
                            }
                            else {
                                return "Never Done"
                            }
                        }

                        text: findPerioDue()
                        color: text.startsWith("Not Due") ? Material.color(Material.Green) : Material.color(Material.Red)

                        Settings {
                            id: reviewPerio
                            fileName: fLocs.getReviewsFile(PATIENT_FILE_NAME);
                        }
                        Settings {
                            id: pracPref
                            fileName: fLocs.getLocalPracticePreferenceFile()
                            category: "Updates"
                        }
                    }

                    Button {
                        text: "New Periodontal Chart"
                        onClicked: {
                            launcher.launchTool(CDToolLauncher.PerioCharting,PATIENT_FILE_NAME);
                        }
                    }

                    CDReviewLauncher {
                        iniProp: "PerioDxTx"
                        launchEnum: CDToolLauncher.PerioDxTx
                        text: "Periodontal Dx and Tx"
                    }
                }

                RowLayout {
                    spacing: 20
                    Label {
                        text: "Patient's OHI"
                        font.bold: true
                    }
                    ComboBox {
                        id: patOHIBox
                        model: ["Excellent", "Good", "Fair", "Poor", "Very Poor"]
                        Layout.minimumWidth: 150
                        Component.onCompleted: {
                            currentIndex = 1
                        }
                    }

                    Label {
                        Layout.minimumWidth: 120
                    }

                    CDDescLabel {
                        text: "Prophy Notes"
                    }
                    ComboBox {

                        Settings { id: prophyPrefs }

                        function commitChanges() {
                            prophyPrefs.setValue("ProphyPref",editText);
                            prophyPrefs.sync();
                            gitMan.commitData("Updated Patient's Prophy pref");
                        }

                        editable: true
                        Layout.minimumWidth: 240
                        onEditTextChanged: commitChanges();
                        onCurrentValueChanged: commitChanges();
                        Component.onCompleted: {
                            prophyPrefs.fileName = fLocs.getPatientProphyPrefs(PATIENT_FILE_NAME)
                            model = [ prophyPrefs.value("ProphyPref",""),
                            "Set Ultrasonic to 50%",
                            "Can't tolerate ultrasonic"];
                        }
                    }
                }

                RowLayout {
                    spacing: 25

                    CheckBox {
                        id: ohiRev
                        text: "OHI Reviewed"
                    }

                    CheckBox {
                        id: usedAirPolish
                        text: "Air polished teeth"
                    }

                    CheckBox {
                        id: usedUltra
                        text: "Scaled with ultrasonic scaler"
                    }

                    CheckBox {
                        id: usedHand
                        text: "Scaled with hand scaler"
                    }

                    CheckBox {
                        id: usedPol
                        text: "Polished with prophy paste"
                    }

                    RowLayout {
                        CheckBox {
                            id: didFlu
                            text: "Fluride Treatment applied"
                        }
                        ComboBox {
                            id: fluorideType
                            opacity: didFlu.checked ? 1:0
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: didFlu.checked
                            Layout.minimumWidth: 200
                            model: ["Varnish", "Gel + Tray", "Foam + Tray"]
                        }
                    }
                }

                RowLayout {
                    spacing: 20
                    Label {
                        text: "Next Prophy"
                        font.bold: true
                    }
                    ComboBox {
                        model: ["12 Months", "6 Months", "3 Months"]
                        Layout.minimumWidth: 150
                        Component.onCompleted: {
                            currentIndex = 1
                        }
                    }
                }
            }
        }

    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            var completeMe = comFuns.findTx(PATIENT_FILE_NAME,"Prophy");
            if(completeMe.length === 0) {
                completeMe = comFuns.findTx(PATIENT_FILE_NAME,"Adult Prophy");
            }

            finProphy.txItemsToComplete = [completeMe[0]];
            var caseNoteString = "Patient presented for a prophy.\n";
            caseNoteString += "Patient's OHI: "+ patOHIBox.currentText+"\n";
            if(ohiRev.checked) {
                caseNoteString += ohiRev.text+".\n";
            }
            if(usedAirPolish.checked) {
                caseNoteString += usedAirPolish.text+".\n";
            }

            if(usedUltra.checked) {
                caseNoteString += usedUltra.text+".\n";
            }
            if(usedHand.checked) {
                caseNoteString += usedHand.text+".\n";
            }
            if(usedPol.checked) {
                caseNoteString += usedPol.text+".\n";
            }
            if(didFlu.checked) {
                caseNoteString += "Fluoride varnish applied.\n";
                var flu = ({});
                flu["ProcedureName"] = "Fluoride varnish";
                flu["DCode"] = "D1206";
                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",flu);
                finProphy.txItemsToComplete = [completeMe[0],flu];
            }

            finProphy.caseNoteString = caseNoteString;

            finProphy.open();
        }
    }

    CDFinishProcedureDialog {
        id: finProphy
    }

}
