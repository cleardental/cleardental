// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: deleteCaseNoteDia

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Delete Case Note"
                }
                Label {
                    text: "Are you sure you want to delete this case note?"
                }
                RowLayout {
                    CDAddButton {
                        text: "No, Don't Delete the case note"
                        icon.name: "go-previous"
                        onClicked: {
                            deleteCaseNoteDia.reject();
                        }
                    }
                    Label {
                        Layout.minimumWidth: 64
                    }

                    CDDeleteButton {
                        text: "Yes, Delete the case note"
                        onClicked: {
                            deleteCaseNoteDia.accept();
                        }
                    }
                }
            }
        }
    }

}
