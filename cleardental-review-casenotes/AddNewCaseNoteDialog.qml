// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1


CDTranslucentDialog {
    id: addCase

    property alias patID: patName.text

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Add New Case Note"
                }

                GridLayout {
                    columns: 2

                    CDDescLabel {
                        text: "Patient Name"
                    }

                    CDPatientComboBox {
                        id: patName
                        Layout.fillWidth: true
                        onTextChanged: {
                            var txItems = comFuns.getAllTxPlanItems(text);
                            var makeModel = [];
                            for(var i=0;i<txItems.length;i++) {
                                var addMe = ({});
                                var txObj = txItems[i];
                                addMe["DisplayName"] = comFuns.makeTxItemString(txObj);
                                addMe["TxObj"] = txObj;
                                makeModel.push(addMe);
                            }

                            var defObj = ({});
                            defObj["DisplayName"] = "None"
                            defObj["TxObj"] = ({});
                            makeModel.push(defObj);

                            procedureBox.model = makeModel;
                        }
                    }

                    CDDescLabel {
                        text: "Associated Procedure"
                    }

                    ComboBox {
                        id: procedureBox
                        Layout.minimumWidth: 360
                        textRole: "DisplayName"
                        valueRole: "TxObj"
                    }
                }

                CDDescLabel {
                    text: "Case Note"
                }

                CDCaseNoteTextArea {
                    Layout.fillWidth: true
                    Layout.minimumHeight:  360
                    id: caseNote
                }
                CheckBox {
                    id: finalize
                    text: "Finalize and Sign"
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    reject();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Add Case Note"
                onClicked: {
                    var caseNoteFile = fileLocs.getCaseNoteFile(patName.text);
                    var caseNoteJSON = textMan.readFile(caseNoteFile);
                    var caseNoteListObj = []
                    if(caseNoteJSON.length > 1) {
                        caseNoteListObj = JSON.parse(caseNoteJSON);
                    }

                    var addMe = ({});
                    addMe["DateTime"] = Date();
                    addMe["Provider"] = providerInfo.getCurrentProviderUsername();
                    addMe["CaseNote"] = caseNote.text;
                    addMe["Final"] = finalize.checked;
                    if(finalize.checked) {
                        addMe["Signature"] = providerInfo.signData(caseNote.text);
                    }
                    addMe["ProcedureObject"] = JSON.stringify([procedureBox.currentValue]);
                    caseNoteListObj.push(addMe);

                    textMan.saveFile( fileLocs.getCaseNoteFile(patName.text),
                                     JSON.stringify(caseNoteListObj, null, '\t'));
                    gitMan.commitData("Added new case note for " + patName.text);

                    accept();
                }
            }
        }

    }



}
