// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1


CDTransparentPage {
    id: caseNotePage
    function isToday(getDate) {
        var getDateObj =new Date(getDate);
        var today = new Date();

        return ( (getDateObj.getMonth() === today.getMonth()) &&
                (getDateObj.getDate() === today.getDate()) &&
                (getDateObj.getFullYear() === today.getFullYear()) );
    }

    function compareDates(a,b) {
        var dateA = Date.parse(a.DateTime);
        var dateB = Date.parse(b.DateTime);
        if(dateA < dateB) {
            return 1;
        }
        else if(dateB < dateA) {
            return -1;
        }

        return 0;
    }

    function isHidden(caseNote) {
        if("Hidden" in caseNote) {
            return caseNote["Hidden"];
        }
        return false;
    }

    function forceRefreshSearch(searchString) {
        var search = searchString.toLowerCase();
        var filteredCaseNotes = [];

        for (var i=0; i < caseNotePage.showModeFullList.length; i++) {
            var caseNote = caseNotePage.showModeFullList[i];
            var notes = caseNote["CaseNote"].toLowerCase();
            if (notes.includes(search)) {
                filteredCaseNotes.push(caseNote);
                continue;
            }
            var procedureList = JSON.parse(caseNote["ProcedureObject"]);

            if ( (procedureList.length >0) &&  (procedureList[0] !== null) && ("ProcedureName" in procedureList[0])) {
                for (var j=0; j < procedureList.length; j++) {
                    var procedure = procedureList[j];
                    var procedureName = procedure["ProcedureName"].toLowerCase();
                    if("DCode" in procedure) {
                        var procedureCode = procedure["DCode"].toLowerCase();
                    }
                    else {
                        procedureCode = "";
                    }

                    if (procedureName.includes(search) || procedureCode.includes(search)) {
                        filteredCaseNotes.push(caseNote);
                        continue;
                    }
                }
            }
            var patientIDList = caseNote["PatientID"].split(" ");
            var patientName = patientIDList[0] + patientIDList[1]; // adds the first and last names back together
            var patientNameLower = patientName.toLowerCase();
            if (patientNameLower.includes(search)) {
                filteredCaseNotes.push(caseNote);
                continue;
            }
        }

        filteredCaseNotes.sort(compareDates);
        caseNoteView.displayList = filteredCaseNotes;
    }

    function forceRefresh() {
        var showMode = rootWin.filterBox.currentIndex;
        var makeMe = [];
        var fullPatList = patMan.getAllPatientIds();

        if(showMode === 0) { //"Show All Unsigned Procedures Done Today"
            for(var i=0;i<fullPatList.length;i++) {
                var patID = fullPatList[i];
                var caseNoteFile = fileLocs.getCaseNoteFile(patID);
                var caseNoteJSON = textMan.readFile(caseNoteFile);
                if(caseNoteJSON.length > 1) {
                    var caseNoteListObj = JSON.parse(caseNoteJSON);
                    for(var caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                        var addMe = caseNoteListObj[caseNoteI];
                        if(isToday(addMe["DateTime"]) && (!addMe["Final"]) && (!isHidden(addMe)) ) {
                            addMe["PatientID"] = patID;
                            addMe["CaseNoteIndex"] =caseNoteI;
                            makeMe.push(addMe);
                        }
                    }
                }
            }
        }

        else if(showMode === 1) { //"Show All Procedures Done Today"
            for(i=0;i<fullPatList.length;i++) {
                patID = fullPatList[i];
                caseNoteFile = fileLocs.getCaseNoteFile(patID);
                caseNoteJSON = textMan.readFile(caseNoteFile);
                if(caseNoteJSON.length > 1) {
                    caseNoteListObj = JSON.parse(caseNoteJSON);
                    for(caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                        addMe = caseNoteListObj[caseNoteI];
                        if(isToday(addMe["DateTime"]) && (!isHidden(addMe))) {
                            addMe["PatientID"] = patID;
                            addMe["CaseNoteIndex"] = caseNoteI;
                            makeMe.push(addMe);
                        }
                    }
                }
            }
        }

        else if(showMode === 2) { //"Show All My Procedures Done Today"
            for(i=0;i<fullPatList.length;i++) {
                patID = fullPatList[i];
                caseNoteFile = fileLocs.getCaseNoteFile(patID);
                caseNoteJSON = textMan.readFile(caseNoteFile);
                if(caseNoteJSON.length > 1) {
                    caseNoteListObj = JSON.parse(caseNoteJSON);
                    for(caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                        addMe = caseNoteListObj[caseNoteI];
                        if( (isToday(addMe["DateTime"])) && (!isHidden(addMe)) ) {
                            addMe["PatientID"] = patID;
                            addMe["CaseNoteIndex"] = caseNoteI;
                            makeMe.push(addMe);
                        }
                    }
                }
            }
        }

        else if(showMode === 3) { //"Show All My Procedures Ever Done"
            for(i=0;i<fullPatList.length;i++) {
                patID = fullPatList[i];
                caseNoteFile = fileLocs.getCaseNoteFile(patID);
                caseNoteJSON = textMan.readFile(caseNoteFile);
                if(caseNoteJSON.length > 1) {
                    caseNoteListObj = JSON.parse(caseNoteJSON);
                    for(caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                        addMe = caseNoteListObj[caseNoteI];
                        if((providerInfo.getCurrentProviderUsername() === addMe["Provider"] ) &&
                                (!isHidden(addMe)) ){
                            addMe["PatientID"] = patID;
                            addMe["CaseNoteIndex"] = caseNoteI;
                            makeMe.push(addMe);
                        }
                    }
                }
            }
        }

        else if(showMode === 4) { //"See all Patient's Case Notes"
            caseNoteFile = fileLocs.getCaseNoteFile(rootWin.typedPatName);
            caseNoteJSON = textMan.readFile(caseNoteFile);
            if(caseNoteJSON.length > 1) {
                caseNoteListObj = JSON.parse(caseNoteJSON);
                for(caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                    addMe = caseNoteListObj[caseNoteI];
                    if(!isHidden(addMe)) {
                        addMe["PatientID"] = rootWin.typedPatName;
                        addMe["CaseNoteIndex"] = caseNoteI;
                        makeMe.push(addMe);
                    }
                }
            }
        }

        else if(showMode === 5) { //Show All Unsigned Case Notes
            for(i=0;i<fullPatList.length;i++) {
                patID = fullPatList[i];
                caseNoteFile = fileLocs.getCaseNoteFile(patID);
                caseNoteJSON = textMan.readFile(caseNoteFile);
                if(caseNoteJSON.length > 1) {
                    caseNoteListObj = JSON.parse(caseNoteJSON);
                    for(caseNoteI =0; caseNoteI< caseNoteListObj.length; caseNoteI++  ) {
                        addMe = caseNoteListObj[caseNoteI];
                        if(!addMe["Final"] && (!isHidden(addMe))) {
                            addMe["PatientID"] = patID;
                            addMe["CaseNoteIndex"] =caseNoteI;
                            makeMe.push(addMe);
                        }
                    }
                }
            }
        }

        makeMe.sort(compareDates);
        caseNoteView.displayList = makeMe;
        caseNotePage.showModeFullList = makeMe;
//        caseNotePage.display = caseNoteView.displayList
    }

    property var showModeFullList: []

    property alias displayAlias: caseNoteView.displayList

    CaseNoteListView {
        id: caseNoteView
        anchors.fill: parent

    }

    Component.onCompleted: {
        forceRefresh(caseNotesObj);
    }
}
