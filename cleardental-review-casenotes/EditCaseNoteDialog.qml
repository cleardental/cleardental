// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1


CDTranslucentDialog {
    id: editCase

    property alias caseNote: caseNote.text
    property alias isFinal: finalBox.checked
    property string patientName: ""
    property string procedure: ""

    ColumnLayout {

        RowLayout {
            CDTranslucentPane {
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Edit Case Note"
                    }

                    CDDescLabel {
                        text: "Patient Name"
                    }

                    Label {
                        text:patientName
                    }

                    CDDescLabel {
                        text: "Procedure"
                    }

                    Label {
                        text: procedure
                    }

                    CDDescLabel {
                        text: "Case Note"
                    }

                    CDCaseNoteTextArea {
                        Layout.fillWidth: true
                        Layout.minimumHeight:  360
                        Layout.minimumWidth: 480
                        id: caseNote
                        onTextChanged: {
                            quickButtonColumn.updateVisibles();
                        }
                    }
                    CheckBox {
                        id: finalBox
                        text: "Finalize and Sign"
                    }
                }
            }
            ColumnLayout {
                id: quickButtonColumn
                property string docNameString: ""
                function updateVisibles() {
                    patTolPro.visible = caseNote.text.indexOf(patTolPro.fullTextTolerated) < 0;
                    signEnd.visible = caseNote.text.indexOf(quickButtonColumn.docNameString) < 0;

                }

                CDAddButton {
                    id: patTolPro
                    property string fullTextTolerated: "Patient tolerated the procedure well."
                    text: "Pt. tolerated procedure"
                    onClicked: {
                        if(caseNote.text.charAt(caseNote.text.length-1) == "\n") {
                            caseNote.text += fullTextTolerated;
                        }
                        else {
                            caseNote.text += "\n" + fullTextTolerated;
                        }
                    }
                }
                CDAddButton {
                    id: signEnd
                    text: "Write name at end"
                    onClicked: {
                        if(caseNote.text.charAt(caseNote.text.length-1) == "\n") {
                            caseNote.text += quickButtonColumn.docNameString;
                        }
                        else {
                            caseNote.text += "\n" + quickButtonColumn.docNameString;
                        }
                    }
                }

                CDTranslucentPane {
                    id: txPlanPane
                    backMaterialColor: Material.Orange
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    ColumnLayout {
                        CDHeaderLabel {
                            text: "Remaining Tx Plan"
                        }
                        ListView {
                            id: txPlanListView
                            property var txPlanList: []
                            model: txPlanList.lengh
                            height: txPlanPane.height - 75
                            width: 300
                            delegate: Label {
                                id: txDelLabel
                                text: comFuns.makeTxItemString(txPlanListView.txPlanList[index])
                                height: implicitHeight
                            }

                            ScrollBar.vertical: ScrollBar { }

                            Component.onCompleted: {
                                txPlanList = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME)
                                model = txPlanList.length
                            }
                        }
                    }
                }

                Settings {
                    id: docFileReader
                }

                CDFileLocations {
                    id: m_fLocs
                }

                CDProviderInfo {
                    id: providerInfo
                }

                Component.onCompleted: {
                    var username = providerInfo.getCurrentProviderUsername();
                    docFileReader.fileName = m_fLocs.getDocInfoFile(username);
                    docNameString = docFileReader.value("FirstName")  + " " +docFileReader.value("LastName") +
                            ", " + docFileReader.value("Title");
                }
            }


        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    reject();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Make Changes"
                onClicked: {
                    accept();
                }
            }
        }

    }



}
