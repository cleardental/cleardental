// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Generate Case note " + " [ID: " + PATIENT_FILE_NAME + "]")
    property var procedureObj: JSON.parse(PROCEDURE_JSON)

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedureObj)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }



    CDCommonFunctions {
        id: comFuns
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    CDToolLauncher {
        id: launcher
    }

    Settings {
        id: missingTeethSettings
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: perioChartSettings
        fileName: fLocs.getPerioChartFile(PATIENT_FILE_NAME)
    }



    CDFinishProcedureButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        onClicked: {
            var caseNoteStr = "Patient presented for Panoramic Radiograph.\n";
            caseNoteStr += "Panoramic radiograph taken.\n";
            finOpDia.caseNoteString = caseNoteStr
            finOpDia.txItemsToComplete = [procedureObj]
            finOpDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finOpDia
    }

}
