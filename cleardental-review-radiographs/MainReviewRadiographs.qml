// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Review Radiographs [ID: " + PATIENT_FILE_NAME + "]")

    property alias showBad: showBadBox.checked

    header: CDPatientToolBar {
        headerText: "Review Radiographs:"
    }

    Label {
        text: "R"
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 26
    }

    Label {
        text: "L"
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 26
    }

    CDTranslucentPane {
        anchors.fill: parent
        anchors.margins:  20
        backMaterialColor: Material.Grey

        GridLayout {
            id: fillGrid
            columns: 7
            anchors.fill: parent
            //columnSpacing: 10
            RowLayout {
                Layout.columnSpan: 7
                CDHeaderLabel {
                    text: "Review Radiographs"
                }
                CheckBox {
                    id: showBadBox
                    text: "Show rejected radiographs"
                    checked: false
                }
                Label {Layout.fillWidth: true;}
                Button {
                    text: "Take new radiographs"
                    CDToolLauncher {
                        id: m_Launcher
                    }
                    onClicked: {
                        m_Launcher.launchTool(CDToolLauncher.TakeRadiograph,PATIENT_FILE_NAME)
                    }
                }

            }

            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Right_Distal
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Right_Mesial
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Anterior_Right
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Anterior_Center
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Anterior_Left
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Left_Mesial
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Maxillary_Left_Distal
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.BW_Right_Distal
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.BW_Right_Mesial
            }

            //Placeholder
            Label{}

            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.Panoramic
            }

            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.FMX_SINGLE_IMAGE
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.BW_Left_Mesial
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.BW_Left_Distal
            }

            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Right_Distal
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Right_Mesial
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Anterior_Right
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Anterior_Center
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Anterior_Left
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Left_Mesial
            }
            RadioFlickable {
                includeBad: rootWin.showBad
                getTypeR: CDRadiographSensor.PA_Mandibular_Left_Distal
            }
        }


    }

}

