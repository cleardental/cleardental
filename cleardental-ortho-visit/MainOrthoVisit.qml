// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.12

CDAppWindow {
    id: rootWin
    title: qsTr(comFuns.makeTxItemString(procedureObj) + " [ID: " + PATIENT_FILE_NAME + "]")
    property var procedureObj: JSON.parse(PROCEDURE_JSON)

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedureObj)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    CDCommonFunctions {
        id: comFuns
    }



    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    CDFinishProcedureButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
    }

    CDFinishProcedureDialog {
        id: finOpDia
    }

}
