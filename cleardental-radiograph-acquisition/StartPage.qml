// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    property int ageYear: getAgeYear()


    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: patPersonalSettings
        fileName: fLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Personal"
    }

    function getAgeYear() {
        //because in QML, you can never assume properties are set in realtime
        patPersonalSettings.fileName =  fLocs.getPersonalFile(PATIENT_FILE_NAME);
        patPersonalSettings.category = "Personal"

        var textDOBArray = patPersonalSettings.value("DateOfBirth","1/1/0001").split("/");
        //console.debug( fLocs.getPersonalFile(PATIENT_FILE_NAME));
        var dobMonth = parseInt(textDOBArray[0]);
        var dobDay = parseInt(textDOBArray[1]);
        var dobYear =  parseInt(textDOBArray[2]);
        var today = new Date();

        var monthDiff = (today.getFullYear() - dobYear) * 12;
        monthDiff -= dobMonth;
        monthDiff += today.getMonth() + 1;

        return monthDiff /12;
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        GridLayout {
            id: buttonGrid
            columns: 7
            CDHeaderLabel {
                Layout.columnSpan: 7
                Layout.alignment: Qt.AlignHCenter
                text: "Select Radiographs"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Right_Distal
                property string billTooth: (ageYear > 12) ? "2" : "A"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Right_Mesial
                property string billTooth: (ageYear > 10) ? "4" : "B"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Right
                property string billTooth: (ageYear > 11) ? "6" : "C"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Center
                property string billTooth: (ageYear > 8) ? "8" : "E"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Left
                property string billTooth: (ageYear > 11) ? "11" : "H"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Left_Mesial
                property string billTooth: (ageYear > 10) ? "12" : "I"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Left_Distal
                property string billTooth: (ageYear > 12) ? "14" : "J"
            }


            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Right_Distal
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Right_Mesial
            }

            Label{}Label{}Label{}

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Left_Mesial
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Left_Distal
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Right_Distal
                property string billTooth: (ageYear > 11) ? "30" : "T"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Right_Mesial
                property string billTooth: (ageYear > 10) ? "28" : "S"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Right
                property string billTooth: (ageYear > 10) ? "27" : "R"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Center
                property string billTooth: (ageYear > 6) ? "24" : "O"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Left
                property string billTooth: (ageYear > 10) ? "22" : "M"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Left_Mesial
                property string billTooth: (ageYear > 10) ? "20" : "L"
            }

            SelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Left_Distal
                property string billTooth: (ageYear > 11) ? "18" : "K"
            }

        }


    }

    CDAddButton {
        id: takeBWButton
        anchors.right: parent.right
        anchors.bottom: nextStep.top
        anchors.margins: 10
        text: "Take Bitewings"
        onClicked: {
            for(var i=8;i<15;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    buttonGrid.children[i].isSelected = true;
                }
            }
        }

        height: 80
        width: 220

        Label {
            CDRadiographManager {
                id: radMan
            }
            text: "Due " + radMan.getBWRadiographDueDate(PATIENT_FILE_NAME)
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 10
            font.pointSize: 12
            color: text.indexOf("Now") >= 0 ? Material.color(Material.Red) : Material.background
        }

    }
    CDAddButton {
        id: takeFMXButton
        anchors.right: parent.right
        anchors.bottom: takeBWButton.top
        anchors.margins: 10
        text: "Take FMX"
        onClicked: {
            for(var i=1;i<buttonGrid.children.length;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    buttonGrid.children[i].isSelected = true;
                }
            }
        }
    }

    CDCommonFunctions {
        id: comFuns
    }

    Button {
        id: nextStep
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        text: "Take Radiographs"

        icon.name: "go-next"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18

        function countBitewings(getArray) {
            var returnMe =0;
            if(getArray.includes(CDRadiographSensor.BW_Right_Distal)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Right_Mesial)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Left_Mesial)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Left_Distal)) {
                returnMe++;
            }
            return returnMe;
        }

        function isBW(getItem) {
            var returnMe = false;
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Right_Distal);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Right_Mesial);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Left_Mesial);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Left_Distal);
            return returnMe;
        }

        onClicked: {
            var radioEnums = [];
            var radioTeeth = [];
            for(var i=1;i<buttonGrid.children.length;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    if(buttonGrid.children[i].isSelected) {
                        radioEnums.push(buttonGrid.children[i].radiographEnum);
                        radioTeeth.push(buttonGrid.children[i].billTooth);
                    }
                }
            }

            var BWCount = countBitewings(radioEnums)

            if(radioEnums.length === 18) { //All of them
                var addFMX = ({});
                addFMX["ProcedureName"] = "Intraoral—Complete series of radiographic images"
                addFMX["DCode"] = "D0210"
                comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,addFMX);
            }
            else if( BWCount > 0) {
                var addBWs = ({});

                if(BWCount === 4) {
                    addBWs["ProcedureName"] = "Four Bitewings"
                    addBWs["DCode"] = "D0274"
                }
                else if(BWCount === 3) {
                    addBWs["ProcedureName"] = "Three Bitewings"
                    addBWs["DCode"] = "D0273" //Nobody is going to pay for this but whatever
                }
                else if(BWCount === 2) {
                    addBWs["ProcedureName"] = "Two Bitewings"
                    addBWs["DCode"] = "D0272"
                }
                else {
                    addBWs["ProcedureName"] = "Single Bitewing"
                    addBWs["DCode"] = "D0270"
                }


                comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,addBWs);

                var leftOvers = radioEnums.length  - BWCount;

                if(leftOvers > 0) {
                    var first= true;

                    for(var x=0;x<radioEnums.length;x++) {
                        if(!isBW(radioEnums[x])) {
                            if(first) {
                                var add1stPA = ({});
                                add1stPA["ProcedureName"] = "First periapical"
                                add1stPA["DCode"] = "D0220"
                                add1stPA["Tooth"] = radioTeeth[x];
                                comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,add1stPA);
                                first = false;
                            }
                            else {
                                var add2ndPA = ({});
                                add2ndPA["ProcedureName"] = "Additional periapical"
                                add2ndPA["DCode"] = "D0230"
                                add2ndPA["Tooth"] = radioTeeth[x];
                                comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",add2ndPA);
                            }
                        }
                    }
                }
            }
            else { //just took a bunch of PAs
                first= true;
                for(x=0;x<radioEnums.length;x++) {
                    //console.debug(radioTeeth[x]);
                    if(first) {
                        add1stPA = ({});
                        add1stPA["ProcedureName"] = "First periapical"
                        add1stPA["DCode"] = "D0220"
                        add1stPA["Tooth"] = radioTeeth[x];
                        comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,add1stPA);
                        first = false;
                    }
                    else {
                        add2ndPA = ({});
                        add2ndPA["ProcedureName"] = "Additional periapical"
                        add2ndPA["DCode"] = "D0230"
                        add2ndPA["Tooth"] = radioTeeth[x];
                        comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",add2ndPA);
                    }
                }
            }

            mainStack.push("TakeRadiographsPage.qml");
            mainStack.currentItem.radiographEnums = radioEnums;
        }
    }


}

