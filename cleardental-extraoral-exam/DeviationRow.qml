// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

RowLayout {
    id: deviantRow
    
    //http://ftp.uws.edu/main.html?download&weblink=e844fae6420c564d3c00a55f4497160b&realfilename=TMD_Exam.pdf

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "No Problems")) {
            return;
        }

        noDevProblems.checked = false;
        devType.currentIndex = devType.find(prevString.split(" ")[0]);

        if(prevString.includes("upon opening")) {
            onOpenCheck.checked = true;
            var openDirection = prevString.split(" ")[5];
            if(openDirection === "right") {
                openRight.checked = true;
            }
            else {
                openLeft.checked = true;
            }
        }

        if(prevString.includes("upon closing")) {
            onCloseCheck.checked = true;
            var startPoint = prevString.split(" ").indexOf("closing");
            var closeDirection = prevString.split(" ")[startPoint + 3];
            if(closeDirection === "right") {
                closeRight.checked = true;
            }
            else {
                closeLeft.checked = true;
            }
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(noDevProblems.checked) {
            returnMe = "No Problems";
        }
        else {
            returnMe = devType.currentText;

            if(onOpenCheck.checked) {
                returnMe += " upon opening"
                if(openLeft.checked) {
                    returnMe += " to the left"
                }
                else {
                    returnMe += " to the right"
                }
            }

            if(onCloseCheck) {
                if(onOpenCheck.checked) {
                    returnMe += " and upon closing"
                }
                else {
                    returnMe += " upon closing"
                }

                if(closeLeft.checked) {
                    returnMe += " to the left"
                }
                else {
                    returnMe += " to the right"
                }
            }
        }

        return returnMe;
    }
    
    CheckBox {
        id: noDevProblems
        text: "No Problems"
        checked: true
    }

    Row {
        opacity: !noDevProblems.checked ? 1: 0
        visible: opacity > 0

        ComboBox {
            id: devType
            model: ["Deviation", "Deflection"]
        }

        CheckBox {
            id: onOpenCheck
            text: "On Opening"
        }

        RowLayout {
            opacity: onOpenCheck.checked ? 1: 0
            visible: opacity > 0
            RadioButton {
                id: openLeft
                text: "To the left"
            }
            RadioButton {
                id: openRight
                text: "To the Right"
            }
            Behavior on opacity {
                PropertyAnimation {
                    duration: 400
                }
            }
        }

        CheckBox {
            id: onCloseCheck
            text: "On Closing"
        }
        RowLayout {
            opacity: onCloseCheck.checked ? 1: 0
            visible: opacity > 0
            RadioButton {
                id: closeLeft
                text: "To the left"
            }
            RadioButton {
                id: closeRight
                text: "To the Right"
            }
            Behavior on opacity {
                PropertyAnimation {
                    duration: 400
                }
            }
        }

        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        move: Transition {
            NumberAnimation {
                properties: "x"
                duration: 200
            }
        }
    }
    

}
