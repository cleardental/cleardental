// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: qsTr("Extraoral Exam [ID: " + PATIENT_FILE_NAME + "]")

    header: CDPatientToolBar {
         headerText: "Extraoral Exam:"
    }

    function loadPrevData() {
        neckroMancer.loadPrevData(eoESaver.value("Neck",""));
        subMan.loadPrevData(eoESaver.value("SubmandibularNodes",""));
        maxRow.loadPrevData(eoESaver.value("MaxillaryNodes",""));
        itsTMD.loadPrevData(eoESaver.value("TMJ",""));
        deviantRow.loadPrevData(eoESaver.value("Deviation-Deflection",""));
        lipRow.loadPrevData(eoESaver.value("Lips",""));
        visRow.loadPrevData(eoESaver.value("VisualAsymmetry",""));
    }

    CDTranslucentPane {
        anchors.centerIn:  parent
        backMaterialColor: Material.Green

        GridLayout {
            id: bigGrid
            columns:  2
            anchors.margins: 10
            rowSpacing: 20
            columnSpacing: 25


            CDDescLabel {
                text: "Neck"
                height: neckroMancer.height
                verticalAlignment: Text.AlignVCenter
            }

            NeckRow {
                id: neckroMancer
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Submandibular Nodes"
                verticalAlignment: Text.AlignVCenter
            }

            SubmandibularRow {
                id: subMan
            }

            CDDescLabel {
                text: "Maxillary Nodes"
                verticalAlignment: Text.AlignVCenter
            }

            MaxillaryRow {
                id: maxRow
            }

            CDDescLabel {
                text: "TMD / Clicking"
                verticalAlignment: Text.AlignVCenter
            }

            TMJRow {
                id: itsTMD
            }

            CDDescLabel {
                text: "Opening/Closing\nDeviation/Deflection"
                verticalAlignment: Text.AlignVCenter
            }

            DeviationRow {
                id: deviantRow
            }

            CDDescLabel {
                text: "Lips"
                verticalAlignment: Text.AlignVCenter
            }

            LipRow {
                id: lipRow
            }

            CDDescLabel {
                text: "Visual asymmetry"
                verticalAlignment: Text.AlignVCenter
            }

            VisualRow {
                id: visRow
            }
        }
    }

    CDSaveAndCloseButton {
        id: saveButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            eoESaver.setValue("Neck",neckroMancer.generateSaveString());
            eoESaver.setValue("SubmandibularNodes",subMan.generateSaveString());
            eoESaver.setValue("MaxillaryNodes", maxRow.generateSaveString());
            eoESaver.setValue("TMJ", itsTMD.generateSaveString());
            eoESaver.setValue("Deviation-Deflection",deviantRow.generateSaveString());
            eoESaver.setValue("Lips",lipRow.generateSaveString());
            eoESaver.setValue("VisualAsymmetry", visRow.generateSaveString());            
            eoESaver.sync();

            helper.updateReviewFile("EoE",PATIENT_FILE_NAME);

            gitMan.commitData("Updated Extraoral Exam for " + PATIENT_FILE_NAME);
            Qt.quit();
        }

        CDFileLocations {
            id: fileLocs
        }

        Settings {
            id: eoESaver
            fileName: fileLocs.getEoEFile(PATIENT_FILE_NAME)
        }

        CDGitManager {
            id: gitMan
        }

        CDCommonFunctions {
            id: helper
        }
    }


    Component.onCompleted: {
        rootWin.loadPrevData();
    }

}
