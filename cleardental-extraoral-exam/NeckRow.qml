// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2

Row {
    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }
        if(prevString.startsWith(neckPain.text)) {
            neckPain.checked = true;
            var side = prevString.split(":")[1].trim();
            neckPainLeft.checked = side.includes("Left");
            neckPainRight.checked = side.includes("Right");
        }
        else {
            neckOther.checked = true;
            neckOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe="";
        if(neckWNL.checked) {
            returnMe = "WNL";
        }
        else if(neckPain.checked) {
            returnMe = "Pain on Palpation";
            var left = neckPainLeft.checked;
            var right = neckPainRight.checked;
            if(left && right) {
                returnMe += ": Left and Right sides"
            }
            else if(left) {
                returnMe += ": Left side"
            }
            else if(right) {
                returnMe += ": Right side"
            }
        }
        else if(neckOther.checked) {
            returnMe = neckOtherInput.text
        }

        return returnMe;
    }

    RadioButton {
        id: neckWNL
        checked: true
        text: "WNL"
    }
    RadioButton {
        id: neckPain
        text: "Pain on Palpation";
    }

    CheckBox {
        id: neckPainLeft
        opacity: neckPain.checked ? 1: 0
        visible: opacity > 0
        text: "Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: neckPainRight
        opacity: neckPain.checked ? 1: 0
        visible: opacity > 0
        text: "Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: neckOther
        text: "Other"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    TextField {
        id: neckOtherInput
        opacity: neckOther.checked ? 1 :0
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    move: Transition {
        NumberAnimation {
            properties: "x"
            duration: 200
        }
    }
}
