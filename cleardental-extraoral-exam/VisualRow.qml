// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2

Row {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }

        visualWNL.checked = false;
        visualOther.checked = true;
        visualOtherInput.text = prevString;
    }

    function generateSaveString() {
        var returnMe = "";
        if(visualWNL.checked) {
            returnMe = "WNL";
        }
        else {
            returnMe = visualOtherInput.text;
        }

        return returnMe;
    }


    RadioButton {
        id: visualWNL
        checked: true
        text: "WNL"
    }


    RadioButton {
        id: visualOther
        text: "Visual asymmetry noted:"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    TextField {
        id: visualOtherInput
        opacity: visualOther.checked ? 1 :0
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }
}
