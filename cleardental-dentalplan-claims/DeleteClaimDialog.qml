// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


CDTranslucentDialog {
    id: deleteClaimDia
    property var claimObj: ({})

    CDCommonFunctions {
        id: m_comFuns;
    }

    CDFileLocations {
        id: m_locs
    }

    CDTextFileManager {
        id: m_tMan
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.fillWidth: true
            backMaterialColor: Material.Pink
            ColumnLayout {
                CDHeaderLabel {
                    id: headLab
                    text:"Delete Claim?"
                }

                Label {
                    text: "Are you sure you want to delete this claim?"
                }

                GridLayout {
                    columns: 2
                    Label {
                        text: claimObj["PatientName"];
                        Layout.alignment: Qt.AlignBottom
                    }
                    Image {
                        id: proImage
                        Layout.maximumHeight: 64
                        Layout.maximumWidth: 64
                        width: 64
                        source: "file://" + m_locs.getProfileImageFile(claimObj["PatientName"])
                    }
                    CDDescLabel {
                        text: "Procedure Date"
                    }
                    Label {
                        text: new Date(claimObj["ProcedureDate"]).toLocaleDateString()
                    }
                    CDDescLabel {
                        text: "Procedure Information"
                    }
                    Label {
                        text: m_comFuns.makeTxItemString(claimObj["Procedure"])
                    }
                    CDDescLabel {
                        text: "Status"
                    }
                    Label {
                        text: claimObj["Status"]
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                text: "No, nevermind"
                onClicked: deleteClaimDia.reject();
            }
            CDDeleteButton {
                text: "Yes, delete the claim"
                onClicked: {
                    var patientID = claimObj["PatientName"];
                    var claimIndex = claimObj["ClaimIndex"];
                    var claimsList = JSON.parse(m_tMan.readFile(m_locs.getClaimsFile(patientID)));
                    claimsList.splice(claimIndex,1);
                    m_tMan.saveFile(m_locs.getClaimsFile(patientID),JSON.stringify(claimsList,null,"\t"));
                    gitMan.commitData("Removed claim from " + patientID);

                    deleteClaimDia.accept();
                }
            }
        }
    }
}
