// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTranslucentDialog {
    id: clearingHouseDia

    property var sendClaimList: []

    property var patDict: ({});



    CDClearingHouseFunctions {
        id: clearingHouseFuns
    }

    CDCommonFunctions {
        id: m_comFuns;
    }

    CDFileLocations {
        id: m_locs
    }

    CDTextFileManager {
        id: m_tMan
    }

    CDGitManager {
        id: m_gitMan
    }

    Component {
        id: labelComp
        Label {
            id: lab
            property bool isSus: true //The Imposter!!!!
        }
    }

    onSendClaimListChanged: {
        for(var k=0;k<claimGridLayout.children.length;k++) {
            var child = claimGridLayout.children[k];
            if("isSus" in child) {
                child.destroy();
            }
        }

        for(var i=0;i<sendClaimList.length;i++) {
            labelComp.createObject(claimGridLayout,{"text":sendClaimList[i]["PatientName"]});
            labelComp.createObject(claimGridLayout,{"text": new Date(sendClaimList[i]["ProcedureDate"]).
                                       toLocaleDateString()});
            labelComp.createObject(claimGridLayout,{"text":
                                       m_comFuns.makeTxItemString(sendClaimList[i]["Procedure"])});
            labelComp.createObject(claimGridLayout,{"text":sendClaimList[i]["Status"]});
            //buttonComp.createObject(claimGridLayout,({"patID":sendClaimList[i]["PatientName"]}));
            //checkComp.createObject(claimGridLayout,({"patID":sendClaimList[i]["PatientName"]}));
        }
    }

    function makeSent(getPatID,getIndex) {
        var claimsList = JSON.parse(m_tMan.readFile(m_locs.getClaimsFile(getPatID)));
        var makeMeSent = claimsList[getIndex]
        makeMeSent["Status"] = "Sent";
        makeMeSent["SentDate"] = (new Date()).toString();
        makeMeSent["SentTo"] = clearingHouseBox.currentText;
        m_tMan.saveFile(m_locs.getClaimsFile(getPatID),JSON.stringify(claimsList,null,"\t"));
        gitMan.commitData("Marked claim as sent for " + getPatID);
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.fillWidth: true
            backMaterialColor: Material.Pink
            ColumnLayout {
                CDHeaderLabel {
                    id: headLab
                    text:"Submit Claims?"
                }

                Label {
                    text: "Are you sure you want to submit these claims?"
                }

                GridLayout {
                    id: claimGridLayout
                    columns: 4
                    CDDescLabel {
                        text: "Patient ID";
                    }
                    CDDescLabel {
                        text: "Procedure Date"
                    }

                    CDDescLabel {
                        text: "Procedure Information"
                    }
                    CDDescLabel {
                        text: "Current Status"
                    }
                }

                MenuSeparator {
                    Layout.fillWidth: true
                }

                RowLayout {
                    CDDescLabel {
                        text: "Sent via which clearing house?"
                    }
                    ComboBox {
                        id: clearingHouseBox
                        Layout.fillWidth: true
                        model: ["EDS EDI","DentalXChange","Vyne / Tesia"]
                    }
                }
                RowLayout {
                    id: attachmentRow
                    CDButton {
                        id: butter
                        text: "Select Radiographs"
                        property string patID: ""

                        CDSelectRadiographsDialog {
                            id: selectRadioDia

                            onAccepted: {
                                butter.text = "Select Radiographs (" + selectedList.length + ")"
                            }
                        }

                        onClicked: {
                            selectRadioDia.patID = patID;
                            selectRadioDia.open();
                        }
                    }

                    CheckBox {
                        id: attachPerio
                        text: "Attach Periodontal Chart"
                    }
                }
            }
        }



        RowLayout {
            CDCancelButton {
                text: "No, nevermind"
                onClicked: {
                    clearingHouseDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDSaveAndCloseButton {
                icon.name:  "document-send"
                text: "Yes, Submit Claims"
                onClicked:  {
                    //now send them out
                    var patIDs = Object.keys(patDict);
                    for(var i=0;i<patIDs.length;i++) {
                        clearingHouseFuns.generateEDSEDI_JSON_string(patIDs[i],patDict[patIDs[i]],
                                                                     false,
                                                                     false,
                                                                     true,
                                                                     attachPerio.checked,
                                                                     selectRadioDia.selectedList);
                    }

                    //now mark each of the claim procedures as "sent" and pray that it will be paid
                    for(i=0;i<sendClaimList.length;i++) {
                        makeSent(sendClaimList[i]["PatientName"],sendClaimList[i]["ClaimIndex"])
                    }


                    clearingHouseDia.accept();
                }
            }
        }
    }

    onVisibleChanged: {
        if(visible) {
            //First make a dictionary with the patID as the key

            for(var i=0;i<sendClaimList.length;i++) {
                if(sendClaimList[i]["PatientName"] in patDict) {
                    (patDict[sendClaimList[i]["PatientName"]]).push(sendClaimList[i]);
                }
                else {
                    patDict[sendClaimList[i]["PatientName"]] = [];
                    (patDict[sendClaimList[i]["PatientName"]]).push(sendClaimList[i]);
                }
            }

            var patIDs = Object.keys(patDict);

            if(patIDs.length === 1) {
                attachmentRow.visible = true;
                butter.patID = patIDs[0];
            }
            else {
                attachmentRow.visible = false;
            }
        }
        else {
            patDict =  ({});
        }
    }

}
