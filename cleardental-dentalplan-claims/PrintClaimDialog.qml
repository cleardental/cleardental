// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTranslucentDialog {
    id: printClaimDia

    property var printList: []

    CDPrinter {
        id: printer
    }

    CDCommonFunctions {
        id: m_comFuns;
    }

    CDFileLocations {
        id: m_locs
    }

    CDTextFileManager {
        id: m_tMan
    }

    CDGitManager {
        id: m_gitMan
    }

    Component {
        id: labelComp
        Label {
            id: lab
            property bool isSus: true //The Imposter!!!!
        }
    }

    onPrintListChanged: {
        for(var k=0;k<claimGridLayout.children.length;k++) {
            var child = claimGridLayout.children[k];
            if("isSus" in child) {
                child.destroy();
            }
        }

        for(var i=0;i<printList.length;i++) {
            labelComp.createObject(claimGridLayout,{"text":printList[i]["PatientName"]});
            labelComp.createObject(claimGridLayout,{"text": new Date(printList[i]["ProcedureDate"]).
                                       toLocaleDateString()});
            labelComp.createObject(claimGridLayout,{"text":
                                       m_comFuns.makeTxItemString(printList[i]["Procedure"])});
            labelComp.createObject(claimGridLayout,{"text":printList[i]["Status"]});
        }
    }

    function makeSent(getPatID,getIndex) {
        var claimsList = JSON.parse(m_tMan.readFile(m_locs.getClaimsFile(getPatID)));
        var makeMeSent = claimsList[getIndex]
        makeMeSent["Status"] = "Sent";
        makeMeSent["SentDate"] = (new Date()).toString();
        makeMeSent["SentTo"] = "Printed";
        m_tMan.saveFile(m_locs.getClaimsFile(getPatID),JSON.stringify(claimsList,null,"\t"));
        gitMan.commitData("Marked claim as sent for " + getPatID);
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.fillWidth: true
            backMaterialColor: Material.Pink
            ColumnLayout {
                CDHeaderLabel {
                    id: headLab
                    text:"Print Claims?"
                }

                Label {
                    text: "Are you sure you want to print out these claims?"
                }

                GridLayout {
                    id: claimGridLayout
                    columns: 4
                    CDDescLabel {
                        text: "Patient ID";
                    }
                    CDDescLabel {
                        text: "Procedure Date"
                    }

                    CDDescLabel {
                        text: "Procedure Information"
                    }
                    CDDescLabel {
                        text: "Current Status"
                    }

                }
            }
        }

        RowLayout {
            CDCancelButton {
                text: "No, nevermind"
                onClicked: {
                    printClaimDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDSaveAndCloseButton {
                icon.name: "document-print"
                text: "Yes, Print Claims"
                onClicked:  {
                    //First make a dictionary with the patID as the key
                    var patDict = ({});
                    for(var i=0;i<printList.length;i++) {
                        if(printList[i]["PatientName"] in patDict) {
                            (patDict[printList[i]["PatientName"]]).push(printList[i]);
                        }
                        else {
                            patDict[printList[i]["PatientName"]] = [];
                            (patDict[printList[i]["PatientName"]]).push(printList[i]);
                        }
                    }

                    //now print them out
                    var patIDs = Object.keys(patDict);
                    for(i=0;i<patIDs.length;i++) {
                        var patIDToSend = patIDs[i];
                        var claimList = patDict[patIDToSend];
                        while(claimList.length > 10) { //we can only handle 10 claims per form
                            printer.printClaimForm(claimList.slice(0,10));
                            claimList.splice(0,10)
                        }
                        printer.printClaimForm(claimList);
                    }

                    //now mark each of the claim procedures as "sent" and pray that it will be paid
                    for(i=0;i<printList.length;i++) {
                        makeSent(printList[i]["PatientName"],printList[i]["ClaimIndex"])
                    }


                    printClaimDia.accept();
                }
            }
        }
    }

}
