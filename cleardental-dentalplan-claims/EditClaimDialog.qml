// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


CDTranslucentDialog {
    id: editClaimDia
    property var claimObj: ({})



    CDCommonFunctions {
        id: m_comFuns;
    }

    CDFileLocations {
        id: m_locs
    }

    CDTextFileManager {
        id: m_tMan
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                RowLayout {
                    CDHeaderLabel {
                        id: headLab
                        text:"Edit Claim"
                    }
                    Label {
                        Layout.fillWidth: true
                    }

                    Label {
                        text: claimObj["PatientName"];
                        Layout.alignment: Qt.AlignBottom
                    }
                    Image {
                        id: proImage
                        Layout.maximumHeight: 64
                        Layout.maximumWidth: 64
                        width: 64
                        source: "file://" + m_locs.getProfileImageFile(claimObj["PatientName"])
                    }

                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Procedure Date"
                    }

                    CDDatePicker {
                        id: procedureDate
                        setToday: false

                    }

                    CDDescLabel {
                        text: "NPI"
                    }

                    TextField {
                        id: npiField
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "ProviderUnixID"
                    }

                    TextField {
                        id: providerUnixID
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "Procedure Name"
                    }

                    TextField {
                        id: procedureName
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "DCode"
                    }

                    TextField {
                        id: dCode
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "Tooth"
                    }

                    TextField {
                        id: toothNumber
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "Base Price"
                    }

                    TextField {
                        id: basePrice
                        Layout.minimumWidth: 200
                    }

                    CDDescLabel {
                        text: "Status"
                    }

                    ComboBox {
                        id: statusBox
                        model: ["Unsent","Sent", "Paid","Rejected","Submitted Again", "Given Up"]
                        Layout.minimumWidth: 200

                        onCurrentIndexChanged: {
                            sentDate.setDateObj(new Date());
                        }
                    }

                    CDDescLabel {
                        text: "Sent Date"
                        visible: statusBox.currentIndex > 0
                    }

                    CDDatePicker {
                        id: sentDate
                        setToday: false
                        visible: statusBox.currentIndex > 0
                    }

                    CDDescLabel {
                        text: "Sent To"
                        visible: statusBox.currentIndex > 0
                    }

                    TextField {
                        id: sentTo
                        visible: statusBox.currentIndex > 0
                    }

                    GridLayout {
                        visible: statusBox.currentIndex === 2
                        Layout.fillWidth: true
                        columns: 4
                        Layout.columnSpan: 2

                        CDDescLabel {
                            text: "Pay Date"
                            visible: "PayDate" in claimObj
                        }

                        CDDatePicker {
                            id: payDate
                            visible: "PayDate" in claimObj
                        }

                        CDDescLabel {
                            text: "Paid Amount"
                        }

                        TextField {
                            id: paidAmount
                        }

                        CDDescLabel {
                            text: "Write Off"
                        }

                        TextField {
                            id: writeOff
                        }

                        CDDescLabel {
                            text: "Max Allowed"
                        }

                        TextField {
                            id: maxAllowed
                        }

                        CDDescLabel {
                            text: "Patient Portion"
                        }

                        TextField {
                            id: patientPortion
                        }

                        CDDescLabel {
                            text: "Downgrade Code"
                        }

                        TextField {
                            id: downgradeCode
                        }
                    }


                }
            }
        }

        RowLayout {
            CDGitManager {
                id: gitMan
            }

            CDCancelButton {
                onClicked: editClaimDia.reject();
            }

            Label {
                Layout.fillWidth: true
            }

            CDSaveAndCloseButton {
                onClicked: {
                    claimObj["ProcedureDate"] = procedureDate.getDateObj().toString();
                    claimObj["NPI"] = npiField.text;
                    claimObj["ProviderUnixID"] = providerUnixID.text;
                    claimObj["Procedure"]["ProcedureName"] = procedureName.text;
                    claimObj["Procedure"]["DCode"] = dCode.text;
                    if(toothNumber.text.length > 0) {
                        claimObj["Procedure"]["Tooth"] = toothNumber.text;
                    }

                    claimObj["Procedure"]["BasePrice"] = basePrice.text;
                    claimObj["Status"] = statusBox.currentText;
                    if(maxAllowed.text.length > 0) {
                        claimObj["MaxAllowed"] = maxAllowed.text;
                    }
                    if(paidAmount.text.length > 0) {
                        claimObj["PaidAmount"] = paidAmount.text;
                    }
                    if(patientPortion.text.length > 0) {
                        claimObj["PatientPortion"] = patientPortion.text;
                    }
                    if(writeOff.text.length > 0) {
                        claimObj["WriteOff"] = writeOff.text;
                    }
                    if(downgradeCode.text.length > 0) {
                        claimObj["DowngradeCode"]  = downgradeCode.text;
                    }
                    if(statusBox.currentIndex > 0) {
                        claimObj["SentDate"] = sentDate.getDateObj().toString();
                    }
                    if(sentTo.text.length > 0) {
                        claimObj["SentTo"] = sentTo.text;
                    }

                    //Prevent the claim item from getting the extra information needed for the claim
                    var patientID = claimObj["PatientName"];
                    delete claimObj["PatientName"];
                    var claimIndex = claimObj["ClaimIndex"];
                    delete claimObj["ClaimIndex"];
                    delete claimObj["Provider"];

                    var claimsList = JSON.parse(m_tMan.readFile(m_locs.getClaimsFile(patientID)));
                    claimsList[claimIndex] = claimObj;

                    m_tMan.saveFile(m_locs.getClaimsFile(patientID),JSON.stringify(claimsList,null,"\t"));
                    gitMan.commitData("Updated claim for " + patientID);
                    editClaimDia.accept(); //parent will reload after the save
                }

            }
        }
    }

    Component.onCompleted: {
        procedureDate.setDateObj(new Date(claimObj["ProcedureDate"]))
        npiField.text = claimObj["NPI"];
        providerUnixID.text = claimObj["ProviderUnixID"];
        procedureName.text = claimObj["Procedure"]["ProcedureName"];
        dCode.text = claimObj["Procedure"]["DCode"];
        if('Tooth' in claimObj["Procedure"]) {
            toothNumber.text = claimObj["Procedure"]["Tooth"]
        }
        basePrice.text = claimObj["Procedure"]["BasePrice"];
        statusBox.currentIndex = statusBox.find(claimObj["Status"]);

        if("MaxAllowed" in claimObj) {
            maxAllowed.text = claimObj["MaxAllowed"];
        }
        if("PaidAmount" in claimObj) {
            paidAmount.text = claimObj["PaidAmount"];
        }
        if("PatientPortion" in claimObj) {
            patientPortion.text = claimObj["PatientPortion"];
        }
        if("WriteOff" in claimObj) {
            writeOff.text = claimObj["WriteOff"];
        }
        if("DowngradeCode" in claimObj) {
            downgradeCode.text = claimObj["DowngradeCode"];
        }
        if("SentDate" in claimObj) {
            sentDate.setDateObj(new Date(claimObj["SentDate"]))
        }
        if("SentTo" in claimObj) {
            sentTo.text =  claimObj["SentTo"];
        }

    }

}
