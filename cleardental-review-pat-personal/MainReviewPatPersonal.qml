// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Edit Personal [ID: " + PATIENT_FILE_NAME + "]")

    function updateReview() {
        var today = new Date();
        var dateString = (today.getMonth() +1) + "/" + (today.getDate()) +
                "/" + today.getFullYear();
        noChangeSettings.setValue("Medical",dateString);
        noChangeSettings.sync();
    }

    header: CDPatientToolBar {
        headerText: "Personal Information:"
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    Drawer {
        id: drawer
        width: 0.33 * rootWin.width
        height: rootWin.height

        background: Rectangle {
            gradient: Gradient {
                     GradientStop { position: 0.0; color: "white" }
                     GradientStop { position: 0.5; color: "white" }
                     GradientStop { position: 1.0; color: "transparent" }
                 }
            anchors.fill: parent
        }

        ColumnLayout {
            anchors.left:  parent.left
            anchors.right: parent.right
            TabButton {
                text: qsTr("Name")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"NamePage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Personal")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"PersonalPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Home Address")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"AddressPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Phone Number(s)")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"PhonePage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Email and Social Media")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"EmailPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Work Information")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"WorkPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Emergency Contact")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"EmergencyPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Preferences")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"PreferencePage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text:qsTr("Photo")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"PhotoPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text:qsTr("Scan Cards")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"ScanImage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text:qsTr("Family")
                //font.family: "Montserrat"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"Family.qml");
                    drawer.close();
                }
            }
        }
    }

    StackView   {
        id: view
        anchors.fill: parent
        initialItem: NamePage{}
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    Settings {
        id: noChangeSettings
        fileName: fileLocs.getReviewsFile(PATIENT_FILE_NAME);
    }
}
