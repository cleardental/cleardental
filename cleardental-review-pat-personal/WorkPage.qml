// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: workPage

    function loadData() {
        workAddr1.text = workSett.value("WorkAddr1","")
        workAddr2.text = workSett.value("WorkAddr2","")
        workCity.text = workSett.value("WorkCity","")
        workState.currentIndex = workState.find(workSett.value("WorkState",""))
        workZip.text = workSett.value("WorkZip","")
        workCountry.text = workSett.value("WorkCountry","")
        workPhone.text = workSett.value("WorkPhone","");
    }

    function saveData() {
        workSett.setValue("WorkAddr1",workAddr1.text);
        workSett.setValue("WorkAddr2",workAddr2.text);
        workSett.setValue("WorkCity",workCity.text);
        workSett.setValue("WorkState",workState.currentText);
        workSett.setValue("WorkZip",workZip.text);
        workSett.setValue("WorkCountry",workCountry.text);
        workSett.setValue("WorkPhone",workPhone.text);
        workSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Work information for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: workSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Work"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: workGrid
            columns: 2
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Work Address"
                Layout.columnSpan: 2
            }

            Label {text: "Work Street Address (Line 1)";font.bold: true}
            CDCopyLabel{
                id:workAddr1;
                placeholderText: "Work Street Address (Line 1)"
                Layout.minimumWidth: 300
            }

            Label {text: "Work Street Address (Line 2)";font.bold: true}
            CDCopyLabel{
                id:workAddr2;
                placeholderText: "Work Street Address (Line 2)"
                Layout.fillWidth: true
            }

            Label {text: "Work City";font.bold: true}
            CDCopyLabel{
                id:workCity;
                placeholderText: "Enter work City"
                Layout.fillWidth: true
            }

            Label {text: "Work State";font.bold: true}
            ComboBox {
                id: workState
                model: ["AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","GU",
                    "HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME","MH","MI",
                    "MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH",
                    "OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI",
                    "VT","WA","WI","WV","WY"]
                Layout.fillWidth: true
            }

            Label {text: "Work Zip code";font.bold: true}
            CDCopyLabel{
                id:workZip;
                placeholderText: "Work Zip code"
                Layout.fillWidth: true
            }

            Label {text: "Country (if not USA)";font.bold: true}
            CDCopyLabel{
                id:workCountry;
                placeholderText: "If not in USA, enter country"
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Work Phone"
            }

            CDCopyLabel {
                id: workPhone
                Layout.fillWidth: true
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
