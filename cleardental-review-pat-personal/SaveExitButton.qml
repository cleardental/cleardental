// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

Button {
    id: saveExitButton
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.margins: 10
    highlighted: true
    Material.accent: Material.Teal
    icon.name: "preflight-verifier"
    text: "Save and Exit"
    icon.width: 32
    icon.height: 32
    font.pointSize: 18

    Component.onCompleted: {
        rootWin.keyboardSaveButton = saveExitButton
    }
}
