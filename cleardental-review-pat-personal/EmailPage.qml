// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: emailPage

    function loadData() {
        homeEmail.text = emailSett.value("Email","");
        facebookID.text = emailSett.value("Facebook","");
        instagramID.text = emailSett.value("Instagram","");
        whatsAppID.text = emailSett.value("WhatsApp","");
    }

    function saveData() {
        emailSett.setValue("Email", homeEmail.text);
        emailSett.setValue("Facebook", homeEmail.text);
        emailSett.setValue("Instagram", homeEmail.text);
        emailSett.setValue("WhatsApp", homeEmail.text);
        emailSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Email and Social Media for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: emailSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Emails"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: emailGrid
            columns: 2
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Email Address"
                Layout.columnSpan: 2
            }

            Label {text: "Regular Email";font.bold: true}
            CDCopyLabel{
                id:homeEmail;
                placeholderText: "Patient's regular e-mail"
                inputMethodHints: Qt.ImhEmailCharactersOnly
                Layout.fillWidth: true
            }

            Label {text: "Facebook ID";font.bold: true}
            CDCopyLabel{
                id:facebookID;
                placeholderText: "Enter in Facebook ID/URL"
                Layout.fillWidth: true
            }

            Label {text: "Instagram ID";font.bold: true}
            CDCopyLabel{
                id:instagramID;
                placeholderText: "Enter in Instagram ID/URL"
                Layout.fillWidth: true
            }

            Label {text: "WhatsApp ID";font.bold: true}
            CDCopyLabel{
                id:whatsAppID;
                placeholderText: "Enter in WhatsApp ID/URL"
                Layout.fillWidth: true
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
