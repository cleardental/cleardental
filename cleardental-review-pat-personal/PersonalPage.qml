// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: personalPage


    function loadData() {
        patDOB.setDate(personalSett.value("DateOfBirth"),"");
        var sex = personalSett.value("Sex");
        if(sex === "Male") {
            maleRadio.checked = true;
        }
        else if(sex === "Female") {
            femaleRadio.checked = true;
        }
        else {
            otherRadio.checked = true;
        }
        gender.text = personalSett.value("Gender","");
        race.currentIndex = race.find(personalSett.value("Race",""));
        ethnicity.currentIndex = ethnicity.find(personalSett.value("Ethnicity",""));

    }

    function saveData() {
        personalSett.setValue("DateOfBirth",patDOB.getDate());
        personalSett.setValue("Sex",radioGroup.checkedButton.text);
        personalSett.setValue("Gender",gender.text);
        personalSett.setValue("Race",race.currentText);
        personalSett.setValue("Ethnicity",ethnicity.currentText);
        personalSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Personal Data for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: personalSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Personal"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: nameGrid
            columns: 2
            anchors.centerIn: parent
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Personal Information"
                Layout.columnSpan: 2
            }

            Label{text:"Date of Birth";font.bold: true}
            CDDatePicker{ id:patDOB;}

            Label{text:"Sex";font.bold: true}
            ButtonGroup { id: radioGroup }
            Row {
                id: sexRow

                RadioButton {
                    id: maleRadio
                    checked: true
                    text: qsTr("Male")
                    ButtonGroup.group: radioGroup
                }

                RadioButton {
                    id: femaleRadio
                    text: qsTr("Female")
                    ButtonGroup.group: radioGroup
                }

                RadioButton {
                    id: otherRadio
                    text: "Other"
                    ButtonGroup.group: radioGroup
                }
            }

            Label{text:"Gender (if different)";font.bold: true}
            CDCopyLabel{
                id:gender;
                placeholderText: "Gender if different from sex;"+
                                 " remember to include pronoun"
                Layout.fillWidth: true
            }

            Label{text:"Race";font.bold: true}
            ComboBox {
                id: race
                model: ["I choose not to answer","American Indian or Alaska native",
                    "Asian", "Black or African American", "Polynesian","White","Other"]
                Layout.fillWidth: true
            }

            Label{text:"Ethnicity";font.bold: true}
            ComboBox {
                id: ethnicity
                model: ["I choose not to answer","Hispanic Or Latino",
                    "Not Hispanic or Latino"]
                Layout.fillWidth: true
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
