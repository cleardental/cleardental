// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: familyPage

    property var relList: patFileMan.getRelationships(PATIENT_FILE_NAME);

    function updateFamily() {
        relList = patFileMan.getRelationships(PATIENT_FILE_NAME);


        for(var k=4;k<familyGrid.children.length;k++) { //delete the previous ones if there are any
            familyGrid.children[k].destroy();
        }

        for(var i=0;i<relList.length;i++) {
            var obj = relList[i];
            labelComp.createObject(familyGrid,{"text":obj[0]});
            labelComp.createObject(familyGrid,{"text":obj[1]});
            deleteButton.createObject(familyGrid,{"rowIndex":i});
        }
    }

    CDPatientFileManager {
        id: patFileMan
    }


    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    Label {
        font.pointSize: 32
        visible: relList.length === 0
        text: "No reationships at this point"
        anchors.centerIn: parent
    }

    CDTranslucentPane {
        id: familyPane
        anchors.centerIn: parent
        visible: relList.length > 0

        GridLayout {
            id: familyGrid
            columns: 3
            columnSpacing: 20

            function killTheRel(getIndex) {
                var startIndex = 4 + (getIndex*3);
                patFileMan.remRelationship(PATIENT_FILE_NAME,familyGrid.children[startIndex].text,
                                           familyGrid.children[startIndex+1].text);
                updateFamily();
            }


            CDHeaderLabel {
                text: "Family"
                Layout.columnSpan: 3
            }
            CDDescLabel {
                text: "Patient ID"
            }
            CDDescLabel {
                text: "Relationship"
            }

            Label{} //placeholder for delete button


            Component {
                id: labelComp

                Label {
                    property bool isTemp: true
                }
            }

            Component {
                id: deleteButton

                Button {
                    icon.name: "list-remove"
                    text: "Remove Relationship"
                    property int rowIndex:-1;
                    property bool isTemp: true
                    onClicked: {
                        familyGrid.killTheRel(rowIndex);
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }
        }
    }


    CDAddButton {
        id: addButonExistingpat
        text: "Add new relation (existing patient)"
        anchors.right: familyPane.right
        anchors.top: familyPane.bottom

        AddRelExistingPatDia {
            id: addExtPatDia
            onAccepted: updateFamily();
        }

        onClicked: addExtPatDia.open();
    }

    CDAddButton {
        id: addButonNewpat
        text: "Add new relation (new patient)"
        anchors.right: addButonExistingpat.right
        anchors.top: addButonExistingpat.bottom

        AddRelNewPatDia {
            id: addRelNewPatDia
            onAccepted: updateFamily();
        }

        onClicked: addRelNewPatDia.open();
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

    Component.onCompleted: {
        updateFamily();
    }

}
