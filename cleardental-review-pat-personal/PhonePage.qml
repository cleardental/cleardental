// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: phonePage

    function loadData() {
        homePhone.text = phoneSett.value("HomePhone","");
        cellPhone.text = phoneSett.value("CellPhone","");
        doNotTextBox.checked = JSON.parse(phoneSett.value("DoNotText",false));
    }

    function saveData() {
        phoneSett.setValue("HomePhone", homePhone.text);
        phoneSett.setValue("CellPhone", cellPhone.text);
        phoneSett.setValue("DoNotText", doNotTextBox.checked);
        phoneSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Phone numbers for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: phoneSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Phones"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: phoneGrid
            columns: 2
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Phone Number"
                Layout.columnSpan: 2
            }

            CDDescLabel {text: "Home Phone Number";}
            CDCopyLabel{ //TODO: Format the text as a phone number
                id:homePhone;
                placeholderText: "Add \"+\" if international"
                inputMethodHints: Qt.ImhDialableCharactersOnly
                Layout.minimumWidth: 200
            }

            CDDescLabel {text: "Cell Phone Number";}
            CDCopyLabel{
                id:cellPhone;
                placeholderText: "Add \"+\" if international"
                inputMethodHints: Qt.ImhDialableCharactersOnly
                Layout.minimumWidth: 200
            }

            CheckBox {
                id: doNotTextBox
                text: "Do not text patient"
                Layout.columnSpan: 2
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
