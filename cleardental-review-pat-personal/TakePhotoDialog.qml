// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtMultimedia 5.11
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12

Dialog {
    id: rootDia
    title: "Take Photo"
    implicitWidth: rootWin.width * .8
    implicitHeight: rootWin.height * .8
    property var theImg
    property var selector

    ColumnLayout {
        anchors.centerIn: parent

        VideoOutput {
            id: vidOutput
            source: camera
            Layout.maximumWidth: rootDia.width * .7
            Layout.maximumHeight: rootDia.height * .7
            Layout.alignment: Qt.AlignHCenter
            Rectangle {
                width: Math.min(vidOutput.contentRect.width, vidOutput.contentRect.height)
                height: width
                anchors.centerIn: parent
                color: "transparent"
                border.width: 5
                radius: 20
                border.color: "black"
            }
        }
        Image {
            id: candidateImg
            visible: source.length > 0
            Layout.maximumWidth: rootDia.width * .7
            Layout.maximumHeight: rootDia.height * .7
            MouseArea {
                anchors.fill: parent
                onVisibleChanged: {
                    if (!selector) {
                        selector = cropComponent.createObject(parent, {"x": 0, "y": 0, "width": parent.width / 4, "height": parent.width / 4})
                    }
                }
            }

        }

        Button {
            id: takePhotoButton
            text: "Take photo"
            icon.name: "camera-photo-symbolic"
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                countDown.start();
            }

            SequentialAnimation {
                id: countDown
                PropertyAnimation {
                    target: takePhotoButton
                    property: "text"
                    to: "3.."
                }
                PauseAnimation {
                    duration: 200
                }
                PropertyAnimation {
                    target: takePhotoButton
                    property: "text"
                    to: "..2.."
                }
                PauseAnimation {
                    duration: 200
                }
                PropertyAnimation {
                    target: takePhotoButton
                    property: "text"
                    to: "..1.."
                }
                PauseAnimation {
                    duration: 200
                }
                PropertyAnimation {
                    target: takePhotoButton
                    property: "text"
                    to: "Smile!"
                }
                PauseAnimation {
                    duration: 1000
                }
                ScriptAction {
                    script: {
                        takePhotoButton.text = "Take photo"
                        takePhotoButton.visible = false;
                        camera.imageCapture.captureToLocation(fileLocs.getTempImageLocation());
                        vidOutput.visible = false;
                    }
                }
            }
        }

        Button {
            id: looksGood
            text: "Looks good! Use it!"
            Layout.alignment: Qt.AlignHCenter
            visible: takeAgainButton.visible
            Material.accent: Material.Green
            highlighted: true
            onClicked: {
                theImg = candidateImg.source;
                rootDia.accept();
            }
        }

        Button {
            id:takeAgainButton
            text: "Lets try again..."
            visible: candidateImg.visible
            Layout.alignment: Qt.AlignHCenter
            Material.accent: Material.Red
            highlighted: true
            onClicked: {
                candidateImg.visible = false;
                takePhotoButton.visible =true;
                vidOutput.visible = true;
                countDown.start();

            }
        }

        Button {
            text: "Cancel taking photo"
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                rootDia.reject();
            }
        }
    }
    standardButtons: Dialog.NoButton

    onVisibleChanged: {
        if(rootDia.visible) {
            camera.start();
        } else {
            camera.stop();
        }
    }

    Component {
        id: cropComponent

        Rectangle {
            id: cropArea
            border {
                width: 2
                color: "steelblue"
            }
            color: "#354682B4"

            property int rulersSize: 18

            property double selectedX: cropArea.x / parent.width * 1920
            property double selectedY: cropArea.y / parent.height * 1080
            property double selectedWidth: cropArea.width / parent.height * 1080

            MouseArea {     // drag mouse area
                anchors.fill: parent
                drag{
                    target: parent
                    minimumX: 0
                    minimumY: 0
                    maximumX: parent.parent.width - parent.width
                    maximumY: parent.parent.height - parent.height
                    smoothed: true
                }
            }

            Rectangle {
                anchors { bottom: parent.bottom; right: parent.right }
                width: rulersSize; height: rulersSize
                color: "transparent"

                Grid {
                    anchors.fill: parent
                    rows: 3; columns: 3; spacing: 1
                    Repeater {
                        model: 9
                        Rectangle {
                            width: 4; height: 4;
                            color: "lightblue"
                            opacity: index != 0 && index != 1 && index != 3 ? 1 : 0
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    drag{
                        target: parent
                        axis: Drag.XAndYAxis
                    }
                    onMouseXChanged: {
                        if (drag.active && mouseX <= mouseY) {
                            cropArea.width = cropArea.width + mouseX;
                            if (cropArea.width < 50)
                                cropArea.width = 50;
                            if (cropArea.width + cropArea.x > parent.parent.parent.width)
                                cropArea.width = parent.parent.parent.width - cropArea.x;
                            cropArea.height = cropArea.width
                            if (cropArea.height + cropArea.y > parent.parent.parent.height) {
                                cropArea.height = parent.parent.parent.height - cropArea.y;
                                cropArea.width = cropArea.height
                            }
                        }
                    }
                    onMouseYChanged: {
                        if (drag.active && mouseY <= mouseX) {
                            cropArea.height = cropArea.height + mouseY;
                            if (cropArea.height < 50)
                                cropArea.height = 50
                            if (cropArea.height + cropArea.y > parent.parent.parent.height)
                                cropArea.height = parent.parent.parent.height - cropArea.y;
                            cropArea.width = cropArea.height;
                            if (cropArea.width + cropArea.x > parent.parent.parent.width) {
                                cropArea.width = parent.parent.parent.width - cropArea.x;
                                cropArea.height = cropArea.width
                            }
                        }
                    }
                }
            }
        }
    }

    Camera {
        id: camera
        deviceId: QtMultimedia.defaultCamera.deviceId
        captureMode: Camera.CaptureStillImage

        imageCapture {
            resolution: imageCapture.supportedResolutions[imageCapture.supportedResolutions.length -1]
            onImageCaptured: {
                console.debug(preview)
                candidateImg.source = preview
                candidateImg.visible = true
            }
        }

        Component.onCompleted: {
            //console.debug(imageCapture.supportedResolutions);
            camera.stop();
        }
    }
}
