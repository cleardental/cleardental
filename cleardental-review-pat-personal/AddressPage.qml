// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: addrPage

    function loadData() {
        addr1.text = addrSett.value("StreetAddr1","");
        addr2.text = addrSett.value("StreetAddr2","");
        city.text = addrSett.value("City","");
        state.currentIndex = state.find(addrSett.value("State"),"");
        zip.text = addrSett.value("Zip","");
        country.text = addrSett.value("Country","");
    }

    function saveData() {
        addrSett.setValue("StreetAddr1", addr1.text);
        addrSett.setValue("StreetAddr2", addr2.text);
        addrSett.setValue("City", city.text);
        addrSett.setValue("State", state.currentText);
        addrSett.setValue("Zip", zip.text);
        addrSett.setValue("Country", country.text);
        addrSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Address for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: addrSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Address"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }


    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: addrGrid
            columns: 2
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Home Address"
                Layout.columnSpan: 2

            }

            Label {text: "Street Address (Line 1)";font.bold: true}
            CDCopyLabel {
                id:addr1;
                placeholderText: "Street Address (Line 1)"
                Layout.fillWidth: true
                Layout.minimumWidth: 360
            }

            Label {text: "Street Address (Line 2)";font.bold: true}
            CDCopyLabel {
                id:addr2;
                placeholderText: "Street Address (Line 2) if needed"
                Layout.fillWidth: true
            }

            Label {text: "City";font.bold: true}
            CDCopyLabel{
                id:city;
                placeholderText: "City of the patient"
                Layout.fillWidth: true
            }

            Label {text: "State";font.bold: true}
            ComboBox {
                id: state
                model: ["AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","GU",
                    "HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME","MH","MI",
                    "MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH",
                    "OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI",
                    "VT","WA","WI","WV","WY"]
                Layout.fillWidth: true
            }

            Label {text: "Zip code";font.bold: true}
            CDCopyLabel{
                id: zip
                placeholderText: "Zip/Postal code"
                inputMethodHints: Qt.ImhDigitsOnly
                Layout.fillWidth: true

            }

            Label {text: "Country";font.bold: true}
            CDCopyLabel{
                id:country
                placeholderText: "If not in USA, enter country"
                Layout.fillWidth: true
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
