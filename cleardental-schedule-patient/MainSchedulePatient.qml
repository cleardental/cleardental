// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 6/10/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtQml.Models 2.1

CDAppWindow {
    id: rootWin
    title: "Schedule Patient [ID:" + PATIENT_FILE_NAME + "]"

    property var selectedDayChairs: []
    property var selectedDay: new Date();
    onSelectedDayChanged: loadUpDay(selectedDay)

    property real pixelsPerMin: 3
    property int timeMargin: 110
    property string viewMode: "Full"
    property int showTimeMins: 30
    property bool showButtonText: true

    //Everything is minutes since midnight
    property int dayStartTime: 9 * 60
    property int dayEndTime: 17 * 60
    property int breakStart: 11.5 * 60
    property int breakEnd: 12.5 * 60
    property bool openThisDay: true
    property bool haveBreakThisDay: true

    property string apptChair: "N/A"
    property string apptTime: "8:00 AM"
    property int apptDuration: 60

    function loadUpDay(newDay) {
        selectedDayChairs = scheduleDB.getChairs(newDay);
        setOpenAndBreakTimes(newDay);
        scheduleSnapshot.loadApptButtons();
    }

    function parseTime(inputTime) {
        //11:30 AM
        var hourMin = inputTime.split(":");
        var hour = parseInt(hourMin[0]);
        var min = hourMin[1].substring(0,2);
        var AMPM = inputTime.split(" ")[1];

        if(AMPM === "PM") {
            if(hour !== 12) {
                hour += 12;
            }
        }
        return (60*hour) + parseInt(min);
    }

    function parseTimeBlock(getTimeBlock) {
        var openTimesList=getTimeBlock.split("|");
        haveBreakThisDay = false;
        for(var i=0;i<openTimesList.length;i++) {
            var schChunk = openTimesList[i].trim();
            var schTimeParts = schChunk.split(" ");
            if(schChunk.startsWith("Open")) {
                rootWin.dayStartTime = parseTime(schTimeParts[1] + " " +
                                                 schTimeParts[2]);
            }
            else if(schChunk.startsWith("Closed")) {
                rootWin.dayEndTime = parseTime(schTimeParts[1] + " " +
                                               schTimeParts[2]);
            }
            else if(schChunk.startsWith("Lunch")) {
                haveBreakThisDay = true;
                rootWin.breakStart = parseTime(schTimeParts[6] + " "
                                               + schTimeParts[7]);
                rootWin.breakEnd = breakStart + parseInt(schTimeParts[3]);
            }
        }
    }

    function minutesToTimeString(timeInMins) {
        var result = "";
        var hours = Math.floor(timeInMins / 60);
        var minutes = timeInMins % 60;
        var AMPM = "";

        // calculate the hour and AMPM, appending the hour to result
        if (hours < 12) { // morning
            if (hours === 0) { // 0 hours -> 12 AM
                result += "12"
            } else { // rest of morning
                result += hours.toString();
            }
            AMPM = "AM";
        } else { // afternoon
            hours -= 12;
            if (hours === 0) { // noon hour
                result += "12";
            } else { // rest of afternoon
                result += hours.toString();
            }
            AMPM = "PM";
        }

        // round minutes to next lowest multiple of 5
        minutes = minutes - (minutes % 5);

        // append minutes and AMPM to result
        if (minutes < 10 ) {
            var minStr = "0" + minutes.toString();
            result = result + ":" + minStr + " " + AMPM;
        } else {
            result = result + ":" + minutes.toString() + " " + AMPM;
        }

        // return result
        return result;
    }

    function setOpenAndBreakTimes(getDate) {
        var dayOfWeek = getDate.getDay();
        var openTimes;
        var dayList = ["Sunday","Monday","Tuesday","Wednesday","Thursday",
                "Friday","Saturday"]
        var isOpen = readOpenCloseTimes.value("Open"+dayList[dayOfWeek],"false")
        if( isOpen === "true") {
            openThisDay = true;
            openTimes = readOpenCloseTimes.value(dayList[dayOfWeek]+"Hours","");
            parseTimeBlock(openTimes);
        }
        else {
            openThisDay = false;
        }
    }

    function requestUpdateGhostAppt() {
        newApptDetails.updateAllGhostFields();
        scheduleSnapshot.updateGhostAppt(apptChair, apptTime, apptDuration);
    }

    function setApptDetails(chair, time) {
        newApptDetails.updateApptDetails(chair, time);
    }


    header: CDPatientToolBar {
        headerText: "Schedule Patient:"
    }

    Settings {id: apptLoaderSettings}

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: readOpenCloseTimes
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Hours"
    }

    CDScheduleManager {
        id: scheduleDB
    }

    CDCommonFunctions {
        id: commonFuns
    }

    RowLayout {
        anchors.fill: parent
        NewApptDetails {
            id: newApptDetails
            Layout.fillHeight: true
        }
        ScheduleSnapshot {
            id: scheduleSnapshot
        }
    }
    Component.onCompleted: {
        rootWin.selectedDayChairs = scheduleDB.getChairs(selectedDay);
    }
}
