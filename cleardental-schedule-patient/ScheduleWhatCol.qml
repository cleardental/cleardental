// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ColumnLayout {
    id: scheduleWhatCol

    property bool hadCompExam: comFuns.hadACompExam(PATIENT_FILE_NAME)

    function addProphy() {
        var addProphyObj = ({});

        addProphyObj["ProcedureName"] = "Prophy";
        addProphyObj["DCode"] = "D1110";

        return comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",addProphyObj)
    }

    function addExam() {
        var addExamObj = ({});
        if(hadCompExam) {
            addExamObj["ProcedureName"] = "Periodic Exam";
            addExamObj["DCode"] = "D0120";
        }
        else {
            addExamObj["ProcedureName"] = "Comprehensive Exam";
            addExamObj["DCode"] = "D0150";
        }

        return comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",addExamObj)
    }

    function addBWs() {
        var addBWObj = ({});
        addBWObj["ProcedureName"] = "Four Bitewings";
        addBWObj["DCode"] = "D0274";
        return comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",addBWObj)
    }

    function generateProceduresString() {
        var returnMe = [];
        if(recareRadio.checked) {
            returnMe = [addExam()];
            if(comFuns.getYearsOld(PATIENT_FILE_NAME) > 13) {
                //If under 13, the exam and prophy will the same appointment so no need to add it in
                returnMe.push(addProphy());
            }
            if(schedule4BWs.checked) {
                returnMe.push(addBWs());
            }

            return JSON.stringify(returnMe);
        }
        else {
            return "[" + txItemList.selectedTxStrings + "]";
        }
    }

    RowLayout {
        RadioButton {
            id: recareRadio
            text: hadCompExam ? "Recare exam and prophy" : "Comprehensive exam and prophy"
        }
        CheckBox {
            id: schedule4BWs
            text: "Schedule 4BWs"
            opacity: recareRadio.checked ? 1 : 0
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }
        }

    }

    RadioButton {
        id: fromTxPlanRadio
        text: "From treatment plan"
    }

    ButtonGroup {
        buttons: [recareRadio,fromTxPlanRadio]
    }

    CDCommonFunctions {
        id: comFuns
    }

    
    ColumnLayout {
        id: txPlanRow
        enabled: fromTxPlanRadio.checked
        property var treatmentPlansObj: ({})
        property string selectedTxPlan: ""

        CDTextFileManager {
            id: textMan
        }

        CDFileLocations {
            id: fLocs
        }

        RowLayout {
            id: lineRow
            Label {
                text: "Treatment Plan Name"
                font.bold: true
            }

            ComboBox {
                id: txPlanNamesBox
                Layout.fillWidth: true
                onDisplayTextChanged: {
                    txItemList.generateList();
                }
            }
        }

        Rectangle {
            width: lineRow.width
            height: 360
            border.color: Material.foreground
            border.width: 2
            radius: 5
            color: "transparent"
            ListView {
                id: txItemList
                anchors.fill: parent
                clip: true
                model: txModel

                property var selectedTxStrings: []

                ScrollBar.vertical: ScrollBar { }

                function generateList() {
                    var phasesList = txPlanRow.treatmentPlansObj[
                                txPlanNamesBox.displayText]
                    var phaseNames = Object.keys(phasesList);
                    txModel.clear();
                    //var txItemModel = [];
                    for(var phaseI=0;phaseI<phaseNames.length;phaseI++) {
                        var txItems = phasesList[phaseNames[phaseI]];
                        for(var ti=0;ti<txItems.length;ti++) {
                            var addMe = {
                                phaseName: phaseNames[phaseI],
                                txIndex: ti
                            };

                            txModel.append(addMe);
                        }
                    }
                }

                delegate:CheckBox {
                    id: checkDel
                    property int myTI: 0
                    property string myPhaseName: ""
                    property var myTxObj: ({})

                    function generateString(getPhase, getTxIndex) {
                        var txPlan = txPlanRow.treatmentPlansObj[txPlanNamesBox.displayText];
                        var phase = txPlan[getPhase];
                        myTxObj = phase[getTxIndex];

                        return comFuns.makeTxItemString(myTxObj);
                    }

                    text: generateString(phaseName,txIndex);
                    width: txItemList.width

                    checked: myTxObj["NextVisit"] === true


                    onCheckedChanged: {
                        if(checked) {
                            txItemList.selectedTxStrings.push(JSON.stringify(myTxObj));
                        }
                        else {
                            var ind =txItemList.selectedTxStrings.indexOf(JSON.stringify(myTxObj))
                            txItemList.selectedTxStrings.splice(ind,1);
                        }
                    }

                    Rectangle {
                        anchors.fill: parent
                        z: checkDel.z -1
                        color: Material.color(Material.Orange)
                        opacity: 0.5
                        visible: myTxObj["NextVisit"] === true
                        radius: 10
                    }
                }


                ListModel {
                    id: txModel
                }
            }
        }

        Component.onCompleted: {
            var txPlansJSON = textMan.readFile( fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
            if(txPlansJSON.length > 0) {
                treatmentPlansObj = JSON.parse(txPlansJSON);
            }
            var txPlanNames = Object.keys(treatmentPlansObj);
            txPlanNamesBox.model = txPlanNames;


            var nextApptIsProphy = false;
            var fullTxList = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME);
            for(var i=0;i<fullTxList.length;i++) {
                var txObj = fullTxList[i];
                if(txObj["NextVisit"] === true) {
                    if( (txObj["ProcedureName"] === "Prophy") ||
                            (txObj["ProcedureName"] === "Periodic Exam") ){
                        nextApptIsProphy = true;
                        i = fullTxList.length;
                    }
                }
            }
            if(nextApptIsProphy) {
                recareRadio.checked = true;
            }
            else {
                fromTxPlanRadio.checked = true;
            }

        }
    }
    
    
}
