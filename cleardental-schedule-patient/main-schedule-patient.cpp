// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cdappointment.h"
#include "cdschedulemanager.h"
#include "cddoctorlistmanager.h"
#include "cdqrcodeimageprovider.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Schedule-Patient");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<2) {
        qDebug()<<"You need to give the patient's name as an argument!";
        return -32;
    }

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDScheduleManager>("dental.clear", 1, 0, "CDScheduleManager");
    qmlRegisterType<CDAppointment>("dental.clear", 1, 0, "CDAppointment");
    qmlRegisterType<CDPatientManager>("dental.clear", 1, 0, "CDPatientManager");
    qmlRegisterType<CDFileLocations>("dental.clear", 1, 0, "CDFileLocations");
    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");

    QQmlApplicationEngine engine;
    engine.addImageProvider("QRCode", new CDQRCodeImageProvider());
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    engine.load(QUrl(QStringLiteral("qrc:/MainSchedulePatient.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
