// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import dental.clear 1.0

ComboBox {
    id: labBox
    textRole: "LabName"

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: textFileMan
    }

    Component.onCompleted:  {
        var labJSON = textFileMan.readFile(fLocs.getPracticeLabsFile("local"));
        var labsObj = ({});
        if(labJSON.length > 0) {
            labsObj = JSON.parse(labJSON);
        }
        model = labsObj
    }
}
