// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ColumnLayout {

    property bool isMax: parent.isMaxillary;
    property int nextStep: 0

    function generateCaseNote() {
        if(isMax) {
            var returnMe = "Patient presented for Maxillary Complete Denture Step 6: Adjustments\n";
        }
        else {
            returnMe = "Patient presented for Mandibular Complete Denture Step 6: Adjustments\n";
        }

        if(changesRequired.checked) {
            returnMe += changesRequired.text + ".\n";
        }

        if(happyDent.checked) {
            returnMe += happyDent.text + ".\n";
        }

        returnMe += "Next step: " + nvBox.currentText + ".\n";
        nextStep = nvBox.currentIndex + 1;
        return returnMe;
    }

    CheckBox {
        id: changesRequired
        text: "Adjustment needed"
        checked: false
    }

    CheckBox {
        id: happyDent
        text: "Patient happy with Denture"
        checked: true
    }

    Label {
        text: "Next Step"
        font.bold: true
    }

    CDNextStepBox {
        id: nvBox
        currentIndex: 5
    }

}
