// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

GridLayout {
    columns: 2
    columnSpacing: 20

    property bool isMax: parent.isMaxillary;
    property int nextStep: 0

    function generateCaseNote() {
        if(isMax) {
            var returnMe = "Patient presented for Maxillary Complete Denture Step 1: Primary Impressions\n";
        }
        else {
            returnMe = "Patient presented for Mandibular Complete Denture Step 1: Primary Impressions\n";
        }

        returnMe += "Impressed with " + impressMaterial.currentText + " using ";
        returnMe += trayMaterial.currentText + " trays.\n";
        if(stonePour.currentIndex == 2) {
            returnMe += "Lab will pour up the impression"
        }
        else {
            returnMe += "Impression was poured up using "+ stonePour.currentText
        }
        returnMe+=".\n"
        returnMe += "Case to be sent to: " + labBox.currentText + ".\n";
        returnMe += "Next step: " + nvBox.currentText + ".\n";
        nextStep = nvBox.currentIndex + 1;
        return returnMe;
    }


    Label {
        text: "Impression Material"
        font.bold: true
    }

    ComboBox {
        id: impressMaterial
        Layout.fillWidth: true
        model: ["Alginate", "Safco True™ alginate substitute","Elastomer putty"]
    }

    Label {
        text: "Trays"
        font.bold: true
    }

    ComboBox {
        id: trayMaterial
        Layout.fillWidth: true
        model: ["Stock Plastic","Stock Metal", "Accu-Dent", "Custom"]
    }

    Label {
        text: "Pour up"
        font.bold: true
    }

    ComboBox {
        id: stonePour
        Layout.fillWidth: true
        model: ["Type 2 Dental Plaster","Type 1 Plaster of Paris", "Lab will pour it up"]
    }

    Label {
        text: "Lab to be Sent To"
        font.bold: true
    }

    SelectLabBox {
        id: labBox
        Layout.fillWidth: true
    }

    Label {
        text: "Next Step"
        font.bold: true
    }

    CDNextStepBox {
        id: nvBox
        currentIndex: 1
    }
}
