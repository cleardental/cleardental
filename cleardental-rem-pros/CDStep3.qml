// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0


GridLayout {
    columns: 2
    columnSpacing: 20

    property bool isMax: parent.isMaxillary;
    property int nextStep: 0

    function generateCaseNote() {
        if(isMax) {
            var returnMe = "Patient presented for Maxillary Complete Denture Step 3: Wax Rim Adjustments\n";
        }
        else {
            returnMe = "Patient presented for Mandibular Complete Denture Step 3: Wax Rim Adjustments\n";
        }

        if(waxRimAdj.checked) {
            returnMe += waxRimAdj.text + ".\n";
        }

        if(postDamSpace.checked) {
            returnMe += postDamSpace.text + ".\n";
        }

        if(midlineRecorded.checked) {
            returnMe += midlineRecorded.text + ".\n";
        }

        if(smilelineRecorded.checked) {
            returnMe += smilelineRecorded.text + ".\n";
        }

        if(newWashImpression.checked) {
            returnMe += newWashImpression.text + ".\n";
        }

        returnMe += "Shade to be: " + shadeColor.text + " ("+ shadeSystem.text +".)\n";
        returnMe += "Case to be sent to: " + labBox.currentText + ".\n";
        returnMe += "Next step: " + nvBox.currentText + ".\n";
        nextStep = nvBox.currentIndex + 1;
        return returnMe;
    }


    CheckBox {
        id: waxRimAdj
        Layout.columnSpan: 2
        text: "Wax rims adjusted"
        checked: true
    }

    CheckBox {
        id: postDamSpace
        text: "Post Dam Space Recorded"
        Layout.columnSpan: 2
        checked: true
        visible:isMax
    }

    CheckBox {
        id: midlineRecorded
        text: "Midline Recorded"
        checked: true
        Layout.columnSpan: 2
    }

    CheckBox {
        id: smilelineRecorded
        text: "Smile Line Recorded"
        checked: true
        Layout.columnSpan: 2
        visible:  isMax
    }

    CheckBox {
        id: newWashImpression
        text: "New Wash Impression Taken"
        checked: false
        Layout.columnSpan: 2
    }

    Label {
        text: "Shade"
        font.bold: true
    }
    TextField {
        id: shadeColor
        text: "A3"
    }

    Label {
        text: "Shade System"
        font.bold: true
    }

    TextField {
        id: shadeSystem
        text: "Vita Classic"
    }

    Label {
        text: "Lab to be Sent To"
        font.bold: true
    }

    SelectLabBox {
        id: labBox
        Layout.fillWidth: true
    }

    Label {
        text: "Next Step"
        font.bold: true
    }

    CDNextStepBox {
        id: nvBox
        currentIndex: 3
    }
}
