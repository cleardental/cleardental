// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

Page {
    id: medicationsPage

    // Saves all data
    function saveData() {
        var jsonData = [];
        for(var i=7;i<medGrid.children.length;i+=6) {
            if(medGrid.children[i].text.length > 2) {
                var addMe = {"DrugName": medGrid.children[i].text,
                    "UsedFor": medGrid.children[i+1].text,
                    "PrescribedHere": medGrid.children[i+2].checked,
                    "Amount": medGrid.children[i+3].text,
                    "Schedule": medGrid.children[i+4].editText};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textManager.saveFile(fileLocs.getMedicationsFile(PATIENT_FILE_NAME),jsonString);
        rootWin.updateReview();
        gitManager.commitData("Updated medications for " + PATIENT_FILE_NAME);

    }

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    CDGitManager {id: gitManager}

    CDTextFileManager {id: textManager}

    Label {
        id: noMedsLabel
        text: "There are currently no recorded Medications"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: medGrid.children.length < 8
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }

    CDTranslucentPane {
        id: medPane
        anchors.centerIn: parent
        backMaterialColor: Material.LightBlue
        visible: opacity > 0
        opacity: medGrid.children.length > 8

        GridLayout {
            id: medGrid
            columns: 6
            columnSpacing: 25

            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            Label {
                text: "Patient's current medications"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 6
            }

            Label {
                font.bold: true
                text: "Medication Name"
            }
            Label {
                font.bold: true
                text: "Used for"
            }

            Label {
                font.bold: true
                text: "Rx here"
            }

            Label {
                font.bold: true
                text: "Amount"
            }

            Label {
                font.bold: true
                text: "Schedule"
            }

            Label { //so we don't offset the delete button
                text: ""
            }

            Component {
                id: drugNameComp
                MedicationBox {
                    Layout.minimumWidth: 200
                }
            }

            Component {
                id: usedForText
                TextField {
                    Layout.minimumWidth: 200
                }
            }

            Component {
                id:isPrescribedHereBox
                CheckBox {
                    enabled: false
                    checkState: Qt.Unchecked
                }
            }

            Component {
                id: amountField
                TextField {

                }
            }

            Component {
                id:scheduleField
                ComboBox {
                    editable: true
                    property var setMe;
                    model: ["QD","BID","TID","QID","QHS","Q4H","Q6H","Q8H","QOD",
                        "PRN","AC","PC","IM","IV","QTT"]
                    Component.onCompleted: {
                        if(setMe) {
                            var setIndex = find(setMe);
                            if(setIndex >-1) {
                                currentIndex = setIndex;
                            } else {
                                model = [setMe, "QD","BID","TID","QID","QHS","Q4H","Q6H","Q8H","QOD",
                                         "PRN","AC","PC","IM","IV","QTT"];
                                currentIndex=0;
                            }
                        }
                    }
                }
            }


            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    text: "Remove Medication"
                    onClicked: {
                        text = "Kill me now!";
                        medGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile(fileLocs.getMedicationsFile(PATIENT_FILE_NAME)));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    drugNameComp.createObject(medGrid,{"text": obj["DrugName"]});
                    usedForText.createObject(medGrid,{"text": obj["UsedFor"]});
                    isPrescribedHereBox.createObject(medGrid,{"checked": obj["PrescribedHere"]});
                    amountField.createObject(medGrid,{"text": obj["Amount"]});
                    scheduleField.createObject(medGrid,{"setMe": obj["Schedule"]});
                    deleteButton.createObject(medGrid,{});
                }
            }

            function makeNewRow() {
                drugNameComp.createObject(medGrid,{});
                usedForText.createObject(medGrid,{});
                isPrescribedHereBox.createObject(medGrid,{});
                amountField.createObject(medGrid,{});
                scheduleField.createObject(medGrid,{});
                deleteButton.createObject(medGrid,{});
            }

            function killTheZombie() {
                for(var i=7;i<medGrid.children.length;i+=6) {
                    var killBut = medGrid.children[i+5];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = medGrid.children[i];
                        var kill2 = medGrid.children[i+1];
                        var kill3 = medGrid.children[i+2];
                        var kill4 = medGrid.children[i+3];
                        var kill5 = medGrid.children[i+4];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        kill4.destroy();
                        kill5.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }

    }

    Button {
        id: addButton
        icon.name: "list-add"
        text: "Add Medication"
        anchors.left: medPane.left
        anchors.top: medPane.bottom
        anchors.margins: 10
        Material.accent: Material.Green
        highlighted: true
        onClicked: medGrid.makeNewRow();
    }

    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
