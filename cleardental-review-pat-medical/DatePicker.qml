// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

RowLayout {
    id: dateRow
    function getDate() {
        var month = monthBox.currentIndex+ 1;
        var day = dayBox.currentIndex + 1;
        var year = 2000 + yearBox.currentIndex;
        return month + "/" + day + "/" + year; //US standard, I know...
    }

    function setDate(newDate) {
        if(newDate.length > 3)  {
            var partsOfDay = newDate.split("/");
            monthBox.currentIndex = partsOfDay[0] -1;
            dayBox.currentIndex = dayBox.find(partsOfDay[1]);
            yearBox.currentIndex = yearBox.find(partsOfDay[2]);
        }
    }

    signal newDate()


    ComboBox {
        id: monthBox
        width: 120
        model: ['January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December'];
    }

    ComboBox {
        id: dayBox
        width: 75
        model:  {
            var returnMe= [];
            for(var i=1;i<33;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
    }

    ComboBox {
        id: yearBox
        width: 85
        model:  {
            var returnMe= [];
            for(var i=2000;i<2050;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
    }

    Component.onCompleted: {
        monthBox.onCurrentIndexChanged.connect(newDate);
        dayBox.onCurrentIndexChanged.connect(newDate);
        yearBox.onCurrentIndexChanged.connect(newDate);
    }

}
