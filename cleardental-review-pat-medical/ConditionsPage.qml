// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details
// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

Page {
    id: conditionPage

    // Saves all data
    function saveData() {
        var jsonData = [];
        for(var i=4;i<conditionGrid.children.length;i+=3) {
            if(conditionGrid.children[i].text.length > 2) {
                var addMe = {"ConditionName": conditionGrid.children[i].text,
                    "ConditionInfo": conditionGrid.children[i+1].text};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textManager.saveFile(fileLocs.getConditionsFile(PATIENT_FILE_NAME),jsonString);
        rootWin.updateReview();
        gitManager.commitData("Updated medical conditions for " + PATIENT_FILE_NAME);
    }

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    CDGitManager {id: gitManager}

    CDTextFileManager {id: textManager}

    Label {
        id: noConditionsLabel
        text: "There are currently no recorded conditions"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: conditionGrid.children.length < 6
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }

    CDTranslucentPane {
        id: condPane
        anchors.centerIn: parent
        backMaterialColor: Material.BlueGrey
        visible: opacity > 0
        opacity: conditionGrid.children.length > 6

        GridLayout {
            id: conditionGrid
            columns: 3
            columnSpacing: 25

            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            Label {
                text: "Patient's current medical conditions"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 3
            }

            Label {
                font.bold: true
                text: "Condition Name"
                Layout.minimumWidth: 300
            }
            Label {
                font.bold: true
                text: "Additional information"
                Layout.minimumWidth: 300
            }

            Label { //just for placeholder to make a new row
                Layout.minimumWidth: 100
            }

            Component {
                id: textFieldComp
                TextField {
                    Layout.minimumWidth: 300
                }
            }

            Component {
                id: deleteButton
                Button {
                    text: "Remove Condition"
                    icon.name: "list-remove"
                    onClicked: {
                        text = "Kill me now!";
                        conditionGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile(fileLocs.getConditionsFile(PATIENT_FILE_NAME)));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(conditionGrid,{"text":obj.ConditionName});
                    textFieldComp.createObject(conditionGrid,{"text":obj.ConditionInfo});
                    deleteButton.createObject(conditionGrid,{"placeholderText":""});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(conditionGrid,{"placeholderText":"Condition Name"});
                textFieldComp.createObject(conditionGrid,{"placeholderText":"Condition Information"});
                deleteButton.createObject(conditionGrid,{"placeholderText":"Condition Information"});
            }

            function killTheZombie() {
                for(var i=4;i<conditionGrid.children.length;i+=3) {
                    var killBut = conditionGrid.children[i+2];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = conditionGrid.children[i];
                        var kill2 = conditionGrid.children[i+1];
                        kill1.destroy();
                        kill2.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }

    }



    Button {
        id: addButton
        icon.name: "list-add"
        text: "Add Condition"
        anchors.left: condPane.left
        anchors.top: condPane.bottom
        anchors.margins: 10
        Material.accent: Material.Green
        highlighted: true
        onClicked: conditionGrid.makeNewRow();

    }

    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }
    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
