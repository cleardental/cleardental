// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: finishDialog
    anchors.centerIn:  Overlay.overlay
    modal: Qt.WindowModal

    property bool willNeedRx: false
    property bool dieOnClose: true
    property var txItemsToComplete: []
    property var txItemsSelected: []

    CDTextFileManager {
        id: m_textFileMan
    }

    CDGitManager {
        id: m_gitMan
    }

    CDFileLocations {
        id: m_fileLocs
    }

    CDCommonFunctions {
        id: m_comFun
    }

    Settings {
        id: m_pracUpdates
        fileName: m_fileLocs.getLocalPracticePreferenceFile()
        category: "Updates"
    }

    Settings {
        id: m_docPrefs
        fileName: m_fileLocs.getWorkingDocInfoFile()
    }

    CDProviderInfo {
        id: proInfo
    }

    function fillOutFields() {
        compCol.refreshList();
    }

    function updateTxItemsSelected() {
        txItemsSelected = [];
        for(var i=0;i<txItemsToComplete.length;i++) {
            var checkBox = repComplete.itemAt(i);
            if(checkBox.checked) {
                txItemsSelected.push(checkBox.txItemObject);
            }
        }
    }

    function isObjEqual(objA,objB) {
        var aKeys = Object.keys(objA);
        var returnMe = true;

        for(var i=0;i<aKeys.length;i++) {
            var key = aKeys[i];
            if(key in objB) {
                if(objB[key] !== objA[key]) {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        return returnMe;

    }

    function areSameProcedure(getA,getB) {
        var returnMe = false;
        var hasDCode = "DCode" in getA;
        var hasTooth = "Tooth" in getA;

        if(hasDCode && hasTooth) {
            // @disable-check M126
            returnMe = (getA["DCode"] == getB["DCode"]) && (getA["Tooth"] == getB["Tooth"]);
        }
        else if(hasDCode) {
            // @disable-check M126
            returnMe = returnMe || (getA["DCode"] == getB["DCode"]);
        }
        else { //no dCode
            // @disable-check M126
            returnMe = getA["ProcedureName"] == getB["ProcedureName"]
        }

        return returnMe;
    }

    //This function does two things:
    //1. Finds the procedure if it already exists in the ledger as a "not completed"
    //2. If it finds it, it sets the completion date to today and returns true
    //2a. If it doesn't find it, it just returns false
    function checkLedgerAndSetToComplete(getLedgerObj, getProcedureObj) {
        var returnMe = false;
        for(var i=0;i<getLedgerObj.length;i++) {
            if(!("ProcedureDate" in getLedgerObj[i])) {
                var procedureFromLedger = getLedgerObj[i]["Procedure"];
                if(areSameProcedure(procedureFromLedger,getProcedureObj)) {
                    getLedgerObj[i]["ProcedureDate"] = new Date();
                    returnMe = true;
                }
            }
        }
        return returnMe;
    }

    function wasSelected(txItem) {
        var returnMe = false;
        for(var i=0;i<txItemsSelected.length;i++) {
            if(isObjEqual(txItemsSelected[i],txItem)) {
                returnMe = true;
                i = txItemsSelected.length
            }
        }
        return returnMe;
    }

    function addCaseNote() {
        var caseNotesJSON = m_textFileMan.readFile(m_fileLocs.getCaseNoteFile(PATIENT_FILE_NAME));
        var caseNotesObj = [];
        if(caseNotesJSON.length > 0) {
            caseNotesObj  = JSON.parse(caseNotesJSON);
        }

        var addMe = ({});
        addMe["DateTime"] = Date();
        addMe["Provider"] =m_docPrefs.value("UnixID");
        addMe["CaseNote"] = caseNoteText.text;
        addMe["Final"] = finalize.checked;
        if(finalize.checked) {
            addMe["Signature"] = proInfo.signData(caseNoteText.text);
        }
        addMe["ProcedureObject"] = JSON.stringify(txItemsToComplete);
        addMe["NextVisit"] = JSON.stringify(nvComboBox.currentValue);

        caseNotesObj.push(addMe);
        m_textFileMan.saveFile(m_fileLocs.getCaseNoteFile(PATIENT_FILE_NAME),JSON.stringify(caseNotesObj, null, '\t'));
    }

    function addToLedger() {
        var ledgerJSON = m_textFileMan.readFile(m_fileLocs.getFullBillingFile(PATIENT_FILE_NAME));
        var ledgerObj = [];
        if(ledgerJSON.length > 1) {
            ledgerObj = JSON.parse(ledgerJSON);
        }

        for(var i=0;i<txItemsSelected.length;i++) {
            var txObj = txItemsSelected[i];

            //Check for pre-payments before adding in a blank one
            if(!checkLedgerAndSetToComplete(ledgerObj,txObj)) {
                var addLine = {
                    ProcedureDate: Date(),
                    Procedure: txObj,
                    Payments: [],
                    ClaimStatus: "Unsent",
                    ProviderUsername: m_docPrefs.value("UnixID")
                };
                ledgerObj.push(addLine);
            }
        }

        ledgerJSON= JSON.stringify(ledgerObj, null, '\t');
        m_textFileMan.saveFile(m_fileLocs.getFullBillingFile(PATIENT_FILE_NAME), ledgerJSON);
    }

    function removeCurrentTxFromPlanAndSetNextVisit() {
        var jsonTxPlans = m_textFileMan.readFile(m_fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        var txPlansObj = ({});

        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
            var txPlanNames = Object.keys(txPlansObj);
            for(var txPlanNameI =0; txPlanNameI< txPlanNames.length; txPlanNameI++) {
                var txPlan = txPlansObj[txPlanNames[txPlanNameI]];
                var phaseNames = Object.keys(txPlan);
                for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                    var phaseList = txPlan[phaseNames[phasNameI]];
                    for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                        var txObj = phaseList[phaseItemI];
                        var txObjJSON = JSON.stringify(txObj);
                        //console.debug(txObjJSON);
                        if(wasSelected(txObj)) {
                            phaseList.splice(phaseItemI,1);
                            phaseItemI--;
                        }
                        else if(m_comFun.isEqual(txObj,nvComboBox.currentValue)) {
                            phaseList[phaseItemI]["NextVisit"] = true;
                        }
                        else {
                            delete phaseList[phaseItemI]["NextVisit"]; //remove all other "NextVisits"
                        }
                    }
                }
            }
        }

        jsonTxPlans= JSON.stringify(txPlansObj, null, '\t');
        m_textFileMan.saveFile(m_fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),jsonTxPlans);
    }


    function completeProcedurePaperwork() {
        addCaseNote();
        updateTxItemsSelected();

        if(txItemsSelected.length != 0) {
            addToLedger();
            removeCurrentTxFromPlanAndSetNextVisit();
            m_gitMan.commitData("Completed procedure " + m_comFun.makeTxItemString(txItemsToComplete[0])+
                                " for " + PATIENT_FILE_NAME);
        }
        else {
            m_gitMan.commitData("Completed procedure for " + PATIENT_FILE_NAME);
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            backMaterialColor: Material.Pink

            ColumnLayout {
                CDHeaderLabel {
                    text: "Finish Procedure"
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Which procedures to complete"
                    }
                    Flickable {
                        Layout.fillWidth: true
                        Layout.minimumHeight: 100
                        Layout.maximumHeight: 200
                        Layout.preferredHeight: contentHeight
                        contentHeight: compCol.height
                        contentWidth: compCol.width

                        ScrollBar.vertical: ScrollBar { }

                        clip: true
                        ColumnLayout {
                            id: compCol

                            function refreshList() {
                                for(var i=0;i<txItemsToComplete.length;i++) {
                                    if(typeof txItemsToComplete[i]== 'undefined') {
                                        txItemsToComplete.splice(i);
                                        i--;
                                    }
                                }

                                repComplete.model = txItemsToComplete.length;
                            }

                            Repeater {
                                id: repComplete
                                CheckBox {
                                    property var txItemObject: txItemsToComplete[index]
                                    checked: true
                                    text: m_comFun.makeTxItemString(txItemsToComplete[index])
                                    onCheckedChanged: {
                                        refreshNextTxPlans();
                                    }
                                }
                            }
                        }
                    }
                }


                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Procedure Provider"
                    }
                    Label {
                        Component.onCompleted:  {
                            text= m_docPrefs.value("FirstName") + " " + m_docPrefs.value("LastName") + " " +
                                    m_docPrefs.value("Title")
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                text: "Back to procedure"
                onClicked: reject();
            }

            Label {
                Layout.fillWidth: true
            }

            Button {
                id: compProcedureButton
                text: "Complete Procedure"
                Material.accent: Material.Cyan
                icon.name: "dialog-ok"
                highlighted: true
                icon.width: 32
                icon.height: 32
                font.pointSize: 18
                onClicked: {
                    completeProcedurePaperwork();
                    if(dieOnClose) {
                        Qt.quit();
                    }
                    else {
                        accept();
                    }
                }
            }
        }
    }

    onVisibleChanged: {
        if(visible) {
            fillOutFields();
        }
    }
}
