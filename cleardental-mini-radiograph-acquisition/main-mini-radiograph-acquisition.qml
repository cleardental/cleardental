// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ApplicationWindow {
    id: rootWin
    width: 1440
    height: 720
    visible: true
    title: "Mini Acquire Radiograph"

    font.family: "Barlow Semi Condensed"
    font.pointSize: 18

    property string selectedPatientID: ""

    function selectPatient(getPatID, photoURL) {
        selectedPatientID = getPatID;
        patPhoto.source = photoURL
        mainStack.push("MiniSelectRadiographTypes.qml")
    }

    header: CDBlankToolBar {
        id: rootToolBar
        headerText: mainStack.depth > 1 ? rootWin.selectedPatientID : "Select Patient"
        height: 72
        ToolButton {
            id: closeButton
            icon.name: "dialog-close"
            icon.color: Material.color(Material.Red)
            onClicked: Qt.quit()
            icon.width: 64
            icon.height: 64
            anchors.right: parent.right
            visible: mainStack.depth <2
        }
        ToolButton {
            id: goLeftButton
            icon.name: "go-previous"
            icon.width: 64
            icon.height: 64
            visible: mainStack.depth > 1
            anchors.right: parent.right
            onClicked: {
                mainStack.pop();
            }
        }

        Image {
            id: patPhoto
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 5
            width: 64
            height: 64
            visible: mainStack.depth > 1
        }

    }

    StackView {
        id: mainStack
        initialItem: "MiniListPatientsPage.qml"
        anchors.fill: parent
    }

    Shortcut {
        sequence: "Esc"
        onActivated: Qt.quit();
    }
}
