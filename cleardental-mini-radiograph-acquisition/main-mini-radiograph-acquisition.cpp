// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cdradiographmanager.h"
#include "cdaquireradiograph.h"
#include "cdschedulemanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Radiograph-Acquisition");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDRadiographManager>("dental.clear", 1, 0, "CDRadiographManager");
    qmlRegisterType<CDAquireRadiograph>("dental.clear", 1, 0, "CDRadiographSensor");
    qmlRegisterType<CDScheduleManager>("dental.clear", 1, 0, "CDScheduleManager");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main-mini-radiograph-acquisition.qml")));

    CDDefaults::enableBlurBackground();

    return app.exec();
}
