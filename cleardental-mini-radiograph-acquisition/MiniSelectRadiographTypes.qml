// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {

    property int ageYear: getAgeYear()


    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: patPersonalSettings
        fileName: fLocs.getPersonalFile(rootWin.selectedPatientID)
        category: "Personal"
    }

    function getAgeYear() {
        //because in QML, you can never assume properties are set in realtime
        patPersonalSettings.fileName =  fLocs.getPersonalFile(rootWin.selectedPatientID);
        patPersonalSettings.category = "Personal"

        var textDOBArray = patPersonalSettings.value("DateOfBirth","1/1/0001").split("/");
        //console.debug( fLocs.getPersonalFile(PATIENT_FILE_NAME));
        var dobMonth = parseInt(textDOBArray[0]);
        var dobDay = parseInt(textDOBArray[1]);
        var dobYear =  parseInt(textDOBArray[2]);
        var today = new Date();

        var monthDiff = (today.getFullYear() - dobYear) * 12;
        monthDiff -= dobMonth;
        monthDiff += today.getMonth() + 1;

        return monthDiff /12;
    }

    CDTranslucentPane {
        id: buttonLayoutPane
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 10

        GridLayout {
            id: buttonGrid
            columns: 7

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Right_Distal
                property string billTooth: (ageYear > 12) ? "2" : "A"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Right_Mesial
                property string billTooth: (ageYear > 10) ? "4" : "B"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Right
                property string billTooth: (ageYear > 11) ? "6" : "C"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Center
                property string billTooth: (ageYear > 8) ? "8" : "E"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Anterior_Left
                property string billTooth: (ageYear > 11) ? "11" : "H"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Left_Mesial
                property string billTooth: (ageYear > 10) ? "12" : "I"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Maxillary_Left_Distal
                property string billTooth: (ageYear > 12) ? "14" : "J"
            }


            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Right_Distal
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Right_Mesial
            }

            Label{}Label{}Label{}

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Left_Mesial
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.BW_Left_Distal
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Right_Distal
                property string billTooth: (ageYear > 11) ? "30" : "T"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Right_Mesial
                property string billTooth: (ageYear > 10) ? "28" : "S"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Right
                property string billTooth: (ageYear > 10) ? "27" : "R"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Center
                property string billTooth: (ageYear > 6) ? "24" : "O"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Anterior_Left
                property string billTooth: (ageYear > 10) ? "22" : "M"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Left_Mesial
                property string billTooth: (ageYear > 10) ? "20" : "L"
            }

            MiniSelectRadiographToggleButton {
                radiographEnum: CDRadiographSensor.PA_Mandibular_Left_Distal
                property string billTooth: (ageYear > 11) ? "18" : "K"
            }
        }
    }

    CDAddButton {
        id: addBWs
        font.pointSize: 18
        text: "BWs"
        anchors.left: buttonLayoutPane.right
        anchors.verticalCenter: buttonLayoutPane.verticalCenter
        anchors.margins: 10

        onClicked: {
            for(var i=7;i<14;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    buttonGrid.children[i].isSelected = true;
                }
            }
        }
    }

    CDAddButton {
        font.pointSize: 18
        text: "FMX"
        anchors.left: buttonLayoutPane.right
        anchors.top: addBWs.bottom
        anchors.margins: 10

        onClicked: {
            for(var i=0;i<buttonGrid.children.length;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    buttonGrid.children[i].isSelected = true;
                }
            }
        }
    }

    Button {
        id: nextStep
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        text: "Take Radiographs"

        icon.name: "go-next"
        icon.width: 16
        icon.height: 16
        font.pointSize: 18

        CDCommonFunctions {
            id: comFuns
        }

        function countBitewings(getArray) {
            var returnMe =0;
            if(getArray.includes(CDRadiographSensor.BW_Right_Distal)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Right_Mesial)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Left_Mesial)) {
                returnMe++;
            }
            if(getArray.includes(CDRadiographSensor.BW_Left_Distal)) {
                returnMe++;
            }
            return returnMe;
        }

        function isBW(getItem) {
            var returnMe = false;
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Right_Distal);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Right_Mesial);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Left_Mesial);
            returnMe = returnMe || (getItem === CDRadiographSensor.BW_Left_Distal);
            return returnMe;
        }

        onClicked: {
            var radioEnums = [];
            var radioTeeth = [];
            for(var i=1;i<buttonGrid.children.length;i++) {
                if(typeof buttonGrid.children[i].isSelected !== 'undefined') {
                    if(buttonGrid.children[i].isSelected) {
                        radioEnums.push(buttonGrid.children[i].radiographEnum);
                        radioTeeth.push(buttonGrid.children[i].billTooth);
                    }
                }
            }

            var BWCount = countBitewings(radioEnums)

            if(radioEnums.length === 18) { //All of them
                var addFMX = ({});
                addFMX["ProcedureName"] = "Intraoral—Complete series of radiographic images"
                addFMX["DCode"] = "D0210"
                comFuns.ensureSingleTxItem(rootWin.selectedPatientID,addFMX);
            }
            else if( BWCount > 0) {
                var addBWs = ({});

                if(BWCount === 4) {
                    addBWs["ProcedureName"] = "Four Bitewings"
                    addBWs["DCode"] = "D0274"
                }
                else if(BWCount === 3) {
                    addBWs["ProcedureName"] = "Three Bitewings"
                    addBWs["DCode"] = "D0273" //Nobody is going to pay for this but whatever
                }
                else if(BWCount === 2) {
                    addBWs["ProcedureName"] = "Two Bitewings"
                    addBWs["DCode"] = "D0272"
                }
                else {
                    addBWs["ProcedureName"] = "Single Bitewing"
                    addBWs["DCode"] = "D0270"
                }


                comFuns.ensureSingleTxItem(rootWin.selectedPatientID,addBWs);

                var leftOvers = radioEnums.length  - BWCount;

                if(leftOvers > 0) {
                    var first= true;

                    for(var x=0;x<radioEnums.length;x++) {
                        if(!isBW(radioEnums[x])) {
                            if(first) {
                                var add1stPA = ({});
                                add1stPA["ProcedureName"] = "First periapical"
                                add1stPA["DCode"] = "D0220"
                                add1stPA["Tooth"] = radioTeeth[x];
                                comFuns.ensureSingleTxItem(rootWin.selectedPatientID,add1stPA);
                                first = false;
                            }
                            else {
                                var add2ndPA = ({});
                                add2ndPA["ProcedureName"] = "Additional periapical"
                                add2ndPA["DCode"] = "D0230"
                                add2ndPA["Tooth"] = radioTeeth[x];
                                comFuns.addTxItem(rootWin.selectedPatientID,"Primary","Unphased",add2ndPA);
                            }
                        }
                    }
                }
            }
            else { //just took a bunch of PAs
                first= true;
                for(x=0;x<radioEnums.length;x++) {
                    //console.debug(radioTeeth[x]);
                    if(first) {
                        add1stPA = ({});
                        add1stPA["ProcedureName"] = "First periapical"
                        add1stPA["DCode"] = "D0220"
                        add1stPA["Tooth"] = radioTeeth[x];
                        comFuns.ensureSingleTxItem(rootWin.selectedPatientID,add1stPA);
                        first = false;
                    }
                    else {
                        add2ndPA = ({});
                        add2ndPA["ProcedureName"] = "Additional periapical"
                        add2ndPA["DCode"] = "D0230"
                        add2ndPA["Tooth"] = radioTeeth[x];
                        comFuns.addTxItem(rootWin.selectedPatientID,"Primary","Unphased",add2ndPA);
                    }
                }
            }

            mainStack.push("MiniTakeRadiographsPage.qml");
            mainStack.currentItem.radiographEnums = radioEnums;
        }
    }
}
