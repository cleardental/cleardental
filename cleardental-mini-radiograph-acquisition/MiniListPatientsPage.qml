// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ListView {
        id: todayPatientList
        anchors.fill: parent
        anchors.margins: 5
        clip: true
        ScrollBar.vertical: ScrollBar { }
        property var patientIDs: []

        CDScheduleManager {
            id: schMan
        }
        Settings {
            id: apptReader
        }
        CDFileLocations {
            id: listLoctions
        }
        spacing: 10

        delegate:  Rectangle {
            id: selectionRect
            width: todayPatientList.width
            height: 66
            radius: 3
            color: "transparent"

            RowLayout {
                Image {
                    Layout.maximumWidth: 64
                    Layout.maximumHeight: 64
                    Layout.minimumWidth: 64
                    Layout.minimumHeight: 64
                    source: "file://" + listLoctions.getProfileImageFile(todayPatientList.patientIDs[index])

                }

                Label {
                    text: todayPatientList.patientIDs[index]
                    Layout.fillWidth: true
                }
            }
            MouseArea {
                anchors.fill: parent
                onPressed:  {
                    selectionRect.color = Material.accentColor
                }
                onReleased: {
                    selectionRect.color = "transparent"
                }

                onClicked: {
                    rootWin.selectPatient(todayPatientList.patientIDs[index],
                                          "file://" + listLoctions.getProfileImageFile(
                                              todayPatientList.patientIDs[index]));

                }
            }
        }



        Component.onCompleted:  {
            var apptFilenames = schMan.getAppointments(new Date());

            for(var i=0;i<apptFilenames.length;i++) {
                apptReader.fileName = apptFilenames[i];
                patientIDs.push(apptReader.value("PatientID",""));
            }
            todayPatientList.model = patientIDs.length
        }
    }

}
