#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QProcess>
#include <QDir>
#include <QPushButton>
#include <QFile>
#include <QScrollBar>
#include <QDebug>
#include <QHostInfo>
#include <QSettings>

#include "cdproviderinfo.h"
#include "cdfilelocations.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->startButton,&QPushButton::clicked,this,&MainWindow::handleStart);
    QSettings getSettings;
    ui->masterUsernameLineEdit->setText(getSettings.value("MasterUsername","").toString());
    ui->masterServerLineEdit->setText(getSettings.value("MasterServer","").toString());
    ui->shareFolderServerLineEdit->setText(getSettings.value("ShareFolderServer","").toString());
    ui->videoServerLineEdit->setText(getSettings.value("VideoFolderServer","").toString());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleOutputUpdate(QString getFilename)
{
    QFile readFile(getFilename);
    readFile.open(QIODevice::ReadOnly);
    QString wholeFile = readFile.readAll();
    ui->consoleOutputTextEdit->setPlainText(wholeFile.trimmed());
    ui->consoleOutputTextEdit->verticalScrollBar()->setValue(ui->consoleOutputTextEdit->verticalScrollBar()->maximum());
    readFile.close();
}

void MainWindow::handleStart()
{
    QSettings setSettings;
    setSettings.setValue("MasterUsername",ui->masterUsernameLineEdit->text());
    setSettings.setValue("MasterServer",ui->masterServerLineEdit->text());
    setSettings.setValue("ShareFolderServer",ui->shareFolderServerLineEdit->text());
    setSettings.setValue("VideoFolderServer",ui->videoServerLineEdit->text());


    m_TempFile = new QTemporaryFile();
    m_TempFile->open();
    m_Watcher = new QFileSystemWatcher();
    m_Watcher->addPath(m_TempFile->fileName());
    connect(m_Watcher,&QFileSystemWatcher::fileChanged,this,&MainWindow::handleOutputUpdate);
    ui->progressBar->setEnabled(true);

    //First we want to make sure we have our own ssh keys
    QProcess makeSshKeys;
    makeSshKeys.setProgram("/usr/bin/ssh-keygen");
    QStringList makeSShKeysArgs;
    makeSShKeysArgs<<"-t"<<"ed25519";
    makeSShKeysArgs<<"-N"<<"";
    makeSShKeysArgs<<"-f"<< (QDir::homePath() + "/.ssh/id_ed25519" );
    makeSshKeys.setArguments(makeSShKeysArgs);
    makeSshKeys.setStandardOutputFile(m_TempFile->fileName());
    makeSshKeys.start();
    m_Watcher->addPath(m_TempFile->fileName());
    makeSshKeys.waitForFinished();
    ui->progressBar->setValue(1);


    //We are now going to blindly trust DNS in getting the keys here
    QProcess keyscanProcess;
    keyscanProcess.setProgram("/usr/bin/ssh-keyscan");
    QStringList keyscanArgs;
    keyscanArgs<<ui->masterServerLineEdit->text();
    if(ui->shareFolderServerLineEdit->text().length() > 1) {
        keyscanArgs<<ui->shareFolderServerLineEdit->text();
    }
    if(ui->videoServerLineEdit->text().length() > 1) {
        keyscanArgs<<ui->videoServerLineEdit->text();
    }
    keyscanProcess.setArguments(keyscanArgs);
    keyscanProcess.setStandardOutputFile(QDir::homePath() + "/.ssh/known_hosts" );
    keyscanProcess.start();
    m_Watcher->addPath(m_TempFile->fileName());
    keyscanProcess.waitForFinished();

    //Copy keys to the master server
    QProcess copySshKeys;
    copySshKeys.setProgram("/usr/bin/sshpass");
    QStringList copySshKeysArgs;
    QString publicKeyPath = QDir::homePath() + "/.ssh/id_ed25519.pub";
    copySshKeysArgs<<"-p"<<ui->masterPasswordLineEdit->text();
    copySshKeysArgs<<"ssh-copy-id";
    copySshKeysArgs<<"-i"<<publicKeyPath;
    copySshKeysArgs<<ui->masterUsernameLineEdit->text() + "@" + ui->masterServerLineEdit->text();
    copySshKeys.setArguments(copySshKeysArgs);
    copySshKeys.setStandardOutputFile(m_TempFile->fileName());
    copySshKeys.start();
    m_Watcher->addPath(m_TempFile->fileName());
    copySshKeys.waitForFinished();
    ui->progressBar->setValue(2);

    //Copy keys to the shared folder server
    if(ui->shareFolderServerLineEdit->text().length() > 0) {
        copySshKeysArgs[copySshKeysArgs.length()-1] = ui->shareFolderServerLineEdit->text();
        copySshKeys.setArguments(copySshKeysArgs);
        copySshKeys.start();
        copySshKeys.waitForFinished();
    }
    ui->progressBar->setValue(3);

    //Copy keys to the video server
    if(ui->videoServerLineEdit->text().length() > 0) {
        copySshKeysArgs[copySshKeysArgs.length()-1] = ui->masterUsernameLineEdit->text() + "@" +
                                                        ui->videoServerLineEdit->text();
        copySshKeys.setArguments(copySshKeysArgs);
        copySshKeys.start();
        copySshKeys.waitForFinished();
    }
    ui->progressBar->setValue(4);


    //Clone the master repo
    QProcess cloneMasterRepo;
    cloneMasterRepo.setProgram("/usr/bin/git");
    QStringList cloneMasterArgs;
    cloneMasterArgs<<"clone"<< ("ssh://" + ui->masterUsernameLineEdit->text() + "@" +ui->masterServerLineEdit->text() +
                                   "/home/"+ui->masterUsernameLineEdit->text() + "/clearDentalData");
    cloneMasterRepo.setArguments(cloneMasterArgs);
    cloneMasterRepo.setWorkingDirectory(QDir::homePath());
    cloneMasterRepo.setStandardOutputFile(m_TempFile->fileName());
    cloneMasterRepo.start();
    m_Watcher->addPath(m_TempFile->fileName());
    cloneMasterRepo.waitForFinished(1000*60*30); //wait up to 30 mins
    ui->progressBar->setValue(90);

    //Set the git usernames and email
    QProcess setupGitFolder;
    setupGitFolder.setProgram("/usr/bin/git");
    QStringList additionalSetupArgs;
    additionalSetupArgs<< "config"<<"alias.publish"<<"!git pull && git push";
    setupGitFolder.setArguments(additionalSetupArgs);
    setupGitFolder.setWorkingDirectory(QDir::homePath() + "/clearDentalData");
    setupGitFolder.setStandardOutputFile(m_TempFile->fileName());
    setupGitFolder.start();
    m_Watcher->addPath(m_TempFile->fileName());
    setupGitFolder.waitForFinished();
    ui->progressBar->setValue(91);

    additionalSetupArgs.clear();
    additionalSetupArgs<< "config"<<"pull.rebase"<<"false";
    setupGitFolder.setArguments(additionalSetupArgs);
    setupGitFolder.start();
    setupGitFolder.waitForFinished();
    ui->progressBar->setValue(92);

    additionalSetupArgs.clear();
    additionalSetupArgs<< "config"<<"--global"<<"user.name"<<CDProviderInfo::getCurrentProviderFullName() +
                                                                        " (" + QHostInfo::localHostName() + ")";
    setupGitFolder.setArguments(additionalSetupArgs);
    setupGitFolder.start();
    setupGitFolder.waitForFinished();
    m_Watcher->addPath(m_TempFile->fileName());
    ui->progressBar->setValue(92);

    QSettings readWebsite(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    readWebsite.beginGroup("GeneralInfo");
    QString website = readWebsite.value("Website","https://clear.dental/").toString();
    website.replace("https://","");
    website.replace("http://",""); //just in case
    website.replace("/","");

    additionalSetupArgs.clear();
    additionalSetupArgs<< "config"<<"--global"<<"user.email"<<CDProviderInfo::getCurrentProviderUsername() +
                                                                        "@" + website;
    setupGitFolder.setArguments(additionalSetupArgs);
    setupGitFolder.start();
    m_Watcher->addPath(m_TempFile->fileName());
    setupGitFolder.waitForFinished();
    ui->progressBar->setValue(93);


    QFile updaterScript;
    if(ui->frontDeskRadioButton->isChecked()) {
        updaterScript.setFileName(QDir::homePath() + "/updateRepo-front.sh");
    }
    else {
        updaterScript.setFileName(QDir::homePath() + "/updateRepo-chair.sh");
    }
    updaterScript.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream writeScript(&updaterScript);
    writeScript<<"#!/bin/bash\n\n";
    writeScript<<"git -C $HOME/clearDentalData/ pull\n";
    if(ui->chairSideRadioButton->isChecked()) {
        writeScript<<"sshfs "+ui->videoServerLineEdit->text()+":/home/"+ui->masterUsernameLineEdit->text()+
                           "/Videos Videos\n";
        //TODO: add in touchscreen XInput hack
    }
    writeScript<<"\n";
    updaterScript.close();
    updaterScript.setPermissions(QFileDevice::ExeUser | QFileDevice::ReadOwner | QFileDevice::WriteOwner |
                                 QFileDevice::ReadUser |QFileDevice::WriteUser | QFileDevice::ExeUser);
    ui->progressBar->setValue(94);

    //Update the crontab
    QProcess crontabProcess;
    crontabProcess.setProgram("/usr/bin/crontab");
    QStringList crontabArgs;
    crontabArgs<<"-"; //for reading stdinput
    crontabProcess.setArguments(crontabArgs);
    crontabProcess.start();
    if(ui->frontDeskRadioButton->isChecked()) {
        crontabProcess.write("* * * * * ~/updateRepo-front.sh\n");
    }
    else {
        crontabProcess.write("* * * * * ~/updateRepo-chair.sh\n");
    }
    crontabProcess.closeWriteChannel();
    crontabProcess.waitForFinished();
    ui->progressBar->setValue(95);

    //Generate the gpg key
    if(ui->gpgPasswordLineEdit->text().length()>1) {
        QProcess setupPGP;
        setupPGP.setProgram("/usr/bin/gpg");
        QStringList pgpArgs;
        pgpArgs<<"--quick-gen-key";
        pgpArgs<<"--batch";
        pgpArgs<<"--passphrase";
        pgpArgs<< ("'" + ui->gpgPasswordLineEdit->text() + "'");
        pgpArgs<<CDProviderInfo::getCurrentProviderUsername() +"@" + website;
        setupPGP.setArguments(pgpArgs);
        setupPGP.start();
        setupPGP.waitForFinished();
    }
    ui->progressBar->setValue(100);

    ui->statusbar->showMessage("Done!");

}


