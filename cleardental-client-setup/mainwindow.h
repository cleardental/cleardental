#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemWatcher>
#include <QTemporaryFile>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void handleStart();
    void handleOutputUpdate(QString getFilename);

private:
    Ui::MainWindow *ui;
    QFileSystemWatcher *m_Watcher;
    QTemporaryFile *m_TempFile;
};
#endif // MAINWINDOW_H
