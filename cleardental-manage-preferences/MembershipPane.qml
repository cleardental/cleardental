// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {

    function loadData() {
        locFile.category = "Membership"

        pricePerMonthBox.value = locFile.value("PricePerMonth",35)
        pricePerYearBox.value = locFile.value("PricePerYear",400);
        feeScheduleBox.currentIndex = feeScheduleBox.find(locFile.value("DiscountFeeScheduleName","Membership Plan"));

        coversExamsCheck.checked = JSON.parse(locFile.value("CoversExams",true))
        coversRadiographsCheck.checked = JSON.parse(locFile.value("CoversRadiographs",true))
        coversCleaningsCheck.checked = JSON.parse(locFile.value("CoversCleanings",true))
        coversSealantCheck.checked = JSON.parse(locFile.value("CoversSelants",false))
    }

    function saveData() {
        locFile.category = "Membership"
        locFile.setValue("PricePerMonth",pricePerMonthBox.value);
        locFile.setValue("PricePerYear",pricePerYearBox.value);
        locFile.setValue("DiscountFeeScheduleName",feeScheduleBox.currentValue);

        locFile.setValue("CoversExams",coversExamsCheck.checked);
        locFile.setValue("CoversRadiographs",coversRadiographsCheck.checked);
        locFile.setValue("CoversCleanings",coversCleaningsCheck.checked);
        locFile.setValue("CoversSelants",coversSealantCheck.checked);
    }

    ColumnLayout {
        anchors.centerIn: parent
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Basic Information"
                }
                CDDescLabel {
                    text: "Price per month"
                }
                PriceSpinBox {
                    id: pricePerMonthBox
                }
                CDDescLabel {
                    text: "Price per year"
                }
                PriceSpinBox {
                    id: pricePerYearBox
                }
                CDDescLabel {
                    text: "Discount Fee Schedule"
                }
                ComboBox {
                    id: feeScheduleBox

                    CDFeeScheduleManager {
                        id: feeMan
                    }

                    model: feeMan.getListOfPlans(manGDPage.locationID)
                    Layout.minimumWidth: 360
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Orange
            Layout.fillWidth: true
            ColumnLayout {
                CDHeaderLabel {
                    text: "Coverage"
                }
                CheckBox {
                    id: coversExamsCheck
                    text: "Covers Exams"
                }
                CheckBox {
                    id: coversRadiographsCheck
                    text: "Covers Radiographs"
                }
                CheckBox {
                    id: coversCleaningsCheck
                    text: "Covers Cleanings"
                }
                CheckBox {
                    id: coversSealantCheck
                    text: "Covers Sealant"
                }
            }
        }

    }
}
