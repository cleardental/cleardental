// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2

SpinBox {
    from: 0
    to: 10000
    textFromValue:  function(getValue) {
        return "$" + getValue;
    }
    editable: true
    valueFromText: function(getText) {return getText.replace("$","")}
}
