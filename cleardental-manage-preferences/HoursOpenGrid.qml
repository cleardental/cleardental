// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

GridLayout {
    id: gridRoot

    function generateSaveString() {
        var returnMe = "";
        returnMe += "Open " + (openHour.currentText) +
                ":" + (openMin.currentText) + " " + openAMPM.displayText +
                " | ";

        returnMe += "Closed " + (closeHour.currentText) +
                ":" + (closeMin.currentText) + " " + closeAMPM.displayText;

        if(lunchBrCheck.checked) {
            returnMe += " | Lunch Break for " +
                    (lunchBreakLength.currentText) + " at " +
                    lunchBreakHours.displayText + ":" +
                    (lunchBreakMins.currentText) + " " +
                    lunchBreakAMPM.displayText;
        }
        else {
            returnMe += "| No Lunch Break"
        }
        return returnMe;
    }

    function parseSaveString(inputString) {
        var parts = inputString.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].startsWith("Open")) {
                var openParts = parts[i].split(" ");
                var openTimeSetter = openParts[1].split(":"); //hh,mm
                var openHourSetter = openTimeSetter[0];
                openHour.currentIndex = openHour.find(openHourSetter);
                var openMinsSetter = openTimeSetter[1];
                openMin.currentIndex = openMin.find(openMinsSetter);
                var openAMPMSetter = openParts[2];
                openAMPM.currentIndex = openAMPM.find(openAMPMSetter);
            }
            else if(parts[i].startsWith(" Closed")) {
                var closedParts = parts[i].split(" ");
                var closedTimeSetter = closedParts[2].split(":"); //hh,mm
                var closedHourSetter = closedTimeSetter[0];
                closeHour.currentIndex = closeHour.find(closedHourSetter);
                var closedMinsSetter = closedTimeSetter[1];
                closeMin.currentIndex = closeMin.find(closedMinsSetter);
                var closedAMPMSetter = closedParts[3];
                closeAMPM.currentIndex = closeAMPM.find(closedAMPMSetter);
            }
            else if(parts[i].startsWith(" No Lunch Break")) {
                lunchBrCheck.checked = false;
            }
            else if(parts[i].startsWith(" Lunch Break")) {
                lunchBrCheck.checked = true;
                var lunchParts = parts[i].split(" ");
                var setLunchBreakLength = lunchParts[4] + " " + lunchParts[5];
                lunchBreakLength.currentIndex =
                        lunchBreakLength.find(setLunchBreakLength);
                var lunchBreakTime = lunchParts[7].split(":");//hh,mm
                var setLunchBreakHour =lunchBreakTime[0];
                lunchBreakHours.currentIndex =
                        lunchBreakHours.find(setLunchBreakHour);
                var setLunchBreakMin = lunchBreakTime[1];
                lunchBreakMins.currentIndex =
                        lunchBreakMins.find(setLunchBreakMin);
                var setLunchBreakAMPM = lunchParts[8];
                lunchBreakAMPM.currentIndex =
                        lunchBreakAMPM.find(setLunchBreakAMPM);
            }
        }
    }

    columns: 6
    Label {
        text: "Open:"
        font.bold: true
    }
    
    ComboBox {
        id: openHour
        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
        Layout.preferredWidth: 75
        currentIndex: 7
    }
    
    Label {
        text: ":"
    }
    
    ComboBox {
        id: openMin
        model: ["00","15","30","45"]
        Layout.preferredWidth: 75
    }
    
    ComboBox {
        id: openAMPM
        model: ["AM","PM"]
        Layout.preferredWidth: 75
    }

    Label {
        Layout.fillWidth: true
    }

    Label {
        text: "Closed:"
        font.bold: true
    }
    
    ComboBox {
        id: closeHour
        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
        Layout.preferredWidth: 75
        currentIndex: 4
    }
    
    Label {
        text: ":"
    }
    
    ComboBox {
        id: closeMin
        model: ["00","15","30","45"]
        Layout.preferredWidth: 75
    }
    
    ComboBox {
        id: closeAMPM
        model: ["AM","PM"]
        Layout.preferredWidth: 75
        currentIndex: 1
    }

    Layout.fillWidth: true

    RowLayout {
        Layout.columnSpan: 6
        CheckBox {
            id: lunchBrCheck
            text: "Lunch Break"
            checked: true
        }
        ComboBox {
            id: lunchBreakLength
            model: ["15 minutes","30 minutes","45 minutes","60 minutes",
                "75 minutes","90 minutes"]
            enabled: lunchBrCheck.checked
            currentIndex: 3
            Layout.columnSpan: 2
        }
        Label {
            text: "Starting at: "
            font.bold: true
            enabled: lunchBrCheck.checked
        }
        ComboBox {
            id: lunchBreakHours
            model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
            Layout.preferredWidth: 75
            enabled: lunchBrCheck.checked
            currentIndex: 10
        }

        Label {
            text: ":"
            enabled: lunchBrCheck.checked
        }

        ComboBox {
            id: lunchBreakMins
            model: ["00","15","30","45"]
            Layout.preferredWidth: 75
            enabled: lunchBrCheck.checked
            currentIndex: 2
        }

        ComboBox {
            id: lunchBreakAMPM
            model: ["AM","PM"]
            Layout.preferredWidth: 75
            enabled: lunchBrCheck.checked
        }
    }
}
