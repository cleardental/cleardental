// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: accountsPane

    function loadData() {
        locFile.category = "Accounts"
        voipMSUsername.text = locFile.value("VoipMSUsername","");
        voipMSPassword.text = locFile.value("VoipMSPassword","");

        edsEDIUsername.text = locFile.value("EDSEDIUsername","");
        edsEDIPassword.text = locFile.value("EDSEDIPassword","");

        dentalXChangeUsername.text = locFile.value("DentalXChangeUsername","");
        dentalXChangePassword.text = locFile.value("DentalXChangePassword","");
        dentalXChangeGroup.text = locFile.value("DentalXChangeGroup","");
        dentalXChangeAPIKey.text = locFile.value("DentalXChangeAPIKey","");

        tesiaUsername.text = locFile.value("TesiaUsername","");
        tesiaPassword.text = locFile.value("TesiaPassword","");

        defaultClearingHouse.currentIndex = defaultClearingHouse.find(locFile.value("DefaultClearingHouse",""));
    }

    function saveData() {
        locFile.category = "Accounts"
        locFile.setValue("VoipMSUsername",voipMSUsername.text);
        locFile.setValue("VoipMSPassword",voipMSPassword.text);

        locFile.setValue("EDSEDIUsername",edsEDIUsername.text);
        locFile.setValue("EDSEDIPassword",edsEDIPassword.text);

        locFile.setValue("DentalXChangeUsername",dentalXChangeUsername.text);
        locFile.setValue("DentalXChangePassword",dentalXChangePassword.text);
        locFile.setValue("DentalXChangeGroup",dentalXChangeGroup.text);
        locFile.setValue("DentalXChangeAPIKey",dentalXChangeAPIKey.text);

        locFile.setValue("TesiaUsername",tesiaUsername.text);
        locFile.setValue("TesiaPassword",tesiaPassword.text);

        locFile.setValue("DefaultClearingHouse",defaultClearingHouse.currentValue);
    }

    GridLayout {
        anchors.centerIn: parent
        columns: 3

        CDTranslucentPane {
            backMaterialColor: Material.LightGreen
            ColumnLayout {
                CDHeaderLabel {
                    text: "voip.ms"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: voipMSUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: voipMSPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.LightBlue
            ColumnLayout {
                CDHeaderLabel {
                    text: "EDS EDI"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: edsEDIUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: edsEDIPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Blue
            ColumnLayout {
                CDHeaderLabel {
                    text: "DentalXChange"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: dentalXChangeUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: dentalXChangePassword
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "Group Number"
                    }
                    TextField {
                        id: dentalXChangeGroup
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "API Key"
                    }
                    TextField {
                        id: dentalXChangeAPIKey
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Cyan
            ColumnLayout {
                CDHeaderLabel {
                    text: "Vyne / Tesia"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: tesiaUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: tesiaPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Grey
            ColumnLayout {
                CDHeaderLabel {
                    text: "Default Clearning House"
                }

                ComboBox {
                    id: defaultClearingHouse
                    Layout.fillWidth: true
                    model: ["EDS EDI", "DentalXChange", "Vyne / Tesia"]
                }
            }
        }


    }
}
