// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: consentFormsPane

    function loadData() {
        locFile.category = "Consent"
        newPatConsent.text = locFile.value("NewPatConsent",defForms.getDefaultConsent("newPatient"));
        finanConsent.text = locFile.value("FinConsent",defForms.getDefaultConsent("finan"));
        osConsent.text = locFile.value("ExtConsent",defForms.getDefaultConsent("extraction"));
        fixedProsConsent.text = locFile.value("FixedProsConsent",defForms.getDefaultConsent("fixedpros"));
        remProsConsent.text = locFile.value("RemProsConsent",defForms.getDefaultConsent("remPros"));
        endoConsent.text = locFile.value("EndoConsent",defForms.getDefaultConsent("endo"));
        genConsent.text = locFile.value("GenProcedureConsent",defForms.getDefaultConsent("genProcedure"));
    }

    function saveData() {
        locFile.category = "Consents"
        locFile.setValue("NewPatConsent",newPatConsent.text);
        locFile.setValue("FinConsent",finanConsent.text);
        locFile.setValue("ExtConsent",osConsent.text);
        locFile.setValue("FixedProsConsent",fixedProsConsent.text);
        locFile.setValue("RemProsConsent",remProsConsent.text);
        locFile.setValue("EndoConsent",endoConsent.text);
        locFile.setValue("GenProcedureConsent",genConsent.text);
    }

    Flickable {
        id: flickView
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: gridConForms.implicitWidth
        contentHeight: gridConForms.implicitHeight + 25

        ScrollBar.vertical: ScrollBar {}

        CDTranslucentPane {
            backMaterialColor: Material.LightBlue
            x: (flickView.width - gridConForms.implicitWidth)/2
            GridLayout {
                id: gridConForms
                columnSpacing: 25
                rowSpacing: 25
                columns: 2

                Label {
                    text: "New Patient Treatment\nConsent Form"
                    font.bold: true
                }

                TextArea {
                    id: newPatConsent
                    Layout.minimumHeight: 300
                    Layout.maximumWidth: 700
                    Layout.minimumWidth: 640
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    placeholderText: "Please paste your consent form here"
                    selectByMouse: true
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "New Patient Financial\nConsent Form"
                    font.bold: true
                }

                TextArea {
                    id: finanConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Oral Surgery Consent Form"
                    font.bold: true
                }

                TextArea {
                    id: osConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Fixed Pros Consent Form"
                    font.bold: true
                }

                TextArea {
                    id: fixedProsConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }


                Label {
                    text: "Removable Pros Consent Form"
                    font.bold: true
                }

                TextArea {
                    id: remProsConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Endo Consent Form"
                    font.bold: true
                }

                TextArea {
                    id: endoConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "General Procedure Consent Form"
                    font.bold: true
                }

                TextArea {
                    id: genConsent
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your consent form here"
                    wrapMode: TextEdit.WordWrap
                }
            }
        }
    }


}
