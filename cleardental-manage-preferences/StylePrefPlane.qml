// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: stylePrefPlane

    function loadData() {
        locFile.category = "Style"
        defaultComposite.editText = locFile.value("DefaultComposite",
                                                  defaultComposite.editText);
        yesFlowableComp.checked = locFile.value("UseFlowables",
                                                "true") === "true";
        noFlowableComp.checked = !yesFlowableComp.checked;
        defaultBonding.editText = locFile.value("DefaultBondingAgent",
                                                defaultBonding.editText);
        defaultMatrix.editText = locFile.value("DefaultMatrix",
                                               defaultMatrix.editText);
        defaultLiner.editText = locFile.value("DefaultLiner",
                                              defaultLiner.editText);
        defaultIsolation.editText = locFile.value("DefaultIsolation",
                                                  defaultIsolation.editText);
        defaultCement.editText = locFile.value("DefaultCement",
                                               defaultCement.editText);
        sayNoToRadiographsBox.currentIndex = sayNoToRadiographsBox.find(
                    locFile.value("SayNoToRadiographs",""));
    }

    function saveData() {
        locFile.category = "Style";
        locFile.setValue("DefaultComposite",defaultComposite.editText);
        locFile.setValue("UseFlowables",yesFlowableComp.checked);
        locFile.setValue("DefaultBondingAgent", defaultBonding.editText);
        locFile.setValue("DefaultMatrix",defaultMatrix.editText);
        locFile.setValue("DefaultLiner",defaultLiner.editText);
        locFile.setValue("DefaultIsolation",defaultIsolation.editText);
        locFile.setValue("DefaultCement",defaultCement.editText);
        locFile.setValue("SayNoToRadiographs", sayNoToRadiographsBox.currentText);
        locFile.setValue("DefaultProviderID",defaultProvider.getSelectedProviderID());
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Indigo
        GridLayout {
            columnSpacing: 25
            columns: 2

            Label {
                text: "Default Composite"
                font.bold: true
            }

            ComboBox {
                id: defaultComposite
                model: ["Filtek™ Supreme Ultra","Gradia","Esthet-X","Safco Illusion Plus"]
                editable: true
                Layout.minimumWidth: 300
            }

            Label {
                text: "Are flowable composites used?"
                font.bold: true
            }

            RowLayout {
                RadioButton {
                    id: yesFlowableComp
                    text: "Yes"
                    checked: true
                }
                RadioButton {
                    id: noFlowableComp
                    text: "No"
                }
            }

            Label {
                text: "Default Bonding Agent"
                font.bold: true
            }

            ComboBox {
                id: defaultBonding
                model: ["OptiBond","All-Bond Universal","BeautiBond",
                    "CLEARFIL SE PROTECT","Cosmedent Complete","G-aenial Bond",
                    "Scotchbond™, Safco Tru-Bond LC"]
                editable: true
                Layout.minimumWidth: 300
            }

            Label {
                text: "Default Composite Matrix"
                font.bold: true
            }

            ComboBox {
                id: defaultMatrix
                model: ["Garrison/Composi-Tight","Tofflemire"]
                editable: true
                Layout.minimumWidth: 300
            }

            Label {
                text: "Default Restoration Liner"
                font.bold: true
            }

            ComboBox {
                id: defaultLiner
                editable: true
                model: ["Vitrebond™","Durelon™","Lime-Lite™", "Fuji II LC"]
                Layout.minimumWidth: 300
            }

            Label {
                text: "Default Restoration Isolation"
                font.bold: true
            }

            ComboBox {
                id: defaultIsolation
                editable: true
                model: ["Rubber Dam", "Cotton", "Isolite", "Isodry","DryShield"]
                Layout.minimumWidth: 300
            }

            Label {
                text: "Default Fixed Pros. Cement"
                font.bold: true
            }

            ComboBox {
                id: defaultCement
                editable: true
                model: ["RelyX™ Luting", "RelyX™ Unicem","GC Fuji PLUS®",
                    "GC FujiCEM® 2"]
                Layout.minimumWidth: 300
            }

            Label {
                text: "Can patients\ndecline radiographs?"
                font.bold: true
            }

            ComboBox {
                id: sayNoToRadiographsBox
                model: ["Yes, only with a consent form", "No, Never",
                    "Yes, no consent needed"]
                Layout.minimumWidth: 300
            }
            CDDescLabel {
                text: "Default Provider"
            }

            CDProviderBox {
                id: defaultProvider
                Layout.fillWidth: true
            }
        }
    }
}
