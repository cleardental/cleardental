// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: openDaysHoursPane

    function loadData() {
        locFile.category = "Hours"
        if(JSON.parse(locFile.value("OpenSunday",false)) === true) {
            sundayCheck.checked = true;
            sunHours.parseSaveString(locFile.value("SundayHours",""));
        }

        if(JSON.parse(locFile.value("OpenMonday",false)) === true) {
            mondayCheck.checked = true;
            monHours.parseSaveString(locFile.value("MondayHours",""));
        }

        if(locFile.value("OpenTuesday",false) === "true") {
            tuesdayCheck.checked = true;
            tuesHours.parseSaveString(locFile.value("TuesdayHours",""));
        }

        if(locFile.value("OpenWednesday",false) === "true") {
            wednesdayCheck.checked = true;
            wedGrid.parseSaveString(locFile.value("WednesdayHours",""));
        }

        if(locFile.value("OpenThursday",false) === "true") {
            thursdayCheck.checked = true;
            thursGrid.parseSaveString(locFile.value("ThursdayHours",""));
        }

        if(locFile.value("OpenFriday",false) === "true") {
            fridayCheck.checked = true;
            friGrid.parseSaveString(locFile.value("FridayHours",""));
        }

        if(locFile.value("OpenSaturday",false) === "true") {
            saturdayCheck.checked = true;
            satGrid.parseSaveString(locFile.value("SaturdayHours",""));
        }
    }

    function saveData() {
        locFile.category = "Hours"
        if(sundayCheck.checked) {
            locFile.setValue("OpenSunday",true);
            locFile.setValue("SundayHours", sunHours.generateSaveString());
        }
        else {
            locFile.setValue("OpenSunday",false);
        }

        if(mondayCheck.checked) {
            locFile.setValue("OpenMonday",true);
            locFile.setValue("MondayHours", monHours.generateSaveString());
        }
        else {

            locFile.setValue("OpenMonday",false);
        }

        if(tuesdayCheck.checked) {
            locFile.setValue("OpenTuesday",true);
            locFile.setValue("TuesdayHours",
                             tuesHours.generateSaveString());
        }
        else {
            locFile.setValue("OpenTuesday",false);
        }

        if(wednesdayCheck.checked) {
            locFile.setValue("OpenWednesday",true);
            locFile.setValue("WednesdayHours",
                             wedGrid.generateSaveString());
        }
        else {
            locFile.setValue("OpenWednesday",false);
        }

        if(thursdayCheck.checked) {
            locFile.setValue("OpenThursday",true);
            locFile.setValue("ThursdayHours",
                             thursGrid.generateSaveString());
        }
        else {
            locFile.setValue("OpenThursday",false);
        }

        if(fridayCheck.checked) {
            locFile.setValue("OpenFriday",true);
            locFile.setValue("FridayHours", friGrid.generateSaveString());
        }
        else {
            locFile.setValue("OpenFriday",false);
        }

        if(saturdayCheck.checked) {
            locFile.setValue("OpenSaturday",true);
            locFile.setValue("SaturdayHours", satGrid.generateSaveString());
        }
        else {
            locFile.setValue("OpenSaturday",false);
        }

    }

    Flickable {
        id: flickView
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: gridHours.implicitWidth
        contentHeight: gridHours.implicitHeight +25

        ScrollBar.vertical: ScrollBar {}

        CDTranslucentPane {
            backMaterialColor: Material.Pink
            x: (flickView.width - gridHours.implicitWidth)/2
            GridLayout {
                id: gridHours
                columns: 2

                CheckBox {
                    id: sundayCheck
                    text: "Sunday"
                    checked: false
                }
                HoursOpenGrid {
                    id: sunHours
                    enabled: sundayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: mondayCheck
                    text: "Monday"
                    checked: false
                }
                HoursOpenGrid {
                    id: monHours
                    enabled: mondayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: tuesdayCheck
                    text: "Tuesday"
                    checked: false
                }
                HoursOpenGrid {
                    id: tuesHours
                    enabled: tuesdayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: wednesdayCheck
                    text: "Wednesday"
                    checked: false
                }
                HoursOpenGrid {
                    id: wedGrid
                    enabled: wednesdayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: thursdayCheck
                    text: "Thursday"
                    checked: false
                }
                HoursOpenGrid {
                    id: thursGrid
                    enabled: thursdayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: fridayCheck
                    text: "Friday"
                    checked: false
                }
                HoursOpenGrid {
                    id: friGrid
                    enabled: fridayCheck.checked
                }

                MenuSeparator {Layout.columnSpan: 2;Layout.fillWidth: true}

                CheckBox {
                    id: saturdayCheck
                    text: "Saturday"
                    checked: false
                }
                HoursOpenGrid {
                    id: satGrid
                    enabled: saturdayCheck.checked
                }
            }
        }


    }


}
