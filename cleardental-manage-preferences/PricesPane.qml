// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
//https://www.decare.com/cm_files/pdf/122010_DDNAdminOfficeGuide_1210_FINAL_Settoprint2-sided.pdf
//https://www.deltadentalco.com/uploadedFiles/ProviderFeeSchedules/DDCO_Par_Provider_Documents/CDT%202017_Code%20on%20Dental%20Proc_Nomenclature%20online.pdf
//http://desiotaku.github.io/TUSDM-Checklist-App/prices.html

CDTransparentPage {
    id: pricesPane

    CDConstLoader{id: m_ConstLoader}

    CDFeeScheduleManager {
        id: feeSchMan
    }

    Settings {
        id: feeScheduleSet
        fileName: fileLocs.getLocalPracticeFeesFile()
    }


    function loadData() {
        if(feeSchBox.model.length === 0) {
            return;
        }

        feeScheduleSet.category = feeSchBox.model[feeSchBox.currentIndex]

        //first remove the previous prices
        for(var i=0;i<priceGrid.children.length;i++) {
            if("deleteMeLater" in priceGrid.children[i]) {
                var killMe = priceGrid.children[i];
                killMe.destroy();
            }
        }

        //Now add in the
        var fullList = JSON.parse(m_ConstLoader.getDCodesJSON());
        for(i=0;i<fullList.length;i++) {
            var makeLabel = labelComp.createObject(priceGrid);
            makeLabel.text = fullList[i]["ProcedureName"] + " (" + fullList[i]["DCode"] + ")";

            var makeSpinner = spinBoxComp.createObject(priceGrid)
            makeSpinner.value = feeScheduleSet.value(fullList[i]["DCode"],0);
        }

    }

    function saveData() {
        var prices = priceGrid.children;
        for(var i=2;i<prices.length;i++) {
            if (typeof prices[i].text !== 'undefined') {
                var spinner = prices[i+1];
                var dCode = prices[i].text.split("(")[1].substring(0,5);
                feeScheduleSet.setValue(dCode,spinner.value);
            }
        }
    }

    Component {
        id: labelComp
        CDDescLabel {
            property bool deleteMeLater: true
        }

    }

    Component {
        id: spinBoxComp
        PriceSpinBox {
            property bool deleteMeLater: true

        }

    }

    Flickable {
        id: flickView
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: priceGrid.implicitWidth
        contentHeight: priceGrid.implicitHeight + 25

        ScrollBar.vertical: ScrollBar {}

        CDTranslucentPane {
            backMaterialColor: Material.Teal
            x: (flickView.width - priceGrid.implicitWidth)/2
            GridLayout {
                id: priceGrid
                columns: 2
                anchors.centerIn: parent

                RowLayout {
                    Layout.columnSpan: 2
                    CDDescLabel {
                        text: "Select Fee Schedule"
                    }
                    ComboBox {
                        id: feeSchBox

                        CDFeeScheduleManager {
                            id: feeMan
                        }

                        model: feeMan.getListOfPlans(manGDPage.locationID)
                        Layout.fillWidth: true
                        onCurrentIndexChanged: pricesPane.loadData();
                    }

                    CDAddButton {
                        AddNewFeeScheduleDia {
                            id: addNewFeeScheduleDia
                            onAccepted: {
                                feeScheduleSet.category = addNewFeeScheduleDia.newName;
                                feeScheduleSet.setValue("D0120",0); //force a value for the group
                                feeScheduleSet.sync();
                                pricesPane.loadData();
                            }
                        }

                        onClicked: {
                            addNewFeeScheduleDia.open();
                        }

                    }
                }



                CDHeaderLabel {
                    text: "Prices in US Dollar"
                    Layout.columnSpan: 2
                }
            }
        }
    }
}
