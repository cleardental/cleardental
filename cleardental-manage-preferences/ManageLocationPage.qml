// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: manGDPage

    property string locationID: ""

    onLocationIDChanged: {
        locationInfoPane.loadData();
        openDaysHoursPane.loadData();
        defaultUpdateTimesPane.loadData();
        stylePrefPlane.loadData();
        consentFormsPane.loadData();
        membershipPane.loadData();
        pricesPane.loadData();
        chairsPane.loadData();
        labsPane.loadData();
        accountsPane.loadData();
    }

    CDClinicLocationManager {
        id: locListMan
    }

    title: "Manage Location (" + locationID + ")"

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: locFile
        fileName: fileLocs.getPracticePreferenceFile(locationID)
    }

    CDDefaultForms {
        id: defForms
    }

    CDGitManager {
        id: gitMan
    }

    header: TabBar {
        id: headerTab

        currentIndex: 7

        Component.onCompleted: {
            currentIndex= 0
        }

        background: Rectangle {
            color: "white"
            opacity: .75
        }

        TabButton {
            text: "Location Information"
        }
        TabButton {
            text: "Open Days and Hours"
        }
        TabButton {
            text: "Default Update Times"
        }
        TabButton {
            text: "Style Preferences"
        }
        TabButton {
            text: "Consent Forms"
        }
//        TabButton {
//            text: "Post-Op Instructions"
//        }
        TabButton {
            text: "Membership"
        }
        TabButton {
            text: "Pricing"
        }
        TabButton {
            text: "Chairs"
        }
        TabButton {
            text: "Labs"
        }
        TabButton {
            text: "Accounts"
        }
    }

    SwipeView {
        anchors.fill: parent
        interactive: false
        currentIndex: headerTab.currentIndex
        LocationInfoPane {
            id: locationInfoPane
        }

        OpenDaysHoursPane {
            id: openDaysHoursPane
        }

        DefaultUpdateTimesPane {
            id: defaultUpdateTimesPane
        }

        StylePrefPlane {
            id: stylePrefPlane
        }

        ConsentFormsPane {
            id: consentFormsPane
        }

//        PostOpInstructionsPane {
//            id: postOpInstructionsPane
//        }

        MembershipPane {
            id: membershipPane
        }

        PricesPane {
            id: pricesPane
        }

        ChairsPane {
            id: chairsPane
        }
        LabsPane {
            id: labsPane
        }
        AccountsPane {
            id: accountsPane
        }
    }

    CDSaveButton {
        id: saveButton
        text: "Save Changes"
        icon.name: "document-save"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        enabled: true
        onClicked: {
            locationInfoPane.saveData();
            openDaysHoursPane.saveData();
            defaultUpdateTimesPane.saveData();
            stylePrefPlane.saveData();
            consentFormsPane.saveData();
            pricesPane.saveData();
            membershipPane.saveData();
            chairsPane.saveData();
            labsPane.saveData();
            accountsPane.saveData();
            locFile.sync();
            gitMan.commitData("Saved changes for " + locationID)
            showSaveToolTip();
        }
    }

}
