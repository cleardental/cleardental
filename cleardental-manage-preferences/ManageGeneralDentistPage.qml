// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: manGDPage

    property string providerID: ""

    title: "Manage General Dentist (" + providerID + ")"

    onProviderIDChanged: {
        docFile.fileName = fileLocs.getDocInfoFile(providerID)
        firstNameField.text = docFile.value("FirstName")
        lastNameField.text = docFile.value("LastName")
        npiField.text = docFile.value("NPINumb","")
        deaField.text = docFile.value("DEANumb","")
        extractTimeBox.value = docFile.value("ExtractTime",30)
        crownPrepBox.value = docFile.value("CrownPrepTime",60)
        baseFillingTime.value = docFile.value("BaseFillingTime",30)
        addlFillingTime.value = docFile.value("AddlFillingTime",10)
        schColor.setColor(docFile.value("ProviderColor","Blue"));

        surgExtCheckBox.checked = JSON.parse(docFile.value("DoesSurgicalExt",false))
        doesImpactExtBox.checked = JSON.parse(docFile.value("DoesImpactExt",false))
        placesImplantsBox.checked = JSON.parse(docFile.value("PlacesImplants",false))
        doesAntRCTBox.checked = JSON.parse(docFile.value("DoesAntRCT",false))
        doesPreMolRCTBox.checked = JSON.parse(docFile.value("DoesPreMolRCT",false))
        doesMolRCTBox.checked =JSON.parse(docFile.value("DoesMolRCT",false))
        doesPedBox.checked = JSON.parse(docFile.value("DoesPed",false))
        doesAmalBox.checked = JSON.parse(docFile.value("DoesAmalgams",false))
        doesProph.checked = JSON.parse(docFile.value("DoesProphies",false))
        doesSCRP.checked = JSON.parse(docFile.value("DoesSCRP",false))

        licensePane.licenseArray = JSON.parse(docFile.value("LicenseArray","[]"));
        licensePane.refreshList();
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: docFile
        fileName: fileLocs.getDocInfoFile(providerID)
    }

    CDGitManager {
        id: gitMan
    }

    RowLayout {
        anchors.centerIn: parent

        CDTranslucentPane {
            id: backPane
            backMaterialColor: schColor.buttonColors[schColor.setColorIndex]
            GridLayout {
                columnSpacing: 25
                columns: 2
                CDDescLabel {
                    text: "First Name"
                }
                TextField {
                    id: firstNameField
                }
                CDDescLabel {
                    text: "Last Name"
                }
                TextField {
                    id: lastNameField
                }
                CDDescLabel {
                    text: "Schedule Color"
                }
                GridLayout {
                    id: schColor
                    columns: 10

                    property int setColorIndex: 0

                    function setColor(stringName) {
                        setColorIndex = buttonColorStrings.indexOf(stringName);

                    }

                    property var buttonColors: [Material.Red,Material.Pink,
                        Material.Purple, Material.DeepPurple, Material.Indigo,
                        Material.Blue, Material.LightBlue, Material.Cyan, Material.Teal,
                        Material.Green, Material.LightGreen, Material.Lime,
                        Material.Yellow, Material.Amber, Material.Orange,
                        Material.DeepOrange, Material.Brown, Material.Grey,
                        Material.BlueGrey]

                    property var buttonColorStrings: ["Red","Pink","Purple",
                        "DeepPurple","Indigo","Blue","LightBlue","Cyan","Teal","Green",
                        "LightGreen","Lime","Yellow","Amber","Orange","DeepOrange",
                        "Brown","Grey","BlueGrey"]

                    Repeater {
                        model: schColor.buttonColors.length
                        Button {
                            property string colorName:
                                schColor.buttonColorStrings[index]
                            highlighted: true
                            Layout.maximumWidth: 50
                            Material.accent:schColor.buttonColors[index]
                            checkable: true
                            checked: index==schColor.setColorIndex
                            icon.name: checked ? "dialog-ok" : ""
                            onClicked: {
                                backPane.backMaterialColor =  schColor.buttonColors[index]
                            }
                        }
                    }

                    ButtonGroup {
                        id: checkedColor
                        buttons: schColor.children
                    }

                }

                CDDescLabel {
                    text: "NPI Number"
                }
                TextField {
                    id: npiField
                }
                CDDescLabel {
                    text: "DEA Number"
                }
                TextField {
                    id: deaField
                }

                CDDescLabel {
                    text: "Time for extractions (per tooth)"
                }

                SpinBox {
                    id: extractTimeBox
                    from: 0
                    to: 90
                    editable: true
                    stepSize: 10
                    textFromValue: function(value) {
                        return value + " minutes"
                    }
                }

                CDDescLabel {
                    text: "Time for crown prep"
                }

                SpinBox {
                    id: crownPrepBox
                    from: 0
                    to: 90
                    editable: true
                    stepSize: 10
                    textFromValue: function(value) {
                        return value + " minutes"
                    }
                }

                CDDescLabel {
                    text: "Time for fillings"
                }

                RowLayout {
                    SpinBox {
                        id: baseFillingTime
                        from: 0
                        to: 90
                        editable: true
                        stepSize: 15
                        textFromValue: function(value) {
                            return value + " minutes"
                        }
                    }

                    Label {
                        text: "plus"
                    }

                    SpinBox {
                        id: addlFillingTime
                        from: 0
                        to: 90
                        editable: true
                        stepSize: 15
                        textFromValue: function(value) {
                            return value + " minutes"
                        }
                    }

                    Label {
                        text: "per surface"
                    }

                }

                CheckBox {
                    id: surgExtCheckBox
                    text: "Does surgical extractions"
                }

                CheckBox {
                    id: doesImpactExtBox
                    text: "Does full impacted extractions"
                }

                CheckBox {
                    id: placesImplantsBox
                    text: "Places Implants"
                }

                CheckBox {
                    id: doesAntRCTBox
                    text: "Does anterior RCT"
                }

                CheckBox {
                    id: doesPreMolRCTBox
                    text: "Does premolar RCT"
                }

                CheckBox {
                    id: doesMolRCTBox
                    text: "Does molar RCT"
                }

                CheckBox {
                    id: doesPedBox
                    text: "Treats pediatric patients"
                }

                CheckBox {
                    id: doesAmalBox
                    text: "Places amalgams"
                }

                CheckBox {
                    id: doesProph
                    text: "Will do prophies"
                }

                CheckBox {
                    id: doesSCRP
                    text: "Will do SCRP"
                }
            }
        }

        CDTranslucentPane {

            id: licensePane
            backMaterialColor: backPane.backMaterialColor

            property var licenseArray: []

            function refreshList() {
                for(var i=0;i<licenseGrid.children.length;i++) { //clear the grid
                    var child = licenseGrid.children[i];
                    if("deleteMeLater" in child) {
                        child.destroy();
                    }
                }

                for(i=0;i<licenseArray.length;i++) {
                    var license = licenseArray[i];
                    var statePicker = stateComp.createObject(licenseGrid,{"myIndex":i});
                    statePicker.setStateVal = license["State"]
                    idComp.createObject(licenseGrid,{"text":license["LicenseID"],"myIndex":i})
                    remButComp.createObject(licenseGrid,{"myIndex":i})
                }
            }

            ColumnLayout {
                CDHeaderLabel {
                    text: "Manage Licenses"
                }

                GridLayout {
                    id: licenseGrid //Hey, its FF12!
                    columns: 3

                    CDDescLabel {
                        text: "State"
                    }

                    CDDescLabel {
                        text: "License ID"
                    }

                    Label {} //placeholder for the deleteButton


                    Component {
                        id: stateComp
                        CDStatePicker {
                            property bool deleteMeLater: true
                            Layout.minimumWidth: 200
                            property int myIndex: -1

                            onDownChanged: {
                                licensePane.licenseArray[myIndex]["State"] = currentValue;
                            }
                        }
                    }

                    Component {
                        id: idComp
                        TextField {
                            property bool deleteMeLater: true
                            property int myIndex: -1
                            onEditingFinished: {
                                licensePane.licenseArray[myIndex]["LicenseID"] = text;
                            }
                        }
                    }

                    Component {
                        id: remButComp
                        CDDeleteButton {
                            text: "Remove License"
                            property bool deleteMeLater: true
                            property int myIndex: -1

                            onClicked: {
                                licensePane.licenseArray.splice(myIndex,1);
                                licensePane.refreshList();
                            }
                        }
                    }
                }

                RowLayout {
                    Label {
                        Layout.fillWidth: true
                    }

                    CDAddButton {
                        text: "Add New License"
                        onClicked: {
                            var fakeLicense = {
                                State: "MA",
                                LicenseID: ""
                            };
                            licensePane.licenseArray.push(fakeLicense);
                            licensePane.refreshList();
                        }
                    }
                }
            }


        }
    }





    CDSaveButton {
        text: "Save + Go Back"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        enabled: firstNameField.text.length > 1 &&
                 lastNameField.text.length > 1
        onClicked: {
            docFile.setValue("FirstName",firstNameField.text);
            docFile.setValue("LastName",lastNameField.text);
            docFile.setValue("ProviderColor",checkedColor.checkedButton.colorName);
            docFile.setValue("NPINumb",npiField.text);
            docFile.setValue("DEANumb", deaField.text);
            docFile.setValue("ExtractTime", extractTimeBox.value);
            docFile.setValue("CrownPrepTime", crownPrepBox.value);
            docFile.setValue("BaseFillingTime", baseFillingTime.value);
            docFile.setValue("AddlFillingTime",addlFillingTime.value);
            docFile.setValue("DoesSurgicalExt",surgExtCheckBox.checked);
            docFile.setValue("DoesImpactExt", doesImpactExtBox.checked);
            docFile.setValue("PlacesImplants", placesImplantsBox.checked);
            docFile.setValue("DoesAntRCT", doesAntRCTBox.checked);
            docFile.setValue("DoesPreMolRCT", doesPreMolRCTBox.checked);
            docFile.setValue("DoesMolRCT", doesMolRCTBox.checked);
            docFile.setValue("DoesPed", doesPedBox.checked);
            docFile.setValue("DoesAmalgams",doesAmalBox.checked);
            docFile.setValue("DoesProphies", doesProph.checked);
            docFile.setValue("DoesSCRP",doesSCRP.checked)
            docFile.setValue("Taxonomy","1223G0001X");
            docFile.setValue("LicenseArray",JSON.stringify(licensePane.licenseArray))
            docFile.sync();
            gitMan.commitData("Updated information for " + providerID);
            mainView.pop();
        }
    }

}
