// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: labsPane

    function loadData() {
        var jsonText = textFileMan.readFile(fileLocs.getPracticeLabsFile(locationID));
        if(jsonText.length === 0) {
            return;
        }

        var result = JSON.parse(jsonText);
        for(var i=0;i<result.length;i++) {
            var obj = result[i];
            textFieldComp.createObject(labsGrid,{"text":obj["LabName"]});
            textFieldComp.createObject(labsGrid,{"text":obj["LabAddress"]});
            textFieldComp.createObject(labsGrid,{"text":obj["LabPhone"]});
            deleteButton.createObject(labsGrid,{});
        }
    }

    function saveData() {
        var jsonData = [];
        for(var i=5;i<labsGrid.children.length;i+=4) {
            if(labsGrid.children[i].text.length > 2) {
                var addMe = {"LabName": labsGrid.children[i].text,
                    "LabAddress": labsGrid.children[i+1].text,
                    "LabPhone": labsGrid.children[i+2].text};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textFileMan.saveFile(fileLocs.getPracticeLabsFile(locationID),jsonString);
    }

    CDTextFileManager {
        id: textFileMan
    }

    Label {
        id: noLabsLabel
        text: "No Labs recorded"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: (labsGrid.children.length < 6) ? 1:0
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }

    CDTranslucentPane {
        id: labPane
        anchors.centerIn: parent
        backMaterialColor: Material.Lime
        GridLayout {
            id: labsGrid
            columnSpacing: 25
            columns: 4
            visible: opacity > 0
            opacity: labsGrid.children.length > 5
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(labsGrid,{"placeholderText":"Lab Name"});
                textFieldComp.createObject(labsGrid,{"placeholderText":"Lab Address"});
                textFieldComp.createObject(labsGrid,{"placeholderText":"Lab Phone"});
                deleteButton.createObject(labsGrid);
            }

            function killTheZombie() {
                for(var i=5;i<labsGrid.children.length;i+=4) {
                    var killBut = labsGrid.children[i+3];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = labsGrid.children[i];
                        var kill2 = labsGrid.children[i+1];
                        var kill3 = labsGrid.children[i+2];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }

            Label {
                text: "Dental Labs"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 4
            }

            Label {
                font.bold: true
                text: "Lab Name"
            }
            Label {
                font.bold: true
                text: "Full Address"
            }

            Label {
                font.bold: true
                text: "Phone Number"
            }

            Label { //just for placeholder to make a new row

            }

            Component {
                id: textFieldComp
                TextField {
                    Layout.minimumWidth: 300
                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    text: "Remove Lab"
                    onClicked: {
                        text = "Kill me now!";
                        labsGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }
        }
    }



    CDAddButton {
        id: addButton
        text: "Add Lab"
        anchors.right: labPane.right
        anchors.top: labPane.bottom
        anchors.margins: 10
        onClicked: labsGrid.makeNewRow();
    }
}
