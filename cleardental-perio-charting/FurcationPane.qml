// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: furcationPane
    backMaterialColor: Material.Teal

    function generateFurcationString() {
        return furButGroup.checkedButton.text;
    }

    function parseFurcationString(getFur) {
        for(var i=0;i<furButGroup.buttons.length;i++) {
            furButGroup.buttons[i].checked = (getFur ===
                                              furButGroup.buttons[i].text);
        }
    }

    ColumnLayout {
        Label {
            text: "Furcation Involvement"
            font.pointSize: 24
            font.underline: true
        }
        
        RowLayout {
            RadioButton {
                id: noFur
                text: "None"
                checked: true
            }
            
            RadioButton {
                id: classIFur
                text: "Class I"
            }
            RadioButton {
                id: classIIFur
                text: "Class II"
            }
            RadioButton {
                id: classIIIFur
                text: "Class III"
            }
        }

        ButtonGroup {
            id: furButGroup
            buttons:[noFur,classIFur,classIIFur,classIIIFur]
        }
    }
}
