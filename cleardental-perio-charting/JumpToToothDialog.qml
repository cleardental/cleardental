// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTranslucentDialog {
    id: jumpToDia

    property string selectedSide: bucOrLing.currentText
    property string selectedTooth: toothNumb.currentValue

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Jump to Which Tooth?"
                }

                RowLayout {
                    ComboBox {
                        id: bucOrLing
                        model: ["Buccal", "Lingual"]
                    }
                    ComboBox {
                        id: toothNumb

                        Component.onCompleted: {
                            var makeModel = [];
                            for(var i=1;i<=32;i++) {
                                if(!rootWin.isMissing(i)) {
                                    makeModel.push(i);
                                }
                            }
                            model = makeModel;
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: jumpToDia.reject();
            }
            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Jump"
                icon.name: "circular-arrow-shape"

                onClicked: {
                    jumpToDia.accept();
                }
            }
        }
    }
}
