// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: perioRecessionPane
    backMaterialColor: Material.Green

    ColumnLayout {
        Label {
            text: "Recession"
            font.pointSize: 24
            font.underline: true
        }
        
        RowLayout {
            Repeater {
                model: 10
                Button {
                    text: index
                    onClicked: currentValsPane.addRec(index)
                }
            }
            Button {
                icon.name: "edit-clear"
                onClicked: currentValsPane.rmRec()
            }
        }
    }
}
