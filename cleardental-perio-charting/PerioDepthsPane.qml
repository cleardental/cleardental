// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: perioDepthsPane
    backMaterialColor: Material.BlueGrey

    ColumnLayout {
        RowLayout {
            Label {
                text: "Periodontal Pockets"
                font.pointSize: 24
                font.underline: true
            }    
        }

        Flickable {
            Layout.preferredWidth:  rootWin.width - 50
            Layout.minimumHeight: 200
            contentWidth: (17 * 205) + 10
            contentHeight: 200
            clip: true
            RowLayout {
                Button {
                    Layout.minimumWidth: 200
                    Layout.minimumHeight: 200
                    icon.name: "edit-clear"
                    icon.width: 50
                    icon.height: 50
                    onClicked: currentValsPane.rmPD();
                }
                Repeater {
                    model: 16
                    Button {
                        text: index
                        font.pointSize: 24
                        Layout.minimumWidth: 200
                        Layout.minimumHeight: 200      
                        onClicked: {
                            //speech.sayThis(index);
                            currentValsPane.addPD(index);
                        }
                    }
                }
            }
        }
        
        
    }
}
