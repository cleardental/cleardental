// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTranslucentDialog {
    id: wnlDia
    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Select the range of teeth that are in Normal Limits"
                }
                RowLayout {
                    CDDescLabel {
                        text: "From"
                    }
                    ComboBox {
                        id: fromBox
                        Component.onCompleted: {
                            var makeModel = [];
                            for(var i=1;i<32;i++) {
                                if(!isMissing(i)) {
                                    makeModel.push(i);
                                }
                            }
                            model = makeModel;
                        }
                    }
                }
                RowLayout {
                    CDDescLabel {
                        text: "To"
                    }
                    ComboBox {
                        id: toBox
                        Component.onCompleted: {
                            var makeModel = [];
                            for(var i=2;i<=32;i++) {
                                if(!isMissing(i)) {
                                    makeModel.push(i);
                                }
                            }
                            model = makeModel;
                        }
                    }
                }
            }
        }
        
        RowLayout {
            CDCancelButton {
                onClicked: wnlDia.close();
            }
            Label {
                Layout.fillWidth: true
            }
            
            CDAddButton {
                text: "Set teeth to be WNL"

                Settings {
                    id: fastSet
                    fileName: fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
                }

                onClicked: {
                    var start = parseInt(fromBox.currentText);
                    var end = parseInt(toBox.currentText);
                    for(var i=start;i<=end;i++) {
                        if(!isMissing(i)) {
                            fastSet.category = i;
                            fastSet.setValue("Bleeding"," , , ");
                            fastSet.setValue("BuccalPockets", "WNL");
                            fastSet.setValue("LingualPockets", "WNL");
                            fastSet.setValue("BuccalRecession", "_,_,_");
                            fastSet.setValue("LingualRecession", "_,_,_");
                            fastSet.setValue("Mobility", "None");
                            if(isMolar(i)) {
                                fastSet.setValue("Furcation", "None");
                            }
                        }
                    }
                    fastSet.sync();
                    while((toothRunList[currentIndex].tooth <= end) && (currentIndex+1 < toothRunList.length) ) {
                        currentIndex++;
                    }

                    loadToothData();
                    wnlDia.close();
                }
            }
        }
    }
}
