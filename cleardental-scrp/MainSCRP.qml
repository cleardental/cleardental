// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr(comFuns.makeTxItemString(procedureObj) + " [ID: " + PATIENT_FILE_NAME + "]")
    property var procedureObj: JSON.parse(PROCEDURE_JSON)

    property var ledgerObj: []

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedureObj)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
        showOtherRooms: false
    }



    CDCommonFunctions {
        id: comFuns
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    CDToolLauncher {
        id: launcher
    }

    Settings {
        id: missingTeethSettings
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: perioChartSettings
        fileName: fLocs.getPerioChartFile(PATIENT_FILE_NAME)
    }

    RowLayout {
        anchors.centerIn: parent
        //anchors.margins: 20
        ColumnLayout {

            CDConsentFormPane {
                id: consentPane
                Layout.fillWidth: true
                consentString: "SCRP-" + procedureObj["Location"];
            }

            CDMedReviewPane {
                id: medRev
                Layout.fillWidth: true
                Layout.minimumWidth: 800
            }
            CDReviewRadiographPane {
                CDRadiographManager {
                    id: radioMan
                }
                Layout.fillWidth: true

                Component.onCompleted: {
                    var quadName = procedureObj["Location"];
                    if(quadName === "UR") {
                        radiographLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_UPPER_RIGHT);
                    }
                    else if(quadName === "UL") {
                        radiographLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_UPPER_LEFT);
                    }
                    else if(quadName === "LL") {
                        radiographLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_LOWER_LEFT);
                    }
                    else if(quadName === "LR") {
                        radiographLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_LOWER_RIGHT);
                    }
                }
            }
        }

        ColumnLayout {
            QuadInfoPane {
                id: quadInfoPane
                Layout.fillWidth: true
            }

            CDLAPane {
                id: laPane
                Layout.fillWidth: true

            }
        }
    }

    CDFinishProcedureButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        onClicked: {
            var caseNoteStr = "Patient presented for " + comFuns.makeTxItemString(procedureObj) +".\n";
            caseNoteStr+= consentPane.generateCaseNoteString();
            caseNoteStr+= medRev.generateCaseNoteString();
            caseNoteStr+= laPane.generateCaseNoteString();
            caseNoteStr+= "An ultrasonic scaler, with the appropriate tip, was used to remove as much supra and " +
                    "subgingival calculus as possible.\n"+
                    "Using hand scalers & curettes, all tooth surfaces were refined and planed with as little trauma "+
                    "as possible to the surrounding soft tisssue.\n"+
                    "All surfaces were checked with the explorer to ensure that they were free of calculus and "+
                    "smooth.\n"+
                    "If previous areas were treated, they were evaluated and if necessary, "+
                    "the scaling and root planing was refined.\n";
            finOpDia.caseNoteString = caseNoteStr
            finOpDia.txItemsToComplete = [procedureObj]
            finOpDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finOpDia
    }

}
