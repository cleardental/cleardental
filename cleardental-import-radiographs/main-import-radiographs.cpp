// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "mainimportradwindow.h"

#include <QApplication>

#include "cddefaults.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(CDDefaults::defaultColorIcon());
    MainImportRadWindow w;
    w.show();
    return a.exec();
}
