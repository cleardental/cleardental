// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "mainimportradwindow.h"
#include "ui_mainimportradwindow.h"

#include <QPixmap>
#include <QIcon>
#include <QDialogButtonBox>
#include <QDebug>
#include <QDir>

#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cdgitmanager.h"

MainImportRadWindow::MainImportRadWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainImportRadWindow)
{
    ui->setupUi(this);

    QStringList patList = CDPatientManager::getAllPatientIds();
    ui->patientIDBox->addItems(patList);
    //ui->patientIDBox->setCurrentText("");

    ui->dateEdit->setDate(QDate::currentDate());

    //Maxillary Row
    QStringList m_maxRowStrings = {
                                   "PA_Maxillary_Right_Distal",
                                   "PA_Maxillary_Right_Mesial",
                                   "PA_Maxillary_Anterior_Right",
                                   "PA_Maxillary_Anterior_Center",
                                   "PA_Maxillary_Anterior_Left",
                                   "PA_Maxillary_Left_Mesial",
                                   "PA_Maxillary_Left_Distal"};
    QWidget *maxillaryRowWidget = new QWidget();
    QHBoxLayout *maxillaryRowLayout = new QHBoxLayout();
    maxillaryRowWidget->setLayout(maxillaryRowLayout);
    for(int i=0;i<m_maxRowStrings.length();i++) {
        QWidget *columnWidget = new QWidget();
        QVBoxLayout *columnLayout = new QVBoxLayout();

        RadiographLabelPreview *radioLabel = new RadiographLabelPreview();
        radioLabel->setObjectName(m_maxRowStrings[i]);
        m_ImageLabelList.append(radioLabel);

        QString showMe = m_maxRowStrings[i];
        showMe = showMe.replace("_"," ");
        QLabel *descLabel = new QLabel(showMe);


        columnWidget->setLayout(columnLayout);
        columnLayout->addWidget(radioLabel,0, Qt::AlignCenter);
        columnLayout->addWidget(descLabel,0, Qt::AlignCenter);
        maxillaryRowLayout->addWidget(columnWidget);
    }
    ui->centralwidget->layout()->addWidget(maxillaryRowWidget);


    //Bitewing Row
    QStringList m_bwRowStrings = {
        "BW_Right_Distal",
        "BW_Right_Mesial",
        "Panoramic",
        "FMX_SINGLE_IMAGE",
        "BW_Left_Mesial",
        "BW_Left_Distal",
    };
    QWidget *bwRowWidget = new QWidget();
    QHBoxLayout *bwRowLayout = new QHBoxLayout();
    bwRowWidget->setLayout(bwRowLayout);
    for(int i=0;i<m_bwRowStrings.length();i++) {
        QWidget *columnWidget = new QWidget();
        QVBoxLayout *columnLayout = new QVBoxLayout();

        RadiographLabelPreview *radioLabel = new RadiographLabelPreview();
        radioLabel->setObjectName(m_bwRowStrings[i]);
        m_ImageLabelList.append(radioLabel);

        QString showMe = m_bwRowStrings[i];
        showMe = showMe.replace("_"," ");
        QLabel *descLabel = new QLabel(showMe);


        columnWidget->setLayout(columnLayout);
        columnLayout->addWidget(radioLabel,0, Qt::AlignCenter);
        columnLayout->addWidget(descLabel,0, Qt::AlignCenter);
        bwRowLayout->addWidget(columnWidget);
    }
    ui->centralwidget->layout()->addWidget(bwRowWidget);


    //Mandibular Row
    QStringList m_manRowStrings = {
        "PA_Mandibular_Right_Distal",
        "PA_Mandibular_Right_Mesial",
        "PA_Mandibular_Anterior_Right",
        "PA_Mandibular_Anterior_Center",
        "PA_Mandibular_Anterior_Left",
        "PA_Mandibular_Left_Mesial",
        "PA_Mandibular_Left_Distal",
    };
    QWidget *manRowWidget = new QWidget();
    QHBoxLayout *manRowLayout = new QHBoxLayout();
    manRowWidget->setLayout(manRowLayout);
    for(int i=0;i<m_manRowStrings.length();i++) {
        QWidget *columnWidget = new QWidget();
        QVBoxLayout *columnLayout = new QVBoxLayout();

        RadiographLabelPreview *radioLabel = new RadiographLabelPreview();
        radioLabel->setObjectName(m_manRowStrings[i]);
        m_ImageLabelList.append(radioLabel);

        QString showMe = m_manRowStrings[i];
        showMe = showMe.replace("_"," ");
        QLabel *descLabel = new QLabel(showMe);


        columnWidget->setLayout(columnLayout);
        columnLayout->addWidget(radioLabel,0, Qt::AlignCenter);
        columnLayout->addWidget(descLabel,0, Qt::AlignCenter);
        manRowLayout->addWidget(columnWidget);
    }
    ui->centralwidget->layout()->addWidget(manRowWidget);

    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::SaveAll);
    ui->centralwidget->layout()->addWidget(m_buttonBox);

    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &MainImportRadWindow::handleSaveAll);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &MainImportRadWindow::close);

}

MainImportRadWindow::~MainImportRadWindow()
{
    delete ui;
}

// void MainImportRadWindow::handleReset(QAbstractButton *getButton)
// {
//     qDebug()<<getButton->ro;

// }

void MainImportRadWindow::handleSaveAll()
{
    QString patientID = ui->patientIDBox->currentText();
    QString destURLString = CDFileLocations::getRadiographDir(patientID);
    destURLString += ui->dateEdit->date().toString("MMM/d/yyyy");
    QDir destDir(destURLString);
    if(!destDir.exists()) {
        destDir.mkpath(".");
    }
    foreach(RadiographLabelPreview *label, m_ImageLabelList) {
        if(label->getFileName().length() > 1) {
            QString fileLocation = destURLString+ "/" + label->objectName() + ".png";
            QFile dest(fileLocation);
            int fileCounter = 1;
            while(dest.exists()) {
                fileLocation = destURLString+ "/" + label->objectName() + "-" + QString::number(fileCounter)
                               + ".png";
                dest.setFileName(fileLocation);
                fileCounter++;
            }

            QString scrStr = label->getFileName();
            QImage source(scrStr);
            source.save(fileLocation);

            QString statusMsg = "Imported radiographs from "  + scrStr + " for " + patientID;
            CDGitManager::commitData(statusMsg);
        }
    }
    ui->statusbar->showMessage("Finished importing images",5000);

    m_buttonBox->clear();
    m_buttonBox->addButton(QDialogButtonBox::SaveAll);
    m_buttonBox->addButton(QDialogButtonBox::Close);
}
