// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef MAINIMPORTRADWINDOW_H
#define MAINIMPORTRADWINDOW_H

#include <QMainWindow>
#include <QDialogButtonBox>

#include "radiographlabelpreview.h"

namespace Ui {
class MainImportRadWindow;
}

class MainImportRadWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainImportRadWindow(QWidget *parent = nullptr);
    ~MainImportRadWindow();

public slots:
    //void handleReset(QAbstractButton *getButton);
    void handleSaveAll();

private:
    Ui::MainImportRadWindow *ui;

    QStringList m_maxRowStrings;
    QStringList m_bwRowStrings;
    QStringList m_manRowStrings;

    QList<RadiographLabelPreview*> m_ImageLabelList;
    QDialogButtonBox *m_buttonBox;

};

#endif // MAINIMPORTRADWINDOW_H
