// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details


#ifndef RADIOGRAPHLABELPREVIEW_H
#define RADIOGRAPHLABELPREVIEW_H

#include <QLabel>
#include <QObject>
#include <QWidget>

#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDragLeaveEvent>
#include <QDropEvent>
#include <QMimeDatabase>

class RadiographLabelPreview : public QLabel
{
    Q_OBJECT
public:
    explicit RadiographLabelPreview(QWidget *parent = nullptr);

    QString getFileName();

protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dragLeaveEvent(QDragLeaveEvent *event) override;
    void dropEvent(QDropEvent *event) override;

private:
    QString m_Filename;
    bool isGood(const QMimeData *getMimeData);
    //QMimeDatabase m_mDB;
};

#endif // RADIOGRAPHLABELPREVIEW_H
