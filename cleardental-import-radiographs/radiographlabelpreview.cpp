// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "radiographlabelpreview.h"

#include <QIcon>
#include <QMimeData>
#include <QDebug>
#include <QFileInfo>

RadiographLabelPreview::RadiographLabelPreview(QWidget *parent)
    : QLabel(parent)
{
    setAcceptDrops(true);
    QIcon defaultImage = QIcon::fromTheme("image-x-generic");
    QPixmap defPix = defaultImage.pixmap(128,128);
    setPixmap(defPix);
}

QString RadiographLabelPreview::getFileName()
{
    return m_Filename;
}

void RadiographLabelPreview::dragEnterEvent(QDragEnterEvent *event)
{
    QMimeDatabase mDB;
    const QMimeData *mimeData = event->mimeData();
    if(isGood(mimeData)) {
        event->acceptProposedAction();
    }
}

void RadiographLabelPreview::dragMoveEvent(QDragMoveEvent *event)
{
    event->acceptProposedAction();
}

void RadiographLabelPreview::dragLeaveEvent(QDragLeaveEvent *event)
{
    event->accept();
}

void RadiographLabelPreview::dropEvent(QDropEvent *event)
{
    QMimeDatabase mDB;
    const QMimeData *mimeData = event->mimeData();
    if(isGood(mimeData)) {
        m_Filename = mimeData->text().replace("file://","");
        QPixmap loadImage = QPixmap(m_Filename);
        setPixmap(loadImage.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
        event->acceptProposedAction();
    }
}

bool RadiographLabelPreview::isGood(const QMimeData *getMimeData)
{
    QMimeDatabase mDB;
    if(getMimeData->hasText()) {
        QFileInfo fileInfo(getMimeData->text());
        QMimeType fileMime = mDB.mimeTypeForFile(fileInfo);
        QStringList fileTypeStrings = fileMime.name().split("/"); // "filetype/fileExt"; "image/png"
        QString fileType = fileTypeStrings.at(0);
        //QString fileExt = fileTypeStrings.at(1);
        if(fileType == "image") {
            return true;
        }
    }
    return false;
}
