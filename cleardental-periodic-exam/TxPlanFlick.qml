// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

Flickable {
    id: txPlanFlick

    width: 500
    height: 500
    clip: true
    contentWidth: 500
    contentHeight: fullPane.implicitContentHeight


    property var treatmentPlanObj: ({})
    property string treatmentPlanName:""
    property var phaseNames: Object.keys(treatmentPlanObj)

    CDCommonFunctions {
        id: commonFun
    }

    CDTranslucentPane {
        id: fullPane
        width: 500
        height: 500

        backMaterialColor: Material.Orange

        ColumnLayout {
            id: itemColumn
            anchors.fill: parent
            Label {
                text: treatmentPlanName
                font.pointSize: 24
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
            }
            MenuSeparator {
                Layout.fillWidth: true
            }

            Repeater {
                model: phaseNames.length

                ColumnLayout {
                    Label {
                        text: phaseNames[index]
                        font.bold: true
                    }
                    Label {
                        Component.onCompleted:  {
                            text = "<ul>";
                            var phaseList = treatmentPlanObj[phaseNames[index]];
                            for(var i=0;i<phaseList.length;i++) {
                                var txItemObj = phaseList[i];
                                text += "<li>"+ commonFun.makeTxItemString(txItemObj)
                                        + "</li>";
                            }
                            text += "</ul>";
                        }
                    }
                }
            }
            MenuSeparator {
                Layout.fillWidth: true
            }
            Label {
                text: "Estimated total"
                font.bold:true
            }
            Label {
                Component.onCompleted:  {
                    var total=0;
                    for(var c=0;c<phaseNames.length;c++) {
                        var phaseList = treatmentPlanObj[phaseNames[c]];

                        for(var i=0;i<phaseList.length;i++) {
                            var txItemObj = phaseList[i];
                            if(typeof txItemObj["BasePrice"] !== "undefined") {
                                total += parseInt(txItemObj["BasePrice"]);
                            }
                        }
                    }
                    text = "$" + total + " base cost";
                }
            }

            Label {
                Layout.fillHeight: true
            }
        }
    }

    Component.onCompleted:  {
        //phaseNames = Object.keys(treatmentPlanObj)


    }
}
