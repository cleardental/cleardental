// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Periodontal Follow-up [ID: " + PATIENT_FILE_NAME + "]")



    CDFileLocations {
        id: fLocs
    }


    header: CDPatientToolBar {
        headerText: "Periodontal Follow-up:"
    }


    ColumnLayout {
        anchors.centerIn: parent
        spacing: 20

        CDReviewLauncher {
            id: perioButton
            text: "Periodontal charting"

            launchEnum: CDToolLauncher.PerioCharting
            iniProp: "PerioCharting"
            font.pointSize: 24
        }

        CDReviewLauncher {
            id: perioDxTxButton
            text: "Periodontal Diagnosis and Treatment Planning"
            launchEnum: CDToolLauncher.PerioDxTx
            iniProp: "PerioDxTx"
            font.pointSize: 24
        }
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        CDCommonFunctions {
            id: comFuns
        }

        onClicked: {
            var caseNoteString = "Patient presented for a periodontal follow-up.\n";
            if(perioButton.wasReviewed) {
                caseNoteString += "Periodontal charting updated.\n";
            }

            finFollow.caseNoteString = caseNoteString;
            var completeMe = comFuns.findTx(PATIENT_FILE_NAME,"Periodontal Followup");
            finFollow.txItemsToComplete = [completeMe[0]];
            finFollow.open();
        }
    }

    CDFinishProcedureDialog {
        id: finFollow
    }


}
