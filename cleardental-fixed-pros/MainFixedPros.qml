// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: comFuns.makeTxItemString(procedure_OBJ) + " [ID: " + PATIENT_FILE_NAME + "]"

    property var procedure_OBJ: JSON.parse(PROCEDURE_JSON)

    CDCommonFunctions {
        id: comFuns
    }

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedure_OBJ)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }


    RowLayout {
        anchors.fill: parent
        anchors.margins: 10
        ColumnLayout {
            CDMedReviewPane{Layout.minimumWidth: 900}
            CDReviewRadiographPane{
                CDRadiographManager {
                    id: radioMan
                }

                Layout.minimumWidth: 900
                toothToReview: procedure_OBJ["Tooth"]
            }
        }
        ColumnLayout {
            CDLAPane{
                id: laPane;
                Layout.fillWidth: true
            }
            CDTranslucentPane {
                backMaterialColor: Material.Lime
                Layout.fillWidth: true
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Crown Stage"
                    }
                    ComboBox {
                        id: crownStageBox
                        model: ["Prep","Delivery"]
                        Component.onCompleted: {
                            if(rootWin.procedure_OBJ.hasOwnProperty("Delivery") &&
                                    (rootWin.procedure_OBJ["Delivery"] === true)) {
                                crownStageBox.currentIndex = 1;
                            }
                        }
                    }
                }
            }

            FixedProsPrepAptInfoPane{
                id: fpaPrepPane;
                Layout.fillWidth: true
                visible: crownStageBox.currentIndex == 0
            }
            FixedProsDelAptInfoPane {
                id: fpDelPane
                Layout.fillWidth: true
                visible: crownStageBox.currentIndex == 1
            }


        }
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            finishDialog.caseNoteString= "";
            finishDialog.caseNoteString+=laPane.generateCaseNoteString();
            finishDialog.txItemsToComplete = [rootWin.procedure_OBJ];
            if(fpDelPane.visible) { //delivery
                finishDialog.caseNoteString+=fpDelPane.generateCaseNoteString();

            }
            else { //prep
                finishDialog.caseNoteString+=fpaPrepPane.generateCaseNoteString();
                if(fpaPrepPane.doingCoreBuildup) {
                    finishDialog.txItemsToComplete.push(comFuns.findTxDCode(PATIENT_FILE_NAME,"D2950")[0])
                }
            }

            finishDialog.open();
        }
    }

    CDFinishProcedureDialog {
        id: finishDialog
    }
}
