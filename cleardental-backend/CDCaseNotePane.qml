// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

Pane {
    id: caseNotePane
    background: Rectangle {
        anchors.fill: parent
        radius: 10
        color: Material.color(Material.Lime)
        opacity: .5
    }

    CDFileLocations {
        id: m_locs
    }


    CDTextFileManager {
        id: m_textMan
    }
    Layout.minimumHeight: 200

    ListView {
        id: caseNoteListView
        property var caseNoteList: []
        anchors.fill: parent

        header: CDHeaderLabel {
            text: "Case notes"
            height:  contentHeight + 5
        }
        spacing: 10
        clip: true

        ScrollBar.vertical: ScrollBar { }

        delegate: Rectangle {
            width: caseNoteListView.width
            height: topRow.height + caseNoteArea.height + 20
            radius: 10
            border.color: Material.foreground
            border.width: 1
            antialiasing: true
            color: "transparent"

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 5
                RowLayout {
                    id: topRow
                    Layout.fillWidth: true
                    Label {
                        Component.onCompleted: {
                            var setIndex = caseNoteListView.caseNoteList.length - 1 - index;
                            var dateObj = new Date(caseNoteListView.caseNoteList[setIndex]["DateTime"]);
                            text = dateObj.toLocaleDateString();
                        }
                    }
                    Label {
                        Layout.minimumWidth: 10
                    }
                    Label {
                        CDCommonFunctions {
                            id: m_funs
                        }

                        Component.onCompleted: {
                            var setIndex = caseNoteListView.caseNoteList.length - 1 - index;
                            var procedureObjList=JSON.parse(caseNoteListView.caseNoteList[setIndex]["ProcedureObject"])
                            var setTextArray = [];
                            for(var i=0;i<procedureObjList.length;i++) {
                                setTextArray.push(m_funs.makeTxItemString(procedureObjList[i]));
                            }
                            text = setTextArray.join("; ")
                        }
                    }

                    Label {Layout.fillWidth: true;}
                    Label {
                        text: caseNoteListView.caseNoteList[caseNoteListView.caseNoteList.length -
                                                            1 - index]["Provider"]

                    }
                }
                Label {
                    id: caseNoteArea
                    Layout.fillWidth: true
                    text: caseNoteListView.caseNoteList[caseNoteListView.caseNoteList.length -
                                                        1 - index]["CaseNote"].trim()

                }
            }
        }

        Component.onCompleted: {
            var caseNoteJSON = m_textMan.readFile(m_locs.getCaseNoteFile(PATIENT_FILE_NAME));
            if(caseNoteJSON.length> 1) {
                caseNoteList = JSON.parse(caseNoteJSON);
                caseNoteListView.model = caseNoteList.length
            }

        }

    }
}
