// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: calibrateSensorDialog
    property int whiteCounter: 0
    property int sensorIndex;

    ColumnLayout {
        CDHeaderLabel {
            text: "Calibrate sensor"
        }
        CDTranslucentPane {
            ColumnLayout {
                Label {
                    text: "Ready for exposure #" + (whiteCounter + 1)
                    visible:  whiteCounter < 3
                }
                CDAddButton {
                    text: "Start"
                    onClicked: {
                        for(whiteCounter=0;whiteCounter<3;whiteCounter++) {
                            console.debug("Calibrating @" + whiteCounter)
                            sensInfo.takeCalibrationExposure(sensorIndex,whiteCounter,3);
                        }
                    }
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: calibrateSensorDialog.close();
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                id: doneButton
                text: "Commit Calibriation"
                enabled: whiteCounter > 3
            }
        }
    }

    onVisibleChanged:  {
        if(visible) {
            //            for(whiteCounter=0;whiteCounter<3;whiteCounter++) {
            //                //sensInfo.takeCalibrationExposure(sensorIndex,whiteCounter,3);
            //            }
        }
    }

}
