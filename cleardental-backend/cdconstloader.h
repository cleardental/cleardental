// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDCONSTLOADER_H
#define CDCONSTLOADER_H

#include <QObject>

class CDConstLoader : public QObject
{
    Q_OBJECT
public:
    explicit CDConstLoader(QObject *parent = nullptr);

    Q_INVOKABLE static QString getDCodesJSON();
    Q_INVOKABLE static QString getUSDentalPlanListJSON();
    Q_INVOKABLE static QString getCommonPatTranslationsJSON();

    Q_INVOKABLE static QString getNodeType();
    Q_INVOKABLE static bool isDebugMode();
    Q_INVOKABLE static bool goTransparent();

signals:

};

#endif // CDCONSTLOADER_H
