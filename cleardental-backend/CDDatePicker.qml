// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

RowLayout {
    id: dateRow

    property bool setToday: true

    function getDate() {
        var month = monthBox.currentIndex+ 1;
        var day = dayBox.currentText;
        var year = yearBox.currentText;
        return month + "/" + day + "/" + year; //US standard, I know...
    }

    function setDate(newDate) {
        if(newDate.length > 3)  {
            var partsOfDay = newDate.split("/");
            monthBox.currentIndex = partsOfDay[0] -1;
            dayBox.currentIndex = dayBox.find(partsOfDay[1]);
            yearBox.currentIndex = yearBox.find(partsOfDay[2]);
            setToday = false;
        }
    }

    function setDateObj(newDateObj) {
        yearBox.currentIndex = newDateObj.getFullYear() - 1900;
        monthBox.currentIndex = newDateObj.getMonth();
        dayBox.currentIndex = newDateObj.getDate() -1;
        setToday = false;
    }

    function getDateObj() {
        var returnMe = new Date();
        returnMe.setFullYear(yearBox.currentIndex + 1900);
        returnMe.setMonth(monthBox.currentIndex);
        returnMe.setDate(dayBox.currentIndex +1);
        return returnMe;
    }

    function getPHPStyleDate() { //YYYY-MM-DD
        var month = monthBox.currentIndex+ 1;
        var day = dayBox.currentText;
        var year = yearBox.currentText;
        return year + "-" + month + "-" + day;
    }

    ComboBox {
        id: monthBox
        Layout.minimumWidth: 140
        model: ['January (1)', 'February (2)', 'March (3)', 'April (4)', 'May (5)', 'June (6)',
            'July (7)', 'August (8)', 'September (9)', 'October (10)', 'November (11)', 'December (12)'];
    }

    ComboBox {
        id: dayBox
        width: 75
        model:  {
            var returnMe= [];
            for(var i=1;i<33;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
    }

    ComboBox {
        id: yearBox
        width: 85
        model:  {
            var returnMe= [];
            for(var i=1900;i<2050;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
    }

    Component.onCompleted: {
        if(setToday) {
            var d = new Date(); //now
            yearBox.currentIndex = d.getFullYear() - 1900;
            monthBox.currentIndex = d.getMonth();
            dayBox.currentIndex = d.getDate() -1;
        }
    }

}
