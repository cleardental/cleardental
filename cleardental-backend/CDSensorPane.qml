// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: sensorPane
    property var locationList: []
    property int listCounter: 0
    property var sensorButtonList: []
    property bool doneTakingRadiographs: false

    function refreshSensorList() {
        while(sensorButtonList.length > 0) {
            sensorButtonList.pop().destroy();
        }

        var senList = sensInfo.getSensorList();
        for(var i=0;i<senList.length;i++) {
            var hasCal  = sensInfo.hasCalibration(senList[i]["SerialNumber"]);

            var newBut = sensorRadioButton.createObject(sensorListCol, {sensorID: senList[i]["SerialNumber"] ,
                                                            sensorSize: senList[i]["SensorSize"],
                                                        hasCalibration: hasCal,
                                                        sensorIndex: i})
            sensorButtonList.push(newBut);
            if(i=== 0) {
                newBut.setChecked = true;
            }
        }
        if(senList.length > 0) {
            sensorConnectLabel.visible = false;
            var readyItem = readyButton.createObject(sensorListCol);
            sensorButtonList.push(readyItem);
        }
    }

    function prepareForTooth(getTooth) {
        locationList = sensInfo.getQMLTypesFromToothNumber(getTooth);
    }

    ColumnLayout {
        id: sensorListCol
        spacing: 50

        CDRadiographAudioCue {
            id: audioCue
        }

        CDHeaderLabel {
            id: sensorConnectLabel
            text: "Please connect sensor"
        }
        Button {
            text: "Check for new sensors"
            icon.name: "view-refresh"
            icon.width: 32
            icon.height: 32
            onClicked: {
                refreshSensorList();
            }
        }
        Button {
            text: "Take Fake Images"
            Material.background: Material.Indigo
            font.pointSize: 18
            visible: sensInfo.allowFakeExposures();
            onClicked: {
                for(var i=0;i<locationList.length;i++) {
                    sensInfo.getFakeExposure(PATIENT_FILE_NAME,locationList[i]);
                    doneTakingRadiographs = true;
                }
            }
        }
    }

    Component {
        id: sensorRadioButton

        RowLayout {
            property string sensorID: ""
            property string sensorSize: ""
            property bool setChecked: false;
            property bool hasCalibration: false
            property int sensorIndex:0;
            RadioButton {
                id: radButton
                property int sensorIndex: parseInt(sensorIndex);
                text: "Sensor " + sensorID + " (Size " + sensorSize + ")"
                checked: setChecked
                Component.onCompleted:  {
                    radioButGroup.buttons.push(radButton);
                }
            }

//            CDCalibrateSensorDialog {
//                id: calSensorDia
//            }

//            Button {
//                text: hasCalibration ? "Re-Calibrate Sensor" : "Calibrate Sensor"
//                highlighted: true
//                Material.accent: hasCalibration ? Material.Green : Material.Red
//                onClicked: {
//                    calSensorDia.sensorIndex = sensorIndex;
//                    calSensorDia.open();
//                }
//            }
        }
    }

    ButtonGroup { //makes sure that only one radiobutton is selected
        id: radioButGroup
    }

    Component { //can't put in it directly without confusing ColumnLayout
        id: readyButton
        RowLayout {
            CDCancelButton {
                text: "Skip"
                onClicked: {
                    listCounter++;
                    if(locationList.length === listCounter) {
                        doneTakingRadiographs = true;
                    }
                }
            }
            Label {
                Layout.fillWidth: true;
            }
            Button {
                text: "Take Image: " + sensInfo.getUserStringFromType(locationList[listCounter]);
                highlighted: true
                Material.accent: Material.LightGreen
                icon.name: "kstars_satellites"
                icon.width: 64
                icon.height: 64
                font.pointSize: 64
                onClicked: {
                    okTheImaDia.open();
                }
                visible: listCounter < locationList.length
            }
        }
    }

    CDRadiographSensor {
        id: sensInfo

        onSensorStatusUpdated: {
            promptText.updateText(newStatus);
        }
    }

    CDTranslucentDialog {
        id: calibrateSensorDialog
        ColumnLayout {
            Image {
                id: resultImage
                Layout.maximumWidth: 512
                Layout.minimumWidth: 512

            }

            Button {
                Layout.alignment: Qt.AlignHCenter
                text: "Cancel"
                icon.name: "dialog-cancel"
                icon.width: 64
                icon.height: 64
                highlighted: true
                Material.accent: Material.Red
            }
        }

        parent: rootWin.contentItem
        modal: true
        anchors.centerIn: parent
    }

    CDTranslucentDialog {
        id: okTheImaDia

        property string canSRC: ""
        closePolicy: Popup.NoAutoClose

        ColumnLayout {
            Image {
                id: candiateImage
                Layout.maximumWidth: 1024
                Layout.maximumHeight: 768
                fillMode: Image.PreserveAspectFit
            }

            CDHeaderLabel {
                id: promptText
                function updateText(newEnum) {
                    resultRowLayout.visible = false;
                    if(newEnum === CDRadiographSensor.NOT_STARTED) {
                        text = "Starting up"
                    }
                    else if(newEnum === CDRadiographSensor.CALIBRATING) {
                        text = "Calibrating"
                    }
                    else if(newEnum === CDRadiographSensor.SENDING_REQUEST) {
                        text = "Requesting to get ready"
                    }
                    else if(newEnum === CDRadiographSensor.WAITING_FOR_RESULT) {
                        text = "Please take image"
                    }
                    else if(newEnum === CDRadiographSensor.READING_RESULT) {
                        text = "Downloading image"
                    }
                    else if(newEnum === CDRadiographSensor.PROCESSING_RESULT) {
                        text = "Processing the image"
                    }
                    else if(newEnum === CDRadiographSensor.AWAITING_USER_INPUT) {
                        text = "Please select to accept or reject"
                        resultRowLayout.visible = true;
                        candiateImage.source = "";
                        candiateImage.source = "file://" + sensInfo.tempRadiographLocation();
                    }
                    else if(newEnum === CDRadiographSensor.SAVING) {
                        text = "Saving image"
                    }
                    else if(newEnum === CDRadiographSensor.DONE) {
                        text = "Done"
                        candiateImage.source = ""; //doesn't show old image when taking a new one
                    }
                    else if(newEnum === CDRadiographSensor.CANCELLED) {
                        text = "Cancelled"
                    }
                }

            }

            RowLayout {
                id: resultRowLayout
                visible: false
                CDCancelButton {
                    focus: true
                    text: "No good, take again"

                    function rejectRadio() {
                        sensInfo.rejectExposure(PATIENT_FILE_NAME, locationList[listCounter]);
                        candiateImage.source = "";
                        okTheImaDia.close();
                    }

                    onClicked: {
                        rejectRadio();
                    }
                }
                Label {
                    Layout.fillWidth: true
                }
                CDAddButton {
                    text: "Good, move on"

                    function acceptRadio() {
                        sensInfo.saveExposure(PATIENT_FILE_NAME, locationList[listCounter]);
                        listCounter++;
                        if(locationList.length === listCounter) {
                            doneTakingRadiographs = true;
                        }

                        okTheImaDia.close();

                    }

                    onClicked: {
                        acceptRadio();
                    }
                }
            }
        }

        onVisibleChanged: {
            if(visible) {
                audioCue.playReady(locationList[listCounter]);
                sensInfo.requestExposure(radioButGroup.checkedButton.sensorIndex,locationList[listCounter]);
            }
        }
    }

    ApplicationWindow {
        x: 0
        y: 0
        width: 1920
        height: 1080
        visible: okTheImaDia.visible && (Qt.application.screens.length > 2)
        color: "transparent"
        Image {
            source: candiateImage.source
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }

    ApplicationWindow {
        x: 0
        y: 1080
        width: 1920
        height: 1080
        color: "transparent"
        visible: okTheImaDia.visible && (Qt.application.screens.length > 2)

        Image {
            source: candiateImage.source
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }



    Component.onCompleted: {
        refreshSensorList();
    }

}
