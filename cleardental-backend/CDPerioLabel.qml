// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

Label {
    property string perioVal: "_"

    text:  perioVal

    function unSetVal() {
            perioVal = "_";
    }
    function isSet() {
        return !(perioVal === "_");
    }

    function combineVals(a,b) {
        if((a === "😊") || (b==="😊")) {
            perioVal = "😊";
        }

        if((a === "_") && (b==="_")) {
            perioVal= "_"
        } else if(a === "_") {
            perioVal= b;
        }else if(b === "_") {
            perioVal= a;
        }else {
            perioVal= parseInt(a)+parseInt(b);
        }
    }

    onPerioValChanged: {
        if((perioVal === "_") || (perioVal === "😊")) {
            color = Material.foreground
        }
        else {
            var intVal = parseInt(perioVal);
            if(intVal < 3) {
                color = Material.color(Material.LightGreen)
            }
            else if(intVal < 4) {
                color = Material.color(Material.Green)
            }
            else if(intVal < 6) {
                color = Material.color(Material.Orange)
            }
            else {
                color = Material.color(Material.Red)
            }
        }
    }
}
