// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdpatientreminder.h"
#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cdtextfilemanager.h"
#include "cdschedulemanager.h"
#include "cdgitmanager.h"

CDPatientReminder::CDPatientReminder()
{
    connect(&manager, &QNetworkAccessManager::authenticationRequired, this, &CDPatientReminder::authenticationRequired);
    connect(&manager, &QNetworkAccessManager::encrypted, this, &CDPatientReminder::encrypted);
    connect(&manager, &QNetworkAccessManager::preSharedKeyAuthenticationRequired, this, &CDPatientReminder::preSharedKeyAuthenticationRequired);
    connect(&manager, &QNetworkAccessManager::proxyAuthenticationRequired, this, &CDPatientReminder::proxyAuthenticationRequired);
    connect(&manager, &QNetworkAccessManager::sslErrors, this, &CDPatientReminder::sslErrors);
}

void CDPatientReminder::get(QString location)
{
    qDebug() << "Getting from server...";
    QNetworkReply* reply = manager.get(QNetworkRequest(QUrl(location)));
    connect(reply, &QNetworkReply::readyRead, this, &CDPatientReminder::readyRead);
}

void CDPatientReminder::post(QString location, QByteArray data)
{
    QNetworkRequest request = QNetworkRequest(QUrl(location));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"test/html");

    QNetworkReply* reply = manager.post(request,data);
    connect(reply,&QNetworkReply::readyRead,this,&CDPatientReminder::readyRead);
}

void CDPatientReminder::readyRead()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    CDPatientManager patMan;
    CDTextfileManager textMan;
    CDFileLocations fileLocs;
    if(reply) {
        QByteArray replyData = reply->readAll();
        qDebug() << replyData;

        QJsonObject replyObj = QJsonDocument::fromJson(replyData).object();
        QJsonArray msgsArray = replyObj.value("sms").toArray();      // Contains info about recieved messages when getting SMS; contains an id number if sending SMS

        foreach (const QJsonValue & smsValue, msgsArray) {     // For each message recieved today
            QJsonObject smsInfo = smsValue.toObject();
            QString phoneNumber = smsInfo.value("contact").toString();
            QList<QVariant> patients = patMan.getAllPatients();
            QVariantMap currPat;
            int patI = 0;
            while (patI < patients.length()) {  // Find patient who sent the message
                QString currCell = "";
                QString currHome = "";
                QSettings personalFile(fileLocs.getPersonalFile(patients[patI].toMap().value("PatID").toString()), QSettings::IniFormat);
                QString originalCell = personalFile.value("Phones/CellPhone").toString();
                QString originalHome = personalFile.value("Phones/HomePhone").toString();
                for (int idx = 0; idx < originalCell.length(); idx++) {     // Remove non-numerical chars from phone number
                    if (originalCell.at(idx) <= '9' && originalCell.at(idx) >= '0')
                        currCell += originalCell.at(idx);
                }
                for (int idx = 0; idx < originalHome.length(); idx++) {
                    if (originalHome.at(idx) <= '9' && originalHome.at(idx) >= '0')
                        currHome += originalHome.at(idx);
                }
                if (currCell == phoneNumber || currHome == phoneNumber) {   // Patient found
                    currPat = patients[patI].toMap();
                    QString patName = "";
                    if (currPat.value("PreferredName").toString().length() > 0)
                        patName += currPat.value("PreferredName").toString();
                    else
                        patName += currPat.value("FirstName").toString();
                    patName += " " + currPat.value("LastName").toString() + " (" + phoneNumber + ")";

                    QString patID = patients[patI].toMap().value("PatID").toString();

                    QString msgJSON = textMan.readFile(fileLocs.getPatientMessagesFile(patID));
                    QJsonArray patMessages = QJsonDocument::fromJson(msgJSON.toUtf8()).array();

                    int msgI = patMessages.size();
                    QVariantMap prevText;
                    QDateTime prevTime;
                    QDateTime smsDate = QDateTime::fromString(smsInfo.value("date").toString(), Qt::ISODate);
                    bool exists = false;
                    while (msgI > 0 && !exists) {
                        msgI--;         // Find The most recent text they sent
                        prevText = patMessages[msgI].toObject().toVariantMap();
                        prevTime = prevText.value("Time").toDateTime();

                        if (prevTime == smsDate)
                            exists = true;
                    }

                    if (!exists) {
                        QVariantList msgList = patMessages.toVariantList();
                        QString message = smsInfo.value("message").toString();

                        QVariantMap addMe;
                        addMe.insert("Time", smsInfo.value("date"));
                        addMe.insert("Sender", patName);
                        addMe.insert("Reciever", "Us (5083873733)");
                        addMe.insert("Contents", message);

                        msgList.append(addMe);
                        QString addMeJson(QJsonDocument::fromVariant(msgList).toJson(QJsonDocument::Indented));

                        if(message == "STOP") {
                            personalFile.setValue("Phones/DoNotText",true);
                        }

                        textMan.saveFile(fileLocs.getPatientMessagesFile(patID), addMeJson);

                        QDate apptDate = prevText.value("ApptTime").toDate();

                        CDScheduleManager scheduleDB;
                        QStringList appts = scheduleDB.getAppointments(apptDate);

                        QDate currDate = QDate::currentDate(); // current date
                        QStringList patAppts = scheduleDB.getAppointments(patID);
                        QDate targetAppt = QDate(); // target date (initialized as null)
                        bool noShow = false;

                        int rescheduleDeadlineDays = 1; // number of days a patient must reschedule in advance in order to avoid a "No Show"
                        /**Example:
                         *
                         * A patient's appointment is Wednesday
                         * if the rescheduleDealineDays is 1,
                         * then the patient has until the end of the day Monday to reschedule.
                         * If they try to reschedule Tuesday or Wednesday, then it will be considered a No Show.
                         */

                        // find next appointment
                        for (int apptI = 0; apptI < patAppts.length(); apptI++) {
                            // parse path string to date object
                            QString currApptPathString = patAppts[apptI];
                            QString currApptFileString = currApptPathString.section("/", -1, -1);
                            QString currApptString = currApptFileString.split("@")[0];
                            QDate currAppt = QDate::fromString(currApptString, "MMM-d-yyyy");

                            // check if appointment is beyond today
                            if(currAppt > currDate) { // if the current appointment is later than the current date
                                if ((currAppt.daysTo(currDate)) <= rescheduleDeadlineDays) {
                                    // current appointment is within a day from today -> NO SHOW
                                    noShow = true;
                                }
                                // current appointment is beyond a day from today -> RESCHEDULE
                                targetAppt = currAppt;
                                break;
                            }
                        }

                        if (!targetAppt.isNull()) { // next appointment found
                            QStringList targetDayAppts = scheduleDB.getAppointments(targetAppt);
                            for (int i=0; i < targetDayAppts.length(); i++) {
                                QSettings apptFile(targetDayAppts[i],QSettings::IniFormat);
                                if (apptFile.value("PatientID", "").toString() == patID) {
                                    if (message.contains("YES", Qt::CaseInsensitive) ||
                                            message.contains("YEAH", Qt::CaseInsensitive) ||
                                            message.contains("YUP", Qt::CaseInsensitive) ||
                                            message.contains("I'LL BE THERE", Qt::CaseInsensitive) ||
                                            message.contains("👍", Qt::CaseInsensitive) ||
                                            message.contains("SURE", Qt::CaseInsensitive) ||
                                            message.contains("SIM", Qt::CaseInsensitive) ||
                                            message.contains("SI", Qt::CaseInsensitive) ||
                                            message.contains("SÍ", Qt::CaseInsensitive)) {
                                        apptFile.setValue("Status", "Confirmed");
                                    } else if (message.contains("RESCHEDULE", Qt::CaseInsensitive) ||
                                               message.contains("REAGENDAR", Qt::CaseInsensitive) ||
                                               message.contains("REPROGRAMAR", Qt::CaseInsensitive) ||
                                               message.contains("CAMBIAR LA FECHA", Qt::CaseInsensitive) ||
                                               message.contains("CAMBIAR LA CITA", Qt::CaseInsensitive)) {
                                        if (noShow) {
                                            apptFile.setValue("Status", "No Show");
                                            apptFile.setValue("Chair", "Waitlist");
                                        } else {
                                            apptFile.setValue("Status", "Wants to Reschedule");
                                            apptFile.setValue("Chair", "Waitlist");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                patI++;
            }
        }
    }

    CDGitManager gitMan;
    gitMan.commitData("Recieved texts new text messages");
}

void CDPatientReminder::authenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator)
{
    Q_UNUSED(reply);
    Q_UNUSED(authenticator);
    qDebug() << "authenticationRequired";

}

void CDPatientReminder::encrypted(QNetworkReply *reply)
{
    Q_UNUSED(reply);
}

void CDPatientReminder::finished(QNetworkReply *reply)
{
    Q_UNUSED(reply);
    qDebug() << "finished";
}


void CDPatientReminder::preSharedKeyAuthenticationRequired(QNetworkReply *reply, QSslPreSharedKeyAuthenticator *authenticator)
{
    Q_UNUSED(reply);
    Q_UNUSED(authenticator);
    qDebug() << "preSharedKeyAuthenticationRequired";
}

void CDPatientReminder::proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator)
{
    Q_UNUSED(proxy);
    Q_UNUSED(authenticator);
    qDebug() << "proxyAuthenticationRequired";
}

void CDPatientReminder::sslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
    Q_UNUSED(reply);
    Q_UNUSED(errors);
    qDebug() << "sslErrors";
}
