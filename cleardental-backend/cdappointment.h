// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDAPPOINTMENT_H
#define CDAPPOINTMENT_H

#include <QObject>
#include <QDateTime>

class CDAppointment : public QObject
{
    Q_OBJECT

public:
    explicit CDAppointment(QObject *parent = nullptr);

    enum AppointmentStatus {
        Invalid,
        Blocker,
        Unconfirmed,
        Confirmed,
        Late,
        Here,
        Seated,
        CheckingOut,
        Left,
        NoShow
    };
    Q_ENUMS(AppointmentStatus)

    Q_PROPERTY(QDateTime appointmentStartTime MEMBER m_appointmentStartTime
               READ appointmentStartTime WRITE setAppointmentStartTime)
    Q_PROPERTY(QDateTime appointmentEndTime MEMBER m_appointmentEndTime
               READ appointmentEndTime WRITE setAppointmentEndTime)
    Q_PROPERTY(QString chair MEMBER m_chair READ chair WRITE setChair)
    Q_PROPERTY(QString patientID MEMBER m_patientID
               READ patientID WRITE setPatientID)
    Q_PROPERTY(QString appointmentComment MEMBER m_appointmentComment
               READ appointmentComment WRITE setAppointmentComment)
    Q_PROPERTY(AppointmentStatus appointmentStatus MEMBER m_appointmentStatus
               READ appointmentStatus WRITE setAppointmentStatus)
    Q_PROPERTY(QString procedureName MEMBER m_procedureName
               READ procedureName WRITE setProcedureName)
    Q_PROPERTY(QString doctorID MEMBER m_doctorID
               READ doctorID WRITE setDoctorID)
    Q_PROPERTY(QString filePath MEMBER m_filePath
               READ filePath WRITE setFilePath)

    QDateTime appointmentStartTime() const;
    QString chair() const;
    QString patientID() const;
    QString appointmentComment() const;
    AppointmentStatus appointmentStatus() const;
    QDateTime appointmentEndTime() const;
    QString procedureName() const;
    QString doctorID() const;
    QString filePath() const;

signals:

public slots:
    void setAppointmentStartTime(QDateTime appointmentStartTime);
    void setChair(QString chair);
    void setPatientID(QString patientID);
    void setAppointmentComment(QString appointmentComment);
    void setAppointmentStatus(AppointmentStatus appointmentStatus);
    void setAppointmentEndTime(QDateTime appointmentEndTime);
    void setProcedureName(QString procedureName);
    void setDoctorID(QString doctorID);
    void setFilePath(QString filePath);

private:
    QDateTime m_appointmentStartTime;
    QString m_chair;
    QString m_patientID;
    QString m_appointmentComment;
    AppointmentStatus m_appointmentStatus;
    QDateTime m_appointmentEndTime;
    QString m_procedureName;
    QString m_doctorID;
    QString m_filePath;
};

#endif // CDAPPOINTMENT_H
