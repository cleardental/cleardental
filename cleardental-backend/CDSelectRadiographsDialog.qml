// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: selectRadioDia

    property alias showBad: showAllBad.checked
    property string patID: ""
    property var selectedList: []

    property var showRadioList: []

    property var allRadioButtons: []

    CDRadiographManager {
        id: radioMan
    }

    CDFileLocations {
        id: m_fLocs;
    }

    function updateList() {
        showRadioList =  radioMan.getRadiographsForTooth(patID,-1,showBad)
    }

    onPatIDChanged: updateList();
    onShowBadChanged:  updateList();


    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                RowLayout {
                    CDHeaderLabel {
                        text: "Select Radiographs"
                    }

                    CheckBox {
                        id: showAllBad
                        text: "Show Bad Radiographs"
                        checked: false;

                        onCheckedChanged: {
                            selectRadioDia.updateList();
                        }

                    }
                }
                Flickable {
                    contentHeight: 320
                    contentWidth:  (320*showRadioList.length) +10
                    clip: true
                    Layout.fillWidth: true
                    Layout.minimumHeight: 320
                    Layout.minimumWidth: 1024

                    ScrollBar.horizontal: ScrollBar{}

                    RowLayout {
                        Repeater {
                            model: showRadioList.length
                            Image {
                                source: "file://" + showRadioList[index]
                                Layout.maximumWidth: 320
                                Layout.maximumHeight: 320
                                fillMode: Image.PreserveAspectCrop
                                mipmap: true

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        radioDia.open();
                                    }
                                }

                                CheckBox {
                                    id: radioLabel
                                    anchors.centerIn: parent
                                    font.pointSize: 16

                                    property string originalFilename: showRadioList[index]

                                    background: Rectangle {
                                        opacity: 0.5
                                        color: "white"
                                    }

                                    function updateText() {
                                        var scr = showRadioList[index]
                                        var filename = scr.substr(scr.lastIndexOf("/") + 1);

                                        var fileSee = filename.substr(0,filename.lastIndexOf("."));

                                        var fileUser = fileSee.replace(/_/g, " ");
                                        if(fileUser.endsWith(".png")) { //Because it was a "bad" radiograph
                                            fileUser = fileUser.replace(".png" , " (bad)");
                                        }

                                        var radioDir = m_fLocs.getRadiographDir(patID);
                                        var dateLong = scr.replace(radioDir,"");
                                        var dater = dateLong.replace( "/"  + filename,"");

                                        text = fileUser + "\n" + dater;
                                    }

                                    Component.onCompleted: {
                                        updateText();
                                        allRadioButtons.push(radioLabel);
                                    }
                                }

                                Dialog {
                                    id: radioDia
                                    parent: Overlay.overlay
                                    width: rootWin.width
                                    height: rootWin.height

                                    background: Rectangle {
                                        anchors.fill: parent
                                        color: "black"
                                        opacity: .8
                                    }

                                    property var screenList: [];

                                    onVisibleChanged: {
                                        if(visible) {
                                            screenList = [];
                                            for(var i=1;i<Qt.application.screens.length;i++) {
                                                var res = radioWindowComp.createObject();
                                                res.x=0;
                                                res.y=1080*(i-1);
                                                screenList.push(res);
                                            }
                                        }
                                        else {
                                            while(screenList.length > 0) {
                                                screenList.pop().destroy();
                                            }
                                        }
                                    }

                                    Image {
                                        id: rawRadio
                                        source: "file://" + showRadioList[index]
                                        anchors.fill: parent
                                        fillMode: Image.PreserveAspectFit
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                radioDia.close();
                                            }
                                        }
                                    }

                                    ShaderEffect {
                                        id: sharpenShader
                                        width: rawRadio.paintedWidth
                                        height: rawRadio.paintedHeight
                                        anchors.centerIn: parent
                                        property variant src: rawRadio
                                        property real sharpenPower: 10
                                        property real setRenderWidth: rawRadio.paintedWidth
                                        property real setRenderHeight: rawRadio.paintedHeight
                                        onStatusChanged: {
                                            if(status == ShaderEffect.Error) {
                                                sharpenShader.visible = false;
                                            }
                                        }

                                        SequentialAnimation {
                                            PropertyAnimation {
                                                target: sharpenShader
                                                property: "sharpenPower"
                                                from: 0
                                                to: 2
                                                duration: 2000
                                                easing.type: Easing.InOutQuad
                                            }
                                            PropertyAnimation {
                                                target: sharpenShader
                                                property: "sharpenPower"
                                                from: 2
                                                to: 0
                                                duration: 2000
                                                easing.type: Easing.InOutQuad
                                            }
                                            loops: Animation.Infinite
                                            running: true
                                            Component.onCompleted: {
                                                start();
                                            }
                                        }
                                        vertexShader: "
                                                    uniform highp mat4 qt_Matrix;
                                                    attribute highp vec4 qt_Vertex;
                                                    attribute highp vec2 qt_MultiTexCoord0;
                                                    varying highp vec2 coord;
                                                    void main() {
                                                        coord = qt_MultiTexCoord0;
                                                        gl_Position = qt_Matrix * qt_Vertex;
                                                    }"
                                        fragmentShader: "
                                                    varying highp vec2 coord;
                                                    uniform sampler2D src;
                                                    uniform lowp float qt_Opacity;
                                                    uniform lowp float sharpenPower;
                                                    uniform lowp float setRenderWidth;
                                                    uniform lowp float setRenderHeight;

                                                    vec4 sharpen(in sampler2D tex, in vec2 coords, in vec2 renderSize) {
                                                      float dx = 1.0 / renderSize.x;
                                                      float dy = 1.0 / renderSize.y;
                                                      vec4 sum = vec4(0.0);
                                                      sum += -1. * sharpenPower  * texture2D(tex, coords + vec2( -1.0 * dx , 0.0 * dy));
                                                      sum += -1. * sharpenPower* texture2D(tex, coords + vec2( 0.0 * dx , -1.0 * dy));
                                                      sum += ((sharpenPower*4.)+1.) * texture2D(tex, coords + vec2( 0.0 * dx , 0.0 * dy));
                                                      sum += -1. * sharpenPower * texture2D(tex, coords + vec2( 0.0 * dx , 1.0 * dy));
                                                      sum += -1. * sharpenPower* texture2D(tex, coords + vec2( 1.0 * dx , 0.0 * dy));
                                                      return sum;
                                                    }
                                                    void main() {
                                                        lowp vec4 tex = texture2D(src, coord);
                                                        gl_FragColor = sharpen(src,coord,vec2(setRenderWidth,setRenderHeight));
                                                    }"
                                    }

                                    Label {
                                        id: headerLabelForRadio
                                        text: radioLabel.text
                                        anchors.bottom: parent.bottom
                                        anchors.right: parent.right
                                        anchors.margins: 10
                                        color: Material.background
                                        style: Text.Outline
                                        styleColor: Material.foreground
                                        font.pointSize: 32
                                        onVisibleChanged: {
                                            if(visible) {
                                                fadeOutLabel.start();
                                            }
                                            else {
                                                opacity = 1;
                                            }
                                        }

                                        PropertyAnimation  {
                                            id: fadeOutLabel
                                            target: headerLabelForRadio
                                            property: "opacity"
                                            from: 1
                                            to: 0
                                            duration: 10000
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: selectRadioDia.reject();
            }
            Label {
                Layout.fillWidth: true
            }

            CDSaveAndCloseButton {
                text: "Attach"
                icon.name: "mail-attachment"

                onClicked: {
                    selectedList = []
                    for(var i=0;i<allRadioButtons.length;i++) {
                        selectedList.push(allRadioButtons[i].originalFilename);
                    }

                    selectRadioDia.accept();
                }

            }
        }
    }


}
