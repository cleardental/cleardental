// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSPEECHTOTEXT_H
#define CDSPEECHTOTEXT_H

#include <QObject>
#include <QProcess>
#include <QFile>

#include "cdfilewatcher.h"

class CDSpeechToText : public QObject
{
    Q_OBJECT
public:
    explicit CDSpeechToText(QObject *parent = nullptr);

signals:
    void newPerioTrio(int a, int b, int c);


public slots:
    void listenForPerio();
    void listenForHardTissueChart();
    void stopListeningForPerio();

private slots:
    void handleNewPerioInput();

private:
    void parsePerioLine(QStringList getLine);
    CDFileWatcher *m_watcher;
};

#endif // CDSPEECHTOTEXT_H
