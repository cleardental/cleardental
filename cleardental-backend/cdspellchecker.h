// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSPELLCHECKER_H
#define CDSPELLCHECKER_H

#include <QObject>
#include <QQuickTextDocument>

#include <hunspell/hunspell.hxx>
#include "cdsyntaxhighlighter.h"

class CDSpellChecker : public QObject
{
    Q_OBJECT
public:
    explicit CDSpellChecker(QObject *parent = nullptr);

    Q_INVOKABLE static QStringList suggest(QString input);
    Q_INVOKABLE static void injectSyntaxHigh(QQuickTextDocument *getDoc);

private:
    static QTextDocument *m_textDoc;
    static CDSyntaxHighlighter *m_highlighter;


};

#endif // CDSPELLCHECKER_H
