// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDELIGIBILITYCHECKER_H
#define CDELIGIBILITYCHECKER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class CDEligibilityChecker : public QObject
{
    Q_OBJECT
public:
    explicit CDEligibilityChecker(QObject *parent = nullptr);

    QString getPatID();
    void setPatID(QString getPatID);
    Q_PROPERTY(QString patID READ getPatID WRITE setPatID)

    enum EligibilityBackend {
        BACKEND_NOT_SET,
        DENTALXCHANGE_BACKEND,
        STEDI_BACKEND
    };
    Q_ENUM(EligibilityBackend)



    enum EligibilityResult {
        NOT_CHECKED_YET,                    //Didn't check yet
        NOT_CHECKED_INVALID_PLAN,           //Didn't check because it's an invalid plan
        ERROR_WITH_SERVICE,                 //Couldn't connect with the 3rd party server
        PAYER_NOT_SUPPORTED,                //Service doesn't support that payer
        VALID_DENTAL_PLAN_ACTIVE,           //Patient has a valid dental plan and it is active
        VALID_DENTAL_PLAN_INACTIVE,         //Patient has a valid dental plan BUT it is inactive
        INVALID_DENTAL_PLAN,                //Patient has an invalid dental plan
    };
    Q_ENUM(EligibilityResult)

    Q_PROPERTY(EligibilityResult requestResult READ getRequestResult)
    EligibilityResult getRequestResult();

public slots:
    void checkEligibility();
    bool hasEligibilityReport();
    QString eligibilityReportJSON();

signals:
    void gotEligibilityResult(EligibilityResult newResult);



private:
    void handleReadyRead();
    void handleError();

    QString m_patID;
    QString m_checkResultString;
    EligibilityResult m_result;
    QNetworkAccessManager m_networkAccessManager;
    QNetworkRequest m_Request;
    QNetworkReply *m_Reply;



};

#endif // CDELIGIBILITYCHECKER_H
