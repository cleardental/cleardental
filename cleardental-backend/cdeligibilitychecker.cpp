// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdeligibilitychecker.h"

#include <QDebug>
#include <QJsonDocument>
#include <QSettings>
#include <QFile>

#include "cdfilelocations.h"
#include "cdtextfilemanager.h"

#define DENTALXCHANGE_URL "https://api.dentalxchange.com/eligibility"
#define STEDI_URL "https://healthcare.us.stedi.com/2024-04-01/change/medicalnetwork/eligibility/v3"

CDEligibilityChecker::CDEligibilityChecker(QObject *parent)
    : QObject{parent}
{
    m_networkAccessManager.setParent(this);
    m_patID ="";
    m_result = NOT_CHECKED_YET;
}

QString CDEligibilityChecker::getPatID()
{
    return m_patID;
}

void CDEligibilityChecker::setPatID(QString getPatID)
{
    m_patID = getPatID;
}

void CDEligibilityChecker::checkEligibility()
{
    QSettings insInfoSettings(CDFileLocations::getDentalPlansFile(m_patID),QSettings::IniFormat);
    insInfoSettings.beginGroup("Primary");
    QString dentalPlanName = insInfoSettings.value("Name","").toString().trimmed();
    QSettings pracInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    pracInfoSettings.beginGroup("Accounts");
    if(dentalPlanName.length() < 2) { //blank / no real dental plan
        m_result = NOT_CHECKED_INVALID_PLAN;
        emit gotEligibilityResult(m_result);
        return;
    }

    //Certain payerID work with DentalXChange, other ones don't
    if(dentalPlanName == "CKMA1") { //MassHealth
        //Use steadi since it is not suppored by dentalxchange
        //https://www.stedi.com/docs/api-reference/healthcare/post-healthcare-eligibility
        m_Request.setUrl(QUrl(STEDI_URL));
        QString apiKey = pracInfoSettings.value("StediAPIKey","").toString();
        pracInfoSettings.endGroup();
        m_Request.setRawHeader("Authorization", apiKey.toLatin1());
        m_Request.setRawHeader("Content-type", "application/json");

        QVariantMap body;
        body["controlNumber"] = QString::number(QDateTime::currentDateTime().toSecsSinceEpoch());
        body["tradingPartnerServiceId"] = dentalPlanName;

        QVariantMap providerObj;
        pracInfoSettings.beginGroup("GeneralInfo");
        providerObj["npi"] = pracInfoSettings.value("PracticeNPI","").toString();
        providerObj["organizationName"] = pracInfoSettings.value("PracticeName","").toString();
        body["provider"] = providerObj;


        QVariantMap patientObj;


        QVariantMap subscriberObj;



    }
    else {
        //For now, will only handle DentalXChange, will handle other clearing houses later
        m_Request.setUrl(QUrl(DENTALXCHANGE_URL));

        QString apiKey = pracInfoSettings.value("DentalXChangeAPIKey","").toString();
        QString dentalXUsername = pracInfoSettings.value("DentalXChangeUsername","").toString();
        QString dentalXPassword = pracInfoSettings.value("DentalXChangePassword","").toString();
        int dentalXGroupNumb = pracInfoSettings.value("DentalXChangeGroup","").toInt();
        pracInfoSettings.endGroup();
        m_Request.setRawHeader("API-Key", apiKey.toLatin1());
        m_Request.setRawHeader("Username", dentalXUsername.toLatin1());
        m_Request.setRawHeader("Password", dentalXPassword.toLatin1());
        m_Request.setRawHeader("Content-type", "application/json");

        //https://staging-developer.dentalxchange.com/eligibility-api
        QVariantMap body;

        //First get the patient information
        QVariantMap patientObj;
        QSettings patPersonalSettings(CDFileLocations::getPersonalFile(m_patID),QSettings::IniFormat);
        patPersonalSettings.beginGroup("Name");
        patientObj["firstName"] = patPersonalSettings.value("FirstName","").toString();
        patientObj["lastName"] = patPersonalSettings.value("LastName","").toString();
        patPersonalSettings.endGroup();
        patPersonalSettings.beginGroup("Personal");
        QDate dob = QDate::fromString(patPersonalSettings.value("DateOfBirth").toString(),"M/d/yyyy");
        patientObj["dateOfBirth"] = dob.toString("yyyy-MM-dd");
        patientObj["memberId"] = insInfoSettings.value("SubID","").toString();
        QString polRel = insInfoSettings.value("PolHolderRel","Self").toString();
        if(polRel == "Self") {
            patientObj["relationship"] = "18";
        }
        else if(polRel == "Spouse") {
            patientObj["relationship"] = "01";
        }
        else if(polRel == "Dependent") {
            patientObj["relationship"] = "19";
        }
        else { //"Other"
            patientObj["relationship"] = "34";
        }
        body["patient"] = patientObj;

        //Now the subscriber information
        QVariantMap subscriberObj;
        if(polRel == "Self") {
            subscriberObj["firstName"] = patientObj["firstName"];
            subscriberObj["lastName"] =  patientObj["lastName"];
            subscriberObj["dateOfBirth"] =  patientObj["dateOfBirth"];
        }
        else {
            subscriberObj["firstName"] = insInfoSettings.value("PolHolderFirst","");
            subscriberObj["lastName"] = insInfoSettings.value("PolHolderLast","");
            QDate polDOB = QDate::fromString(insInfoSettings.value("PolHolderDOB").toString(),"M/d/yyyy");
            subscriberObj["dateOfBirth"] = polDOB.toString("yyyy-MM-dd");
        }
        subscriberObj["memberId"] =patientObj["memberId"];
        body["subscriber"] = subscriberObj;

        //Provider; as in the local practice
        QVariantMap providerObj;
        pracInfoSettings.beginGroup("GeneralInfo");
        providerObj["type"] = "2"; //Organization; 1 = individual
        providerObj["taxId"] = pracInfoSettings.value("FederalTaxID","").toString().replace("-","");
        providerObj["npi"] = pracInfoSettings.value("PracticeNPI","").toString();
        providerObj["organizationName"] = pracInfoSettings.value("PracticeName","").toString();
        body["provider"] = providerObj;

        //PayerID stuff
        QVariantMap payerObj;
        payerObj["payerIdCode"] = insInfoSettings.value("ClearingHouseID","").toString();
        payerObj["name"] = dentalPlanName;
        body["payer"] = payerObj;

        QString patGroupNumber = insInfoSettings.value("GroupID","").toString().trimmed();
        if(patGroupNumber.length() > 2) {
            body["groupNumber"]= patGroupNumber;
        }

        body["dxcGroupId"] = dentalXGroupNumb;

        QJsonDocument jsonObj = QJsonDocument::fromVariant(body);
        QByteArray jsonStr = jsonObj.toJson(QJsonDocument::Compact);
        qDebug()<<jsonStr;
        m_Reply = m_networkAccessManager.post(m_Request,jsonStr);
        connect(m_Reply,&QIODevice::readyRead,this,&CDEligibilityChecker::handleReadyRead);
        connect(m_Reply,&QNetworkReply::errorOccurred,this,&CDEligibilityChecker::handleError);
        connect(m_Reply,&QNetworkReply::sslErrors,this,&CDEligibilityChecker::handleError);

    }



}

bool CDEligibilityChecker::hasEligibilityReport()
{
    QFile jsonFile(CDFileLocations::getPatientDentalPlanEligibilityFile(m_patID));
    return jsonFile.exists();
}

QString CDEligibilityChecker::eligibilityReportJSON()
{
    return CDTextfileManager::readFile(CDFileLocations::getPatientDentalPlanEligibilityFile(m_patID));
}

CDEligibilityChecker::EligibilityResult CDEligibilityChecker::getRequestResult()
{
    return m_result;
}

void CDEligibilityChecker::handleReadyRead()
{
    QVariant statusCode = m_Reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
    qDebug()<<"Got a handleReadyRead status of: "<<statusCode;
    QByteArray fullResponseString = m_Reply->readAll();
    qDebug()<<fullResponseString;
    QJsonDocument responseDocument = QJsonDocument::fromJson(fullResponseString);
    QVariant responseVariant = responseDocument.toVariant();

    QVariantMap resultsMap;
    resultsMap["DateTimeChecked"] = QDateTime::currentDateTime();
    resultsMap["FullResponse"] = responseVariant;

    QFile jsonFile(CDFileLocations::getPatientDentalPlanEligibilityFile(m_patID));
    jsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QJsonDocument writeDoc = QJsonDocument::fromVariant(resultsMap);
    jsonFile.write(writeDoc.toJson(QJsonDocument::Indented));
    jsonFile.close();


    QString statusString = responseVariant.toMap().value("response").toMap().value("status").toString();
    QString resultStatusCode = responseVariant.toMap().value("status").toMap().value("code").toString();

    if(resultStatusCode == "2003") {
        m_result = PAYER_NOT_SUPPORTED;
    }
    else if(statusString == "Active") {
        m_result = VALID_DENTAL_PLAN_ACTIVE;
    }
    else if(statusString == "Inactive") {
        m_result = VALID_DENTAL_PLAN_INACTIVE;
    }

    emit gotEligibilityResult(m_result);
}

void CDEligibilityChecker::handleError()
{
    QVariant statusCode = m_Reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
    qDebug()<<"Got a handleError status of: "<<statusCode;
    qDebug()<<m_Reply->readAll();

    m_result = ERROR_WITH_SERVICE;
    emit gotEligibilityResult(m_result);
}
