// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdfilelocations.h"
#include "cddefaults.h"

#include <QDebug>
#include <QDir>

CDFileLocations::CDFileLocations(QObject *parent) : QObject(parent)
{

}

QString CDFileLocations::getPatientDirectory(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/";
}

QString CDFileLocations::getPatientAppointmentDirectory(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/appointments/";
}

QString CDFileLocations::getPersonalFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/personal.ini";
}

QString CDFileLocations::getMessagesFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/messages.json";
}

QString CDFileLocations::getMedicationsFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/medications.json";
}

QString CDFileLocations::getAllergiesFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/allergies.json";
}

QString CDFileLocations::getConditionsFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/conditions.json";
}

QString CDFileLocations::getSurgeriesFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/surgeries.json";
}

QString CDFileLocations::getRecallFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/recall.ini";
}

QString CDFileLocations::getVitalsFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/vitals.ini";
}

QString CDFileLocations::getReviewsFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/reviews.ini";
}

QString CDFileLocations::getHardTissueChartFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/hardTissue.ini";
}

QString CDFileLocations::getPerioChartFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/perio.ini";
}

QString CDFileLocations::getEoEFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/EoE.ini";
}

QString CDFileLocations::getIoEFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/IoE.ini";
}

QString CDFileLocations::getTreatmentPlanFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/txPlan.json";
}

QString CDFileLocations::getNewPatInterviewFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/newPatInterview.ini";
}

QString CDFileLocations::getDentalPlansFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/financial/dentalPlans.ini";
}

QString CDFileLocations::getOtherMedInfoFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/medicalInfo.ini";
}

QString CDFileLocations::getLedgerFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/financial/ledger.json";
}

QString CDFileLocations::getClaimsFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/financial/claims.json";
}

QString CDFileLocations::getPatientDentalPlanEligibilityFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/financial/eligibility.json";
}

QString CDFileLocations::getFullBillingFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/financial/fullBilling.json";
}

QString CDFileLocations::getRemovableStatusFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/removableStatus.ini";
}

QString CDFileLocations::getCOVIDStatusFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/medical/covidStatus.ini";
}

QString CDFileLocations::getCaseNoteFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/caseNotes.json";
}

QString CDFileLocations::getPatientMessagesFile(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/messages.json";
}

QString CDFileLocations::getPatientScanDir(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/images/scans/";
}

QString CDFileLocations::getPatientCardFile(QString patID)
{
    return getPatientScanDir(patID) + "cards.png";
}

QString CDFileLocations::getPatientProphyPrefs(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/prophyPrefs.ini";
}

QString CDFileLocations::getPatientPrevTx(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/prevTxPlan.json";
}

QString CDFileLocations::getPatientPrevPerioHxDirectory(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/dental/perioHx/";
}

QString CDFileLocations::getPatientFamilyDirectory(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/family/";
}

QString CDFileLocations::getLocalPracticePreferenceFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/prefs.ini";
}

QString CDFileLocations::getDocInfoFile(QString docID)
{
    return CDDefaults::getDocPrefDir() + docID+ "/prefs.ini";
}

QString CDFileLocations::getWorkingDocInfoFile()
{
    QString name = qgetenv("USER");
    if (name.isEmpty()) {
        name = qgetenv("USERNAME");
    }

    return getDocInfoFile(name);
}

QString CDFileLocations::getLocalDefaultChairFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/chairs.json";
}

QString CDFileLocations::getLocalSensorCalibrationFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/sensorCalibrations.ini";
}

QString CDFileLocations::getLocalChairStatus()
{
    return CDDefaults::getLocalLocationInfoDir() + "/chairStatus.ini";
}

QString CDFileLocations::getLocalPracticeCOVIDFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/covidScreenings.json";
}

QString CDFileLocations::getLocalNewPatientCommentFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/newPatientComments.ini";
}

QString CDFileLocations::getLocalAppointmentsDirectory()
{
    return CDDefaults::getLocalLocationInfoDir() + "/appointments/";
}

QString CDFileLocations::getLocalTaskFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/fullTaskList.json";
}

QString CDFileLocations::getLocalPracticeFeesFile()
{
    return CDDefaults::getLocalLocationInfoDir() + "/feeSchedule.ini";
}

QString CDFileLocations::getPracticeFeesFile(QString pracID)
{
    return CDDefaults::getLocationsDir() + pracID + "/feeSchedule.ini";
}

QString CDFileLocations::getPracticePreferenceFile(QString pracID)
{
    return CDDefaults::getLocationsDir() + pracID + "/prefs.ini";
}

QString CDFileLocations::getPracticeChairFile(QString pracID)
{
    return CDDefaults::getLocationsDir() + pracID + "/chairs.json";
}

QString CDFileLocations::getPracticeLabsFile(QString pracID)
{
    return CDDefaults::getLocationsDir() + pracID + "/labs.json";
}

QString CDFileLocations::getProfileImageFile(QString patID)
{
    QString returnMe = CDDefaults::getPatDir() + patID + "/images/photographs/extraoral/profile.jpg";
//    QFile checker(returnMe);
//    if(checker.exists()) {
        return returnMe;
//    } else {
//        return "/usr/share/icons/breeze/apps/48/kuser.svg";
//    }
}

QString CDFileLocations::getProfileImageDir(QString patID)
{
    return CDDefaults::getPatDir() + patID + "/images/photographs/extraoral/";
}

QString CDFileLocations::getRadiographDir(QString patID)
{
    return CDDefaults::getPatDir() + patID +
            "/images/radiographs/";
}

QString CDFileLocations::getFGTPDir(QString patID)
{
    return CDDefaults::getPatDir() + patID +
            "/images/photographs/extraoral/fgtp/";
}

QString CDFileLocations::getIntraOralDir(QString patID)
{
    return CDDefaults::getPatDir() + patID +"/images/photographs/intraoral/";
}

QString CDFileLocations::getConsentImageFile(QString patID, QDate day,
                                             QString procedure)
{
    QString dirName = CDDefaults::getPatDir() + patID +
            "/images/consents/";
    QString dateString = day.toString("MMM") + "/" +
            day.toString("d") + "/" +
            day.toString("yyyy") + "/";
    QDir dir( CDDefaults::getPatDir() + patID +"/images/consents/");
    dir.mkpath(dateString);

    return dirName + dateString + procedure + ".png";;
}

QString CDFileLocations::getTempImageLocation()
{
    QString name = qgetenv("USER");
    if (name.isEmpty()) {
        name = qgetenv("USERNAME");
    }

    return "/tmp/"+name+"-profile.jpg";
}
