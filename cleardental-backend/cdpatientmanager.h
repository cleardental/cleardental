// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDPATIENTMANAGER_H
#define CDPATIENTMANAGER_H

#include <QObject>
#include <QVariant>


class CDPatientManager : public QObject
{
    Q_OBJECT
public:
    explicit CDPatientManager(QObject *parent = nullptr);

signals:

public slots:
    static QList<QVariant> getAllPatients();
    static QStringList getAllPatientDirs();
    static QStringList getAllPatientIds();
    static QString getFirstNameString();
    static QString getMiddleNameString();
    static QString getLastNameString();
    static QString getPreferredNameString();
    static QString getDOBString();
    static QString getProfileString();
    static QString getDirString();
    static QString getPatIDString();
};

#endif // CDPATIENTMANAGER_H
