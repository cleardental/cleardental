// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

QtObject {

    property string endPointURL: "https://api.dentalxchange.com/"

    property var m_locs: CDFileLocations{}
    property var patPersonalSettings: Settings {}
    property var loadInsSettings:  Settings {}
    property var loadProviderSettings:  Settings {}
    property var m_RadioMan:  CDRadiographManager {}
    property var m_textFileMan: CDTextFileManager {}
    property var m_CommonFuns: CDCommonFunctions{}
    property var m_Printer: CDPrinter{}
    property var m_Narrative: CDNarrativeGenerator{}
    property var m_loadOtherSettings: Settings{}
    property string m_dxcAttachmentId: "";

    property string errorJSON: ""; //use this for creating dialogs for errors from DentalXChange

    property var locPrefsSettings: Settings {
        fileName: m_locs.getLocalPracticePreferenceFile()
        category: "ClaimsInfo"
    }

    function convertTaxonomyToDentalXChangeType(getTaxo) {
        if(getTaxo==="1223G0001X") { //General Practice
            return "GP";
        }
        else if(getTaxo==="1223D0001X") { //Dental Public Health
            return "PUBLIC";
        }
        else if(getTaxo==="1223E0200X") { //Endodontics
            return "ENDO";
        }
        else if(getTaxo==="1223X0400X") { //Orthodontics
            return "ORTHO";
        }
        else if(getTaxo==="1223P0221X") { //Pediatric
            return "PEDI";
        }
        else if(getTaxo==="1223P0300X") { //Periodontics
            return "PERI";
        }
        else if(getTaxo==="1223P0700X") { //Prosthodontics
            return "PROS";
        }
        else if(getTaxo==="1223P0106X") { //Pathology
            return "ORALMAXP";
        }
        else if(getTaxo==="1223X0008X") { //Radiology
            return "ORALMAXR";
        }
        else if(getTaxo==="1223S0112X") { //Surgery
            return "ORALMAX";
        }
        else { //??????
            return "DENT" //I actually have no clue what it even means....
        }
    }

    function convertDateObjectToYYYYLeadings(getDate) {
        var year = getDate.getFullYear();
        var month = getDate.getMonth() + 1;
        if(month < 10) {
            month = "0" + month;
        }
        var day = getDate.getDate();
        if(day < 10) {
            day = "0" + day;
        }
        return year + "-" + month + "-" + day;
    }


    function convertDateToYYYYLeadings(getDate) {
        var splitTimes = getDate.split('/');
        var intMonth = parseInt(splitTimes[0]);
        if(intMonth < 10) {
            intMonth = '0' + intMonth;
        }
        var intDay = parseInt(splitTimes[1]);
        if(intDay < 10) {
            intDay = '0' + intDay;
        }
        var intYear = parseInt(splitTimes[2]);
        return (intYear + '-' + intMonth + '-' + intDay);
    }

    function convertRelationshipCode(getRelStr) {
        if(getRelStr === "Self") {
            return "18";
        } else if(getRelStr === "Spouse") {
            return "01";
        } else if(getRelStr === "Dependent") {
            return "76";
        }
        return "99"; //default

    }

    function getRadiographsBeforeDate(getPatID,getTooth,getDate) {
        var radioList = m_RadioMan.getRadiographObjectsForTooth(getPatID,getTooth,false);
        var sendIndex =0;
        //console.debug(JSON.stringify(radioList,null,'\t'));
        for(var i=0;i<radioList.length;i++) {
            var radiodate = radioList[i]["date"];
            // console.debug("Radiodate: " + radiodate);
            // console.debug("getDate: " + getDate);
            // console.debug("Radiodate <  getDate: " + (radiodate < getDate));
            if(radiodate < getDate) {
                sendIndex = i;
                i = radioList.length;
            }
        }
        return radioList.splice(sendIndex);
    }

    function getRadiographsBeforeOrOnDate(getPatID,getTooth,getDate) {
        var radioList = m_RadioMan.getRadiographObjectsForTooth(getPatID,getTooth,false);
        var sendIndex =0;
        for(var i=0;i<radioList.length;i++) {
            var radiodate = radioList[i]["date"];
            if(radiodate <= getDate) {
                sendIndex = i;
                i = radioList.length;
            }
        }
        return radioList.splice(sendIndex);

    }

    function generateDentalXChangeAttachmentRadiographObject(getRadiographObject, getIsLeft) {
        var returnMe = ({});
        returnMe["imageTypeCode"] = "RB" //X-Rays
        returnMe["imageFileAsBase64"] = m_textFileMan.readBase64File(getRadiographObject["absoluteFilename"]);
        returnMe["imageFileName"] = getRadiographObject["filename"];
        // if(getIsLeft) {
        //     returnMe["imageOrientationType"] = "LEFT";
        // }
        // else {
            returnMe["imageOrientationType"] = "RIGHT"; //Image Left is patient Right
        // }
        returnMe["imageDate"] = convertDateObjectToYYYYLeadings(getRadiographObject["date"]);
        returnMe["vendorImageId"] = getRadiographObject["filename"]
        return returnMe;
    }

    function generateDentalXChangeAttachmentNarrativeObject(getText,getPatID, getProvider) {
        var returnMe = ({});
        returnMe["imageTypeCode"] = "03" //Narrative
        returnMe["imageFileAsBase64"] = m_Narrative.generateGenericNarrative(getText,getPatID,getProvider);
        returnMe["vendorImageId"] =  getPatID + "-" + getProvider + "--" + new Date();
        returnMe["imageFileName"] = returnMe["vendorImageId"] + ".pdf";
        return returnMe;
    }

    function generateCrownAttachmentObj(getPatID, getToothNumb, getProcedureDate) {
        var returnMe = ({});
        var beforeRadios = getRadiographsBeforeDate(getPatID, getToothNumb, getProcedureDate);

        //we prefer to send a PA but if we don't have one, send a BW
        var sendIndex =0;
        for(var i=0;i<beforeRadios.length;i++) {
            if(beforeRadios[i]["filename"].startsWith("PA_")) {
                sendIndex = i;
                i = beforeRadios.length;
            }
        }

        returnMe = generateDentalXChangeAttachmentRadiographObject(beforeRadios[sendIndex],
                                                                   m_CommonFuns.isLeft(getToothNumb));
        return returnMe;
    }

    function generateRCTAttachmentObj(getToothNumb, getProcedureDate) {
        var returnMe = ({});
        //PA (Initial) + post

        return returnMe;
    }

    function generateSCRPAttachmentObjs(getPatID, getQuad, getProcedureDate, getAlreadyAttachedPerio,
                                        getAttachmentPaths) {
        var returnMe = [];

        //Attach all radiographs for that quad
        var fullRadiographObjList = m_RadioMan.getRadiographObjectsForQuad(getPatID,getQuad);
        var isLeft = false;
        if((getQuad === "UL") || (getQuad === "LL")) {
            isLeft = true;
        }

        for(var i=0;i<fullRadiographObjList.length;i++) {
            var fullPath = fullRadiographObjList[i]["absoluteFilename"];
            if(!getAttachmentPaths.includes(fullPath)) { //not already in the list of attachments
                returnMe.push(generateDentalXChangeAttachmentRadiographObject(fullRadiographObjList[i],isLeft));
                getAttachmentPaths.push(fullPath);
            }
        }

        //Also need periodontal chart
        if(!getAlreadyAttachedPerio) {
            var perioAttachObj = ({});
            perioAttachObj["imageTypeCode"] = "P6" //Periodontal Charts
            var periodontalChartArray = m_Printer.printPeriodontalChart(getPatID,false);
            //console.debug(JSON.stringify(periodontalChartArray));
            //0 = date periodontal chart was done ; 1 = base64 of the pdf
            perioAttachObj["imageFileAsBase64"] =periodontalChartArray[1];
            perioAttachObj["imageFileName"] = getPatID + "-PeridontalChart.pdf";
            perioAttachObj["imageDate"] = convertDateToYYYYLeadings(periodontalChartArray[0]);
            perioAttachObj["vendorImageId"] =  getPatID + "-PDC.pdf";
            returnMe.push(perioAttachObj);
        }

        // TODO: For D4342, there needs to be a narrative that says which teeth needs the SCRP

        return [returnMe,getAttachmentPaths];
    }

    function generatePerioMainAttachmentObj() {

    }

    function generateDentureAttachmentObjs(getPatID, getProviderID, getArch, getAttachmentPaths) {
        var returnMe = [];
        //Attach all radiographs for that arch
        var fullRadiographObjList = [];
        if(getArch === "UA") {
            fullRadiographObjList.concat(m_RadioMan.getRadiographObjectsForQuad(getPatID,"UR"));
            fullRadiographObjList.concat(m_RadioMan.getRadiographObjectsForQuad(getPatID,"UL"));
        }
        else {
            fullRadiographObjList.concat(m_RadioMan.getRadiographObjectsForQuad(getPatID,"LL"));
            fullRadiographObjList.concat(m_RadioMan.getRadiographObjectsForQuad(getPatID,"LR"));
        }

        for(var i=0;i<fullRadiographObjList.length;i++) {
            var fullPath = fullRadiographObjList[i]["absoluteFilename"];
            if(!getAttachmentPaths.includes(fullPath)) { //not already in the list of attachments
                returnMe.push(generateDentalXChangeAttachmentRadiographObject(fullRadiographObjList[i],isLeft));
                getAttachmentPaths.push(fullPath);
            }
        }

        //Now return also a narrative that explains when last arch was done
        m_loadOtherSettings.filename = m_locs.getRemovableStatusFile(getPatID);
        m_loadOtherSettings.category = "Existing";

        var narrativeText = "";
        if(JSON.parse(m_loadOtherSettings.value("Maxillary"))) {
            narrativeText = "Patient presented with a previous maxillary ";
            if(m_loadOtherSettings.value("MaxillaryType") === "CD") {
                narrativeText += "Complete Denture";
            }
            else {
                narrativeText += "Removable Partial Denture";
            }
            narrativeText += " that was delivered around " + m_loadOtherSettings.value("MaxillaryDate") + ". "
        }
        else {
            narrativeText = "Patient presented with no previous maxillary denture (neither complete or partial). ";
        }
        if(JSON.parse(m_loadOtherSettings.value("Mandibular"))) {
            narrativeText += "Patient presented with a previous mandibular ";
            if(m_loadOtherSettings.value("MandibularType") === "CD") {
                narrativeText += "Complete Denture";
            }
            else {
                narrativeText += "Removable Partial Denture";
            }
            narrativeText += " that was delivered around " + m_loadOtherSettings.value("MandibularDate") + "."
        }
        else {
            narrativeText += "Patient presented with no previous mandibular denture (neither complete or partial).";
        }
        returnMe.push(generateDentalXChangeAttachmentNarrativeObject(narrativeText,getPatID, getProviderID));


        return [returnMe,getAttachmentPaths];
    }

    function generateBridgeAttachmentObjs() {

    }

    function generateImplantObjs(getToothNumb) {

    }
    function generateSurgicalExtObj(getPatID, getToothNumb, getProcedureDate) {
        var returnMe = ({});
        var beforeRadios = getRadiographsBeforeOrOnDate(getPatID, getToothNumb, getProcedureDate);

        //For 3rd molars, a Pano would make sure sense
        var panoIndex = -1;
        var toothNumbString = getToothNumb.toString();
        if(     (toothNumbString === "1") ||
                (toothNumbString === "16") ||
                (toothNumbString === "17") ||
                (toothNumbString === "32") ) {
            for(var i=0;i<beforeRadios.length;i++) {
                if(beforeRadios[i]["filename"].startsWith("Panoramic")) {
                    panoIndex = i;
                    i = beforeRadios.length;
                }
            }
        }

        if(panoIndex >= 0) {
            returnMe = generateDentalXChangeAttachmentRadiographObject(beforeRadios[panoIndex],
                                                                       m_CommonFuns.isLeft(getToothNumb));
        }
        else {
            //we prefer to send a PA but if we don't have one, send a BW
            var sendIndex =0;
            for(i=0;i<beforeRadios.length;i++) {
                if(beforeRadios[i]["filename"].startsWith("PA_")) {
                    sendIndex = i;
                    i = beforeRadios.length;
                }
            }

            returnMe = generateDentalXChangeAttachmentRadiographObject(beforeRadios[sendIndex],
                                                                       m_CommonFuns.isLeft(getToothNumb));
        }

        return returnMe;
    }

    function generateOrthoObjs() {

    }

    //https://developer.dentalxchange.com/claim-api#tag/Claim/operation/claims-submit
    function dentalXchangeSubmitClaim(getPatID, getProcedureList,isPreEstimate=false,
                                      sendToSecondary=false, authorizePayment=true) {

        locPrefsSettings.category = "Accounts";
        var userName = locPrefsSettings.value("DentalXChangeUsername","");
        var password = locPrefsSettings.value("DentalXChangePassword","");
        var groupID = locPrefsSettings.value("DentalXChangeGroup","");
        var apiKey = locPrefsSettings.value("DentalXChangeAPIKey","");
        var submitClaimEndpoint = endPointURL + "claims/submit"


        var getProviderID = getProcedureList[0]["ProviderUsername"];
        //Technically there is an issue if there are multiple providers for the claim, only the first gets paid.
        //In the future, we should fix this such that it would submit a new claim for each provider

        loadProviderSettings.fileName = m_locs.getDocInfoFile(getProviderID);
        patPersonalSettings.fileName = m_locs.getPersonalFile(getPatID);


        locPrefsSettings.category = "GeneralInfo";
        var taxo = locPrefsSettings.value("Taxonomy","1223G0001X");

        var sendMe = ({});

        var sendClaim = ({});

        ///providers
        var providers = [];

        var billingProvider = ({}); //we will assume billing = practice
        billingProvider["type"] = "BILLING";
        billingProvider["specialty"] = convertTaxonomyToDentalXChangeType(taxo);
        billingProvider["taxonomy"] = taxo
        billingProvider["taxId"] = locPrefsSettings.value("FederalTaxID","1234567890").replace("-","");
        billingProvider["billingNpi"] = locPrefsSettings.value("PracticeNPI"," ");

        var billingProviderAddress = ({});
        billingProviderAddress["organizationName"] = locPrefsSettings.value("PracticeName","Dental Practice");
        billingProviderAddress["address1"] = locPrefsSettings.value("AddrLine1"," ");
        billingProviderAddress["address2"] = locPrefsSettings.value("AddrLine2"," ");
        billingProviderAddress["city"] = locPrefsSettings.value("City"," ");
        billingProviderAddress["state"] = locPrefsSettings.value("State"," ");
        billingProviderAddress["zipCode"] = locPrefsSettings.value("Zip"," ");
        billingProviderAddress["entityType"] = "2"; //"1" = Individual; "2"= Organization
        //billingProviderAddress["type"] = "0"//  "0" = Default ; "1" = PayToAddress

        billingProvider["addresses"] = [billingProviderAddress];
        providers.push(billingProvider);

        var renderingProvider = ({});
        renderingProvider["type"] = "RENDERING";
        renderingProvider["specialty"] = convertTaxonomyToDentalXChangeType(loadProviderSettings.value("Taxonomy",""));

        //renderingProvider["licenseNumber"] =  loadProviderSettings.value("LicenseID"," ");
        var licenseArray =  JSON.parse(loadProviderSettings.value("LicenseArray","[]"));
        var useIndex=0;
        for(var li=0;li<licenseArray.length;li++) {
            if(licenseArray[li]["State"] === billingProviderAddress["state"]) {
                useIndex = li;
            }
        }
        renderingProvider["licenseNumber"] = licenseArray[useIndex]["LicenseID"];

        renderingProvider["taxId"] = billingProvider["taxId"] //they need it for some reason
        renderingProvider["renderingNpi"] = loadProviderSettings.value("NPINumb",null);

        //Someday we can handle different billing vs. rendering addresses;
        //but that will require the concept of a "HQ" office which can be put in at a later time
        var renderingProviderAddress = ({});
        renderingProviderAddress["address1"] = locPrefsSettings.value("AddrLine1"," ");
        renderingProviderAddress["address2"] = locPrefsSettings.value("AddrLine2"," ");
        renderingProviderAddress["city"] = locPrefsSettings.value("City"," ");
        renderingProviderAddress["state"] = locPrefsSettings.value("State"," ");
        renderingProviderAddress["zipCode"] = locPrefsSettings.value("Zip"," ");
        renderingProviderAddress["entityType"] = "1"; //"1" = Individual; "2"= Organization
        //renderingProviderAddress["type"] = "0"//  "0" = Default ; "1" = PayToAddress
        renderingProviderAddress["firstName"] = loadProviderSettings.value("FirstName","");
        renderingProviderAddress["lastName"] = loadProviderSettings.value("LastName","");
        renderingProvider["addresses"] = [renderingProviderAddress];

        providers.push(renderingProvider);
        sendClaim["providers"] = providers;

        ///payer
        //Object for the dental plan
        loadInsSettings.fileName = m_locs.getDentalPlansFile(getPatID);
        loadInsSettings.category = "Primary";
        var payerObject = ({});
        payerObject["payerIdCode"] = loadInsSettings.value("ClearingHouseID","");
        payerObject["employerName"] = loadInsSettings.value("EmployerName","");

        var payerAddress = ({});
        payerAddress["address1"] = loadInsSettings.value("Addr","");
        payerAddress["city"] =  loadInsSettings.value("City","");
        payerAddress["state"] = loadInsSettings.value("State","");
        payerAddress["zipCode"] = loadInsSettings.value("Zip","");
        payerAddress["entityType"] = "2"; //1 = "Indivdual; 2= Organization
        payerObject["address"] = payerAddress;

        sendClaim["payer"] = payerObject;

        ///patient
        var patientObj = ({});

        var patientAddrObj = ({});
        patPersonalSettings.category = "Address";
        patientAddrObj["address1"] = patPersonalSettings.value("StreetAddr1","");
        patientAddrObj["address2"] = patPersonalSettings.value("StreetAddr2","");
        patientAddrObj["city"] = patPersonalSettings.value("City","");
        patientAddrObj["state"] = patPersonalSettings.value("State","");
        patientAddrObj["zipCode"] = patPersonalSettings.value("Zip","");
        //For whatever reason, the name of the patient is part of the address object
        patPersonalSettings.category = "Name";
        patientAddrObj["firstName"] = patPersonalSettings.value("FirstName","");
        patientAddrObj["middleName"] = patPersonalSettings.value("MiddleName","");
        patientAddrObj["lastName"] = patPersonalSettings.value("LastName","");
        patientAddrObj["entityType"] = "1" // "1" = Individual; "2" = Organization
        patientObj["address"] = patientAddrObj;

        patientObj["memberType"] = "PATIENT";
        patPersonalSettings.category = "Personal"
        patientObj["gender"] = patPersonalSettings.value("Sex","U")[0];
        patientObj["memberId"] = loadInsSettings.value("SubID","");
        patientObj["dateOfBirth"] = convertDateToYYYYLeadings(patPersonalSettings.value("DateOfBirth","1/1/1970"));
        patientObj["sequenceCode"] = "P"; //"P"=Primary; "S"=Secondary; "T"=Tertiary
        patientObj["relationship"] = convertRelationshipCode(loadInsSettings.value("PolHolderRel","Self"));
        if(loadInsSettings.value("GroupID","").length > 0) {
            patientObj["groupNumber"] = loadInsSettings.value("GroupID","");
        }


        sendClaim["patient"] = patientObj;

        ///subscriber
        var subscriberObj = ({});
        var subscriberAddrObj = ({});
        if(loadInsSettings.value("PolHolderRel","Self") === "Self") { //use the patient information
            subscriberAddrObj = patientAddrObj;
            subscriberObj["gender"] =  patientObj["gender"];
            subscriberObj["dateOfBirth"] =  patientObj["dateOfBirth"];
        }
        else { //use the dentalPlans.ini information
            subscriberAddrObj["address1"] = loadInsSettings.value("PolHolderAddress","");
            subscriberAddrObj["city"] = loadInsSettings.value("PolHolderCity","");
            subscriberAddrObj["state"] = loadInsSettings.value("PolHolderState","");
            subscriberAddrObj["zipCode"] = loadInsSettings.value("PolHolderZip","");
            subscriberAddrObj["firstName"] = loadInsSettings.value("PolHolderFirst","");
            subscriberAddrObj["middleName"] = loadInsSettings.value("PolHolderMiddle","");
            subscriberAddrObj["lastName"] = loadInsSettings.value("PolHolderLast","");
            subscriberAddrObj["entityType"] = "1" // "1" = Individual; "2" = Organization
            subscriberObj["gender"] = loadInsSettings.value("PolHolderGender","U")[0];
            subscriberObj["dateOfBirth"] = convertDateToYYYYLeadings(loadInsSettings.value("PolHolderDOB","1/1/1970"));
        }
        subscriberObj["address"] = subscriberAddrObj;
        subscriberObj["memberType"] = "SUBSCRIBER";
        subscriberObj["memberId"] = loadInsSettings.value("SubID","");
        subscriberObj["sequenceCode"] = "P";
        subscriberObj["planName"] = loadInsSettings.value("Name","");
        subscriberObj["relationship"] = "18";
        if(loadInsSettings.value("GroupID","").length > 0) {
            subscriberObj["groupNumber"] = loadInsSettings.value("GroupID","");
        }
        sendClaim["subscriber"] = subscriberObj;

        //claimItems
        var claimItems = [];
        var sendAttachments = false;
        var alreadyAttachedPerio = false;
        var attachmentPaths = [];
        var attachementObjs = [];
        for(var i=0;i< getProcedureList.length;i++) {
            var addItem = ({});
            var procedureItem = getProcedureList[i];

            if(!isPreEstimate) {
                var procedureDateObj = new Date(procedureItem["ProcedureDate"]);
                addItem["startDate"] = procedureDateObj.toISOString().split('T')[0];
                addItem["endDate"] = addItem["startDate"];
            }

            addItem["quantity"] = 1; //in clear.dental; repeats are seperate procedures
            addItem["fee"] = parseFloat( procedureItem["Procedure"]["BasePrice"]);
            addItem["procedureCode"] = procedureItem["Procedure"]["DCode"];

            if("Location" in procedureItem["Procedure"]) {
                var locationString = procedureItem["Procedure"]["Location"];
                if(locationString === "UA") {
                    addItem["quadrant"] ="01"
                }
                else if(locationString === "LA") {
                    addItem["quadrant"] ="02"
                }
                else if(locationString === "UR") {
                    addItem["quadrant"] ="10"
                }
                else if(locationString === "LR") {
                    addItem["quadrant"] ="40"
                }
                else if(locationString === "UL") {
                    addItem["quadrant"] ="20"
                }
                else if(locationString === "LL") {
                    addItem["quadrant"] ="30"
                }
            }

            ///check for attachment needed
            //https://www.dentalclaimsupport.com/blog/why-attachments-dental-insurance-claims-important
            //https://drive.google.com/file/d/1sE4pr9AcjtVhxy5dt8vnCzLdglQ9Ytgu/view

            if( (addItem["procedureCode"] === "D2740") || //Crowns and core build up
                    (addItem["procedureCode"] === "D2750") ||
                    (addItem["procedureCode"] === "D2790") ||
                    (addItem["procedureCode"] === "D2950") ) {
                attachementObjs.push(generateCrownAttachmentObj(
                                         getPatID,
                                         procedureItem["Procedure"]["Tooth"].toString(),
                                         procedureDateObj
                                         ));
                sendAttachments = true;
            }
            else if( (addItem["procedureCode"] === "D3310") || //RCT
                    (addItem["procedureCode"] === "D3320") ||
                    (addItem["procedureCode"] === "D3330") ) {
                attachementObjs.push(generateRCTAttachmentObj(
                                         getPatID,
                                         procedureItem["Procedure"]["Tooth"].toString(),
                                         procedureDateObj
                                         ));
                sendAttachments = true;

            }
            else if( (addItem["procedureCode"] === "D4341") || //SCRP
                    (addItem["procedureCode"] === "D4342") ) {
                var returnArrays = generateSCRPAttachmentObjs(getPatID,
                                                              procedureItem["Procedure"]["Location"],
                                                              procedureDateObj,
                                                              alreadyAttachedPerio,
                                                              attachmentPaths
                                                              );
                attachementObjs = attachementObjs.concat(returnArrays[0]);
                attachmentPaths = returnArrays[1];
                sendAttachments = true;
                alreadyAttachedPerio = true;
            }
            else if( (addItem["procedureCode"] === "D4910")) { //Periodontal Main.
                attachementObjs.push(generatePerioMainAttachmentObj());
                sendAttachments = true;
            }
            else if( (addItem["procedureCode"] === "D5211") || //Dentures
                    (addItem["procedureCode"] === "D5212") ||
                    (addItem["procedureCode"] === "D5110") ||
                    (addItem["procedureCode"] === "D5140") ||
                    (addItem["procedureCode"] === "D5120") ||
                    (addItem["procedureCode"] === "D5130") ) {
                returnArrays = generateSCRPAttachmentObjs(getPatID,
                                                          getProviderID,
                                                          procedureItem["Procedure"]["Location"],
                                                          attachmentPaths
                                                          );
                attachementObjs.concat(returnArrays[0]);
                attachmentPaths = returnArrays[1];
                sendAttachments = true;
            }
            else if( (addItem["procedureCode"] === "D6740") || //Bridge
                    (addItem["procedureCode"] === "D6245")) {


            }
            else if( (addItem["procedureCode"] === "D6010") || //Implants
                    (addItem["procedureCode"] === "D6057") ||
                    (addItem["procedureCode"] === "D6058")) {
                attachementObjs.push(...generateImplantObjs());
                sendAttachments = true;
            }
            else if(addItem["procedureCode"] === "D7210") { //Surgical ext.
                attachementObjs.push(generateSurgicalExtObj(getPatID,
                                                            procedureItem["Procedure"]["Tooth"].toString(),
                                                            procedureDateObj));
                sendAttachments = true;
            }
            else if(addItem["procedureCode"] === "D8090") { //Ortho
                attachementObjs.push(...generateOrthoObjs());
                sendAttachments = true;
            }




            if("Tooth" in procedureItem["Procedure"]) {
                var addTooth = []; //yeah, I know... they want it as an array for some reason
                if("Surfaces" in  procedureItem["Procedure"]) {
                    var surfaces =  procedureItem["Procedure"]["Surfaces"];
                    for(var s=0;s<surfaces.length;s++) {
                        let addToothObj = ({});
                        addToothObj["surface"]= surfaces[s];
                        addToothObj["toothNumber"] = procedureItem["Procedure"]["Tooth"].toString();
                        addTooth.push(addToothObj);
                    }
                }
                else {
                    let addToothObj = ({});
                    addToothObj["toothNumber"] = procedureItem["Procedure"]["Tooth"].toString();
                    addTooth.push(addToothObj);
                }

                addItem["tooth"] = addTooth;
            }
            claimItems.push(addItem);
        }
        sendClaim["items"] = claimItems;

        //All the other things in the base level
        if(isPreEstimate) {
            sendClaim["type"] = "PB"
        }
        else {
            sendClaim["type"] = "CL"
        }

        sendClaim["providerClaimId"] = getPatID + "-" + Date.now();
        sendClaim["infoSign"] = true;
        sendClaim["benefitSign"] =authorizePayment;

        if(sendAttachments) { //they recommend you send the attachment first
            dentalXchangeSubmitAttachments(sendClaim["providerClaimId"],
                                           payerObject["payerIdCode"],
                                           attachementObjs,
                                           getPatID,
                                           patientObj,
                                           subscriberObj,
                                           billingProvider
                                           );
            sendClaim["dxcAttachmentId"] = m_dxcAttachmentId;
        }

        sendMe["claim"] = sendClaim;
        sendMe["autoAddProvider"] = true;
        sendMe["saveOnError"] = true;
        var sendMeJSON = JSON.stringify(sendMe,null,'\t');

        console.debug("Claim JSON to Send: \n" + sendMeJSON);
        console.debug("URL to send to: " + submitClaimEndpoint);
        //console.debug("Username: " + userName);
        //console.debug("Password: " + password);
        //console.debug("API-Key: " + apiKey);

        //Now send the data
        var request = new XMLHttpRequest();
        request.open('POST', submitClaimEndpoint, true);
        request.setRequestHeader("Username", userName);
        request.setRequestHeader("Password", password);
        request.setRequestHeader("API-Key", apiKey);
        request.setRequestHeader("Content-type", "application/json");
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status === 200) {
                    console.debug("Claim Good: ", request.responseText)

                } else {
                    console.debug("Claim Bad: ", request.status, request.statusText,request.responseText)
                    errorJSON = request.responseText;
                }
            }
        }
        request.send(sendMeJSON)
    }

    //https://staging-developer.dentalxchange.com/attachment-api#tag/Attachment/operation/attachments-complete
    function dentalXchangeSubmitAttachments(getProviderClaimId, //string
                                            getPayerIDCode, //string
                                            getAttachmentObjects, // array of attachment objects ready to send
                                            getPatID, //string
                                            getPatObj, //object used for submission
                                            getSubscriberObj, //object used for submission
                                            getBillingProviderObj, //object used for submission
                                            ) {
        var sendMe = ({}); //CompleteAttachmentImagesRequest
        sendMe["providerClaimId"] = getProviderClaimId;
        sendMe["payerIdCode"] = getPayerIDCode;

        var patientObject = ({});
        patientObject["firstName"] = getPatObj["address"]["firstName"];
        patientObject["lastName"] = getPatObj["address"]["lastName"];
        patientObject["dateOfBirth"] = getPatObj["dateOfBirth"];
        sendMe["patient"] = patientObject;

        var subscriberObject = ({});
        subscriberObject["id"] = getSubscriberObj["memberId"];
        subscriberObject["firstName"] = getSubscriberObj["address"]["firstName"];
        subscriberObject["lastName"] = getSubscriberObj["address"]["lastName"];
        sendMe["subscriber"] = subscriberObject;

        var billingProviderObject = ({});
        billingProviderObject["address1"] = getBillingProviderObj["addresses"][0]["address1"];
        billingProviderObject["address2"] = getBillingProviderObj["addresses"][0]["address2"];
        billingProviderObject["city"] = getBillingProviderObj["addresses"][0]["city"];
        billingProviderObject["state"] = getBillingProviderObj["addresses"][0]["state"];
        billingProviderObject["zipCode"] = getBillingProviderObj["addresses"][0]["zipCode"];
        billingProviderObject["taxId"] = getBillingProviderObj["taxId"];
        billingProviderObject["npi"] = getBillingProviderObj["billingNpi"];
        billingProviderObject["lastName"] = getBillingProviderObj["addresses"][0]["organizationName"];
        billingProviderObject["taxonomy"] = getBillingProviderObj["taxonomy"]
        sendMe["billingProvider"] = billingProviderObject;
        //console.debug("getBillingProviderObj: " + JSON.stringify(getBillingProviderObj,null,'\t'));

        sendMe["images"] = getAttachmentObjects;
        sendMe["releaseAttachment"] = true;
        var sendMeJSON = JSON.stringify(sendMe,null,'\t');

        locPrefsSettings.category = "Accounts";
        var userName = locPrefsSettings.value("DentalXChangeUsername","");
        var password = locPrefsSettings.value("DentalXChangePassword","");
        var groupID = locPrefsSettings.value("DentalXChangeGroup","");
        var apiKey = locPrefsSettings.value("DentalXChangeAPIKey","");
        var submitClaimEndpoint = endPointURL + "/attachments/complete"

        //console.debug("Attachment JSON to Send: \n" + sendMeJSON);

        //m_textFileMan.saveFile("/home/someUser/attachmentSend.json",sendMeJSON);

        //console.debug("URL to send to: " + submitClaimEndpoint);

        //Now send the data
        var request = new XMLHttpRequest();
        request.open('POST', submitClaimEndpoint, false); //need to wait for the response first so the claim will have the attachment
        request.setRequestHeader("Username", userName);
        request.setRequestHeader("Password", password);
        request.setRequestHeader("API-Key", apiKey);
        request.setRequestHeader("Content-type", "application/json");
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status === 200) {
                    //console.debug("Attachment Good: ", request.responseText)
                    var responseTextObj = JSON.parse(request.responseText);
                    console.debug("dxcAttachmentId: " + responseTextObj["response"]["dxcAttachmentId"]);
                    m_dxcAttachmentId = responseTextObj["response"]["dxcAttachmentId"];
                } else {
                    console.debug("Attachment Bad: ", request.status, request.statusText,request.responseText)
                }
            }
        }
        request.send(sendMeJSON)
    }

    function dentalXchangeGetPatientPayments(getPatID, getRead) {
        //https://staging-developer.dentalxchange.com/claim-api#tag/Payment/operation/claims-payments-unread
        locPrefsSettings.category = "Accounts";
        var userName = locPrefsSettings.value("DentalXChangeUsername","");
        var password = locPrefsSettings.value("DentalXChangePassword","");
        var groupID = locPrefsSettings.value("DentalXChangeGroup","");
        var apiKey = locPrefsSettings.value("DentalXChangeAPIKey","");
        var submitClaimEndpoint = endPointURL + "claims/payments/unread"
        if(getRead) {
            submitClaimEndpoint = endPointURL + "claims/payments/read"
        }

        patPersonalSettings.fileName = m_locs.getPersonalFile(getPatID);
        var patientObj = ({});
        patientObj["firstName"] = patPersonalSettings.value("FirstName","");
        patientObj["lastName"] = patPersonalSettings.value("LastName","");

        loadInsSettings.fileName = m_locs.getDentalPlansFile(getPatID);
        loadInsSettings.category = "Primary";
        patientObj["memberId"] =  loadInsSettings.value("SubID","");

        var sendMe = ({});
        sendMe["patient"]  = patientObj;
        var sendMeJSON = JSON.stringify(sendMe,null,'\t');

        //Now send the data
        var request = new XMLHttpRequest();
        request.open('POST', submitClaimEndpoint, false); //need to wait for the response first so the claim will have the attachment
        request.setRequestHeader("Username", userName);
        request.setRequestHeader("Password", password);
        request.setRequestHeader("API-Key", apiKey);
        request.setRequestHeader("Content-type", "application/json");
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status === 200) {
                    //console.debug("Patient Payment Good: ", request.responseText)
                    var responseTextObj = JSON.parse(request.responseText);
                    console.debug(JSON.stringify(responseTextObj,null,'\t'));
                } else {
                    console.debug("Patient Payment Bad: ", request.status, request.statusText,request.responseText)
                }
            }
        }
        request.send(sendMeJSON)
    }

}
