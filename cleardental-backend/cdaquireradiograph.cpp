// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdaquireradiograph.h"


#include <QDebug>
#include <QDir>
#include <QRandomGenerator>
#include <QImageWriter>
#include <QDateTime>
#include <QSettings>

#include "cdfilelocations.h"
#include "cdgitmanager.h"
#include "cdsensorworker.h"
#include "cdsensorinterface.h"

CDAquireRadiograph::CDAquireRadiograph(QObject *parent) : QObject(parent){}


QVariantList CDAquireRadiograph::getSensorList()
{
    QVariantList returnMe;

    CDSensorInterface *inst = CDSensorInterface::getInstance();
    inst->meetAndGreet(); //force a handshake update
    m_sensorStatus.clear();

    for(int i=0;i<inst->sensorCount();i++) {
        QVariantMap addMe;
        addMe["SensorSize"] = inst->getSize(i);
        addMe["SerialNumber"] = inst->getSerialNumber(i);
        returnMe.append(addMe);
        m_sensorStatus.append(NOT_STARTED);
        QVector<quint16> blankVector;
        m_blackVals.append(blankVector); //so we can override it when taking a radiograph
        m_radiographRequestTypes.append(INVALID_RADIOGRAPH_TYPE);
        m_workers.append(nullptr); //will overrride with a valid pointer
    }

    return returnMe;
}

CDAquireRadiograph::SensorStatus CDAquireRadiograph::getSensorStatus(int sensorIndex)
{
    return m_sensorStatus.at(sensorIndex);
}

void CDAquireRadiograph::requestExposure(int sensorIndex,RadiographType rType)
{
    m_sensorStatus[sensorIndex] = CALIBRATING;
    emit sensorStatusUpdated(sensorIndex,CALIBRATING);

    CDSensorWorker *m_newWorker = new CDSensorWorker();
    m_newWorker->setDeviceIndex(sensorIndex);
    m_newWorker->setRequestType(CDSensorWorker::BLACK_IMAGE);
    m_workers[sensorIndex] = m_newWorker;
    m_radiographRequestTypes[sensorIndex] = rType;

    qRegisterMetaType<QVector<quint16> >("QVector<quint16>"); //otherwise can't connect the signals

    connect(m_newWorker,&CDSensorWorker::gotBlackImage,this,&CDAquireRadiograph::handleBlackResult);
    connect(m_newWorker,&CDSensorWorker::gotExposedImage,this,&CDAquireRadiograph::handleExposureResult);

    m_newWorker->start();
}

QString CDAquireRadiograph::saveExposure(QString patientID, CDAquireRadiograph::RadiographType rType)
{
    emit sensorStatusUpdated(-1,SAVING);
    QString destURLString = CDFileLocations::getRadiographDir(patientID);
    QDate today = QDate::currentDate();
    destURLString += today.toString("MMM/d/yyyy");

    QDir destDir(destURLString);
    if(!destDir.exists()) {
        destDir.mkpath(".");
    }

    QString fileLocation = destURLString+ "/" + getStringFromType(rType) + ".png";
    QFile dest(fileLocation);
    int fileCounter = 1;
    while(dest.exists()) {
        fileLocation = destURLString+ "/" + getStringFromType(rType) + "-" + QString::number(fileCounter) + ".png";
        dest.setFileName(fileLocation);
        fileCounter++;
    }

    QFile source(tempRadiographLocation());
    source.copy(fileLocation);

    CDGitManager::commitData("Saved radiograph for " + patientID + "at " + getStringFromType(rType));

    emit sensorStatusUpdated(-1,DONE);

    return fileLocation;
}

void CDAquireRadiograph::rejectExposure(QString patientID, CDAquireRadiograph::RadiographType rType)
{
    QString destURLString = CDFileLocations::getRadiographDir(patientID);
    QDate today = QDate::currentDate();
    destURLString += today.toString("MMM/d/yyyy");

    QDir destDir(destURLString);
    if(!destDir.exists()) {
        destDir.mkpath(".");
    }

    QString fileLocation = destURLString+ "/" + getStringFromType(rType) + ".png.bad";
    QFile destFile(fileLocation);
    int badCounter=2;
    while(destFile.exists()) {
        fileLocation = destURLString+ "/" + getStringFromType(rType) + ".png.bad" + QString::number(badCounter);
        destFile.setFileName(fileLocation);
        badCounter++;
    }


    QFile source(tempRadiographLocation());
    source.copy(fileLocation);
}

QString CDAquireRadiograph::getFakeExposure(QString patientID, RadiographType rType)
{
    QString destURLString = CDFileLocations::getRadiographDir(patientID);
    QDate today = QDate::currentDate();
    destURLString += today.toString("MMM/d/yyyy");

    QDir destDir(destURLString);
    if(!destDir.exists()) {
        destDir.mkpath(".");
    }

    QImage saveMe(1920,1080,QImage::Format_Grayscale16);
    QString saveText = "";
    for(int y=0;y<1080;y++) {
        for(int x=0;x<1920;x++) {
            quint16 setVal = QRandomGenerator::global()->bounded(65535);
            QRgba64 setRGBA = QRgba64::fromRgba64(setVal,setVal,setVal,65535);
            QColor setColor = QColor::fromRgba64(setRGBA);
            saveMe.setPixelColor(x,y,setColor);

            saveText += QString::number(setVal);
        }
    }

    QPoint center = saveMe.rect().center();
    QTransform qtrans;
    qtrans.translate(center.x(),center.y());
    qtrans.rotate(getRotationAmount(rType));
//    QMatrix matrix;
//    matrix.translate(center.x(), center.y());
//    matrix.rotate(getRotationAmount(rType));
    saveMe = saveMe.transformed(qtrans);

    QImageWriter writer(destURLString+ "/" + getStringFromType(rType) + ".tiff");
    writer.setCompression(1);
    writer.setText("Raw Data", saveText);
    writer.write(saveMe);

    return destURLString+ "/" + getStringFromType(rType);
}

bool CDAquireRadiograph::allowFakeExposures()
{
#ifdef QT_DEBUG
    return true;
#else
    return false;
#endif
}

QString CDAquireRadiograph::tempRadiographLocation()
{
    return "/var/tmp/radiographImage.png";
}

bool CDAquireRadiograph::hasCalibration(QString serialID)
{
    QSettings calFile(CDFileLocations::getLocalSensorCalibrationFile(),QSettings::IniFormat);
    return calFile.contains(serialID);
}

void CDAquireRadiograph::takeCalibrationExposure(int sensorIndex, int currentIndex, int calibrationCount)
{
    //TODO: need to make it whole thing
    Q_UNUSED(sensorIndex);
    Q_UNUSED(currentIndex);
    Q_UNUSED(calibrationCount);
}

int CDAquireRadiograph::getRotationAmount(CDAquireRadiograph::RadiographType rType)
{
    int returnMe =0;
    switch (rType) {
    case BW_Right_Distal:
    case BW_Right_Mesial:
        returnMe = -90;
        break;
    case BW_Left_Mesial:
    case BW_Left_Distal:
        returnMe = 90;
        break;
    case PA_Maxillary_Anterior_Right:
    case PA_Maxillary_Anterior_Center:
    case PA_Maxillary_Anterior_Left:
    case PA_Maxillary_Right_Distal:
    case PA_Maxillary_Right_Mesial:
    case PA_Maxillary_Left_Mesial:
    case PA_Maxillary_Left_Distal:
        returnMe = 0;
        break;
    case PA_Mandibular_Anterior_Left:
    case PA_Mandibular_Anterior_Center:
    case PA_Mandibular_Anterior_Right:
    case PA_Mandibular_Right_Mesial:
    case PA_Mandibular_Right_Distal:
    case PA_Mandibular_Left_Distal:
    case PA_Mandibular_Left_Mesial:
        returnMe = 180;
        break;
    case Panoramic:
    case FMX_SINGLE_IMAGE:
    default:
        returnMe =0;
        break;
    }
    return returnMe;
}

QString CDAquireRadiograph::getStringFromType(CDAquireRadiograph::RadiographType rType)
{
    QString returnMe = "";
    switch (rType) {
    case BW_Right_Distal:
        returnMe = "BW_Right_Distal";
        break;
    case BW_Right_Mesial:
        returnMe = "BW_Right_Mesial";
        break;
    case BW_Left_Mesial:
        returnMe = "BW_Left_Mesial";
        break;
    case BW_Left_Distal:
        returnMe = "BW_Left_Distal";
        break;
    case PA_Maxillary_Right_Distal:
        returnMe = "PA_Maxillary_Right_Distal";
        break;
    case PA_Maxillary_Right_Mesial:
        returnMe = "PA_Maxillary_Right_Mesial";
        break;
    case PA_Maxillary_Anterior_Right:
        returnMe = "PA_Maxillary_Anterior_Right";
        break;
    case PA_Maxillary_Anterior_Center:
        returnMe = "PA_Maxillary_Anterior_Center";
        break;
    case PA_Maxillary_Anterior_Left:
        returnMe = "PA_Maxillary_Anterior_Left";
        break;
    case PA_Maxillary_Left_Mesial:
        returnMe = "PA_Maxillary_Left_Mesial";
        break;
    case PA_Maxillary_Left_Distal:
        returnMe = "PA_Maxillary_Left_Distal";
        break;
    case PA_Mandibular_Left_Distal:
        returnMe = "PA_Mandibular_Left_Distal";
        break;
    case PA_Mandibular_Left_Mesial:
        returnMe = "PA_Mandibular_Left_Mesial";
        break;
    case PA_Mandibular_Anterior_Left:
        returnMe = "PA_Mandibular_Anterior_Left";
        break;
    case PA_Mandibular_Anterior_Center:
        returnMe = "PA_Mandibular_Anterior_Center";
        break;
    case PA_Mandibular_Anterior_Right:
        returnMe = "PA_Mandibular_Anterior_Right";
        break;
    case PA_Mandibular_Right_Mesial:
        returnMe = "PA_Mandibular_Right_Mesial";
        break;
    case PA_Mandibular_Right_Distal:
        returnMe = "PA_Mandibular_Right_Distal";
        break;
    case Panoramic:
        returnMe = "Panoramic";
        break;
    case FMX_SINGLE_IMAGE:
        returnMe = "FMX";
        break;
    default:
        returnMe = "Other";
        break;
    }


    return returnMe;

}

//What to show the end user (because having '_' in the name looks werid)
QString CDAquireRadiograph::getUserStringFromType(CDAquireRadiograph::RadiographType rType)
{
    QString underscore = getStringFromType(rType);
    return underscore.replace("_"," ");
}

QList<CDAquireRadiograph::RadiographType> CDAquireRadiograph::getTypesFromToothNumber(QString getTooth)
{
    QList<CDAquireRadiograph::RadiographType> returnMe;

    //qDebug()<<"getTooth: "<<getTooth;

    if(getTooth.length() < 1) {
        return returnMe;
    }

    //First check to see if it is a pediatric tooth
    bool isAdult;
    int toothNumber = getTooth.toInt(&isAdult);

    if(isAdult) {
        if(toothNumber <= 0) {
            for(int typeCounter = INVALID_RADIOGRAPH_TYPE+1;typeCounter<=PA_Mandibular_Right_Distal;typeCounter++) {
                returnMe.append((RadiographType)typeCounter);
            } //no need to return early since the switch will go straight to default
        }

        switch (toothNumber) {
        case 1:
        case 2:
        case 3:
            returnMe.append(PA_Maxillary_Right_Distal);
            returnMe.append(BW_Right_Distal);
            break;
        case 4:
        case 5:
            returnMe.append(PA_Maxillary_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            break;
        case 6:
        case 7:
            returnMe.append(PA_Maxillary_Anterior_Right);
            returnMe.append(PA_Maxillary_Anterior_Center);
            break;
        case 8:
            returnMe.append(PA_Maxillary_Anterior_Right);
            returnMe.append(PA_Maxillary_Anterior_Center);
            break;
        case 9:
        case 10:
            returnMe.append(PA_Maxillary_Anterior_Center);
            returnMe.append(PA_Maxillary_Anterior_Left);
            break;
        case 11:
            returnMe.append(PA_Maxillary_Anterior_Left);
            break;
        case 12:
        case 13:
            returnMe.append(PA_Maxillary_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            break;
        case 14:
        case 15:
        case 16:
            returnMe.append(PA_Maxillary_Left_Distal);
            returnMe.append(BW_Left_Distal);
            break;
        case 17:
        case 18:
        case 19:
            returnMe.append(PA_Mandibular_Left_Distal);
            returnMe.append(BW_Left_Distal);
            break;
        case 20:
        case 21:
            returnMe.append(PA_Mandibular_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            break;
        case 22:
        case 23:
        case 24:
            returnMe.append(PA_Mandibular_Anterior_Left);
            returnMe.append(PA_Mandibular_Anterior_Center);
            break;
        case 25:
        case 26:
        case 27:
            returnMe.append(PA_Mandibular_Anterior_Right);
            returnMe.append(PA_Mandibular_Anterior_Center);
            break;
        case 28:
        case 29:
            returnMe.append(PA_Mandibular_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            break;
        case 30:
        case 31:
        case 32:
            returnMe.append(PA_Mandibular_Right_Distal);
            returnMe.append(BW_Right_Distal);
            break;
        default:
            break;
        }
    }

    else { //is pediatric
        char toothChar = getTooth.at(0).toUpper().toLatin1();
        switch(toothChar) {
        case 'A':
        case 'B':
            returnMe.append(PA_Maxillary_Right_Distal);
            returnMe.append(BW_Right_Distal);
            returnMe.append(PA_Maxillary_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            break;
        case 'C':
            returnMe.append(PA_Maxillary_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            returnMe.append(PA_Maxillary_Anterior_Right);
            returnMe.append(PA_Maxillary_Anterior_Center);
            break;
        case 'D':
            returnMe.append(PA_Maxillary_Anterior_Right);
            returnMe.append(PA_Maxillary_Anterior_Center);
            break;
        case 'E':
        case 'F':
            returnMe.append(PA_Maxillary_Anterior_Right);
            returnMe.append(PA_Maxillary_Anterior_Center);
            returnMe.append(PA_Maxillary_Anterior_Left);
            break;
        case 'G':
            returnMe.append(PA_Maxillary_Anterior_Center);
            returnMe.append(PA_Maxillary_Anterior_Left);
            break;
        case 'H':
            returnMe.append(PA_Maxillary_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            returnMe.append(PA_Maxillary_Anterior_Left);
            returnMe.append(PA_Maxillary_Anterior_Center);
            break;
        case 'I':
        case 'J':
            returnMe.append(PA_Maxillary_Left_Distal);
            returnMe.append(BW_Left_Distal);
            returnMe.append(PA_Maxillary_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            break;

        case 'K':
        case 'L':
            returnMe.append(PA_Mandibular_Left_Distal);
            returnMe.append(BW_Left_Distal);
            returnMe.append(PA_Mandibular_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            break;
        case 'M':
            returnMe.append(PA_Mandibular_Left_Mesial);
            returnMe.append(BW_Left_Mesial);
            returnMe.append(PA_Mandibular_Anterior_Left);
            returnMe.append(PA_Mandibular_Anterior_Center);
            break;
        case 'N':
            returnMe.append(PA_Mandibular_Anterior_Center);
            returnMe.append(PA_Mandibular_Anterior_Left);
            break;
        case 'O':
        case 'P':
            returnMe.append(PA_Mandibular_Anterior_Right);
            returnMe.append(PA_Mandibular_Anterior_Center);
            returnMe.append(PA_Mandibular_Anterior_Left);
            break;
        case 'Q':
            returnMe.append(PA_Mandibular_Anterior_Center);
            returnMe.append(PA_Mandibular_Anterior_Right);
            break;
        case 'R':
            returnMe.append(PA_Mandibular_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            returnMe.append(PA_Mandibular_Anterior_Right);
            returnMe.append(PA_Mandibular_Anterior_Center);
            break;
        case 'T':
        case 'S':
            returnMe.append(PA_Mandibular_Right_Distal);
            returnMe.append(BW_Right_Distal);
            returnMe.append(PA_Mandibular_Right_Mesial);
            returnMe.append(BW_Right_Mesial);
            break;
        default:
            break;
        }

    }

    returnMe.append(FMX_SINGLE_IMAGE);
    returnMe.append(Panoramic);

    return returnMe;
}

QVariantList CDAquireRadiograph::getQMLTypesFromToothNumber(QString getTooth)
{
    QList<CDAquireRadiograph::RadiographType> origList = getTypesFromToothNumber(getTooth);
    QVariantList returnMe;
    foreach(CDAquireRadiograph::RadiographType rType, origList) {
        returnMe.append(rType);
    }

    return returnMe;
}


void CDAquireRadiograph::setCalibration(QString serialID, QVector<quint16> calibration)
{
    QSettings calFile(CDFileLocations::getLocalSensorCalibrationFile(),QSettings::IniFormat);
    QString setMe = "";
    foreach(quint16 val, calibration) {
        setMe += QString::number(val) + ",";
    }
    calFile.setValue(serialID,setMe);
    calFile.sync();
}

QVector<quint16> CDAquireRadiograph::getCalibration(QString serialID)
{
    QSettings calFile(CDFileLocations::getLocalSensorCalibrationFile(),QSettings::IniFormat);
    QVector<quint16> returnMe;
    QString stringVals = calFile.value(serialID,",").toString();

    foreach(QString stringVal, stringVals.split(",")) {
        if(stringVal.length() > 0) {
            quint16 val = stringVal.toInt();
            returnMe.append(val);
        }
    }

    return returnMe;
}

void CDAquireRadiograph::handleBlackResult(int sensorID, QVector<quint16> result)
{
    m_blackVals[sensorID] = result;

    m_sensorStatus[sensorID] = SENDING_REQUEST;
    emit sensorStatusUpdated(sensorID,SENDING_REQUEST);
    m_workers[sensorID]->wait();

    m_workers[sensorID]->setRequestType(CDSensorWorker::EXPOSED_IMAGE);
    m_workers[sensorID]->start();
    //We should already have the connections set up so it will call handleExposureResult() when it is done
    emit sensorStatusUpdated(sensorID,WAITING_FOR_RESULT);
}

void CDAquireRadiograph::handleExposureResult(int sensorID, QVector<quint16> result)
{
    emit sensorStatusUpdated(sensorID,READING_RESULT);

    QVector<quint16> theFinalAnswer16;

    //https://en.wikipedia.org/wiki/Flat-field_correction#Flat_field_correction_in_X-ray_imaging
//    if(hasCalibration(inst->getSerialNumber(sensorIndex))) {
//        QList<quint16> calibrationVals = getCalibration(inst->getSerialNumber(sensorIndex));
//        for(int i=0;i<exposeVals.length();i++) {
//            quint16 addMe=1;
//            if(blackVals.at(i) != calibrationVals.at(i)) {
//                addMe = (exposeVals.at(i) - blackVals.at(i)) / (calibrationVals.at(i) - blackVals.at(i) );
//            }
//            theFinalAnswer16.append(addMe);
//        }
//    }
//    else {
//        for(int i=0;i<blackVals.length();i++) {
//            quint16 addMe = exposeVals.at(i) - blackVals.at(i);
//            theFinalAnswer16.append(addMe);
//        }
//    }

    QVector<quint16> blackVals = m_blackVals[sensorID];


    for(int i=0;i<blackVals.length();i++) {
        quint16 addMe = result.at(i) - blackVals.at(i);
        theFinalAnswer16.append(addMe);
    }

    emit sensorStatusUpdated(sensorID,PROCESSING_RESULT);

    QVector<quint16> sortedVals = theFinalAnswer16;
    std::sort(sortedVals.begin(),sortedVals.end());
    quint16 lowCut = sortedVals.at((int)(sortedVals.length() * .05));
    quint16 highCut = sortedVals.at((int)(sortedVals.length() * .90));
    quint16 range = highCut - lowCut;


    qDebug()<<"Count: " << theFinalAnswer16.length();
    qDebug()<<"lowCut: " << lowCut;
    qDebug()<<"highCut: " << highCut;
    qDebug()<<"range: " << range;

    CDSensorInterface *inst = CDSensorInterface::getInstance();
    int setWidth =inst->getSensorWidth(sensorID);
    int setHeight =inst->getSensorHeight(sensorID);

    QImage saveMe(setWidth,setHeight,QImage::Format_Grayscale16);
    QString saveText = "";
    int i=0;
    for(int y=0;y<setHeight;y++) {
        for(int x=0;x<setWidth;x++) {
            quint16 val = theFinalAnswer16.at(i);
            int pixVal=0;
            if(val > highCut) {
                pixVal = 65535;
            }
            else if (val < lowCut) {
                pixVal = 0;
            }
            else {
                qreal fract = ((qreal) val - lowCut) / range ;
                //qDebug()<<fract;
                pixVal =(int) (65535 * fract);
            }

            QColor setColor = QColor::fromRgba64(pixVal,pixVal,pixVal);
            saveMe.setPixelColor(x,y,setColor);

            //saveText += QString::number(setVal);
            i++;
        }
    }
    saveMe.invertPixels();

    QPoint center = saveMe.rect().center();

    QTransform qtrans;
    qtrans.translate(center.x(),center.y());
    qtrans.rotate(getRotationAmount(m_radiographRequestTypes[sensorID]));
    /*QMatrix matrix;
    matrix.translate(center.x(), center.y());
    matrix.rotate(getRotationAmount(rType));*/
    saveMe = saveMe.transformed(qtrans);

    saveMe.save(tempRadiographLocation());

    QDir reviewDir(QDir::homePath() + "/radioReview/");
    if(reviewDir.exists()) {
        i=0;
        QImage saveRawBlack(setWidth,setHeight,QImage::Format_Grayscale16);
        for(int y=0;y<setHeight;y++) {
            for(int x=0;x<setWidth;x++) {
                quint16 val = blackVals.at(i);
                QColor setColor = QColor::fromRgba64(val,val,val);
                saveRawBlack.setPixelColor(x,y,setColor);
                i++;
            }
        }
        QString blackFN = QDir::homePath() + "/radioReview/black-" +
                QString::number(QDateTime::currentSecsSinceEpoch()) + ".png";
        saveRawBlack.save(blackFN);

        i=0;
        QImage saveRawWhite(setWidth,setHeight,QImage::Format_Grayscale16);
        for(int y=0;y<setHeight;y++) {
            for(int x=0;x<setWidth;x++) {
                quint16 val = result.at(i);
                QColor setColor = QColor::fromRgba64(val,val,val);
                saveRawWhite.setPixelColor(x,y,setColor);
                i++;
            }
        }
        QString whiteFN = QDir::homePath() + "/radioReview/white-" +
                QString::number(QDateTime::currentSecsSinceEpoch()) + ".png";
        saveRawWhite.save(whiteFN);


        i=0;
        QImage saveRawDiff(setWidth,setHeight,QImage::Format_Grayscale16);
        for(int y=0;y<setHeight;y++) {
            for(int x=0;x<setWidth;x++) {
                quint16 val = theFinalAnswer16.at(i);
                QColor setColor = QColor::fromRgb(val,val,val);
                saveRawDiff.setPixelColor(x,y,setColor);
                i++;
            }
        }
        QString diffFN = QDir::homePath() + "/radioReview/diff-" +
                QString::number(QDateTime::currentSecsSinceEpoch()) + ".png";
        saveRawDiff.save(diffFN);
    }

//    QImageWriter writer(tempRadiographLocation());
//    writer.setCompression(1);
//    writer.setText("Raw Data", saveText);
//    writer.write(saveMe);

    emit sensorStatusUpdated(sensorID,AWAITING_USER_INPUT);

}
