// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDIMAGEMANAGER_H
#define CDIMAGEMANAGER_H

#include <QQuickImageProvider>


class CDImageManager : public QQuickImageProvider
{
public:
    CDImageManager();
    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // CDIMAGEMANAGER_H
