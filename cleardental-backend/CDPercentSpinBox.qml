// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4

SpinBox {
    from : 0
    to: 100
    stepSize: 10
    editable: true

    textFromValue: function(getValue) {
        return getValue + "%"
    }

    valueFromText: function(getText) {
        getText = getText.replace("%","");
        return parseInt(getText);
    }
}
