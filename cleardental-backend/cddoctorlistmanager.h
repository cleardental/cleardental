// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// This class should ready be called "CDProviderListManager"
// but it would be too much work to refactor all of that.
// Sorry.

#ifndef CDDOCTORLISTMANAGER_H
#define CDDOCTORLISTMANAGER_H

#include <QObject>
#include <QVariantMap>

class CDDoctorListManager : public QObject
{
    Q_OBJECT
public:
    explicit CDDoctorListManager(QObject *parent = nullptr);

    Q_INVOKABLE static QStringList getListOfProviderFiles();
    Q_INVOKABLE static QStringList getListOfStaffFiles();
    Q_INVOKABLE static QString addProvider(QVariantMap data);
    Q_INVOKABLE static void removeProvider(QString providerID);
    Q_INVOKABLE static void makeDefaultProvider(QString providerID);
    Q_INVOKABLE static QString getDefaultProviderID();

    Q_INVOKABLE static QString currentProvider(); //yes, it is implemented somewhere else but just to make it easier

signals:

public slots:
};

#endif // CDDOCTORLISTMANAGER_H
