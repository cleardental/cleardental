// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdfeeschedulemanager.h"

#include <QSettings>

#include "cddefaults.h"
#include "cdlocationmanager.h"
#include "cdfilelocations.h"

CDFeeScheduleManager::CDFeeScheduleManager(QObject *parent)
    : QObject{parent}
{

}

QStringList CDFeeScheduleManager::getListOfPlans(QString pracID)
{
    QSettings readFees(CDFileLocations::getPracticeFeesFile(pracID),QSettings::IniFormat);
    return readFees.childGroups();
}

QStringList CDFeeScheduleManager::getLocalListOfPlans()
{
    return getListOfPlans(CDLocationManager::getLocalLocationName());

}
