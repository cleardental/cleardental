// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdsensorworker.h"

#include <QDebug>

#include "cdsensorinterface.h"


CDSensorWorker::CDSensorWorker(QObject *parent)
    : QThread{parent}
{
    m_deviceIndex = -1;
    m_requestType = INVALID_REQUEST;
}

void CDSensorWorker::run()
{
    CDSensorInterface *sInter = CDSensorInterface::getInstance();
    if(m_requestType == BLACK_IMAGE) {
        m_results = sInter->requestBlackImage(m_deviceIndex);
        emit gotBlackImage(m_deviceIndex,m_results);
    }
    else if(m_requestType == EXPOSED_IMAGE) {
        m_results = sInter->requestWhiteImage(m_deviceIndex);
        emit gotExposedImage(m_deviceIndex,m_results);
    }
}

void CDSensorWorker::setDeviceIndex(int setIndex)
{
    m_deviceIndex = setIndex;
}

void CDSensorWorker::setRequestType(RequestType getRequest)
{
    m_requestType = getRequest;
}

QVector<quint16> CDSensorWorker::getResults()
{
    return m_results;
}
