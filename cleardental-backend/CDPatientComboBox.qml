// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

TextField {
    id: patientListComboBox

    property alias boxPop: popList

    CDPatientManager {id: patMan}

    CDFileLocations {id: fLocs}

    Settings {id: iniReader}

    function update() {
        if(text.length > 2) {
            if(!popList.visible) {
                popList.open();
            }
        }
        if(popList.visible) {
            patListView.updateList();
        }
    }

    onTextEdited: {
        update();
    }

    selectByMouse: true

    signal patientSelected();

    Popup {
        id: popList
        width: patientListComboBox.width
        y: patientListComboBox.height
        height: 300


        ListView {
            id: patListView

            property var patList: []

            function updateList() {
                var patListJS = patMan.getAllPatients();

                model = 0;
                patList = [];
                for(var i=0;i<patListJS.length;i++) {
                    var pat = patListJS[i];
                    iniReader.fileName = fLocs.getPersonalFile(pat["PatID"]);
                    iniReader.category = "Name";
                    var patIDLower = pat["PatID"].toLowerCase();
                    var inputLower = patientListComboBox.text.toLowerCase();
                    var nicknameLower = iniReader.value("PreferredName","").toLowerCase();
                    var firstnameLower = iniReader.value("FirstName","").toLowerCase();
                    var lastnameLower = iniReader.value("LastName","").toLowerCase();
                    if(patIDLower.includes(inputLower)) {
                        patList.push(pat["PatID"]);
                    } else if (nicknameLower.includes(inputLower)) {
                        patList.push(pat["PatID"]);
                    }
                    else if (firstnameLower.includes(inputLower)) {
                        patList.push(pat["PatID"]);
                    }
                    else if (lastnameLower.includes(inputLower)) {
                        patList.push(pat["PatID"]);
                    }
                }
                model = patList.length
            }

            width: parent.width
            height: parent.height
            model: patList.length
            //clip: true
            delegate: Button {
                text: patListView.patList[index]
                flat: true
                width: patientListComboBox.boxPop.width - 10
                onClicked: {
                    patientListComboBox.text = patListView.patList[index];
                    popList.close();
                    patientSelected();
                }

                Image {
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    source: "file://" + fLocs.getProfileImageFile(patListView.patList[index])
                    fillMode: Image.PreserveAspectFit
                    opacity: 0.50
                }
            }
            ScrollBar.vertical: ScrollBar { }
        }

    }

}
