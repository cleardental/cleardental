// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDPRINTER_H
#define CDPRINTER_H

#include <QObject>
#include <QDate>
#include <QVariantMap>
#include <QVariantList>

class CDPrinter : public QObject
{
    Q_OBJECT
public:
    explicit CDPrinter(QObject *parent = nullptr);

    Q_INVOKABLE static void printRx(QVariantMap inputData);
    Q_INVOKABLE static void printPostOpInstr(QString opName);
    Q_INVOKABLE static void printReminder(QString patID, QDate when);
    Q_INVOKABLE static void printClaimForm(QVariantList inputList);
    Q_INVOKABLE static void printClaimForm(QString patientID, QVariantList claims);
    Q_INVOKABLE static void printTreatmentPlan(QString patID, QVariantMap phaseList);
    Q_INVOKABLE static void printCaseNotes(QVariantList caseNotesItems);
    Q_INVOKABLE static void printPatientLedger(QString patID, QVariantList procedureList);
    Q_INVOKABLE static QVariantList printPeriodontalChart(QString patID, bool reallyPrint=true);


signals:

public slots:

private:
    static QChar getProcedureCoverageType(QString DCode);
    static bool isMissing(QString patID, QString toothNumber);
    static QString formatDate(QString inputString);
    static QString formatMoney(QString inputString);
};

#endif // CDPRINTER_H
