// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdproviderinfo.h"

#include <QProcess>
#include <QFile>
#include <QDebug>

CDProviderInfo::CDProviderInfo(QObject *parent) : QObject(parent) {}

QString CDProviderInfo::getCurrentProviderUsername() {
    QString name = qgetenv("USER");
    if (name.isEmpty()) {
        name = qgetenv("USERNAME");
    }

    return name;
}

QString CDProviderInfo::getCurrentProviderFullName()
{
    QString returnMe="";
    QString username = getCurrentProviderUsername();
    QFile readUsers("/etc/passwd");
    readUsers.open(QIODevice::ReadOnly | QIODevice::Text);
    bool foundIt = false;
    while((!foundIt) && (!readUsers.atEnd())) {
        QString line = readUsers.readLine();
        if(line.startsWith(username)) {
            foundIt = true;
            QStringList lineParts = line.split(":");
            returnMe = lineParts.at(4);
            returnMe = returnMe.replace(",","");
        }
    }

    return returnMe;
}

QString CDProviderInfo::signData(QString data)
{
    QString returnMe = "";

    QFile tempFile("/tmp/signMe");
    tempFile.open(QIODevice::WriteOnly | QIODevice::Text);
    tempFile.write(data.toLocal8Bit());
    tempFile.close();

    QProcess gpg;
    gpg.setProgram("/usr/bin/gpg");
    QStringList arguments;
    arguments<<"--clearsign";
    arguments<<"--yes";
    arguments<<"/tmp/signMe";
    gpg.setArguments(arguments);
    gpg.start();
    gpg.waitForFinished();

    tempFile.setFileName("/tmp/signMe.asc");
    tempFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QByteArray signedData = tempFile.readAll();
    tempFile.close();

    QString strSigned = signedData;
    int startPoint = strSigned.indexOf("-----BEGIN PGP SIGNATURE-----");
    returnMe = strSigned.mid(startPoint);

    return returnMe;
}
