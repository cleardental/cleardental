// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: schDateTimePane
    property string currentChair
    property date currentDate
    property string currentStartTime: ""
    property int currentApptDuration

    backMaterialColor: Material.Cyan

    CDScheduleManager {
        id: scheduleDB
    }

    CDFileLocations {
        id: m_fileLocs
    }

    Component {
        id: radComp
        RadioButton {
            onClicked: {
                schDateTimePane.currentStartTime = text
            }
        }
    }

    Settings {
        id: pracSettings
        fileName: m_fileLocs.getLocalPracticePreferenceFile()
        category: "Hours"
    }

    function generateAvailableTimes() {
        var availTimes = [];
        radioRow.children = "";
        schDateTimePane.currentStartTime = "";

        //First check if we are even open that day...
        var weekStrings= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        schDateTimePane.currentDate = datePicker.myDate;
        schDateTimePane.currentChair = chairBox.currentText;
        schDateTimePane.currentApptDuration = parseInt((durationBox.currentIndex + 1) *15);
        if(schDateTimePane.currentApptDuration <= 0) {
            return;
        }
        var dayOfWeekStr = "Open" + weekStrings[schDateTimePane.currentDate.getDay()];
        var isOpen = JSON.parse(pracSettings.value(dayOfWeekStr));
        if(isOpen) {
            var hoursArray = pracSettings.value(weekStrings[schDateTimePane.currentDate.getDay()] + "Hours").split("|");

            var openTimeMins=0;
            var closedTimeMins=0;
            var breakTimeStartMins=0;
            var breakTimeEndMins=0;
            for(var hI=0;hI < hoursArray.length;hI++) {
                var hoursItem = hoursArray[hI].trim().split(" ");
                //console.debug(hoursItem);
                if(hoursItem[0] === "Open") {
                    openTimeMins = scheduleDB.convertTimeToMinsSinceMidnight(hoursItem[1] + " " + hoursItem[2]);
                }
                else if(hoursItem[0] === "Closed") {
                    closedTimeMins = scheduleDB.convertTimeToMinsSinceMidnight(hoursItem[1] + " " + hoursItem[2]);

                }
                else if(hoursItem[0] === "Lunch") {
                    breakTimeStartMins = scheduleDB.convertTimeToMinsSinceMidnight(hoursItem[6] + " " + hoursItem[7]);
                    breakTimeEndMins = breakTimeStartMins + parseInt(hoursItem[3]);
                }
            }

            if(breakTimeStartMins === 0) {
                breakTimeStartMins = closedTimeMins;
                breakTimeEndMins = closedTimeMins+1;
            }

            for(var minCounter=openTimeMins;minCounter<= (breakTimeStartMins-schDateTimePane.currentApptDuration);
                minCounter+=30) {
                var apptsInTime = scheduleDB.getAppointments(schDateTimePane.currentDate,
                                                             schDateTimePane.currentChair,
                                                             minCounter, schDateTimePane.currentApptDuration);
                if((doubleBookBox.checked) || (apptsInTime.length === 0)) {
                    availTimes.push(scheduleDB.convertMinsSinceMidnightToString(minCounter));
                }
            }

            for(minCounter=breakTimeEndMins;minCounter<= (closedTimeMins-schDateTimePane.currentApptDuration);
                minCounter+=30) {
                apptsInTime = scheduleDB.getAppointments(schDateTimePane.currentDate,
                                                             schDateTimePane.currentChair,
                                                             minCounter, schDateTimePane.currentApptDuration);
                if((doubleBookBox.checked) || (apptsInTime.length === 0)) {
                    availTimes.push(scheduleDB.convertMinsSinceMidnightToString(minCounter));
                }
            }
        }

        for(var availCounter=0;availCounter < availTimes.length;availCounter++) {
            var radObj = radComp.createObject(radioRow);
            radObj.text = availTimes[availCounter];
        }

        timeWarning.visible = availTimes.length ===0;
        timeFlick.visible = availTimes.length > 0;
    }


    ColumnLayout {
        CDHeaderLabel {
            text: "Appointment Date and Time"
        }
        CDCalendarDatePicker {
            id: datePicker
            onMyDateChanged: {
                var chairArray = scheduleDB.getChairs(datePicker.myDate)
                var setMyModel= [];
                for(var i=0;i<chairArray.length;i++) {
                    setMyModel.push(chairArray[i]);
                }
                chairBox.model = setMyModel;
                onCurrentIndexChanged: generateAvailableTimes();
            }
        }

        GridLayout {
            columns: 2
            Layout.fillWidth: true
            CDDescLabel {
                text: "Chair"
            }
            ComboBox {
                id: chairBox
                Layout.minimumWidth: 200
                onCurrentTextChanged: generateAvailableTimes();
            }

            CDDescLabel {
                text: "Duration"
            }

            ComboBox {
                id: durationBox
                property var durModel: []
                Layout.minimumWidth: 200
                onCurrentIndexChanged: {generateAvailableTimes();}
                Component.onCompleted:  {
                    for(var i=15;i<=180;i+=15) {
                        var hours = Math.floor(i / 60);
                        var mins = i%60;
                        if(hours === 0) {
                            durModel.push(mins + " minutes")
                        }
                        else if(hours === 1) {
                            if(mins === 0) {
                                durModel.push("1 hour")
                            }
                            else {
                                durModel.push("1 hour and " + mins + " minutes")
                            }
                        }
                        else {
                            if(mins === 0) {
                                durModel.push(hours + " hours")
                            }
                            else {
                                durModel.push(hours + " hours and " + mins + " minutes")
                            }
                        }
                    }
                    model = durModel
                    currentIndex =3;
                }
            }

            CheckBox {
                id: doubleBookBox
                Layout.columnSpan: 2
                text: "Double book patient"
                checked: false
                onCheckedChanged: {generateAvailableTimes();}
            }

            CDDescLabel {
                text: "Available Times:"
            }

            Label {
                id: timeWarning
                text: "No available times"
                color: Material.color(Material.Red)
                visible: false
            }

            Flickable {
                id: timeFlick
                Layout.fillWidth: true
                clip: true
                contentWidth: radioRow.width
                //width: 200
                height: durationBox.height

                RowLayout {
                    id: radioRow
                }
            }

        }
    }

}
