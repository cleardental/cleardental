// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 7/9/2022 Alex Vernes

#ifndef CDGITMANAGER_H
#define CDGITMANAGER_H

#include <QObject>
#include <QVariant>
#include <QDate>

class CDGitManager : public QObject
{
    Q_OBJECT
public:
    enum FilterProcedureType {
        ALL_PROCEDURES,
        TEMP_CASE_NOTES,
        SIGNED_CASE_NOTES,
    };
    Q_ENUM(FilterProcedureType)


    explicit CDGitManager(QObject *parent = nullptr);


signals:

public slots:
    static QString commitData(QString comment);
    static QString commitAndSignData(QString comment);
    static QString pullData();
    static QString runCommit(QString comment, bool signData);
    static void initRepo();

private:
    static void checkRepo();
    static QStringList generatePullArgs();
};

#endif // CDGITMANAGER_H
