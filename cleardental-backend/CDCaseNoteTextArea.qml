// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

TextArea {
    id: caseNote
    font.family: "Monda"
    property int setLeft: -1;
    property int setRight: -1
    property string setWord: ""
    selectByMouse: true


    CDSpellChecker {
        id: speller
    }

    onVisibleChanged: {
        if(visible) {
            speller.injectSyntaxHigh(textDocument);
        }
    }

    Popup {
        id: suggestionPopper
        width: 150
        height: 300
        background: Rectangle {
            color: "white"
            opacity: 0.7
            radius: 10

        }

        ListView {
            id: suggestListView
            width: 150
            height: 300
            clip: true
            property var suggestionList;
            delegate: Button {
                id: selectSuggestionButton
                text: suggestListView.suggestionList[index]
                flat: true
                width: suggestListView.width -20
                onClicked: {
                    var original = caseNote.text;
                    var toTheLeft = original.substring(0,caseNote.setLeft);
                    var toTheRight = original.substring(caseNote.setRight);
                    caseNote.text = toTheLeft + selectSuggestionButton.text + toTheRight;
                    suggestionPopper.close();
                }
            }
        }

    }

    onPressAndHold: {
        var getPos = positionAt(event.x,event.y);
        var goOn = true;
        for(setLeft = getPos;goOn;setLeft-- ) {
            if((text[setLeft] === " ") || (text[setLeft] === ".") || (text[setLeft] === "\n")) {
                goOn = false;
            }
            else if(setLeft ===0) {
                setLeft = -2;
                goOn = false;
            }
        }
        setLeft+=2;
        goOn = true;
        for(setRight = getPos;goOn;setRight++ ) {
            if((text[setRight] === " ") || (text[setRight] === ".") || (text[setRight] === "\n")) {
                goOn = false;
            }
            else if(setRight >= text.length) {
                goOn = false;
            }
        }
        setRight--;
        setWord= getText(setLeft,setRight);
        var suggestList = speller.suggest(setWord);
        //console.debug(suggestList);
        if(suggestList.length > 0) {
            suggestListView.suggestionList = suggestList;
            suggestListView.model = suggestList.length;
            suggestionPopper.x = event.x;
            suggestionPopper.y = event.y;
            suggestionPopper.open();
        }
    }
}
