// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick 2.2 as QQ2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

import Qt3D.Core 2.0
import Qt3D.Render 2.14
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Material {
    id: metalMaterial

    property real alphaAmount: .5



    effect:  Effect {
        id: effect

        parameters: [
            Parameter {
                name: "iResolution"
                value: Qt.vector2d(toothScene3D.width,toothScene3D.height)
            },

            Parameter {
                name: "fAlphaAmount"
                value: alphaAmount
            }

        ]


        techniques: [
            Technique {
                id: gl3Technique
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }

                filterKeys: [ FilterKey { name: "renderingStyle"; value: "forward" } ]
                renderPasses: [
                    RenderPass {
                        id: gl3Pass
                        shaderProgram: ShaderProgram {
                            vertexShaderCode: loadSource("qrc:/MetalVertexShader.vsh")
                            fragmentShaderCode: loadSource("qrc:/MetalFragmentShader.fsh")
                        }
                    }
                ]
            }
        ]
    }
}
