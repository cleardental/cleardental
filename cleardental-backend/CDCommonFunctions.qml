// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

QtObject {
    property var m_FileLocs: CDFileLocations {}
    property var m_Settings: Settings {}
    property var m_textFileMan: CDTextFileManager{}
    property var m_gitMan: CDGitManager{}
    property var m_constLoader: CDConstLoader{}
    property bool m_loadedNames: false
    property var m_txPlanNames: []

    function getYearsOld(patientID) {
        m_Settings.fileName = m_FileLocs.getPersonalFile(patientID);
        m_Settings.category = "Personal";
        var dob = m_Settings.value("DateOfBirth").split("/");
        var dobDate = new Date();
        dobDate.setMonth(dob[0]-1,dob[1]);
        dobDate.setYear(dob[2]);
        var now = new Date();
        var diff = new Date(now - dobDate);
        return Math.abs(diff.getUTCFullYear() - 1970);
    }

    function updateReviewFile(reviewKey, patientID) {
        m_Settings.fileName = m_FileLocs.getReviewsFile(patientID);
        var today = new Date();
        var dateString = (today.getMonth() +1) + "/" + (today.getDate()) +
                "/" + today.getFullYear();
        m_Settings.setValue(reviewKey,dateString);
        m_Settings.sync();
    }

    function makeHardTissueString(getAttrs) {
        if(getAttrs.length === 1) { //single item
            return getAttrs[0].trim();
        }
        else {
            for(var i=0;i<getAttrs.length;i++) {
                var trimmedVal = getAttrs[i].trim();
                var makeVal = trimmedVal;
                if(i === 0) { //First item
                    makeVal = trimmedVal + " ";
                }
                else if(i === (getAttrs.length-1)) { //last item
                    makeVal = " " + trimmedVal;
                }
                else { //middle item
                    makeVal = " " + trimmedVal + " ";
                }
                getAttrs[i] = makeVal;
            }
        }
        return getAttrs.join("|");
    }

    function getHardTissueAttrs(fullString) {
        fullString = fullString.trim();
        var returnMe = [];
        if(fullString.length < 2) {
            return returnMe;
        }

        var splitted = fullString.split("|");
        for(var i=0;i<splitted.length;i++) {
            if(splitted[i].trim().length > 1) {
                returnMe.push(splitted[i].trim());
            }
        }
        return returnMe;
    }

    function addHardTissueAttr(fullString, addMe) {
        //First find out if it already had that attribute
        var missIndex = -1;
        var attrs = getHardTissueAttrs(fullString);
        for(var attrI=0;attrI<attrs.length;attrI++) {
            if(attrs[attrI].includes(addMe)) {
                missIndex = attrI;
            }
        }

        if(missIndex === -1) {
            attrs.push(addMe);
        }

        return makeHardTissueString(attrs);
    }

    function remHardTissueAttr(fullString, remMe) {
        //First find out if it already had that attribute
        var missIndex = -1;
        var attrs = getHardTissueAttrs(fullString);
        for(var attrI=0;attrI<attrs.length;attrI++) {
            if(attrs[attrI].includes(remMe)) {
                missIndex = attrI;
            }
        }

        if(missIndex >= 0) {
            attrs.splice(missIndex,1);
        }

        return makeHardTissueString(attrs);
    }

    function makeTxItemString(txItemObj) {
        var returnMe = "";

        if(txItemObj === undefined) {
            return "N/A";
        }
        else if(!("ProcedureName" in txItemObj)) {
            return "N/A";
        }

        if(txItemObj["ProcedureName"] === "Watch") {
            returnMe += "Watch #" + txItemObj["Tooth"];
        }
        else if((txItemObj["ProcedureName"] === "Composite") ||
                (txItemObj["ProcedureName"].includes("Composite"))) {
            returnMe += txItemObj["Surfaces"] + " Composite #" + txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Amalgam") {
            returnMe +=  txItemObj["Surfaces"] + " Amalgam #" +
                    txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Crown") {
            returnMe +=  txItemObj["Material"] + " Crown #" +
                    txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Extraction") {
            returnMe += "Extract #" + txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Implant Placement") {
            returnMe += "Place Implant #" + txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Root Canal Treatment") {
            returnMe += "RCT #" + txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"].endsWith("RCT")) {
            returnMe += "RCT #" + txItemObj["Tooth"];
        }
        else if(txItemObj["ProcedureName"] === "Post and Core") {
            returnMe +=  txItemObj["Material"] + " Post and Core #" +
                    txItemObj["Tooth"];
        }
        else {
            returnMe += txItemObj["ProcedureName"];

            if("Tooth" in txItemObj) {
                returnMe += " #" + txItemObj["Tooth"];
            }
            else if("Location" in txItemObj) {
                returnMe += " " + txItemObj["Location"];
            }
        }

        return returnMe;
    }

    function isPost(getTooth) {
        var currentTooth = parseInt(getTooth);
        var isPost = false;

        if(isNaN(currentTooth)) { //primary tooth
            switch(getTooth) {
            case "A":
            case "B":
            case "I":
            case "J":
            case "K":
            case "L":
            case "S":
            case "T":
                isPost = true;
                break;
            default:
                isPost  = false;
                break;
            }
        }

        else { //perm tooth
            isPost = true;
            if((currentTooth >= 6) && (currentTooth <= 11)) { //maxillary anterior
                isPost = false;
            } else if((currentTooth >= 22)
                      && (currentTooth <= 27)) { //mandibular anterior
                isPost = false;
            }
        }

        return isPost;
    }

    function isLeft(getTooth) {
        var currentTooth = parseInt(getTooth);
        var returnMe = false;

        if(isNaN(currentTooth)) { //primary tooth
            switch(getTooth) {
            case "F":
            case "G":
            case "H":
            case "I":
            case "J":
            case "K":
            case "L":
            case "M":
            case "N":
            case "O":
                returnMe = true;
                break;
            default:
                returnMe  = false;
                break;
            }
        }
        else {
            if((currentTooth >= 9) && (currentTooth <= 24)) {
                returnMe = true
            }
        }

        return returnMe;

    }

    function ensurePrimaryAndUnphased(txPlanObj) {
        if(!("Primary" in txPlanObj)) {
            txPlanObj["Primary"] = ({});
            txPlanObj["Primary"]["Unphased"] = [];
        }
        else if(!("Unphased" in txPlanObj["Primary"])) {
            txPlanObj["Primary"]["Unphased"] = [];
        }
    }

    function addTxItem(patName, txPlanName, phase,txObj) {
        var jsonTxPlans = m_textFileMan.readFile(m_FileLocs.getTreatmentPlanFile(patName));
        var txPlansObj = ({});
        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
        }

        if(!(txPlanName in txPlansObj)) {
            txPlansObj[txPlanName] = ({});
            txPlansObj[txPlanName][phase] = [];
        }
        else if(!(phase in txPlansObj[txPlanName])) {
            txPlansObj[txPlanName][phase] = [];
        }

        var hasDCode = "DCode" in txObj;
        var hasBasePrice = "BasePrice" in txObj;
        if((hasDCode) && (!(hasBasePrice))) {
            m_Settings.fileName = m_FileLocs.getLocalPracticeFeesFile();
            m_Settings.category = "Base Prices";
            txObj["BasePrice"] = m_Settings.value(txObj["DCode"],0)
        }

        txPlansObj[txPlanName][phase].push(txObj);
        jsonTxPlans = JSON.stringify(txPlansObj, null, '\t');
        m_textFileMan.saveFile(m_FileLocs.getTreatmentPlanFile(patName),jsonTxPlans);
        m_gitMan.commitData("Added treatment " + txPlanName["ProcedureName"] + " to " + patName);
        return txObj;
    }

    function getAllTxPlanItems(patName) {
        var jsonTxPlans = m_textFileMan.readFile(m_FileLocs.getTreatmentPlanFile(patName));
        var txPlansObj = ({});
        var returnMe = [];
        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
            var txPlanNames = Object.keys(txPlansObj);
            for(var txPlanNameI =0; txPlanNameI< txPlanNames.length; txPlanNameI++) {
                var txPlan = txPlansObj[txPlanNames[txPlanNameI]];
                var phaseNames = Object.keys(txPlan);
                for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                    var phaseList = txPlan[phaseNames[phasNameI]];
                    for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                        returnMe.push(phaseList[phaseItemI]);
                    }
                }
            }
        }
        return returnMe;
    }

    function getPrimaryTxPlanItems(patName) {
        var jsonTxPlans = m_textFileMan.readFile(m_FileLocs.getTreatmentPlanFile(patName));
        var txPlansObj = ({});
        var returnMe = [];
        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
            if("Primary" in txPlansObj) {
                var txPlan = txPlansObj["Primary"];
                var phaseNames = Object.keys(txPlan);
                for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                    var phaseList = txPlan[phaseNames[phasNameI]];
                    for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                        returnMe.push(phaseList[phaseItemI]);
                    }
                }
            }

        }
        return returnMe;
    }

    function findTx(patName,procedureName) {
        var items = getAllTxPlanItems(patName);
        var returnMe = [];
        for(var i=0;i<items.length;i++) {
            // @disable-check M126
            if(items[i]["ProcedureName"] == procedureName) {
                returnMe.push(items[i]);
            }
        }
        return returnMe;
    }

    function findTxDCode(patName,dCode) {
        var items = getAllTxPlanItems(patName);
        var returnMe = [];
        for(var i=0;i<items.length;i++) {
            // @disable-check M126
            if(items[i]["DCode"] == dCode) {
                returnMe.push(items[i]);
            }
        }
        return returnMe;
    }


    function getToothHistory(patName, toothNumber) {
        var caseNoteJson = m_textFileMan.readFile(m_FileLocs.getCaseNoteFile(patName));
        var caseNoteList = [];
        var returnMe = [];
        if(caseNoteJson.length > 2) {
            caseNoteList = JSON.parse(caseNoteJson);
        }
        for(var i=0;i<caseNoteList.length;i++) {
            if("ProcedureObject" in caseNoteList[i]) {
                var procedureObjArray = JSON.parse(caseNoteList[i]["ProcedureObject"]);
                for(var a=0;a<procedureObjArray.length;a++) {
                    var procedureObj =procedureObjArray[a];
                    if("Tooth" in procedureObj) {
                        // @disable-check M126
                        if(procedureObj["Tooth"] == toothNumber) {
                            returnMe.push([caseNoteList[i]["DateTime"],procedureObj]);
                        }
                    }
                }
            }
        }
        return returnMe;
    }

    function getProcedureHistory(patName,procedureName) {
        var caseNoteJson = m_textFileMan.readFile(m_FileLocs.getCaseNoteFile(patName));
        var caseNoteList = [];
        var returnMe = [];
        if(caseNoteJson.length > 2) {
            caseNoteList = JSON.parse(caseNoteJson);
        }
        for(var i=0;i<caseNoteList.length;i++) {
            var procedureListJSON = caseNoteList[i]["ProcedureObject"];
            var procedureList = JSON.parse(procedureListJSON);

            for(var j=0;j<procedureList.length;j++) {
                // @disable-check M126
                if(procedureList[j]["ProcedureName"] == procedureName) {
                    returnMe.push(caseNoteList[i]);
                    j = procedureList.length;
                }
            }
        }
        return returnMe;
    }

    function hadACompExam(patName) {
        var caseNoteJson = m_textFileMan.readFile(m_FileLocs.getCaseNoteFile(patName));
        var caseNoteList = [];
        var returnMe = false;
        if(caseNoteJson.length > 2) {
            caseNoteList = JSON.parse(caseNoteJson);
        }
        for(var i=0;i<caseNoteList.length;i++) {
            var procedureListJSON = caseNoteList[i]["ProcedureObject"];
            var procedureList = JSON.parse(procedureListJSON);

            for(var j=0;j<procedureList.length;j++) {
                // @disable-check M126
                if(procedureList[j]["ProcedureName"] == "Comprehensive Exam") {
                    returnMe = true;
                    j = procedureList.length;
                    i = caseNoteList.length;
                }
            }
        }
        return returnMe;
    }

    function getProcedureHistoryBroad(patName,procedureName) {
        var caseNoteJson = m_textFileMan.readFile(m_FileLocs.getCaseNoteFile(patName));
        var caseNoteList = [];
        var returnMe = [];
        if(caseNoteJson.length > 2) {
            caseNoteList = JSON.parse(caseNoteJson);
        }
        for(var i=0;i<caseNoteList.length;i++) {
            var procedureListJSON = caseNoteList[i]["ProcedureObject"];
            var procedureList = JSON.parse(procedureListJSON);

            for(var j=0;j<procedureList.length;j++) {
                if( ("ProcedureName" in procedureList[j]) &&
                        (procedureList[j]["ProcedureName"].includes(procedureName))) {
                    returnMe.push(caseNoteList[i]);
                    j = procedureList.length;
                }
            }
        }
        return returnMe;
    }

    function isEqual(objA, objB) {
        var returnMe = true;
        var KeysA = Object.keys(objA);
        //console.debug(KeysA);
        for(var i=0;i< KeysA.length;i++) {
            if(!(KeysA[i] in objB)) {
                returnMe = false;
            }
            else if(objB[KeysA[i]] !== objA[KeysA[i]]) {
                returnMe = false;
            }
            if(!returnMe) {
                i = KeysA.length;
            }
        }
        return returnMe;
    }

    function ensureSingleTxItem(patName, procedureObj) {
        var jsonTxPlans = m_textFileMan.readFile(m_FileLocs.getTreatmentPlanFile(patName));
        var txPlansObj = ({});
        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
            var txPlanNames = Object.keys(txPlansObj);
            for(var txPlanNameI =0; txPlanNameI< txPlanNames.length; txPlanNameI++) {
                var txPlan = txPlansObj[txPlanNames[txPlanNameI]];
                var phaseNames = Object.keys(txPlan);
                for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                    var phaseList = txPlan[phaseNames[phasNameI]];
                    for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                        var txObj = phaseList[phaseItemI];
                        if(txObj["ProcedureName"] === procedureObj["ProcedureName"]) {
                            phaseList.splice(phaseItemI,1);
                        }
                    }
                }
            }
            jsonTxPlans = JSON.stringify(txPlansObj, null, '\t');
            m_textFileMan.saveFile(m_FileLocs.getTreatmentPlanFile(patName),jsonTxPlans);
        }

        addTxItem(patName,"Primary","Unphased",procedureObj);
    }

    function getNameForCode(getCode) {
        if(!m_loadedNames) {
            var jsonText = m_constLoader.getDCodesJSON();
            var fullList = JSON.parse(jsonText);
            m_txPlanNames = ({});
            for(var i=0;i<fullList.length;i++) {
                var item = fullList[i];
                m_txPlanNames[item["DCode"]] = item["ProcedureName"];
            }

            m_loadedNames = true;
        }
        var returnMe = "";
        if(getCode in m_txPlanNames) {
            returnMe = m_txPlanNames[getCode];
        }

        return returnMe;
    }

    function getPriceForCode(getCode) {
        m_Settings.fileName = m_FileLocs.getLocalPracticeFeesFile();
        m_Settings.category = "Base Prices";
        return m_Settings.value(getCode,0)
    }

    function addTxItemFromDCode(patName, getDCode, txPlanName ="Primary", phase = "Unphased") {
        var txName = getNameForCode(getDCode);
        var txObjToSend = ({});
        txObjToSend["ProcedureName"] = txName;
        txObjToSend["DCode"] = getDCode;
        addTxItem(patName,txPlanName,phase,txObjToSend);
    }

    function convertTextDateToMils(getMonth,getDay,getYear) {
        var parseString = getDay + " " + getMonth + " " + getYear;
        var unixTime = Date.parse(parseString);
        return unixTime;
    }
}
