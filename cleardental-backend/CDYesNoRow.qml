// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12

RowLayout {
    property bool isYesSelected: yesButton.checked

    function setYes() {
        yesButton.checked = true;
    }

    function setNo() {
        noButton.checked = true;
    }

    RadioButton {
        id: yesButton
        text: "Yes"
    }

    RadioButton {
        id: noButton
        text: "No"
    }

    Component.onCompleted:  {
        yesButton.checked = true;
    }

}
