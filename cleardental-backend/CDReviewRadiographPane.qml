// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import dental.clear 1.0

CDTranslucentPane {
    id: radiographPane
    contentWidth: radioCol.implicitWidth
    contentHeight: radioCol.implicitHeight
    backMaterialColor: Material.Red
    property var toothToReview: -1
    property var radiographLocs: radioMan.getRadiographsForTooth(PATIENT_FILE_NAME,toothToReview,includeBad)
    property alias includeBad: showAllBad.checked

    onToothToReviewChanged: {
        showAllBox.checked = (toothToReview == -1) ||  (toothToReview == 0)
        //console.debug("Asking for: " + toothToReview);
        forceRefresh();
    }

    function forceRefresh() {
        radiographLocs = radioMan.getRadiographsForTooth(PATIENT_FILE_NAME,toothToReview,includeBad)
    }

    CDFileLocations {
        id: m_fLocs
    }

    CDRadiographManager {
        id: radioMan
    }

    // CDFileWatcher {
    //     id: watchDir
    //     fileName: m_fLocs.getRadiographDir(PATIENT_FILE_NAME);
    //     onFileUpdated: {
    //         forceRefresh();
    //     }
    // }

    ColumnLayout {
        id: radioCol
        anchors.fill: parent
        RowLayout {
            CDHeaderLabel {
                text: "Radiographs"
            }
            CheckBox {
                id: showAllBox
                property var oldToothToReview
                text: "Show All Radiographs"
                checked: true
                onCheckedChanged: {
                    if(checked) {
                        oldToothToReview = radiographPane.toothToReview;
                        radiographPane.toothToReview = -1;
                        forceRefresh();
                    }
                    else {
                        if(typeof oldToothToReview !== 'undefined') {
                            radiographPane.toothToReview = oldToothToReview;
                            forceRefresh();
                        }
                    }
                }
            }
            CheckBox {
                id: showAllBad
                text: "Show Bad Radiographs"
                checked: false;

                onCheckedChanged: {
                    forceRefresh();
                }
            }

        }
        Flickable {
            contentHeight: 320
            contentWidth:  (320*radiographLocs.length) +10
            clip: true
            Layout.fillWidth: true
            Layout.minimumHeight: 320

            ScrollBar.horizontal: ScrollBar{}

            RowLayout {
                Repeater {
                    model: radiographLocs.length
                    Image {
                        source: "file://" + radiographLocs[index]
                        Layout.maximumWidth: 320
                        Layout.maximumHeight: 320
                        fillMode: Image.PreserveAspectCrop
                        mipmap: true

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                radioDia.setSource = parent.source
                                radioDia.open()
                            }
                        }

                        Label {
                            id: radioLabel
                            anchors.centerIn: parent
                            style: Text.Outline
                            styleColor: Material.background
                            font.pointSize: 16

                            property var canary: toothToReview
                            onCanaryChanged: updateText();

                            function updateText() {
                                var scr = radiographLocs[index]
                                var filename = scr.substr(scr.lastIndexOf("/") + 1);

                                var fileSee = filename.substr(0,filename.lastIndexOf("."));

                                var fileUser = fileSee.replace(/_/g, " ");
                                if(fileUser.endsWith(".png")) { //Because it was a "bad" radiograph
                                    fileUser = fileUser.replace(".png" , " (bad)");
                                }

                                var radioDir = m_fLocs.getRadiographDir(PATIENT_FILE_NAME);
                                var dateLong = scr.replace(radioDir,"");
                                var dater = dateLong.replace( "/"  + filename,"");

                                text = fileUser + "\n" + dater;
                            }

                            Component.onCompleted: {
                                updateText();
                            }

                            CDRadioDia {
                                id: radioDia
                            }
                        }
                    }
                }
            }
        }
    }

    Button {
        parent: radiographPane.contentItem
        anchors.right: radiographPane.contentItem.right
        text: "Take new radiographs"
        CDToolLauncher {
            id: m_Launcher
        }
        onClicked: {
            m_Launcher.launchTool(CDToolLauncher.TakeRadiograph,PATIENT_FILE_NAME)
        }

    }


}
