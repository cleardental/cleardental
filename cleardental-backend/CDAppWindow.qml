// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1920
    height: 1000

    property var keyboardSaveButton

    visibility: {
        if((constsL.getNodeType() === "touch") || ((Screen.height < 1200) || (Screen.width < 2000))) {
            return ApplicationWindow.Maximized;
        }
        return  ApplicationWindow.Windowed;
    }
    flags:{
        if(constsL.getNodeType() === "touch") {
            return Qt.FramelessWindowHint;
        }
        return Qt.Window;
    }
    font.family: "Barlow Semi Condensed"
    font.pointSize: 12


    CDConstLoader {
        id: constsL;
    }

    onVisibilityChanged: {
        if((visibility == ApplicationWindow.Windowed) && (constsL.getNodeType() === "touch")) {
            showMaximized();
        }
    }

    Shortcut {
        sequence: "Esc"
        onActivated: Qt.quit();
    }

    Shortcut {
        sequence: "Return"
        context: Qt.ApplicationShortcut
        onActivated: {
            if(keyboardSaveButton !== undefined) {
                keyboardSaveButton.clicked();
                keyboardSaveButton = undefined;
            }
        }
    }


}
