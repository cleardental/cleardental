// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdmediafilemanager.h"

#include <QDir>
#include <QDebug>
#include <QFileInfo>
#include <QProcess>


#include <random>

#include "cddefaults.h"

CDMediaFileManager::CDMediaFileManager(QObject *parent) : QObject(parent)
{

}

QStringList CDMediaFileManager::getMusicTypes()
{
    QString musicPath = QDir::homePath() + "/Music/";
    QStringList returnMe;

    QDir musicDir(musicPath);
    foreach(QFileInfo musicItem, musicDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        returnMe.append(musicItem.fileName());
    }

    return returnMe;
}

QStringList CDMediaFileManager::getMusicList(QString getType)
{
    QString musicPath = QDir::homePath() + "/Music/" + getType;
    QStringList returnMe;

    QDir musicDir(musicPath);
    foreach(QFileInfo musicItem, musicDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot)) {
        QString addMeMaybe = musicItem.absoluteFilePath();
        if(addMeMaybe.endsWith(".mp3") || addMeMaybe.endsWith(".opus") || addMeMaybe.endsWith(".aac") ||
                addMeMaybe.endsWith(".flac") || addMeMaybe.endsWith(".ogg") || addMeMaybe.endsWith(".m4a")) {
            returnMe.append(musicItem.absoluteFilePath());
        }
    }
    return returnMe;
}

QStringList CDMediaFileManager::getMusicPics(QString getType)
{
    QString musicPath = QDir::homePath() + "/Music/" + getType;
    QStringList returnMe;

    QDir musicDir(musicPath);
    foreach(QFileInfo musicItem, musicDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot)) {
        QString addMeMaybe = musicItem.absoluteFilePath();
        if(addMeMaybe.endsWith(".jpg") || addMeMaybe.endsWith(".png")) {
            returnMe.append(musicItem.absoluteFilePath());
        }
    }
    return returnMe;
}

QStringList CDMediaFileManager::getVideoTypes()
{
    QString videoPath = QDir::homePath() + "/Videos/";
    QStringList returnMe;

    QDir videoDir(videoPath);
    foreach(QFileInfo videoItem, videoDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        returnMe.append(videoItem.fileName());
    }

    return returnMe;
}

QStringList CDMediaFileManager::getVideoList(QString getType)
{
    QString videoPath = QDir::homePath() + "/Videos/" + getType;
    QStringList returnMe;

    QDir videoDir(videoPath);
    foreach(QFileInfo videoItem, videoDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot)) {
        returnMe.append(videoItem.absoluteFilePath());
    }

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(returnMe.begin(), returnMe.end(),g);

    return returnMe;
}

QStringList CDMediaFileManager::getSupportedGames()
{
    QStringList returnMe;
    returnMe<<"TuxRacer"<<"CaveStory+"<<"Wonder Boy"<<"Cluster Truck"<<"Thomas was Alone";
    return returnMe;
}

void CDMediaFileManager::launchGame(QString gameName)
{
    QProcess runMe;
    if(gameName == "Wonder Boy") {
        runMe.setProgram("");
    }
}

