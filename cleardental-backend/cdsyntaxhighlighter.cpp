// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdsyntaxhighlighter.h"

#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>

CDSyntaxHighlighter::CDSyntaxHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    m_spell = new Hunspell("/usr/share/hunspell/en_US.aff", "/usr/share/hunspell/en_US.dic");

}

void CDSyntaxHighlighter::highlightBlock(const QString &text)
{
    QStringList words = text.split(" ");
    QTextCharFormat myClassFormat;
    myClassFormat.setFontWeight(QFont::Bold);
    myClassFormat.setForeground(Qt::darkRed);
    QRegularExpression expression("\\w+");
    QRegularExpressionMatchIterator i = expression.globalMatch(text);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        QString matchString = match.capturedTexts()[0];
        if(!m_spell->spell(matchString.toStdString())) {
            setFormat(match.capturedStart(), match.capturedLength(), myClassFormat);
        }
    }
}
