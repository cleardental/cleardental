// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdspeechtotext.h"

#include "cdtoollauncher.h"
#include "cdfilewatcher.h"

#include <QDebug>
#include <QDir>

CDSpeechToText::CDSpeechToText(QObject *parent) : QObject(parent)
{

}

void CDSpeechToText::listenForPerio()
{
    CDToolLauncher::launchListenerProcess(CDToolLauncher::SHERPA_NCNN);
    m_watcher = new CDFileWatcher();
    m_watcher->setFileName(QDir::tempPath() + "/periolog.txt");
    connect(m_watcher,SIGNAL(fileUpdated()),this,SLOT(handleNewPerioInput()));
}

void CDSpeechToText::listenForHardTissueChart()
{

}

void CDSpeechToText::stopListeningForPerio()
{
    CDToolLauncher::killListenerProcess(CDToolLauncher::SHERPA_NCNN);
}

void CDSpeechToText::handleNewPerioInput()
{
    QFile readOutput(QDir::tempPath() + "/periolog.txt");
    readOutput.open(QIODevice::ReadOnly);
    QString entireThing = readOutput.readAll();
    int lastLineIndex = entireThing.lastIndexOf(':');
    QString lastLineStr = entireThing.mid(lastLineIndex+2);
    qDebug()<<lastLineStr;
    QStringList lastLineList = lastLineStr.split(" ");
    if(lastLineList.length() == 3) {
        parsePerioLine(lastLineList);
    }

}

void CDSpeechToText::parsePerioLine(QStringList getLine)
{
    QList<int> emitMe;
    foreach(QString numb, getLine) {
        if(numb == "one" || numb == "on" || numb == "won" || numb == "1") {
            emitMe.append(1);
        }
        else if( (numb == "two") || (numb == "to") || (numb == "too") || (numb == "2")) {
            emitMe.append(2);
        }
        else if( (numb == "three") || (numb == "the") || (numb == "there") || (numb == "3")) {
            emitMe.append(3);
        }
        else if( (numb == "four") || (numb == "for") || (numb == "4")) {
            emitMe.append(4);
        }
        else if( (numb == "five") || (numb == "5") ) {
            emitMe.append(5);
        }
        else if( (numb == "six") || (numb == "6") ) {
            emitMe.append(6);
        }
        else if( (numb == "seven") || (numb == "7") ) {
            emitMe.append(7);
        }
        else if( (numb == "eight") || (numb == "8") ) {
            emitMe.append(8);
        }
        else if( (numb == "nine") || (numb == "9") ) {
            emitMe.append(9);
        }
        else if( (numb == "ten") || (numb == "10") ) {
            emitMe.append(10);
        }
        else {
            qDebug()<<"Didn't understand: " << numb;
        }
    }

    if(emitMe.length() == 3) {
        emit newPerioTrio(emitMe[0],emitMe[1],emitMe[2]);
    }

}

//void CDSpeechToText::handleNewPerioSpeech()
//{
//    QString output = m_Process.readAllStandardOutput();
//    output = output.replace("\n","");
//    QStringList numbs = output.split(" ");
//    foreach(QString numb, numbs) {
//        if((numb == "one" || numb == "on" || numb == "won")) {
//            emit newPerioNumber(1);
//        }
//        else if((numb == "two") || (numb == "to") || (numb == "too")) {
//            emit newPerioNumber(2);
//        }
//        else if((numb == "three") || (numb == "the") || (numb == "there")) {
//            emit newPerioNumber(3);
//        }
//        else if((numb == "four") || (numb == "for")) {
//            emit newPerioNumber(4);
//        }
//        else {
//            qDebug()<<"Unknown: " << numb;
//        }
//    }
//}
