// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDPATIENTFILEMANAGER_H
#define CDPATIENTFILEMANAGER_H

#include <QObject>
#include <QVariantMap>
#include <QDate>

class CDPatientFileManager : public QObject
{
    Q_OBJECT
public:
    explicit CDPatientFileManager(QObject *parent = nullptr);

    Q_INVOKABLE static void importNewProfileImage(QString patientID, QString url, double x, double y, double width);
    Q_INVOKABLE static void importNewIntraOralImage(QString patientID,QString location, QString url);

    Q_INVOKABLE static QString importPatient(QVariantMap jsonData);
    Q_INVOKABLE static bool patientExists(QString patientFileName);

    Q_INVOKABLE static QStringList getPatientPerioHistoryFiles(QString patientFileName);
    Q_INVOKABLE static void saveTodaysPerioChartAsHistory(QString patientFileName);

    Q_INVOKABLE static void addRelationship(QString basePat, QString relPat, QString relationship);
    Q_INVOKABLE static void addNewPatWithRelationship(QString basePat, QString relationship, QString getFirstName,
                                                      QString getLastName, QDate getDOB, QString getSex, bool sameAddr,
                                                      bool sameDentalPlan, bool samePhoneNumber);
    Q_INVOKABLE static void remRelationship(QString basePat, QString relPat, QString relationship);
    Q_INVOKABLE static QVariantList getRelationships(QString basePat);

};

#endif // CDPATIENTFILEMANAGER_H
