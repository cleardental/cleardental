// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdbillingestimator.h"

#include "cdfilelocations.h"
#include "cdtextfilemanager.h"

#include <QDebug>
#include <QFile>


CDBillingEstimator::CDBillingEstimator(QObject *parent)
    : QObject{parent}
{

}

QVariantMap CDBillingEstimator::calculatePatientPortions(QString getPatID,QVariantMap getTxPlan)
{
    QVariantMap returnMe;

    QSettings insInfo(CDFileLocations::getDentalPlansFile(getPatID),QSettings::IniFormat);

    insInfo.beginGroup("Membership");
    bool hasMembership = insInfo.value("HasMembership",false).toBool();
    QList<TxType> membershipCoveredTypes;

    QSettings feeSchedules(CDFileLocations::getLocalPracticeFeesFile(),QSettings::IniFormat);
    QStringList feeScheduleNames = feeSchedules.childGroups();

    insInfo.beginGroup("Primary");
    QString priName = insInfo.value("Name","").toString();
    bool hasPrimary = priName.length() > 1;
    int priMax =insInfo.value("AnnMax",1000).toInt();
    int priDeductBasicMajor =insInfo.value("AnnDeductBasicMajor",50).toInt();
    int priDeductAll =insInfo.value("AnnDeductAll",0).toInt();
    int priDeductCounter=insInfo.value("AnnDeductLeft",0).toInt();
    int priInsPaymentCounter=insInfo.value("AmountUsed",priMax).toInt();
    int priDiaCoveragePercent = insInfo.value("DiagPercent",100).toInt();
    int priPrevCoveragePercent = insInfo.value("PrevPercent",100).toInt();
    int priBasicCoveragePercent = insInfo.value("BasicPercent",80).toInt();
    int priMajorCoveragePercent = insInfo.value("MajorPercent",50).toInt();

    if(feeScheduleNames.contains(priName)) {
        feeSchedules.beginGroup(priName);
    }

//    insInfo.beginGroup("Secondary");
//    QString secName = insInfo.value("Name","").toString();
//    bool hasSecondary = secName.length() > 1;
//    int secInsPaymentCounter=0;


//    bool priHasFeeSchedule = false;
//    if(hasPrimary) {
//        priHasFeeSchedule = feeScheduleNames.contains(priName);
//    }
//    bool secHasFeeSchedule = false;
//    if(hasSecondary) {
//        secHasFeeSchedule = feeScheduleNames.contains(secName);
//    }
    if(hasMembership) {
        feeSchedules.beginGroup("LocalMembership");
        membershipCoveredTypes.append(EXAM);
        membershipCoveredTypes.append(DIAGNOSTIC);
        membershipCoveredTypes.append(PREVENTATIVE);
        membershipCoveredTypes.append(CLEANING);
        membershipCoveredTypes.append(FLUORIDE);
    }

    int phaseCount = getTxPlan.keys().length();
    insInfo.beginGroup("Primary");

    for(int i=0; i<phaseCount;i++) {
        QString phaseName = QString::number(i);
        if(i==0) {
            phaseName = "Unphased";
        }

        QVariantList phaseList = getTxPlan.value(phaseName).toList();
        QVariantList returnList;
        for(int phaseI=0;phaseI<phaseList.length();phaseI++) {
            QVariantMap txItem = phaseList[phaseI].toMap();
            int basePrice = txItem.value("BasePrice",0).toInt();
            int setPatPortion =0;
            int setInsPortion =0;
            TxType txType = getTxType(txItem);
            QVariantMap addMe;
            if(hasMembership) {
                if(membershipCoveredTypes.contains(txType)) {
                    setPatPortion =0;
                }
                else {
                    setPatPortion = feeSchedules.value(txItem.value("DCode","DNONE").toString()).toInt();
                }
            }
            else if(hasPrimary) {
                basePrice = feeSchedules.value(txItem.value("DCode","DNONE").toString()).toInt();
                qreal coverageF = 0;
                bool needToDeduct = false;
                if((txType == EXAM) || (txType == DIAGNOSTIC) ) {
                    coverageF = (priDiaCoveragePercent / 100.0) * basePrice;
                    needToDeduct = (priDeductAll > 0) && (priDeductCounter < priDeductAll) ;
                }
                else if((txType == PREVENTATIVE) || (txType == CLEANING) ) {
                    coverageF = (priPrevCoveragePercent / 100.0) * basePrice;
                    needToDeduct = (priDeductAll > 0) && (priDeductCounter < priDeductAll);
                }
                else if(txType == FLUORIDE) {
                    if(insInfo.value("FluorideCoverage",false).toBool()) {
                        int ageLimit = insInfo.value("FluorideAgeLimit",18).toInt();
                        QDateTime today =  QDateTime::currentDateTime();
                        QSettings patPersonalInfo(CDFileLocations::getPersonalFile(getPatID),QSettings::IniFormat);
                        patPersonalInfo.beginGroup("Personal");
                        QString dobString = patPersonalInfo.value("DateOfBirth","1/1/1970").toString();
                        QDateTime patDOB = QDateTime::fromString(dobString,"M/d/yyyy");
                        QDateTime postFluorideDate = patDOB.addYears(ageLimit);
                        if(postFluorideDate > today) {
                            coverageF = (priPrevCoveragePercent / 100.0) * basePrice;
                            needToDeduct = priDeductAll > 0;
                        }
                    }
                }
                else if(txType == BASIC) {
                    coverageF = (priBasicCoveragePercent / 100.0) * basePrice;
                    needToDeduct = priDeductBasicMajor > 0;
                }
                else if(txType == MAJOR) {
                    coverageF = (priMajorCoveragePercent / 100.0) * basePrice;
                    needToDeduct = priDeductBasicMajor > 0;
                }


                //Do the deduction calculations
                bool deductionMet = !needToDeduct;
                if(needToDeduct) {
                    int topOver = priDeductCounter + basePrice;
                    if(priDeductAll > 0) {
                        if(topOver > priDeductAll) { //deductable for everything is met after this one
                            int remainingAmount = priDeductAll - priDeductCounter;
                            setPatPortion+= remainingAmount;
                            basePrice -= remainingAmount;
                            priDeductCounter = priDeductAll;
                            deductionMet = true;
                        }
                        else { //still need to pay more deductable after this procedure (rare)
                            priDeductCounter+= basePrice;
                            deductionMet = false;
                        }
                    }
                    else { //priDeductBasicMajor > 0
                        if(topOver > priDeductBasicMajor) { //deductable for everything is met after this one
                            int remainingAmount = priDeductBasicMajor - priDeductCounter;
                            setPatPortion+= remainingAmount;
                            basePrice -= remainingAmount;
                            priDeductCounter = priDeductBasicMajor;
                            deductionMet = true;
                        }
                        else { //still need to pay more deductable after this procedure (rare)
                            priDeductCounter+= priDeductBasicMajor;
                            deductionMet = false;
                        }
                    }
                }

                if(deductionMet) { //either met or there wasn't one
                    bool alreadyMaxed = priInsPaymentCounter >= priMax;
                    if(!alreadyMaxed) { //Ins has to pay something
                        //First make sure it doesn't max out the ins
                        int topOver = (int) coverageF + priInsPaymentCounter;
                        if(topOver > priMax) { //it tops over, patient will have to pay remaining amount
                            setInsPortion =  priMax - priInsPaymentCounter;
                            setPatPortion = basePrice - setInsPortion;
                        }
                        else { //ins pays for the whole treatment as per percentage
                            setInsPortion = coverageF;
                            setPatPortion = basePrice - coverageF;
                        }
                    }
                    else { //already maxed up the ins; pat pays remaining base price
                        setPatPortion = basePrice;
                        setInsPortion =0;
                    }
                }
            }
            else {
                setPatPortion = basePrice;
            }

            addMe["TxObj"] = txItem;
            addMe["PatientPortion"] = setPatPortion;
            addMe["InsPortion"] = setInsPortion;
            returnList.append(addMe);
        }
        returnMe.insert(phaseName,returnList);


    }

    return returnMe;
}

CDBillingEstimator::TxType CDBillingEstimator::getTxType(QVariantMap getTx)
{
    QString dCode = getTx.value("DCode","DNONE").toString();
    TxType returnMe = NON_COVERED;
    QChar typeChar = dCode.at(1);
    if( (dCode == "D0150") || (dCode == "D0120") || (dCode == "D0180") ) {
        returnMe = EXAM;
    }
    else if(typeChar == '0') { //includes radiographs
        returnMe = DIAGNOSTIC;
    }
    else if( (dCode == "D1110") || (dCode == "D1120")) {
        returnMe = CLEANING;
    }
    else if( (dCode == "D1206") || (dCode == "D1208") ) {
        returnMe = FLUORIDE;
    }
    else if(typeChar == '1') { //includes sealants
        returnMe = PREVENTATIVE;
    }
    else if(dCode.startsWith("D27")) { //crowns
        returnMe = MAJOR;
    }
    else if(typeChar == '2') { //restorative
        returnMe = BASIC;
    }
    else if(typeChar == '3') { //endo
        returnMe = BASIC; //Not sure, may have to fix later on
    }
    else if(typeChar == '4') { //perio
        returnMe = BASIC; //Not sure, may have to fix later on
    }
    else if(typeChar == '5') { //rem prostho
        returnMe = MAJOR; //Not sure, may have to fix later on
    }
    else if(typeChar == '6') { //fixed prostho (bridge)
        returnMe = MAJOR; //Not sure, may have to fix later on
    }
    else if(typeChar == '7') { //OMFS
        returnMe = BASIC; //Not sure, may have to fix later on
    }
    else if(typeChar == '8') { //ortho
        returnMe = ORTHO;
    }
    else {
        returnMe = NON_COVERED;
    }

    return returnMe;
}


