// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentPane {
    id: medPane

    contentWidth: colMed.implicitWidth + 100
    contentHeight: colMed.implicitHeight
    backMaterialColor: Material.BlueGrey
    property int buttonWidth: covidScreen.width + takeVitalsButton.width + medReviewButton.width + medHeader.width +
                              100

    function generateCaseNoteString() {
        var returnMe = ""
        if(covidScreen.wasReviewed) {
            returnMe += "COVID-19 Screening Completed.\n";
        }
        if(takeVitalsButton.wasReviewed) {
            returnMe += "Vitals were taken.\n";
        }
        if(medReviewButton.wasReviewed) {
            returnMe += "Medical history was reviewed.\n";
        }

        return returnMe;
    }

    CDToolLauncher {
        id: m_launcher
    }

    CDFileLocations {
        id: m_fileLocs
    }

    CDTextFileManager {
        id: m_textMan
    }

    ColumnLayout {
        id: colMed
        Label {
            id: medHeader
            text: "Medical history"
            font.pointSize: 24
            font.underline: true
        }

        Label {
            id: alertLabel
            color: Material.color(Material.Red);
            visible: text.length > 1
            text: medInfoSettings.value("MedicalAlert","")
            font.pointSize: 24

            function updateText() {
                medInfoSettings.sync();

                medInfoSettings.category = "Alerts"
                alertLabel.text = medInfoSettings.value("MedicalAlert","")

                medInfoSettings.category = "Pregnancy"
                var isPreg = JSON.parse(medInfoSettings.value("IsPregnant","false"));
                if(isPreg) {
                    alertLabel.text += "🤰";
                }

            }

            Settings {
                id: medInfoSettings
                fileName: m_fileLocs.getOtherMedInfoFile(PATIENT_FILE_NAME);
                category: "Alerts"
            }


            CDFileWatcher {
                fileName: m_fileLocs.getOtherMedInfoFile(PATIENT_FILE_NAME);
                onFileUpdated: {
                    alertLabel.updateText();
                }
            }

            Component.onCompleted: {
                updateText();
            }
        }

        CDDescLabel {
            text: "Conditions:"
        }
        Label {
            id: conditionListLabel
            function parseConditions() {
                var returnMe = "<ul>"
                var jsonCond = m_textMan.readFile(m_fileLocs.getConditionsFile(PATIENT_FILE_NAME));
                var condObj = JSON.parse(jsonCond);
                for(var i=0;i<condObj.length;i++) {
                    var cond = condObj[i];
                    returnMe +="<li>" + cond.ConditionName;
                    if(cond.ConditionInfo.length > 1) {
                        returnMe+= " (" + cond.ConditionInfo + ")"
                    }
                    returnMe += "</li>"
                }

                if(condObj.length === 0) {
                    returnMe += "<li>None</li>"
                }

                returnMe += "</ul>"
                text = returnMe;
            }

            CDFileWatcher {
                fileName: m_fileLocs.getConditionsFile(PATIENT_FILE_NAME);
                onFileUpdated: conditionListLabel.parseConditions();
            }

            Component.onCompleted: parseConditions()
        }

        CDDescLabel {
            text: "Medications:"
        }
        Label {
            id: medListLabel
            function parseMedications() {
                var returnMe = "<ul>"
                var jsonMeds = m_textMan.readFile(m_fileLocs.getMedicationsFile(PATIENT_FILE_NAME));
                var medsObj = JSON.parse(jsonMeds);
                for(var i=0;i<medsObj.length;i++) {
                    var med = medsObj[i];
                    returnMe +="<li>" + med.DrugName;
                    if(med.UsedFor.length > 1) {
                        returnMe+= " (" + med.UsedFor + ")"
                    }
                    returnMe += "</li>"
                }

                if(medsObj.length === 0) {
                    returnMe += "<li>None</li>"
                }

                returnMe += "</ul>"
                text = returnMe;
            }

            CDFileWatcher {
                fileName: m_fileLocs.getMedicationsFile(PATIENT_FILE_NAME)
                onFileUpdated: medListLabel.parseMedications();
            }

            Component.onCompleted: parseMedications()
        }

        CDDescLabel {
            text: "Allergies:"
        }
        Label {
            id: allergyRow

            function parseAllergies() {
                var jsonAllergies = m_textMan.readFile(m_fileLocs.getAllergiesFile(PATIENT_FILE_NAME));
                var returnMe = "<ul>"
                if(jsonAllergies.length === 0) {
                    returnMe += "<li>None</li>"
                }
                else {
                    var allergyObj = JSON.parse(jsonAllergies);
                    for(var i=0;i<allergyObj.length;i++) {
                        var allergy = allergyObj[i];
                        returnMe += "<li>" + allergy.AllergyName;
                        returnMe+= " (" + allergy.AllergyReaction + ")"
                        returnMe += "</li>"
                    }

                    if(allergyObj.length === 0) {
                        returnMe += "None"
                    }
                }
                returnMe += "</ul>"

                text = returnMe;
            }

            CDFileWatcher {
                fileName: m_fileLocs.getAllergiesFile(PATIENT_FILE_NAME)
                onFileUpdated: allergyListLabel.text = allergyRow.parseAllergies()
            }

            Component.onCompleted: parseAllergies();
        }
    }

    CDReviewLauncher {
        id: covidScreen
        parent: medPane.contentItem
        anchors.right: takeVitalsButton.left
        anchors.top:  medPane.contentItem.top
        iniProp: "COVID"
        anchors.rightMargin: 20
        launchEnum: CDToolLauncher.COVID_Screen
        text: "COVID-19 Screening"
    }

    CDReviewLauncher {
        id: takeVitalsButton
        parent: medPane.contentItem
        anchors.right: medReviewButton.left
        anchors.top:  medPane.contentItem.top
        anchors.rightMargin: 20
        iniProp: "Vitals"
        launchEnum: CDToolLauncher.Vitals
        text: "Take Vitals"
    }

    CDReviewLauncher {
        id: medReviewButton
        parent: medPane.contentItem
        anchors.right: medPane.contentItem.right
        anchors.top:  medPane.contentItem.top
        iniProp: "Medical"
        launchEnum: CDToolLauncher.ReviewPatMedical
        text: "Review Medical history"
    }


}
