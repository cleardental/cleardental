// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDLOCATIONMANAGER_H
#define CDLOCATIONMANAGER_H

#include <QObject>
#include <QVariantMap>

class CDLocationManager : public QObject
{
    Q_OBJECT
public:
    explicit CDLocationManager(QObject *parent = nullptr);

    Q_INVOKABLE static QStringList getListOfLocationFiles();
    Q_INVOKABLE static QString getLocalLocationName();
    Q_INVOKABLE static QString addLocation(QVariantMap data);
    Q_INVOKABLE static void removeLocation(QString locationID);

signals:

public slots:
};

#endif // CDLOCATIONMANAGER_H
