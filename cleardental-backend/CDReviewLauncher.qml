// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

Button {
    id: reviewLaunch
    property string iniProp: ""
    property var launchEnum: CDToolLauncher.Invalid
    property bool wasReviewed: icon.color === Material.color(Material.Green)
    property var lastDone;

    function updateFlag() {
        m_reviewFile.refresh();
        lastDone = m_reviewFile.value(reviewLaunch.iniProp);
        icon.color =m_reviewFile.isToday(reviewLaunch.iniProp) ?
                    Material.color(Material.Green) :
                    Material.color(Material.Yellow)
    }

    CDFileLocations {
        id: m_fLocs
    }

    CDFileWatcher {
        id: m_Watcher
        fileName: m_fLocs.getReviewsFile(PATIENT_FILE_NAME);
        onFileUpdated: updateFlag();
    }

    Settings {
        id: m_reviewFile
        fileName: m_fLocs.getReviewsFile(PATIENT_FILE_NAME)

        function isToday(getName) {
            var today = new Date();
            var dateString = (today.getMonth() +1) + "/" + (today.getDate()) +
                    "/" + today.getFullYear();
            return dateString === m_reviewFile.value(getName);
        }
        function refresh() {
            fileName ="";
            fileName= m_fLocs.getReviewsFile(PATIENT_FILE_NAME);
        }
    }

    CDToolLauncher {
        id: m_Launcher
    }

    icon.name: "flag-black"

    onClicked: {
        icon.color = Material.color(Material.Green)
        m_Launcher.launchTool(launchEnum,PATIENT_FILE_NAME);
    }

    Component.onCompleted:  {
        updateFlag();
    }
}
