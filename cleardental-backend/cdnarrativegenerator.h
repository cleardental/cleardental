#ifndef CDNARRATIVEGENERATOR_H
#define CDNARRATIVEGENERATOR_H

#include <QObject>

class CDNarrativeGenerator : public QObject
{
    Q_OBJECT
public:
    explicit CDNarrativeGenerator(QObject *parent = nullptr);

    Q_INVOKABLE static QString generateGenericNarrative(QString getNarrativeText,
                                                        QString getPatID,
                                                        QString getProviderID);

signals:
};

#endif // CDNARRATIVEGENERATOR_H
