// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSPEECH_H
#define CDSPEECH_H

#include <QObject>

class CDSpeech : public QObject
{
    Q_OBJECT
public:
    explicit CDSpeech(QObject *parent = nullptr);

signals:

public slots:
    static void sayString(QString sayIt);

};

#endif // CDSPEECH_H
