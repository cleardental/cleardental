// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import Qt.labs.calendar 1.0


Pane {
    id: rootPane
    property date myDate: new Date()
    property int myWeekNumb

    onMyDateChanged: {
        var dayOfWeek = myDate.getDay();

        startDay = new Date(myDate.getFullYear(),myDate.getMonth(),myDate.getDate() - myDate.getDay());
        endDay = new Date(myDate.getFullYear(),myDate.getMonth(),myDate.getDate() + (6-myDate.getDay()) );
        myWeekNumb = getWeek(myDate);

    }

    property date startDay
    property date endDay

    property var monthStrings: ["January", "February", "March", "April", "May",
        "June", "July", "August", "September", "October", "November",
        "December"]
    property var weekStrings: ["Sunday", "Monday", "Tuesday", "Wednesday",
        "Thursday", "Friday", "Saturday"]

    background: Rectangle {
        anchors.fill: parent
        radius: 10
        opacity: .5
        color: Material.color(Material.Grey,Material.Shade100)
    }

    ColumnLayout {
        RowLayout {
            Layout.fillWidth: true
            spacing: 20
            Button {
                text: "-1M"
                icon.name: "go-previous"
                onClicked: {
                    myDate = new Date(myDate.getFullYear(),
                                      myDate.getMonth()-1,
                                      myDate.getDate());
                }
            }
            Label {
                id: monthLabel
                text: monthStrings[myDate.getMonth()].slice(0, 3)
                font.pointSize: 24
                font.bold: true
            }
            Label {
                id: yearLabel
                text: myDate.getFullYear()
                font.pointSize: 24
                font.bold: true
            }
            Button {
                text: "+1M"
                icon.name: "go-next"
                onClicked: {
                    myDate = new Date(myDate.getFullYear(),
                                      myDate.getMonth()+1,
                                      myDate.getDate());
                }
            }
            Button {
                text: "+6M"
                icon.name: "go-next"
                onClicked: {
                    myDate = new Date(myDate.getFullYear(),
                                      myDate.getMonth()+6,
                                      myDate.getDate());
                }
            }
            Button {
                icon.name: "arrow-right-double"
                text: "+1Y"
                onClicked: {
                    myDate = new Date(myDate.getFullYear()+1,
                                      myDate.getMonth(),
                                      myDate.getDate());
                }
            }
        }
        DayOfWeekRow {
            locale: Qt.locale("en_US")
            Layout.fillWidth: true
        }
        MonthGrid {
            id: monthGrid
            locale: Qt.locale("en_US")
            Layout.fillWidth: true
            month: myDate.getMonth();
            year: myDate.getFullYear();

            delegate: Label {
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                opacity: model.month === myDate.getMonth() ? 1 : .1
                text: model.day

                Rectangle {
                    anchors.fill: parent
                    color: Material.color(Material.LightBlue)
                    opacity: .2
                    visible: getWeek(model.date) === myWeekNumb
                    radius: 10
                }

                Rectangle {
                    anchors.fill: parent
                    color: Material.color(Material.Blue)
                    opacity: .5
                    visible: model.today
                    radius: 10
                }
            }




            onClicked: {
                myDate = new Date(date.getFullYear(),
                                  date.getMonth(),
                                  date.getDate()+1);
            }
        }


    }

    function getWeek(inputDate) {
     var onejan = new Date(inputDate.getFullYear(),0,1);
     return Math.ceil((((inputDate - onejan) / 86400000) + onejan.getDay()+1)/7);
   }



}
