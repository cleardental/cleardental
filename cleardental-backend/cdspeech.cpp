// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdspeech.h"

#include <QTextToSpeech>
#include <QDebug>
#include <QMediaRecorder>
#include <QUrl>
#include <QThread>
#include <QVoice>

#include <QProcess>

CDSpeech::CDSpeech(QObject *parent) : QObject(parent)
{

}


void CDSpeech::sayString(QString sayIt) {
    //Because QTextToSpeech doesn't seem to pick up pico properly
    QProcess pro;
    pro.setProgram("/usr/bin/pico2wave");
    QStringList args;
    args<<"--wave=/tmp/sayMe.wav";
    args<<sayIt;
    pro.setArguments(args);
    pro.start();
    pro.waitForFinished();

    QProcess mplayer;
    mplayer.setProgram("/usr/bin/mplayer");
    args.clear();
    args<<"/tmp/sayMe.wav";
    mplayer.setArguments(args);
    mplayer.startDetached();
}
