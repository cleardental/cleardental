// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdschedulemanager.h"

#include <QDate>
#include <QDir>
#include <QDebug>
#include <QSettings>
#include <QFile>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QTime>

#include "cddefaults.h"
#include "cdfilelocations.h"
#include "cdgitmanager.h"

CDScheduleManager::CDScheduleManager(QObject *parent) : QObject(parent)
{

}

QStringList CDScheduleManager::getAppointments(QDate date)
{
    QStringList returnMe;

    if(!date.isValid()) {
        return returnMe;
    }

    QString dayDirPath = CDDefaults::getLocalScheduleDir() + date.toString("MMM/d/yyyy/");

    QDir scheduleDir(dayDirPath);

    foreach(QString appointmentFile, scheduleDir.entryList(QDir::Files | QDir::NoDotAndDotDot)) {
        returnMe.append(dayDirPath + appointmentFile);
    }

    return returnMe;
}

QStringList CDScheduleManager::getAppointments(QDate date, QString getChairID)
{
    QStringList scanMe = getAppointments(date);
    QStringList returnMe;

    foreach(QString file, scanMe) {
        QSettings seeFile(file,QSettings::IniFormat);
        if(seeFile.value("Chair") == getChairID) {
            returnMe.append(file);
        }
    }

    return returnMe;

}

QStringList CDScheduleManager::getAppointments(QDate date, QString getChairID, int getStartTime, int getDuration)
{
    QStringList scanMe = getAppointments(date,getChairID);
    QStringList returnMe;
    int getEndTime = getStartTime + getDuration;

    foreach(QString file, scanMe) {
        QSettings seeFile(file,QSettings::IniFormat);
        QString startTimeStr = seeFile.value("StartTime").toString();
        int startTimeMin = convertTimeToMinsSinceMidnight(startTimeStr);
        int endTimeMin = startTimeMin + seeFile.value("Duration").toInt();

        if(getStartTime < startTimeMin) { //before the comp apointment
            if(getEndTime > startTimeMin ) {
                returnMe.append(file);
            }
        }
        else if( getStartTime < endTimeMin) {
            returnMe.append(file);
        }
    }

    return returnMe;

}

QStringList CDScheduleManager::getAppointments(QString getPatID)
{
    QStringList returnMe;

    QString patDirStr = CDFileLocations::getPatientAppointmentDirectory(getPatID);
    QDir patDir(patDirStr);

    foreach(QString appointmentFile, patDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::System)) {
        returnMe.append(patDirStr + appointmentFile);
    }

    return returnMe;
}

int CDScheduleManager::getNoShowAppointments(QString getPatID)
{
    int returnMe =0;
    QStringList allAppts = getAppointments(getPatID);
    foreach(QString appt, allAppts) {
        QSettings readAppt(appt, QSettings::IniFormat);
        QString apptStatus = readAppt.value("Status","").toString();
        if(apptStatus == "No Show") {
            returnMe++;
        }
    }

    return returnMe;
}

QList<QString> CDScheduleManager::getChairs(QDate date)
{
    QList<QString> returnMe;

    if(!date.isValid()) {
        return returnMe;
    }

    QFile chairFile(CDFileLocations::getLocalDefaultChairFile());
    chairFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QJsonDocument chairDoc = QJsonDocument::fromJson(chairFile.readAll());
    foreach(QJsonValue val, chairDoc.array()) {
        QJsonObject chairObj = val.toObject();
        returnMe.append(chairObj.value("ChairName").toString());
    }

    return returnMe;
}

QString CDScheduleManager::addAppointment(QDate getDate, QString getTime,
                                          QString getChair, QString getPatID,
                                          QString getProcedures,
                                          QString getProviderID,
                                          int getDurationMins,
                                          QString getComments)
{
    //QDate addDate = QDate::fromString(getDate,"M/d/yyyy");
    QDate addDate = getDate;

    QString makePath = CDDefaults::getLocalScheduleDir() +
            addDate.toString("MMM") + "/" + QString::number(addDate.day()) +
            "/" + QString::number(addDate.year());
    QDir addDir(makePath);
    if(!addDir.exists()) {
        addDir.mkpath(".");
    }

    int idNumb = 1;
    QFile existChecker(makePath + "/appt" + QString::number(idNumb) + ".ini");
    while(existChecker.exists()) {
        idNumb++;
        existChecker.setFileName(makePath + "/appt" + QString::number(idNumb) +
                                 ".ini");
    }

    QSettings writeAppt(makePath + "/appt" + QString::number(idNumb) +
                        ".ini", QSettings::IniFormat);
    writeAppt.setValue("StartTime",getTime);
    writeAppt.setValue("Chair",getChair);
    writeAppt.setValue("PatientID",getPatID);
    writeAppt.setValue("Procedures",getProcedures);
    writeAppt.setValue("ProviderID",getProviderID);
    writeAppt.setValue("Duration",getDurationMins);
    writeAppt.setValue("Comments",getComments);
    writeAppt.setValue("Status","Unconfirmed");
    writeAppt.sync();

    QString patAptDir = CDFileLocations::getPatientAppointmentDirectory(getPatID);
    QDir patAptDirObj(patAptDir);
    if(!patAptDirObj.exists()) {
        patAptDirObj.mkpath(".");
    }

    QFile::link("../../../locInfo/local/appointments/" + addDate.toString("MMM") + "/" +
                QString::number(addDate.day()) + "/" + QString::number(addDate.year()) + "/appt" +
                QString::number(idNumb) +".ini",
                patAptDir + addDate.toString("MMM-d-yyyy") + "@" + getTime + ".ini" );

    writeAppt.sync();
    CDGitManager::commitData("Added appointmentment for " + getPatID);

    return makePath + "/appt" + QString::number(idNumb) +".ini";
}

QString CDScheduleManager::addBlocker(QDate getDate, bool allDay,
                                      QString getTime, bool allChairs,
                                      QString getChair, int getDurationMins,
                                      QString getComments)
{

    QString makePath = CDDefaults::getLocalScheduleDir() +
            getDate.toString("MMM") + "/" + QString::number(getDate.day()) +
            "/" + QString::number(getDate.year());
    QDir addDir(makePath);
    if(!addDir.exists()) {
        addDir.mkpath(".");
    }

    int idNumb = 1;
    QFile existChecker(makePath + "/appt" + QString::number(idNumb) + ".ini");
    while(existChecker.exists()) {
        idNumb++;
        existChecker.setFileName(makePath + "/appt" + QString::number(idNumb) +
                                 ".ini");
    }

    QSettings writeAppt(makePath + "/appt" + QString::number(idNumb) +
                        ".ini", QSettings::IniFormat);

    if(allDay) {
        writeAppt.setValue("StartTime","12:00 AM");
        writeAppt.setValue("Duration",24*60);
    }
    else {
        writeAppt.setValue("StartTime",getTime);
        writeAppt.setValue("Duration",getDurationMins);
    }
    if(allChairs) {
        writeAppt.setValue("Chair","ALL_CHAIRS");
    } else {
        writeAppt.setValue("Chair",getChair);
    }

    writeAppt.setValue("Comments",getComments);
    writeAppt.setValue("Status","Blocker");
    writeAppt.sync();

    CDGitManager::commitData("Added in a blockout time for " +  getDate.toString("MMM") + "/" +
                             QString::number(getDate.day()) + "/" + QString::number(getDate.year()));

    return makePath + "/appt" + QString::number(idNumb) +".ini";
}

void CDScheduleManager::removeAppointment(QString filename)
{
    //First remove the symbolic link
    QSettings readAppt(filename,QSettings::IniFormat);
    QString patientID = readAppt.value("PatientID").toString();
    QString patAptDirStr = CDFileLocations::getPatientAppointmentDirectory(patientID);
    QDir patAptDir(patAptDirStr);
    QStringList files = patAptDir.entryList(QDir::NoDotAndDotDot | QDir::Files);
    foreach(QString file, files) {
        QString target = QFile::symLinkTarget(patAptDirStr + "/" + file);
        if(target == filename) {
            QFile delMe(patAptDirStr + "/" + file);
            delMe.remove();
        }
    }

    QFile removeMe(filename);
    if(removeMe.exists()) {
        removeMe.remove();
    }

    CDGitManager::commitData("Removed appointment for " + patientID);
}

void CDScheduleManager::moveAppointment(QString filename, QDate newDate)
{
    //First remove the symbolic link
    QSettings readAppt(filename,QSettings::IniFormat);
    QString patientID = readAppt.value("PatientID").toString();
    QString patAptDirStr = CDFileLocations::getPatientAppointmentDirectory(patientID);
    QString startTime = readAppt.value("StartTime").toString();
    QDir patAptDir(patAptDirStr);
    QStringList files = patAptDir.entryList(QDir::NoDotAndDotDot | QDir::Files);
    foreach(QString file, files) {
        QString target = QFile::symLinkTarget(patAptDirStr + "/" + file);
        if(target == filename) {
            QFile delMe(patAptDirStr + "/" + file);
            delMe.remove();
        }
    }

    //Now make the new file
    QString makePath = CDDefaults::getLocalScheduleDir() + newDate.toString("MMM") + "/" +
            QString::number(newDate.day()) + "/" + QString::number(newDate.year());
    QDir addDir(makePath);
    if(!addDir.exists()) {
        addDir.mkpath(".");
    }

    int idNumb = 1;
    QFile existChecker(makePath + "/appt" + QString::number(idNumb) + ".ini");
    while(existChecker.exists()) {
        idNumb++;
        existChecker.setFileName(makePath + "/appt" + QString::number(idNumb) +".ini");
    }

    QFile removeMe(filename);
    removeMe.copy(makePath + "/appt" + QString::number(idNumb) +".ini");

    if(removeMe.exists()) {
        removeMe.remove();
    }

    QFile::link("../../../locInfo/local/appointments/" + newDate.toString("MMM") + "/" +
                QString::number(newDate.day()) + "/" + QString::number(newDate.year()) + "/appt" +
                QString::number(idNumb) +".ini",
                patAptDirStr + newDate.toString("MMM-d-yyyy") + "@" + startTime + ".ini" );
    CDGitManager::commitData("Moved appointment for " + patientID);
}

int CDScheduleManager::convertTimeToMinsSinceMidnight(QString inputTime) //H:MM XM
{
    QTime timeObj = QTime::fromString(inputTime,"h:mm AP");
    //qDebug()<<"Got: " << inputTime<< " and returning " << timeObj.msecsSinceStartOfDay()/60000;
    return timeObj.msecsSinceStartOfDay()/60000;
}

QString CDScheduleManager::convertMinsSinceMidnightToString(int inputTime)
{
    QTime time = QTime::fromMSecsSinceStartOfDay(inputTime * 60 * 1000);
    return time.toString("h:mm AP");
}

QDateTime CDScheduleManager::getDateTimeForPatDirAppt(QString apptFile)
{
    QDateTime returnMe;
    int lastSlashIndex = apptFile.lastIndexOf("/");
    int lastDot = apptFile.lastIndexOf(".");
    QString dateTimeSubStr = apptFile.mid(lastSlashIndex+1,lastDot - lastSlashIndex-1);
    returnMe = QDateTime::fromString(dateTimeSubStr,"MMM-d-yyyy@h:mm AP");
    return returnMe;
}

QVariantList CDScheduleManager::getSearchResult(QString searchString)
{
    QVariantList returnMe;

    QList<QPair<QDate,QString>> apptNames = getAllAppointmentsEver();

    for(int i=0;i<apptNames.length();i++) {
        QPair<QDate,QString> item = apptNames.at(i);
        QDate setDate = item.first;
        QString file = item.second;
        QSettings reader(file,QSettings::IniFormat);
        if(reader.contains("PatientID")) {
            if(reader.value("PatientID").toString().contains(searchString,Qt::CaseInsensitive)) {
                returnMe.append(addAppointmentToSearchResult(&reader,setDate));
            }
        }
        if(reader.value("Comments").toString().contains(searchString,Qt::CaseInsensitive)) {
            returnMe.append(addAppointmentToSearchResult(&reader,setDate));
        }
    }


    return returnMe;
}

QList<QPair<QDate,QString>> m_AllAppointmentsEverCache;

QList<QPair<QDate,QString>> CDScheduleManager::getAllAppointmentsEver()
{
    if(m_AllAppointmentsEverCache.length() > 0) {
        return m_AllAppointmentsEverCache;
    }

    QList<QDate> sortMe;
    QString basePath = CDDefaults::getLocalScheduleDir();
    QDir startDir(basePath);
    QList<QVariantList> sortList;
    QStringList months = startDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString month, months) {
        QDir monthDir(basePath + "/" + month);
        QStringList days = monthDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        foreach(QString day, days) {
            QDir dayDir(basePath + "/" + month + "/" + day);
            QStringList years = dayDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
            foreach(QString year, years) {
                QDir yearDir(basePath + "/" + month + "/" + day + "/" + year);
                QStringList files = yearDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
                foreach(QString file, files) {
                    QString filePath = yearDir.absolutePath() + "/" + file;
                    QDate addDate = QDate::fromString(month + "/" + day + "/" + year,"MMM/d/yyyy");
                    QVariantList addToSort;
                    addToSort.append(addDate);
                    addToSort.append(filePath);
                    sortList.append(addToSort);
                }
            }
        }
    }


    std::sort(sortList.begin(),sortList.end(),dComp);
    foreach(QVariantList item, sortList) {
        QPair<QDate,QString> addMe;
        addMe.first = item.at(0).toDate();
        addMe.second = item.at(1).toString();
        m_AllAppointmentsEverCache.append(addMe);
    }

    return m_AllAppointmentsEverCache;

}

QVariant CDScheduleManager::addAppointmentToSearchResult(QSettings *appt, QDate getDate)
{
    QVariantMap returnMe;
    if(appt->contains("PatientID")) { //regular appointment
        returnMe["Type"] = "Appointment";
        returnMe["PatientID"] = appt->value("PatientID").toString();
    }
    else { //blocker
        returnMe["Type"] = "Blocker";

    }
    returnMe["Comments"] = appt->value("Comments").toString();
    returnMe["Date"] = getDate.toString("MMM/d/yy");
    returnMe["DateMonth"] = getDate.month();
    returnMe["DateDay"] = getDate.day();
    returnMe["DateYear"] = getDate.year();
    return returnMe;
}

bool CDScheduleManager::dComp(QVariantList left, QVariantList right)
{
    return left.at(0).toDate() > right.at(0).toDate();
}

