// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 7/8/2022 Alex Vernes

#include "cdgitmanager.h"
#include "cddefaults.h"

#include <QProcess>
#include <QDir>
#include <QDebug>
#include <QCoreApplication>
#include <QRunnable>


#define GITBIN QString("/usr/bin/git")

CDGitManager::CDGitManager(QObject *parent) : QObject(parent){}

QString CDGitManager::commitData(QString comment)
{
    return runCommit(comment,false);
}

QString CDGitManager::commitAndSignData(QString comment)
{
    return runCommit(comment,true);
}

QString CDGitManager::pullData()
{
    checkRepo();
    QString commitPath = CDDefaults::getBaseDataPath();
    QProcess pro;
    pro.setProgram(GITBIN);
    pro.setWorkingDirectory(commitPath);
    pro.setArguments(generatePullArgs());
    pro.start();
    pro.waitForFinished();

    return pro.readAllStandardError();
}

QString CDGitManager::runCommit(QString comment, bool signData)
{
    checkRepo();

    QString commitPath = CDDefaults::getBaseDataPath();
    //check if existing .git repo
    QDir gitDir = commitPath + "/.git/";
    QProcess pro;
    pro.setProgram(GITBIN);
    pro.setWorkingDirectory(commitPath);

    //Add any possible files
    QStringList addArgs;
    addArgs.append("add");
    addArgs.append("--all");
    pro.setArguments(addArgs);
    pro.start();
    pro.waitForFinished();

    //commit with comment
    QStringList commitArgs;
    commitArgs.clear();
    commitArgs.append("commit");
    commitArgs.append("-a");
    if(signData) {
        commitArgs.append("-S");
    }
    commitArgs.append("-m");
    commitArgs.append(comment);
    pro.setArguments(commitArgs);
    pro.start();
    pro.waitForFinished();
    QString allOut = pro.readAllStandardOutput();
    QString returnMe = "Good: " + allOut;
    if(pro.exitCode() != 0) {
        returnMe = QString("Error ") + QString::number(pro.exitCode()) + ": "
                + allOut;
    }

    //attempt to merge in remote changes

    pro.setArguments(generatePullArgs());
    pro.start();
    pro.waitForFinished();

    //push most recent changes / newest version
    QStringList publishArgs;
    publishArgs.append("publish");
    pro.setArguments(publishArgs);
    pro.startDetached();

    return returnMe;
}

void CDGitManager::initRepo()
{
    checkRepo();
    QDir checkDocDir(CDDefaults::getDocPrefDir());
    if(!checkDocDir.exists()) {
        checkDocDir.mkpath(".");
    }
    QDir checkLocDir(CDDefaults::getLocationsDir());
    if(!checkLocDir.exists()) {
        checkLocDir.mkpath(".");
    }
    QDir checkPatDir(CDDefaults::getPatDir());
    if(!checkPatDir.exists()) {
        checkPatDir.mkpath(".");
    }
    //No need to commit because git ignores empty directories
}

void CDGitManager::checkRepo()
{
    QString commitPath = CDDefaults::getBaseDataPath();

    //first check if ~/clearDentalData exists
    QDir baseDir(commitPath);
    if(!baseDir.exists()) {
        baseDir.mkpath(".");
    }

    //now check if there is an existing .git repo
    QDir gitDir(commitPath + "/.git/");
    if(!gitDir.exists()) { //init the repo if it doesn't exist
        QProcess pro;
        pro.setProgram(GITBIN);
        pro.setWorkingDirectory(commitPath);
        QStringList initArgs;
        initArgs.append("init");
        pro.setArguments(initArgs);
        pro.start();
        pro.waitForFinished();
    }
}

QStringList CDGitManager::generatePullArgs()
{
    QStringList returnMe;
    returnMe.append("pull");
    returnMe.append("-s");
    returnMe.append("recursive");
    returnMe.append("-X");
    returnMe.append("ours");
    return returnMe;
}
