// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.12

Pane {
    property var backMaterialColor: Material.Green
    property int setRadius: 10
    property real setOpacity: 0.5
    property int borderWidth:0
    property color borderColor: "transparent"

    background: Rectangle {
        id: paneBack
        anchors.fill: parent
        radius: setRadius
        color: Material.color(backMaterialColor)
        opacity: setOpacity
        border.width: borderWidth
        border.color: borderColor
    }
}
