// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdtoollauncher.h"


#include <QCoreApplication>
#include <QDebug>
#include <QDir>

#include "cdfilelocations.h"

#define INSTALL_DIR QString("/usr/bin/")

static QProcess m_Process; //adding it to the header file will cause errors
qint64 m_pid = -1;
static QMap<int,QProcess*> m_listenMap;
static QMap<int,qint64> m_listenProcessMap;

CDToolLauncher::CDToolLauncher(QObject *parent) : QObject(parent){}

void CDToolLauncher::launchTool(CDTool whichApp) {
    QProcess pro;
    QString progString;

    switch (whichApp) {
    case DentalPlanClaims:
        progString = "cleardental-dentalplan-claims";
        break;
    case ManagePreferences:
        progString = "cleardental-manage-preferences";
        break;
    case NewPatient:
        progString = "cleardental-new-patient";
        break;
    case Schedule:
        progString = "cleardental-schedule";
        break;
    case ProductionReport:
        progString = "cleardental-production-report";
        break;
    case SelectPatient:
        progString = "cleardental-select-patient";
        break;
    case Media_Controls:
        progString = "cleardental-media-controls";
        break;
    case Messaging:
        progString = "cleardental-messaging";
        break;
    default:
        qDebug()<<"Invalid App";
        break;
    }
    pro.setProgram(INSTALL_DIR + progString);
    pro.startDetached();
}

void CDToolLauncher::launchTool(CDToolLauncher::CDTool whichApp, QString patID)
{
    QProcess pro;
    QString progString;

    switch (whichApp) {
    case Billing:
        progString = "cleardental-superbilling";
        break;
    case LegacyBilling:
        progString = "cleardental-billing";
        break;
    case ChartMissingTeeth:
        progString = "cleardental-chart-missing-teeth";
        break;
    case CompExam:
        progString = "cleardental-comprehensive-exam";
        break;
    case DentalPlanClaims:
        progString = "cleardental-dentalplan-claims";
        break;
    case DentureTx:
        progString = "cleardental-denture-tx";
        break;
    case ExtraoralExam:
        progString = "cleardental-extraoral-exam";
        break;
    case HardTissueCharting:
        progString = "cleardental-hard-tissue-charting";
        break;
    case IntraoralExam:
        progString = "cleardental-intraoral-exam";
        break;
    case LimitedExam:
        progString = "cleardental-limited-exam";
        break;
    case PerioCharting:
        progString = "cleardental-perio-charting";
        break;
    case PerioDxTx:
        progString = "cleardental-perio-dxtx";
        break;
    case PeriodicExam:
        progString = "cleardental-periodic-exam";
        break;
    case PediatricExam:
        progString = "cleardental-pediatric-exam";
        break;
    case TakePhotograph:
        progString = "cleardental-photograph-acquisition";
        break;
    case Prescription:
        progString = "cleardental-prescription";
        break;
    case Prophy:
        progString = "cleardental-prophy";
        break;
    case TakeRadiograph:
        progString = "cleardental-radiograph-acquisition";
        break;
    case ReviewCaseNotes:
        progString = "cleardental-review-casenotes";
        break;
    case ReviewPatInterview:
        progString = "cleardental-review-pat-interview";
        break;
    case ReviewPatMedical:
        progString = "cleardental-review-pat-medical";
        break;
    case ReviewPatPersonal:
        progString = "cleardental-review-pat-personal";
        break;
    case ReviewRadiographs:
        progString = "cleardental-review-radiographs";
        break;
    case ReviewTxPlan:
        progString = "cleardental-review-tx-plan";
        break;
    case SchedulePatient:
        progString = "cleardental-schedule-patient";
        break;
    case Vitals:
        progString = "cleardental-vitals";
        break;
    case COVID_Screen:
        progString = "cleardental-covid-screening";
        break;
    case ReviewPatTxHistory:
        progString = "cleardental-review-pat-tx-history";
        break;
    case RemPros:
        progString = "cleardental-rem-pros";
        break;
    case PeriodontalFollowup:
        progString = "cleardental-perio-followup";
        break;
    case Messaging:
        progString = "cleardental-messaging";
        break;
    default:
        qDebug()<<"Invalid App";
        break;
    }

    QStringList arguments;
    arguments <<patID;

    pro.setProgram(INSTALL_DIR + progString );
    pro.setArguments(arguments);
    pro.startDetached();

}

void CDToolLauncher::launchTool(CDToolLauncher::CDTool whichApp, QString patID, QStringList procedureStrings)
{
    QProcess pro;
    QString progString;

    switch (whichApp) {
    case Extraction:
        progString = "cleardental-extraction";
        break;
    case FixedPros:
        progString = "cleardental-fixed-pros";
        break;
    case Operative:
        progString = "cleardental-operative";
        break;
    case OrthoVisit:
        progString = "cleardental-ortho-visit";
        break;
    case RCT:
        progString = "cleardental-rct";
        break;
    case SCRP:
        progString = "cleardental-scrp";
        break;
    case SchedulePatient:
        progString = "cleardental-schedule-patient";
        break;
    case Sealant:
        progString = "cleardental-sealant";
        break;
    case Generic_Procedure:
        progString = "cleardental-generic-procedure";
        break;
    case PediatricExam:
        progString = "cleardental-pediatric-exam";
        break;
    case SpaceMaintainer:
        progString = "cleardental-space-maintainer";
        break;
    case PanoramicCaseNote:
        progString = "cleardental-panoramic-casenote";
        break;
    default:
        qDebug()<<"Invalid App";
        break;
    }
    pro.setProgram(INSTALL_DIR + progString );
    QStringList arguments;
    arguments <<patID<< procedureStrings;
    pro.setArguments(arguments);
    pro.startDetached();
}

void CDToolLauncher::launchGame(CDToolLauncher::LinuxGame game, bool topMonitor)
{
    Q_UNUSED(topMonitor); //KWin / KDE right now takes care of this; probably will not need this arg in the future
    QString progString;
    QStringList arguments;

    switch (game) {
    case TUX_RACER:
        progString = "/usr/local/bin/etr";
        break;
    case CAVE_STORY:
        progString = QDir::homePath() +  "/Games/CaveStoryPlus/CaveStory+_64";
        m_Process.setWorkingDirectory(QDir::homePath() +  "/Games/CaveStoryPlus/");
        break;
    case WONDER_BOY:
        progString = QDir::homePath() +  "/Games/Wonder Boy The Dragons Trap/game/x86_64/wb.x86_64";
        m_Process.setWorkingDirectory(QDir::homePath() +  "/Games/Wonder Boy The Dragons Trap/game/x86_64/");
        break;
    case ALL_STAR:
        progString = "/usr/bin/ares";
        arguments.append(QDir::homePath() +"/Games/roms/allStar.sfc");
        break;
    case TUX_KART:
        progString = "/usr/local/bin/supertuxkart";
        arguments.append("--race-now");
    default:
        break;
    }
    m_Process.setProgram(progString);


//    if(topMonitor) {
//        arguments <<"";
//    }
    m_Process.setArguments(arguments);
    m_Process.startDetached(&m_pid);
    //qDebug()<<m_pid;
}

void CDToolLauncher::launchListenerProcess(ListenerProcess getProcess)
{

    if(getProcess == SHERPA_NCNN) {
        QProcess *pro = new QProcess(); //make sure it's in the heap just in case
        pro->setWorkingDirectory(QDir::homePath() + "/3rd/sherpa-ncnn/");
        pro->setProgram(QDir::homePath() + "/3rd/sherpa-ncnn/build/bin/sherpa-ncnn-microphone");
        QStringList args;
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/tokens.txt";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/encoder_jit_trace-pnnx.ncnn.param";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/encoder_jit_trace-pnnx.ncnn.bin";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/decoder_jit_trace-pnnx.ncnn.param";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/decoder_jit_trace-pnnx.ncnn.bin";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/joiner_jit_trace-pnnx.ncnn.param";
        args<< "sherpa-ncnn-streaming-zipformer-20M-2023-02-17/joiner_jit_trace-pnnx.ncnn.bin";
        args<< "2";
        args<< "greedy_search";
        pro->setArguments(args);
        qint64 getPid;
        pro->setStandardErrorFile(QDir::tempPath() + "/periolog.txt");
        if(pro->startDetached(&getPid)) {
            m_listenMap.insert((int)SHERPA_NCNN, pro);
            m_listenProcessMap.insert((int)SHERPA_NCNN, getPid);
        }

    }

}

QProcess *CDToolLauncher::getListenerProcess(ListenerProcess getProcess)
{
    QProcess *returnMe = nullptr;
    int enumVal = (int) getProcess;
    if(m_listenMap.contains(enumVal)) {
        returnMe = m_listenMap.value(enumVal);
    }
    return returnMe;
}

void CDToolLauncher::killListenerProcess(ListenerProcess getProcess)
{
    int enumVal = (int) getProcess;
    if(m_listenMap.contains(enumVal)) {
        QProcess pro;
        QStringList args;
        args<<"-9";
        args<<QString::number(m_listenProcessMap.value(enumVal));
        pro.setProgram("/usr/bin/kill");
        pro.setArguments(args);
        pro.start();
        pro.waitForFinished();
        m_listenMap.remove(enumVal);
        m_listenProcessMap.remove(enumVal);
    }
}

void CDToolLauncher::killGame()
{
    if(m_pid > 0) {
        //qDebug()<<"Killing game: " << m_pid;
        QProcess pro;
        QStringList args;
        args<<"-9";
        args<<QString::number(m_pid);
        pro.setProgram("/usr/bin/kill");
        pro.setArguments(args);
        pro.start();
        pro.waitForFinished();
        m_pid = -1;
    }

}

void CDToolLauncher::launchDolphin(QString url)
{
    QProcess pro;
    QStringList arguments;
    pro.setProgram("/usr/bin/dolphin");
    arguments<<url;
    pro.setArguments(arguments);
    pro.startDetached();
}

void CDToolLauncher::launchKate(QString url)
{
    QProcess pro;
    QStringList arguments;
    pro.setProgram("/usr/bin/kate");
    arguments<<url;
    pro.setArguments(arguments);
    pro.startDetached();

}
