// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDCLEARINGHOUSECOMM_H
#define CDCLEARINGHOUSECOMM_H

#include <QObject>
#include <QVariantMap>
#include <QVariantList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QAuthenticator>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QSettings>

class CDClearinghouseComm : public QObject
{
    Q_OBJECT
public:
    explicit CDClearinghouseComm(QObject *parent = nullptr);

    Q_INVOKABLE static QString generate837ClaimString(QVariantMap inputData,bool claimPrimary);
    Q_INVOKABLE static QString sendEDSEDIPeriodontalAttachment(QString patID, QString EDSEDIClaimID);
    Q_INVOKABLE static QString sendRadiographAttachments(QString patID, QString EDSEDIClaimID,QStringList filenames);


//    Q_INVOKABLE static QString generateEdsediXMLString(QVariantMap inputData,bool claimPrimary,
//                                                       bool isPreEstimate = false);
private:
    static QString makePad(QString input, int pad);

//    static QString generateEdsediXMLClaimString(QVariantMap inputData,bool claimPrimary,
//                                                bool isPreEstimate = false);


    QNetworkAccessManager *dxActiveStatusAccessManager;
    QNetworkReply *dxActiveStatusReply;
};

#endif // CDCLEARINGHOUSECOMM_H
