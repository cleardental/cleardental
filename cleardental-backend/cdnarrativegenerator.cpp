#include "cdnarrativegenerator.h"

#include <QDebug>
#include <QPainter>
#include <QPrinter>
#include <QDir>
#include <QProcess>
#include <QSettings>
#include <QStaticText>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFontMetrics>
#include <QPageSize>
#include <QMarginsF>

#include "cdfilelocations.h"
#include "cdproviderinfo.h"

CDNarrativeGenerator::CDNarrativeGenerator(QObject *parent)
    : QObject{parent}
{}

QString CDNarrativeGenerator::generateGenericNarrative(QString getNarrativeText, QString getPatID,
                                                       QString getProviderID)
{
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/tempNar-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setPageMargins(QMarginsF(0.5,0.5,0.5,0.5),QPageLayout::Inch);
    QPainter painter;

    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return "";
    }

    QFont regularFont("FreeSerif",12);
    QFont headerFont("FreeSerif",18);

    //First do the header
    QSettings pracInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    pracInfoSettings.beginGroup("GeneralInfo");
    painter.setFont(headerFont);
    QString headerText = pracInfoSettings.value("PracticeName").toString() + "\n";
    headerText += pracInfoSettings.value("AddrLine1").toString() + "\n";
    if( pracInfoSettings.value("AddrLine2").toString().trimmed().length() > 1) {
        headerText += pracInfoSettings.value("AddrLine2").toString() + "\n";
    }
    headerText += pracInfoSettings.value("City").toString() + ", ";
    headerText += pracInfoSettings.value("State").toString() + " ";
    headerText += pracInfoSettings.value("Zip").toString() + "\n";
    headerText += "Phone: " + pracInfoSettings.value("PhoneNumber").toString() + "\n";
    headerText += "Website: " +pracInfoSettings.value("Website").toString() + "\n";

    QRectF fullPageRect(0,0,printer.width(),printer.height());
    QFontMetrics headerMetric(headerFont);
    qreal headerHeight = headerMetric.boundingRect(fullPageRect.toRect(),
                                                   Qt::AlignTop | Qt::AlignLeft,headerText).height();
    painter.drawText(fullPageRect, Qt::AlignTop | Qt::AlignLeft,headerText);

    QSettings patInfo(CDFileLocations::getPersonalFile(getPatID),QSettings::IniFormat);
    patInfo.beginGroup("Name");
    QString bodyText = "To whom it may concern:\n\t";
    bodyText+= "This letter is regarding my patient ";
    bodyText+= patInfo.value("FirstName").toString() + " ";
    bodyText+= patInfo.value("LastName").toString() + " ";
    patInfo.endGroup();
    patInfo.beginGroup("Personal");
    bodyText+= "(Date of birth: " + patInfo.value("DateOfBirth").toString() + "). ";
    bodyText+= getNarrativeText;
    bodyText+="\n\t\t\t\t\t\t\tSincerely:";

    QSettings providerInfoSettings(CDFileLocations::getDocInfoFile(getProviderID),QSettings::IniFormat);
    bodyText+="\n\t\t\t\t\t\t\t\t" +
                providerInfoSettings.value("FirstName").toString() + " " +
                providerInfoSettings.value("LastName").toString() + " " +
                providerInfoSettings.value("Title").toString();

    painter.setFont(regularFont);
    QRectF lowerPageRect(0,headerHeight+10,printer.width(),printer.height());
    painter.drawText(lowerPageRect, Qt::AlignTop | Qt::AlignLeft | Qt::TextExpandTabs | Qt::TextWordWrap,bodyText);
    painter.end();

    // QFontMetrics bodyMetric(regularFont);
    // qreal bodyHeight =  bodyMetric.boundingRect(lowerPageRect.toRect(),
    //                                              Qt::AlignTop | Qt::AlignLeft | Qt::TextExpandTabs | Qt::TextWordWrap,
    //                                            bodyText).height();
    // QRectF bottomPageRect(0,headerHeight + bodyHeight +10,printer.width(),printer.height());



    QFile readPDF(printer.outputFileName());
    readPDF.open(QIODevice::ReadOnly);
    QByteArray pdfBytes = readPDF.readAll();
    return pdfBytes.toBase64();
}
