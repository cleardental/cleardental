// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdpatientmanager.h"
#include "cddefaults.h"
#include "cddefaults.h"

#include <QFileInfo>
#include <QFileInfoList>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QSettings>

#include "cdfilelocations.h"

CDPatientManager::CDPatientManager(QObject *parent) : QObject(parent)
{

}


QList<QVariant> CDPatientManager::getAllPatients() {
    QList<QVariant> returnMe;

    QDir patDir = QDir(CDDefaults::getPatDir());
    QStringList filters;
    foreach(QFileInfo info, patDir.entryInfoList(filters,QDir::Dirs |
                                                 QDir::NoDotAndDotDot)) {
        QMap<QString, QVariant> map;
        map[getDirString()] = info.absoluteFilePath();

        QString patientFilename =CDFileLocations::getPersonalFile(info.fileName());
        QSettings reader(patientFilename,QSettings::IniFormat);

        reader.beginGroup("Name");
        map[getFirstNameString()] = reader.value("FirstName");
        map[getLastNameString()] = reader.value("LastName");
        map[getMiddleNameString()] = reader.value("MiddleName");
        map[getPreferredNameString()] = reader.value("PreferredName");
        reader.endGroup();

        reader.beginGroup("Personal");
        map[getDOBString()] = reader.value("DateOfBirth");
        map[getProfileString()] = CDFileLocations::getProfileImageFile(info.fileName());
        map[getPatIDString()] = info.fileName();
        reader.endGroup();

        reader.beginGroup("Phones");
        map["CellPhone"] = reader.value("CellPhone","").toString().replace("-","").replace(".","").replace(" ","");
        map["HomePhone"] = reader.value("HomePhone","").toString().replace("-","").replace(".","").replace(" ","");

        reader.endGroup();

        foreach(QString key, map.keys() ) {
            if(!map[key].isValid()) {
                map[key] = "";
            }
        }

        QVariant addMe(map);

        returnMe.append(addMe);
    }
    return returnMe;
}

QStringList CDPatientManager::getAllPatientDirs()
{
    QStringList returnMe;

    QDir patDir = QDir(CDDefaults::getPatDir());
    QStringList filters;
    foreach(QFileInfo info, patDir.entryInfoList(filters,QDir::Dirs |QDir::NoDotAndDotDot)) {
        returnMe.append(info.absoluteFilePath());
    }
    return returnMe;
}

QStringList CDPatientManager::getAllPatientIds()
{
    QStringList returnMe;

    QDir patDir = QDir(CDDefaults::getPatDir());
    QStringList filters;
    foreach(QFileInfo info, patDir.entryInfoList(filters,QDir::Dirs |QDir::NoDotAndDotDot)) {
        returnMe.append(info.fileName());
    }
    return returnMe;
}


QString CDPatientManager::getFirstNameString() {
    return "FirstName";
}

QString CDPatientManager::getMiddleNameString() {
    return "MiddleName";
}

QString CDPatientManager::getLastNameString() {
    return "LastName";
}

QString CDPatientManager::getPreferredNameString() {
    return "PreferredName";
}

QString CDPatientManager::getDOBString() {
    return "DOB";
}

QString CDPatientManager::getProfileString() {
    return "Profile";
}

QString CDPatientManager::getDirString() {
    return "Dir";
}

QString CDPatientManager::getPatIDString()
{
    return "PatID";
}

