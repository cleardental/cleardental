// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 6/3/2022 Alex Vernes

#include "cdpatientfilemanager.h"

#include <QFile>
#include <QCoreApplication>
#include <QDebug>
#include <QProcess>
#include <QDir>
#include <QFileInfo>
#include <QSettings>
#include <QImage>
#include <QPixmap>
#include <QPainter>
#include <QPen>

#include <QJsonDocument>

#include "cddefaults.h"
#include "cdfilelocations.h"
#include "cdgitmanager.h"

CDPatientFileManager::CDPatientFileManager(QObject *parent) : QObject(parent)
{

}

void CDPatientFileManager::importNewProfileImage(QString patientID, QString url, double x, double y, double width)
{
    //First make sure the directory exists
    QDir patProfileDir(CDFileLocations::getProfileImageDir(patientID));
    if(!patProfileDir.exists()) {
        patProfileDir.mkpath(".");
    }

    //Remove the "file://" part of the URL because QFile can't handle that
    if(url.startsWith("file://")) {
        url.remove(0,7);
    }

    QFile fromFile(url);
    QFile toFile( CDFileLocations::getProfileImageFile(patientID) );

    if(toFile.exists()) {
        toFile.remove();
    }

    QImage input(url);
    QImage output = input.copy(x,y,width,width);
    output.save(CDFileLocations::getProfileImageFile(patientID));
    CDGitManager::commitData("Added profile photo for " + patientID);
}

void CDPatientFileManager::importNewIntraOralImage(QString patientID,QString location, QString url)
{
    QDir saveDir(CDFileLocations::getIntraOralDir(patientID));
    if(!saveDir.exists()) {
        saveDir.mkpath(".");
    }

    //Remove the "file://" part of the URL because QFile can't handle that
    if(url.startsWith("file://")) {
        url.remove(0,7);
    }

    QFile fromFile(url);
    QString destString = CDFileLocations::getIntraOralDir(patientID) + QDate::currentDate().toString("MMM-d-yyyy")
            +"-" + location+ ".png";
    QFile *toFile = new QFile( destString );

    int counter = 1;
    while(toFile->exists()) {
        toFile = new QFile(CDFileLocations::getIntraOralDir(patientID) + QDate::currentDate().toString("MMM-d-yyyy") +
                           +"-" + location+ + "-" +QString::number(counter) + ".png");
        counter++;
    }

    fromFile.copy(toFile->fileName());

    CDGitManager::commitData("Added intraoral image for " + patientID);
}

QString CDPatientFileManager::importPatient(QVariantMap jsonData)
{
    //qDebug()<<jsonData.keys();

    //Make the offical filename for the patient
    QString lastName = jsonData.value("lastName").toString().trimmed();
    QString firstName = jsonData.value("firstName").toString().trimmed();
    QString dobString = jsonData.value("dob").toString();//YYYY-MM-DD
    QStringList dobArray = dobString.split("-"); //YYYY,MM,DD
    QDate dob(dobArray[0].toInt(),dobArray[1].toInt(),dobArray[2].toInt());
    QString patFileName = lastName + ", " + firstName + " (" + dob.toString("MMM-d-yyyy") + ")";
    qint64 submitTime = jsonData.value("submitTime").toULongLong();
    QDateTime submitTimeObj;
    submitTimeObj.setSecsSinceEpoch(submitTime);

    //Now make the directories
    QDir patsDir(CDDefaults::getPatDir());
    patsDir.mkdir(patFileName);
    QString fullPath = CDDefaults::getPatDir() + patFileName + "/";

    QDir patDir(fullPath);
    patDir.mkpath("dental");
    patDir.mkpath("medical");
    patDir.mkpath("financial");
    patDir.mkpath("appointments");
    QString consentPath = "images/consents/" + submitTimeObj.toString("MMM/d/yyyy/");
    patDir.mkpath(consentPath);


    //This is because of a bug in git which doesn't allow empty directories
    QFile gitKeep(fullPath + "appointments/.gitkeep");
    gitKeep.open(QIODevice::WriteOnly);
    gitKeep.close();
    QFile gitKeep2(fullPath + "dental/.gitkeep");
    gitKeep2.open(QIODevice::WriteOnly);
    gitKeep2.close();


    //***personal.ini***

    QSettings personalINI(CDFileLocations::getPersonalFile(patFileName),QSettings::IniFormat);
    personalINI.beginGroup("Address");
    personalINI.setValue("City",jsonData.value("HomeCity",""));
    personalINI.setValue("Country","USA"); //TODO: Make this based on locInfo
    personalINI.setValue("State",jsonData.value("HomeState",""));
    personalINI.setValue("StreetAddr1",jsonData.value("addr1",""));
    personalINI.setValue("StreetAddr2",jsonData.value("addr2",""));
    personalINI.setValue("Zip",jsonData.value("HomeZip",""));
    personalINI.endGroup();

    personalINI.beginGroup("Emails");
    personalINI.setValue("Email",jsonData.value("homeEmail",""));
    personalINI.setValue("Facebook",jsonData.value("facebookName",""));
    personalINI.setValue("Instagram",jsonData.value("instagramName",""));
    personalINI.setValue("WhatsApp",jsonData.value("whatsAppName",""));
    personalINI.endGroup();

    personalINI.beginGroup("Name");
    lastName[0] = lastName[0].toUpper();
    firstName = firstName.toLower();
    firstName[0] = firstName[0].toUpper();
    personalINI.setValue("FirstName",firstName);
    personalINI.setValue("LastName",lastName);
    personalINI.setValue("MiddleName",jsonData.value("middleName",""));
    personalINI.setValue("PreferredName",jsonData.value("preferredName",""));
    personalINI.endGroup();

    personalINI.beginGroup("Personal");
    personalINI.setValue("DateOfBirth",dob.toString("M/d/yyyy"));
    personalINI.setValue("Ethnicity",jsonData.value("Ethnicity",""));
    personalINI.setValue("Gender",jsonData.value("gender",""));
    personalINI.setValue("Race",jsonData.value("Race",""));
    personalINI.setValue("Sex",jsonData.value("sex",""));
    personalINI.endGroup();

    personalINI.beginGroup("Phones");
    personalINI.setValue("CellPhone",jsonData.value("cellPhone",""));
    personalINI.setValue("HomePhone",jsonData.value("homePhone",""));
    personalINI.endGroup();

    personalINI.beginGroup("Preferences");
    personalINI.setValue("AvailableDays",jsonData.value("whenAvailable").toStringList().join(" "));
    personalINI.setValue("AvailableTime",jsonData.value("dayTimeAvail").toStringList().join(" "));
    personalINI.setValue("PreferredContact",jsonData.value("howContact",""));
    if(jsonData.value("language","").toString() == "pr") {
        personalINI.setValue("PreferredLanguage","pt"); //oh boy... we now have to decide between pr vs. pt....
    }
    personalINI.endGroup();

    personalINI.beginGroup("Work");
    personalINI.setValue("WorkPhone",jsonData.value("workPhone",""));
    personalINI.endGroup();

    if(jsonData.contains("bestContactMethod")) { //pain patient
        QString method = jsonData.value("bestContactMethod").toString();
        if(method == "CellPhone") {
            personalINI.beginGroup("Preferences");
            personalINI.setValue("PreferredContact","callCell");
            personalINI.endGroup();

            personalINI.beginGroup("Phones");
            personalINI.setValue("CellPhone",jsonData.value("CellPhoneNumber"));
            personalINI.endGroup();
        }
        else if(method == "HomePhone") {
            personalINI.beginGroup("Preferences");
            personalINI.setValue("PreferredContact","callHome");
            personalINI.endGroup();

            personalINI.beginGroup("TextMe");
            personalINI.setValue("HomePhone",jsonData.value("HomePhoneNumber"));
            personalINI.endGroup();
        }
        else if(method == "TextMe") {
            personalINI.beginGroup("Preferences");
            personalINI.setValue("PreferredContact","textCell");
            personalINI.endGroup();

            personalINI.beginGroup("Phones");
            personalINI.setValue("CellPhone",jsonData.value("TextCellPhoneNumber"));
            personalINI.endGroup();
        }
    }

    personalINI.sync();

    //***newPatInterview.ini***
    QSettings newPatINI(CDFileLocations::getNewPatInterviewFile(patFileName),QSettings::IniFormat);
    QStringList newPatKeys = {"RefName","RefSource","bracesBefore","cc","docIssue","getDone","gumBleed","hadSCRP",
                              "haveHeadaches","lastCleaning","nightGrind","oftenBrush","oftenFloss","patientInPain",
                              "sleepIssue","submitTime","whereChip","whereSen","painLength","whereFeel","wantSave",
                              "howSoon","painLength", "whereFeel","wantSave","dependsPrice","howSoon","otherInfoText"};
    foreach(QString newPatKey, newPatKeys) {
        if(jsonData.contains(newPatKey)) {
            QString capNewPatKey = newPatKey;
            capNewPatKey[0] = capNewPatKey[0].toUpper();
            newPatINI.setValue(capNewPatKey,jsonData.value(newPatKey));
        }
    }
    newPatINI.sync();

    //***Medications***
    QList<QVariant> meds;
    for(int medCounter=1;medCounter<=6;medCounter++) {
        QString medKey = "medName" + QString::number(medCounter);
        if(jsonData.value(medKey).toString().length()>1) {
            QVariantMap med;
            med["DrugName"] = jsonData.value(medKey).toString();
            med["UsedFor"] = jsonData.value("medWhy" + QString::number(medCounter)).toString();
            med["PrescribedHere"] = false;
            meds.append(med);
        }
    }
    QJsonDocument  medDoc = QJsonDocument::fromVariant(meds);
    QFile writeMeds(CDFileLocations::getMedicationsFile(patFileName));
    writeMeds.open(QIODevice::WriteOnly | QIODevice::Text);
    writeMeds.write(medDoc.toJson());
    writeMeds.close();

    //***Medical conditions***
    QList<QVariant> conds;
    foreach(QString condKey,jsonData.keys()) {
        if(condKey.startsWith("condition-")) {
            QString conditionName = condKey.replace("condition-","");
            conditionName = conditionName.replace("_"," ");
            QVariantMap cond;
            cond["ConditionName"] = conditionName;
            cond["ConditionInfo"] = "";
            conds.append(cond);
        }
    }
    QJsonDocument medCondsDoc = QJsonDocument::fromVariant(conds);
    QFile writeConds(CDFileLocations::getConditionsFile(patFileName));
    writeConds.open(QIODevice::WriteOnly | QIODevice::Text);
    writeConds.write(medCondsDoc.toJson());
    writeConds.close();

    //***Allergies***
    QList<QVariant> allergies;
    for(int allergyCounter=1;allergyCounter<=6;allergyCounter++) {
        QString allergyKey = "allName" + QString::number(allergyCounter);
        if(jsonData.value(allergyKey).toString().length()>1) {
            QVariantMap allergy;
            allergy["AllergyName"] = jsonData.value(allergyKey).toString();
            QString sevKey = "severity" + QString::number(allergyCounter);
            if(jsonData.value(sevKey).toString().startsWith("Mild")) {
                allergy["AllergyReaction"] = "Mild / Rash";
            }
            else if(jsonData.value(sevKey).toString().startsWith("Moderate")) {
                allergy["AllergyReaction"] = "Moderate";
            }
            else if(jsonData.value(sevKey).toString().startsWith("Severe")) {
                allergy["AllergyReaction"] = "Severe";
            }
            else {
                allergy["AllergyReaction"] = "Unknown";
            }
            allergies.append(allergy);
        }
    }
    QJsonDocument allergyDoc = QJsonDocument::fromVariant(allergies);
    QFile writeAllergies(CDFileLocations::getAllergiesFile(patFileName));
    writeAllergies.open(QIODevice::WriteOnly | QIODevice::Text);
    writeAllergies.write(allergyDoc.toJson());
    writeAllergies.close();


    //***Dental Plans***
    QSettings dentalPlansINI(CDFileLocations::getDentalPlansFile(patFileName),QSettings::IniFormat);
    foreach(QString insKey, jsonData.keys()) {
        if(insKey.startsWith("priInsPolHolderDOB")) {
            dentalPlansINI.beginGroup("Primary");
            QString setKey = "PolHolderDOB";
            QDate jsonDate = QDate::fromString(jsonData.value(insKey).toString(),"yyyy-M-d");
            dentalPlansINI.setValue(setKey,jsonDate.toString("M/d/yyyy"));
            dentalPlansINI.endGroup();
        }
        else if(insKey.startsWith("secInsPolHolderDOB")) {
            dentalPlansINI.beginGroup("Secondary");
            QString setKey = "PolHolderDOB";
            QDate jsonDate = QDate::fromString(jsonData.value(insKey).toString(),"yyyy-M-d");
            dentalPlansINI.setValue(setKey,jsonDate.toString("M/d/yyyy"));
            dentalPlansINI.endGroup();
        }
        else if(insKey.startsWith("priIns")) {
            dentalPlansINI.beginGroup("Primary");
            QString setKey = insKey.replace("priIns", "");
            dentalPlansINI.setValue(setKey,jsonData.value("priIns" + insKey,""));
            dentalPlansINI.endGroup();
        }
        else if(insKey.startsWith("secIns")) {
            dentalPlansINI.beginGroup("Secondary");
            QString setKey = insKey.replace("secIns", "");
            dentalPlansINI.setValue(setKey,jsonData.value("secIns" + insKey,""));
            dentalPlansINI.endGroup();
        }
    }
    dentalPlansINI.beginGroup("Secondary");
    if(!dentalPlansINI.contains("Medical")) {
        dentalPlansINI.setValue("Medical",false);
    }
    if(!dentalPlansINI.contains("Dental")) {
        dentalPlansINI.setValue("Dental",false);
    }
    dentalPlansINI.endGroup();

    if(jsonData.value("selfPolicyHolder","").toString() == "yesSelf") {
        dentalPlansINI.beginGroup("Primary");
        dentalPlansINI.setValue("PolHolderRel","Self");
        dentalPlansINI.endGroup();
    }

    dentalPlansINI.sync();

    //***Consent***
    QStringList consentDots = jsonData.value("consentValues","0,0").toString().split(",");
    //first find the max values
    int maxX=0;
    int maxY=0;
    for(int i=0;i<consentDots.length();i++) {
        if(consentDots[i] != ".") {
            int getX = consentDots[i].toInt();
            int getY = consentDots[i+1].toInt();
            if(getX > maxX) {
                maxX = getX;
            }
            if(getY > maxY) {
                maxY = getY;
            }
            i++;
        }
    }

    QPixmap px(maxX+1,maxY+1);
    px.fill(Qt::transparent);
    int prevX = -1;
    int prevY = -1;
    QPainter paintFromMe(&px);
    paintFromMe.setPen(QPen(Qt::black,8,Qt::SolidLine,Qt::RoundCap));

    for(int i=0;i<consentDots.length();i++) {
        if(consentDots[i] != ".") {
            int getX = consentDots[i].toInt();
            int getY = consentDots[i+1].toInt();

            if(prevX > 0) {
                paintFromMe.drawLine(prevX,prevY,getX,getY);
            }
            prevX=getX;
            prevY =getY;
            i++;
        }
        else {
            prevX = -1;
            prevY = -1;
        }
    }
    paintFromMe.end();
    px.save(fullPath + consentPath + "newPatient.png");

    CDGitManager::commitData("Imported new patient from web server: " + patFileName);

    return patFileName;
}

bool CDPatientFileManager::patientExists(QString patientFileName)
{
    QDir patDir(CDFileLocations::getPatientDirectory(patientFileName));
    return patDir.exists();
}

QStringList CDPatientFileManager::getPatientPerioHistoryFiles(QString patientFileName)
{
    QStringList returnMe;

    QDir perioHistoryDir(CDFileLocations::getPatientPrevPerioHxDirectory(patientFileName));
    if(!perioHistoryDir.exists()) {
        perioHistoryDir.mkpath(".");
    }

    foreach(QFileInfo info, perioHistoryDir.entryInfoList(QDir::NoDotAndDotDot)) {
        returnMe.append(info.absoluteFilePath());
    }

    return returnMe;
}

void CDPatientFileManager::saveTodaysPerioChartAsHistory(QString patientFileName)
{
    QDir perioHistoryDir(CDFileLocations::getPatientPrevPerioHxDirectory(patientFileName));
    if(!perioHistoryDir.exists()) {
        perioHistoryDir.mkpath(".");
    }

    QFile todayPerioChart(CDFileLocations::getPerioChartFile(patientFileName));
    QString dest = CDFileLocations::getPatientPrevPerioHxDirectory(patientFileName) +
            "/" + QDate::currentDate().toString("MMM-d-yyyy") + ".ini";
    todayPerioChart.copy(dest);
}

void CDPatientFileManager::addRelationship(QString basePat, QString relPat, QString relationship)
{
    QDir patFamDir(CDFileLocations::getPatientFamilyDirectory(basePat));
    patFamDir.mkpath("."); //will still work even if it already exists

    QDir distFamDir(CDFileLocations::getPatientFamilyDirectory(relPat));
    distFamDir.mkpath(".");

    QString linkDest = "../../" + relPat + "/";

    QFile::link(linkDest,CDFileLocations::getPatientFamilyDirectory(basePat)+relationship);

    //Now to go to the relPat and set the inverse
    QString inverseString = "";
    if(relationship == "Spouse") {
        inverseString = "Spouse";
    }
    else if(relationship == "Parent") {
        inverseString = "Dependent";
    }
    else if(relationship == "Dependent") {
        inverseString = "Parent";
    }
    else if(relationship == "Sibling") {
        inverseString = "Sibling";
    }

    if(inverseString.length() > 0) {
        linkDest = "../../" + basePat + "/";
        QFile::link(linkDest,CDFileLocations::getPatientFamilyDirectory(relPat)+inverseString);
    }

    CDGitManager::commitData(basePat + " is now related to " + relPat + " as " + relationship);
}

void CDPatientFileManager::remRelationship(QString basePat, QString relPat, QString relationship)
{
    QFile::remove(CDFileLocations::getPatientFamilyDirectory(basePat)+relationship);

    QString inverseString = "";
    if(relationship == "Spouse") {
        inverseString = "Spouse";
    }
    else if(relationship == "Parent") {
        inverseString = "Dependent";
    }
    else if(relationship == "Dependent") {
        inverseString = "Parent";
    }
    else if(relationship == "Sibling") {
        inverseString = "Sibling";
    }

    if(inverseString.length() > 0) {
        QFile::remove(CDFileLocations::getPatientFamilyDirectory(relPat)+inverseString);
    }

    CDGitManager::commitData(basePat + " is no longer related to " + relPat + " as " + relationship);
}

void CDPatientFileManager::addNewPatWithRelationship(QString basePat, QString relationship, QString getFirstName,
                                                     QString getLastName, QDate getDOB, QString getSex, bool sameAddr,
                                                     bool sameDentalPlan, bool samePhoneNumber)
{
    //First create a new patient
    QVariantMap jsonData;
    jsonData["lastName"] = getLastName;
    jsonData["firstName"] = getFirstName;
    jsonData["dob"] = getDOB.toString("yyyy-MM-dd");
    jsonData["sex"] = getSex;

    QSettings basePatAddrReader(CDFileLocations::getPersonalFile(basePat),QSettings::IniFormat);

    if(sameAddr) {
        basePatAddrReader.beginGroup("Address");
        jsonData["HomeCity"] = basePatAddrReader.value("City");
        jsonData["HomeState"] = basePatAddrReader.value("State");
        jsonData["addr1"] = basePatAddrReader.value("StreetAddr1");
        jsonData["addr2"] = basePatAddrReader.value("StreetAddr2");
        jsonData["HomeZip"] = basePatAddrReader.value("Zip");
        basePatAddrReader.endGroup();
    }

    if(samePhoneNumber) {
        basePatAddrReader.beginGroup("Phones");
        jsonData["cellPhone"] = basePatAddrReader.value("CellPhone");
        jsonData["homePhone"] = basePatAddrReader.value("HomePhone");
        basePatAddrReader.endGroup();
    }

    QString newPatID = importPatient(jsonData);
    addRelationship(basePat,newPatID,relationship);

    if(sameDentalPlan) {
        QFile::remove(CDFileLocations::getDentalPlansFile(newPatID));
        QFile::copy(CDFileLocations::getDentalPlansFile(basePat),
                    CDFileLocations::getDentalPlansFile(newPatID));
        QSettings readNewDPlan(CDFileLocations::getDentalPlansFile(newPatID),QSettings::IniFormat);
        readNewDPlan.beginGroup("Primary");

        if(relationship == "Parent") {
            readNewDPlan.setValue("PolHolderRel","Self");
        }
        else {

            basePatAddrReader.beginGroup("Name");
            readNewDPlan.setValue("PolHolderFirst",basePatAddrReader.value("FirstName"));
            readNewDPlan.setValue("PolHolderLast",basePatAddrReader.value("LastName"));
            basePatAddrReader.endGroup();

            basePatAddrReader.beginGroup("Personal");
            readNewDPlan.setValue("PolHolderGender",basePatAddrReader.value("Sex"));
            readNewDPlan.setValue("PolHolderDOB",basePatAddrReader.value("DateOfBirth"));
            basePatAddrReader.endGroup();

            basePatAddrReader.beginGroup("Address");
            readNewDPlan.setValue("PolHolderAddress",basePatAddrReader.value("StreetAddr1"));
            readNewDPlan.setValue("PolHolderCity",basePatAddrReader.value("City"));
            readNewDPlan.setValue("PolHolderState",basePatAddrReader.value("State"));
            readNewDPlan.setValue("PolHolderZip",basePatAddrReader.value("Zip"));
            basePatAddrReader.endGroup();

            if(relationship == "Spouse") {
                readNewDPlan.setValue("PolHolderRel","Spouse");
            }
            else if(relationship == "Dependent") {
                readNewDPlan.setValue("PolHolderRel","Dependent");
            }
            else if(relationship == "Sibling") {
                readNewDPlan.setValue("PolHolderRel","Dependent");
            }
        }
    }

    CDGitManager::commitData(newPatID + " got some basic information from " + basePat);
}



QVariantList CDPatientFileManager::getRelationships(QString basePat)
{
    QVariantList returnMe;
    QDir patFamDir(CDFileLocations::getPatientFamilyDirectory(basePat));
    QString fullPatDir = CDDefaults::getPatDir();
    foreach(QString relPatID, patFamDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
        QFile relFile(CDFileLocations::getPatientFamilyDirectory(basePat) + relPatID);
        QStringList addMe;
        QString target = relFile.symLinkTarget();
        QString addPatID = target.replace(fullPatDir,"");
        addMe.append(addPatID);
        addMe.append(relPatID);
        returnMe.append(addMe);
    }
    return returnMe;
}

