// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDTOOLLAUNCHER_H
#define CDTOOLLAUNCHER_H

#include <QObject>
#include <QProcess>

class CDToolLauncher : public QObject
{
    Q_OBJECT
public:
    enum CDTool {
        Invalid,
        Billing,
        LegacyBilling,
        ChartMissingTeeth,
        CompExam,
        DentalPlanClaims,
        DentureTx,
        Extraction,
        ExtraoralExam,
        FixedPros,
        HardTissueCharting,
        IntraoralExam,
        LimitedExam,
        ManagePreferences,
        NewPatient,
        Operative,
        OrthoVisit,
        PerioCharting,
        PerioDxTx,
        PeriodicExam,
        PediatricExam,
        TakePhotograph,
        Prescription,
        ProductionReport,
        Prophy,
        TakeRadiograph,
        RCT,
        RemPros,
        ReviewCaseNotes,
        ReviewPatInterview,
        ReviewPatMedical,
        ReviewPatPersonal,
        ReviewPatTxHistory,
        ReviewRadiographs,
        ReviewTxPlan,
        Schedule,
        SchedulePatient,
        SCRP,
        SelectPatient,
        Vitals,
        COVID_Screen,
        Media_Controls,
        PeriodontalFollowup,
        Sealant,
        Messaging,
        SpaceMaintainer,
        PanoramicCaseNote,
        Generic_Procedure
    };
    Q_ENUM(CDTool)

    enum LinuxGame {
        TUX_RACER,
        SUPER_TUX,
        CAVE_STORY,
        WONDER_BOY,
        ALL_STAR,
        DEMON_BLADE,
        TUX_KART
    };
    Q_ENUM(LinuxGame)

    enum ListenerProcess {
        SHERPA_NCNN
    };
    Q_ENUM(ListenerProcess)

    explicit CDToolLauncher(QObject *parent = nullptr);

public slots:
    static void launchTool(CDTool whichApp);
    static void launchTool(CDTool whichApp, QString patID);
    static void launchTool(CDTool whichApp, QString patID, QStringList procedureStrings);
    static void launchGame(LinuxGame game, bool topMonitor);
    static void launchListenerProcess(ListenerProcess getProcess);
    static QProcess *getListenerProcess(ListenerProcess getProcess);
    static void killListenerProcess(ListenerProcess getProcess);
    static void killGame();
    static void launchDolphin(QString url);
    static void launchKate(QString url);


};

#endif // CDTOOLLAUNCHER_H
