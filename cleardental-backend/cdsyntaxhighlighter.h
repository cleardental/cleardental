// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSYNTAXHIGHLIGHTER_H
#define CDSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QObject>


QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

#include <hunspell/hunspell.hxx>

class CDSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    CDSyntaxHighlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text) override;

private:
    Hunspell *m_spell;
};

#endif // CDSYNTAXHIGHLIGHTER_H
