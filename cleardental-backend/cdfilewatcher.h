// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDFILEWATCHER_H
#define CDFILEWATCHER_H

#include <QObject>
#include <QFileSystemWatcher>

class CDFileWatcher : public QObject
{
    Q_OBJECT
    QString m_fileName;

public:
    explicit CDFileWatcher(QObject *parent = nullptr);

    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName)

    QString getFileName() const;


signals:
    void fileUpdated();

public slots:
    void setFileName(QString fileName);

private slots:
    void handleUpdate(QString fileName);

private:
    QFileSystemWatcher m_Watcher;
    void recurseDir(QString readDir);
};

#endif // CDFILEWATCHER_H
