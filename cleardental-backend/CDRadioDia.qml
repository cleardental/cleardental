// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import dental.clear 1.0

Dialog {
    id: radioDia
    parent: Overlay.overlay
    width: rootWin.width
    height: rootWin.height
    property string setSource: ""
    property real setSharpen: 2
    property real setBright: 0
    property real setContrast: 1

    property int highRectX: -1
    property int highRectY: -1
    property int highRectRadiusCanary

    property string vertexShaderString: "
                    uniform highp mat4 qt_Matrix;
                    attribute highp vec4 qt_Vertex;
                    attribute highp vec2 qt_MultiTexCoord0;
                    varying highp vec2 coord;
                    void main() {
                        coord = qt_MultiTexCoord0;
                        gl_Position = qt_Matrix * qt_Vertex;
                    }"
    property string fragmentShaderString: "
                            varying highp vec2 coord;
                            uniform sampler2D src;
                            uniform lowp float qt_Opacity;
                            uniform lowp float sharpenPower;
                            uniform lowp float brightenPower;
                            uniform lowp float contrastPower;
                            uniform lowp float setRenderWidth;
                            uniform lowp float setRenderHeight;

                            mat4 brightnessMatrix( float brightness )
                            {
                                return mat4( 1, 0, 0, 0,
                                             0, 1, 0, 0,
                                             0, 0, 1, 0,
                                             brightness, brightness, brightness, 1 );
                            }

                            mat4 contrastMatrix( float contrast )
                            {
                                float t = ( 1.0 - contrast ) / 2.0;

                                return mat4( contrast, 0, 0, 0,
                                             0, contrast, 0, 0,
                                             0, 0, contrast, 0,
                                             t, t, t, 1 );

                            }

                            vec4 sharpen(in sampler2D tex, in vec2 coords, in vec2 renderSize) {
                              float dx = 1.0 / renderSize.x;
                              float dy = 1.0 / renderSize.y;
                              vec4 sum = vec4(0.0);
                              sum += -1. * sharpenPower  * texture2D(tex, coords + vec2( -1.0 * dx , 0.0 * dy));
                              sum += -1. * sharpenPower* texture2D(tex, coords + vec2( 0.0 * dx , -1.0 * dy));
                              sum += ((sharpenPower*4.)+1.) * texture2D(tex, coords + vec2( 0.0 * dx , 0.0 * dy));
                              sum += -1. * sharpenPower * texture2D(tex, coords + vec2( 0.0 * dx , 1.0 * dy));
                              sum += -1. * sharpenPower* texture2D(tex, coords + vec2( 1.0 * dx , 0.0 * dy));
                              return sum;
                            }

                            void main() {
                                lowp vec4 tex = texture2D(src, coord);
                                gl_FragColor = brightnessMatrix( brightenPower )*
                                contrastMatrix( contrastPower ) *
                                sharpen(src,coord,vec2(setRenderWidth,setRenderHeight));
                            }"



    background: Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: .8
    }

    Component {
        id: radioWindowComp
        CDAppWindow {
            x: 0
            y: 1080
            width: 1920
            height: 1080
            visibility: ApplicationWindow.Windowed
            //visible: radioDia.visible

            Image {
                id: windowImage
                source: radioDia.setSource
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
            }

            ShaderEffect {
                id: windowShader
                width: windowImage.paintedWidth
                height: windowImage.paintedHeight
                anchors.centerIn: parent
                property variant src: windowImage
                property real sharpenPower: radioDia.setSharpen
                property real brightenPower: radioDia.setBright
                property real contrastPower: radioDia.setContrast
                property real setRenderWidth: windowImage.paintedWidth
                property real setRenderHeight: windowImage.paintedHeight
                onStatusChanged: {
                    if(status == ShaderEffect.Error) {
                        windowShader.visible = false;
                    }
                }
                vertexShader:radioDia.vertexShaderString
                fragmentShader:radioDia.fragmentShaderString
            }

            Rectangle {
                id: highlightRect
                property int candaryWatcher: highRectRadiusCanary
                onCandaryWatcherChanged: {
                    startHighlight.restart();
                }

                x: highRectX
                y: highRectY
                radius: 0
                width: radius
                height: radius
                color: "red"
                PropertyAnimation {
                    id: startHighlight
                    target: highlightRect
                    property: "radius"
                    from: 50
                    to: 0
                    duration: 1000
                }
            }
        }

    }

    property var screenList: [];

    CDConstLoader {
        id: m_constsL; //because you don't always get from the AppWindow from Dialog
    }

    onVisibleChanged: {
        if(visible) {
            screenList = [];

            if((Qt.application.screens.length > 2) && (m_constsL.getNodeType() === "touch")) {
                for(var i=1;i<Qt.application.screens.length;i++) {
                    var res = radioWindowComp.createObject();
                    res.x=0;
                    res.y=1080*(i-1);
                    screenList.push(res);
                }
            }
        }
        else {
            while(screenList.length > 0) {
                screenList.pop().destroy();
            }
        }
    }

    Image {
        id: rawRadio
        source: radioDia.setSource
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit



        MouseArea {
            anchors.fill: parent
            onPressed: (mouseEvent) => {
                           var leftPoint = (rawRadio.width - rawRadio.paintedWidth) / 2
                           var rightPoint = leftPoint + rawRadio.paintedWidth;
                           var topPoint = (rawRadio.height - rawRadio.paintedHeight) / 2
                           var bottomPoint = topPoint + rawRadio.paintedHeight;

                           if((mouseEvent.x < leftPoint) ||
                              (mouseEvent.x > rightPoint) ||
                              (mouseEvent.y < topPoint) ||
                              (mouseEvent.y > bottomPoint)){
                               radioDia.close();
                           }

                           highRectX = mouseEvent.x - 25
                           highRectY = mouseEvent.y -25
                           highRectRadiusCanary++;
                       }
            onPositionChanged: (mouseEvent) => {
                                   highRectX = mouseEvent.x -25
                                   highRectY = mouseEvent.y - 25
                                   highRectRadiusCanary++;
                               }
            onDoubleClicked: { //for the rare case where the screen and image aspect ratios are
                              //exactly the same
                radioDia.close();
            }
        }
    }

    GridLayout {
        z: rawRadio.z + 1
        anchors.left:  parent.left
        anchors.bottom: parent.bottom
        columns: 3
        Label {
            style: Text.Outline
            styleColor: Material.foreground
            color: Material.backgroundColor
            text: "Sharpen"
        }
        Label {
            style: Text.Outline
            styleColor: Material.foreground
            color: Material.backgroundColor
            text: "Brighten"
        }
        Label {
            style: Text.Outline
            styleColor: Material.foreground
            color: Material.backgroundColor
            text: "Contrast"
        }
        Slider {
            orientation: Qt.Vertical
            from: 0
            to:4
            value: 2
            onValueChanged: {
                radioDia.setSharpen = value
            }
        }

        Slider {
            orientation: Qt.Vertical
            from: -1
            to:1
            value: 0
            onValueChanged: {
                radioDia.setBright = value
            }
        }

        Slider {
            orientation: Qt.Vertical
            from:.5
            to:2
            value: 1
            onValueChanged: {
                radioDia.setContrast = value;
            }
        }
    }

    ShaderEffect {
        id: adjShader
        width: rawRadio.paintedWidth
        height: rawRadio.paintedHeight
        anchors.centerIn: parent
        property variant src: rawRadio
        property real sharpenPower: radioDia.setSharpen
        property real brightenPower: radioDia.setBright
        property real contrastPower: radioDia.setContrast
        property real setRenderWidth: rawRadio.paintedWidth
        property real setRenderHeight: rawRadio.paintedHeight
        onStatusChanged: {
            if(status == ShaderEffect.Error) {
                adjShader.visible = false;
            }
        }

        vertexShader: radioDia.vertexShaderString
        fragmentShader: radioDia.fragmentShaderString
    }

    Label {
        id: headerLabelForRadio
        text: radioLabel.text
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        color: Material.background
        style: Text.Outline
        styleColor: Material.foreground
        font.pointSize: 32
        onVisibleChanged: {
            if(visible) {
                fadeOutLabel.start();
            }
            else {
                opacity = 1;
            }
        }

        PropertyAnimation  {
            id: fadeOutLabel
            target: headerLabelForRadio
            property: "opacity"
            from: 1
            to: 0
            duration: 10000
        }
    }

    Rectangle {
        id: highlightRect
        property int candaryWatcher: highRectRadiusCanary
        onCandaryWatcherChanged: {
            startHighlight.restart();
        }

        x: highRectX
        y: highRectY
        radius: 0
        width: radius
        height: radius
        color: "red"
        PropertyAnimation {
            id: startHighlight
            target: highlightRect
            property: "radius"
            from: 50
            to: 0
            duration: 1000
        }
    }
}
