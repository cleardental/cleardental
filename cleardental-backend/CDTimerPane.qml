// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtMultimedia 5.12

CDTranslucentPane {
    backMaterialColor: Material.Pink

    property bool timerDone: false

    Audio {
        id: finAudio
        source: "file:///usr/share/sounds/Oxygen-Im-Sms.ogg"
        loops: timerDone ? Audio.Infinite : 0
        onLoopsChanged: {
            if(timerDone) {
                play();
            }
            else {
                stop();
            }
        }
    }

    ColumnLayout {
        CDHeaderLabel {
            text: "Timer"
        }
        GridLayout {
            columns: 2
            CDDescLabel {
                text: "Set Timer:"
            }
            SpinBox {
                id: setMins
                from: 1
                to: 20
                value: 2
                textFromValue: function(value)  {
                    return value + " minutes"
                }
            }
            CDDescLabel {
                text: "Current timer"
            }
            Label {
                id: current
                property int secs: 0
                color: timerDone ? Material.color(Material.Green) : Material.foreground
                onSecsChanged: {
                    var setMe = Math.floor(secs/60);
                    setMe += ":";
                    var secShow = secs %60
                    if(secShow < 10) {
                        setMe += "0" + secShow;
                    }
                    else {
                        setMe += secShow;
                    }
                    text = setMe;
                }
            }

            CDCancelButton {
                text: "Reset"
                onClicked: {
                    makeStart.stop();
                    current.secs = 0;
                    timerDone = false;
                }
            }

            CDButton {
                Layout.alignment: Qt.AlignRight
                icon.name: "dialog-ok"
                text: "Start"
                onClicked: {
                    makeStart.start();
                }

                PropertyAnimation {
                    id: makeStart
                    target: current
                    property: "secs"
                    from: 0
                    to: setMins.value * 60
                    duration:  to *1000
                    onStopped: {
                        timerDone = true;
                    }
                }
            }
        }
    }

}
