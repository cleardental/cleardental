// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdspellchecker.h"


#include <QDebug>

QTextDocument* CDSpellChecker::m_textDoc = nullptr;
CDSyntaxHighlighter* CDSpellChecker::m_highlighter = nullptr;

CDSpellChecker::CDSpellChecker(QObject *parent) : QObject(parent)
{


}


QStringList CDSpellChecker::suggest(QString input)
{
    QStringList returnMe;
    Hunspell spell("/usr/share/hunspell/en_US.aff", "/usr/share/hunspell/en_US.dic");
    std::vector<std::string> results = spell.suggest(input.toStdString());
    for(std::string addMe : results) {
        returnMe.append(QString::fromStdString(addMe));
    }

    return returnMe;
}

void CDSpellChecker::injectSyntaxHigh(QQuickTextDocument *getDoc)
{
    m_textDoc = getDoc->textDocument();
    m_highlighter = new CDSyntaxHighlighter(getDoc->textDocument());
}
