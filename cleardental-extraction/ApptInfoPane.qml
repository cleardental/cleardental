// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentPane {
    backMaterialColor: Material.Green

    property bool toothWasExt: removedUsingBox.currentIndex !== (removedUsingBox.model.length -1)

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: hardTissueSettings
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    function generateCaseNoteString() {
        var returnMe ="Elevated tooth.\n";

        if(usedPeriotomes.checked) {
            returnMe += "Periotomes were used.\n"
        }

        if(didSection.checked) {
            returnMe += "Tooth was sectioned.\n"
        }

        if(didSocketPresCheckbox.checked) {
            returnMe += "Socket Preservation was done with "+
                    graftingMat.displayText + " and " +
                    membrane.displayText + ".\n";
        }

        if(didFlap.checked) {
            returnMe += didFlap.text + ".\n"
        }

        if(toothWasExt) {
            returnMe += "Removed tooth using " + removedUsingBox.editText + " forceps.\n"
            var fullStringAttr = hardTissueSettings.value(procedureObj["Tooth"],"");
            fullStringAttr = comFuns.addHardTissueAttr(fullStringAttr,"Missing");
            hardTissueSettings.setValue(procedureObj["Tooth"],fullStringAttr);
        }
        else {
            returnMe += "Tooth was not extracted.\n";
        }


        returnMe += sutureBox.displayText +  " were used.\n"




        return returnMe;
    }

    ColumnLayout {
        CDHeaderLabel {
            text: "Today's Appointment"
        }

        GridLayout {
            columns: 2

            CheckBox {
                id: usedPeriotomes
                text: "Periotomes"
            }
            CheckBox {
                id: didSection
                text: "Sectioned"
            }

            CheckBox {
                id: didSocketPresCheckbox
                text: "Socket Preservation"
                Layout.columnSpan: 2
            }

            GridLayout {
                id: socketPresInfo
                Layout.columnSpan: 2
                visible: didSocketPresCheckbox.checked
                columns: 2
                Label {
                    text: "Grafting Material"
                    font.bold: true
                }
                ComboBox {
                    id: graftingMat
                    model: ["Symbios®","Bio-Oss Collagen®", "Bio-Oss®"]
                    Layout.minimumWidth: 200

                }
                Label {
                    text: "Membrane"
                    font.bold: true
                }
                ComboBox {
                    id: membrane
                    model: ["OsteoShield®","Bio-Gide®"]
                    Layout.minimumWidth: 200
                }
            }

            ComboBox {
                id: sutureBox
                model: ["No sutures", "Single sutures",
                    "Vertical mattress sutures",
                    "Horizontal mattress sutures"]
                Layout.fillWidth: true
                Layout.minimumWidth: 250
            }
            CheckBox {
                id: didFlap
                text: "Flap Created"
            }
        }


        RowLayout {
            Label {
                text: "Extracted tooth using"
                font.bold: true
            }
            ComboBox {
                id: removedUsingBox
                model: ["Universal Maxillary", "88 L/R", "Cowhorn", "Universal Mandibular", "Ashe"]
                editable: true
                Layout.fillWidth: true

                Component.onCompleted: {
                    var tooth = rootWin.procedureObj["Tooth"];
                    if(comFuns.isPost(tooth)) {
                        model = ["Universal Maxillary", "88 L/R", "Cowhorn", "Universal Mandibular", "Ashe",
                                 "Elevation", "Didn't extract tooth"];
                    }
                    else {
                        model = ["Universal Maxillary", "Universal Mandibular", "Ashe", "Elevation",
                                 "Didn't extract tooth"];
                    }
                }
            }
        }

    }


}
